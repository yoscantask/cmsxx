-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 04, 2021 at 04:32 AM
-- Server version: 10.4.14-MariaDB
-- PHP Version: 7.4.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `liatmenu`
--

-- --------------------------------------------------------

--
-- Table structure for table `bell`
--

CREATE TABLE `bell` (
  `id` int(11) UNSIGNED NOT NULL,
  `catalog_id` int(10) DEFAULT NULL,
  `table_position` varchar(20) DEFAULT NULL,
  `message` text DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `bell`
--

INSERT INTO `bell` (`id`, `catalog_id`, `table_position`, `message`, `created_at`, `updated_at`) VALUES
(1, 1, '12', 'Rudet lah', '2021-03-05 13:32:25', '2021-03-05 13:32:25'),
(2, 7, '10', '', '2021-03-05 13:44:51', '2021-03-05 13:44:51'),
(3, 7, '10', '', '2021-03-05 13:45:02', '2021-03-05 13:45:02'),
(4, 7, '10', '', '2021-03-05 13:45:03', '2021-03-05 13:45:03'),
(5, 7, '10', 'Sendok', '2021-03-05 13:45:11', '2021-03-05 13:45:11'),
(6, 7, '1', 'Sedotan 1', '2021-03-05 15:01:46', '2021-03-05 15:01:46'),
(7, 7, '1', 'Ambilin tisu', '2021-03-27 13:36:33', '2021-03-27 13:36:33'),
(8, 7, '2', 'Ambilin tisu', '2021-03-27 13:38:42', '2021-03-27 13:38:42'),
(9, 1, '10', 'mau sendok 1', '2021-04-01 09:19:28', '2021-04-01 09:19:28'),
(10, 7, '5', 'Sendok', '2021-04-05 11:12:11', '2021-04-05 11:12:11'),
(11, 1, '10', 'Minta Nasi Lagi', '2021-05-17 03:47:42', '2021-05-17 03:47:42');

-- --------------------------------------------------------

--
-- Table structure for table `catalog`
--

CREATE TABLE `catalog` (
  `id` int(10) NOT NULL,
  `catalog_type` tinyint(4) DEFAULT 1,
  `user_id` int(10) DEFAULT NULL,
  `domain` varchar(100) DEFAULT NULL,
  `catalog_username` varchar(255) DEFAULT NULL,
  `catalog_title` varchar(50) DEFAULT NULL,
  `catalog_logo` varchar(255) DEFAULT NULL,
  `catalog_tagline` varchar(255) DEFAULT NULL,
  `wa_number` varchar(15) DEFAULT NULL,
  `wa_show_item` tinyint(4) DEFAULT NULL,
  `wa_show_cart` tinyint(4) DEFAULT NULL,
  `show_cart` tinyint(4) DEFAULT NULL,
  `background_header_color` varchar(11) DEFAULT NULL,
  `background_header_image` varchar(255) DEFAULT NULL,
  `sliders` text DEFAULT NULL,
  `steps` text DEFAULT NULL,
  `layout` enum('Column','List') DEFAULT NULL,
  `theme_color` varchar(15) DEFAULT NULL,
  `checkout_type` enum('System','Whatsapp') DEFAULT NULL,
  `lat` varchar(40) DEFAULT NULL,
  `long` varchar(40) DEFAULT NULL,
  `distance` int(5) DEFAULT NULL,
  `show_detail` enum('Y','N') DEFAULT 'Y',
  `show_notification` enum('Y','N') DEFAULT 'Y',
  `show_catalog` enum('Open','Close') DEFAULT 'Open',
  `phone_contact` varchar(100) DEFAULT NULL,
  `email_contact` varchar(255) DEFAULT NULL,
  `advance_payment` enum('Y','N') DEFAULT 'Y',
  `bank_info` text DEFAULT NULL,
  `transfer_payment` enum('N','Y') DEFAULT 'N',
  `payment_gateway` enum('N','Y') NOT NULL DEFAULT 'N',
  `tax` tinyint(2) NOT NULL DEFAULT 0,
  `client_key` text DEFAULT NULL,
  `server_key` text DEFAULT NULL,
  `catalog_password` text DEFAULT NULL,
  `feature` enum('Basic','Full') NOT NULL,
  `customer_data` enum('N','Y') NOT NULL DEFAULT 'N',
  `view_subcat` varchar(10) NOT NULL DEFAULT 'grid',
  `view_item` varchar(10) NOT NULL DEFAULT 'grid',
  `lang` varchar(4) NOT NULL DEFAULT 'id',
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `catalog`
--

INSERT INTO `catalog` (`id`, `catalog_type`, `user_id`, `domain`, `catalog_username`, `catalog_title`, `catalog_logo`, `catalog_tagline`, `wa_number`, `wa_show_item`, `wa_show_cart`, `show_cart`, `background_header_color`, `background_header_image`, `sliders`, `steps`, `layout`, `theme_color`, `checkout_type`, `lat`, `long`, `distance`, `show_detail`, `show_notification`, `show_catalog`, `phone_contact`, `email_contact`, `advance_payment`, `bank_info`, `transfer_payment`, `payment_gateway`, `tax`, `client_key`, `server_key`, `catalog_password`, `feature`, `customer_data`, `view_subcat`, `view_item`, `lang`, `created_at`, `updated_at`) VALUES
(1, 2, 1, 'liatmenu.id', 'ceudedeh', 'Kedai Ceu Dedeh', '/users/1/catalog/kedai_ceu_dedeh_1618257336.png', 'Warung Makan Khas Sunda', '085294651500', 0, 0, 0, '#000', NULL, '[\"2\",\"1\"]', '[\"Process\"]', 'Column', '#ff6633', 'System', '-7.0152972', '107.8761104', 10000000, 'Y', 'Y', 'Open', '082219155441', 'ceudedeh@gmail.com', 'Y', 'Bank BNI 3000 000 633 a/n Yayasan Kemanusiaan Muslim Indonesia', 'N', 'Y', 10, 'SB-Mid-client-me-ea9KiatAU-oSY', 'SB-Mid-server-VQunL82aei5OGz3SbUu8mHN1', '$2y$10$CN7leSXVmMQCUql6iQ2Q4.7rxbGsdlBG1agbP6KY1R505bio/yMv.', 'Full', 'Y', 'list', 'list', 'id', '2020-11-10 09:43:26', '2021-05-18 22:51:59'),
(2, 1, 5, 'liatmenu.id', 'ottimista', 'Ottimista Coffee Lebak Bulus', '/users/5/catalog/ottimista_coffee_lebak_bulus_1609005429.png', NULL, '+6288211976244', 1, 0, NULL, '#000', NULL, '[\"3\"]', '[\"Approve\"]', 'Column', '#ffcc66', 'System', '-6.1702144', '106.7515904', 100000000, 'Y', 'Y', 'Open', '088211976244', 'rian@ottimista.id', 'Y', 'Muhammad Adriansa BCA 6090392792', 'Y', 'N', 0, NULL, NULL, NULL, 'Full', 'N', 'grid', 'grid', 'id', '2020-12-11 20:49:37', '2021-06-01 22:21:25'),
(7, 1, 7, 'liatmenu.id', 'r18cafe', 'R18 Cafe', '/users/7/catalog/r18_cafe_1617682558.jpg', NULL, NULL, 0, 0, NULL, '#000', NULL, '[\"5\",\"4\"]', '[\"Process\"]', 'Column', '#3399cc', 'System', '-6.292282', '106.798573', 1000000000, 'Y', 'Y', 'Open', '083873505399', 'r18cafe.bmw@gmail.com', 'Y', NULL, 'Y', 'Y', 10, NULL, NULL, '$2y$10$CM1Zp9YOkC/mIX5Cj7bbsedMFj0nKlP7RsMRmQxt1UNa/cXMo2k2C', 'Full', 'Y', 'grid', 'grid', 'id', '2021-01-25 14:55:04', '2021-04-12 18:29:23'),
(8, 1, 13, 'liatmenu.id', 'supersunday1', 'Kopi Pasar Minggu', '/users/13/catalog/kopi_pasar_minggu_1617868138.jpg', NULL, NULL, NULL, NULL, NULL, '#000', NULL, '', NULL, 'Column', '#cc0033', NULL, '-6.2087634', '106.84559899999999', 1000, 'Y', 'Y', 'Open', '081380735857', 'vivi@supersunday.id', 'Y', NULL, 'N', 'N', 0, NULL, NULL, '$2y$10$ktdObMpzIPdunDjvnOgCh.9qLdGar.SVU5NBKxeRSir9cugYTjVIG', 'Basic', 'N', 'grid', 'grid', 'id', '2021-04-08 14:45:03', '2021-04-12 18:29:24'),
(9, 1, 14, 'liatmenu.id', 'supersunday', 'Super Sunday Cafe', '/users/14/catalog/super_sunday_cafe_1617869478.jpg', NULL, NULL, NULL, NULL, NULL, '#000', NULL, '[\"8\",\"7\"]', NULL, 'Column', '#3300cc', NULL, '-6.2087634', '106.84559899999999', 1000, 'Y', 'Y', 'Open', '082124091527', 'supersundaycafe@gmail.com', 'Y', NULL, 'N', 'N', 0, NULL, NULL, '$2y$10$t7CHPo2S.YeoRjTC22T85urm8qLoxAb3.nzSvb.C9rVZInqs0aGiG', 'Basic', 'N', 'grid', 'list', 'id', '2021-04-08 15:02:21', '2021-06-02 20:45:10'),
(10, 2, 1, 'warkop.dki', 'warkopDki', 'Warung Kopi DKI Jkt', '/users/1/catalog/kedai_ceu_dedeh_1618257336.png', 'Warung Kopi DKI Jkt', '08988787878868', 0, 0, 0, '#000', NULL, '[\"2\",\"1\"]', '[\"Process\"]', 'Column', '#333399', 'System', '-6.874619806083098', '107.5286865234375', 10000000, 'Y', 'Y', 'Open', '0888789878945', 'warkopdki@gmail.com', 'Y', NULL, 'N', 'N', 0, NULL, NULL, '$2y$10$CN7leSXVmMQCUql6iQ2Q4.7rxbGsdlBG1agbP6KY1R505bio/yMv.', 'Full', 'N', 'grid', 'grid', 'id', '2021-06-02 11:01:18', '2021-06-02 11:10:02');

-- --------------------------------------------------------

--
-- Table structure for table `catalogdetail`
--

CREATE TABLE `catalogdetail` (
  `id` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `catalog_id` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `category_position` int(5) DEFAULT NULL,
  `subcategory_id` int(11) DEFAULT NULL,
  `subcategory_position` int(5) DEFAULT NULL,
  `item` int(11) DEFAULT NULL,
  `item_position` int(5) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `catalogdetail`
--

INSERT INTO `catalogdetail` (`id`, `user_id`, `catalog_id`, `category_id`, `category_position`, `subcategory_id`, `subcategory_position`, `item`, `item_position`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 1, 1, 4, 1, 12, 1, '2020-11-10 09:47:24', '2021-06-03 06:35:52'),
(2, 1, 1, 1, 1, 4, 1, 11, 2, '2020-11-10 09:47:24', '2021-06-03 06:35:52'),
(4, 1, 1, 1, 1, 4, 1, 8, 4, '2020-11-10 09:47:24', '2021-04-12 15:53:51'),
(5, 1, 1, 1, 1, 5, 2, 15, 1, '2020-11-10 16:42:52', '2021-04-12 15:53:51'),
(6, 1, 1, 1, 1, 5, 2, 14, 2, '2020-11-10 16:42:52', '2021-04-12 15:53:51'),
(7, 1, 1, 1, 1, 5, 2, 13, 3, '2020-11-10 16:42:52', '2021-04-12 15:53:51'),
(10, 1, 1, 3, 3, 2, 2, 4, 1, '2020-11-10 16:46:31', '2020-11-12 12:22:52'),
(11, 1, 1, 3, 3, 2, 2, 3, 2, '2020-11-10 16:46:31', '2020-11-12 12:22:52'),
(12, 1, 1, 3, 3, 3, 3, 5, 1, '2020-11-10 16:47:21', '2020-11-12 12:22:52'),
(13, 1, 1, 3, 3, 1, 4, 2, 1, '2020-11-10 16:47:45', '2020-11-12 12:22:52'),
(14, 1, 1, 3, 3, 1, 4, 1, 2, '2020-11-10 16:47:45', '2020-11-12 12:22:52'),
(17, 1, 1, 1, 1, 7, 3, 19, 1, '2020-11-12 10:39:25', '2021-04-12 15:53:51'),
(18, 1, 1, 1, 1, 7, 3, 18, 2, '2020-11-12 10:39:25', '2021-04-12 15:53:51'),
(19, 1, 1, 1, 1, 7, 3, 20, 3, '2020-11-12 10:41:23', '2021-04-12 15:53:51'),
(20, 1, 1, 1, 1, 8, 4, 21, 1, '2020-11-12 10:44:44', '2021-04-12 15:53:51'),
(21, 1, 1, 1, 1, 8, 4, 24, 2, '2020-11-12 10:48:19', '2021-04-12 15:53:51'),
(22, 1, 1, 1, 1, 8, 4, 23, 3, '2020-11-12 10:48:19', '2021-04-12 15:53:51'),
(23, 1, 1, 1, 1, 8, 4, 22, 4, '2020-11-12 10:48:19', '2021-04-12 15:53:51'),
(24, 1, 1, 1, 1, 9, 5, 28, 1, '2020-11-12 10:53:17', '2021-04-12 15:53:51'),
(25, 1, 1, 1, 1, 9, 5, 27, 2, '2020-11-12 10:53:17', '2021-04-12 15:53:51'),
(26, 1, 1, 1, 1, 9, 5, 26, 3, '2020-11-12 10:53:17', '2021-04-12 15:53:51'),
(27, 1, 1, 1, 1, 9, 5, 25, 4, '2020-11-12 10:53:17', '2021-04-12 15:53:51'),
(28, 1, 1, 1, 1, 10, 6, 32, 1, '2020-11-12 11:31:53', '2021-04-12 15:53:51'),
(29, 1, 1, 1, 1, 10, 6, 31, 2, '2020-11-12 11:31:53', '2021-04-12 15:53:51'),
(30, 1, 1, 1, 1, 10, 6, 30, 3, '2020-11-12 11:31:53', '2021-04-12 15:53:51'),
(31, 1, 1, 1, 1, 10, 6, 29, 4, '2020-11-12 11:31:53', '2021-04-12 15:53:51'),
(37, 1, 1, 2, 2, 15, 1, 41, 1, '2020-11-12 11:46:02', '2021-04-12 15:53:51'),
(38, 1, 1, 2, 2, 15, 1, 40, 2, '2020-11-12 11:46:02', '2021-04-12 15:53:51'),
(39, 1, 1, 2, 2, 15, 1, 39, 3, '2020-11-12 11:46:02', '2021-04-12 15:53:51'),
(40, 1, 1, 2, 2, 15, 1, 38, 4, '2020-11-12 11:46:02', '2021-04-12 15:53:51'),
(41, 1, 1, 2, 2, 13, 3, 45, 1, '2020-11-12 11:50:31', '2021-04-12 15:53:51'),
(42, 1, 1, 2, 2, 13, 3, 44, 2, '2020-11-12 11:50:31', '2021-04-12 15:53:51'),
(43, 1, 1, 2, 2, 13, 3, 43, 3, '2020-11-12 11:50:31', '2021-04-12 15:53:51'),
(44, 1, 1, 2, 2, 13, 3, 42, 4, '2020-11-12 11:50:31', '2021-04-12 15:53:51'),
(45, 1, 1, 2, 2, 14, 2, 49, 1, '2020-11-12 11:59:18', '2021-04-12 15:53:51'),
(46, 1, 1, 2, 2, 14, 2, 48, 2, '2020-11-12 11:59:18', '2021-04-12 15:53:51'),
(47, 1, 1, 2, 2, 14, 2, 47, 3, '2020-11-12 11:59:18', '2021-04-12 15:53:51'),
(48, 1, 1, 2, 2, 14, 2, 46, 4, '2020-11-12 11:59:18', '2021-04-12 15:53:51'),
(49, 1, 1, 2, 2, 12, 4, 52, 1, '2020-11-12 12:03:59', '2021-04-12 15:53:51'),
(50, 1, 1, 2, 2, 12, 4, 51, 2, '2020-11-12 12:03:59', '2021-04-12 15:53:51'),
(51, 1, 1, 2, 2, 12, 4, 50, 3, '2020-11-12 12:03:59', '2021-04-12 15:53:51'),
(52, 1, 1, 2, 2, 12, 4, 53, 4, '2020-11-12 12:22:24', '2021-04-12 15:53:51'),
(53, 5, 2, 4, 1, 16, 1, 54, 1, '2020-12-15 08:26:59', '2020-12-15 08:26:59'),
(54, 5, 2, 4, 1, 16, 1, 66, 2, '2020-12-18 23:14:33', '2020-12-18 23:14:33'),
(55, 5, 2, 4, 1, 16, 1, 65, 3, '2020-12-18 23:14:33', '2020-12-18 23:14:33'),
(56, 5, 2, 4, 1, 16, 1, 64, 4, '2020-12-18 23:14:33', '2020-12-18 23:14:33'),
(57, 5, 2, 4, 1, 16, 1, 63, 5, '2020-12-18 23:14:33', '2020-12-18 23:14:33'),
(58, 5, 2, 4, 1, 16, 1, 62, 6, '2020-12-18 23:14:33', '2020-12-18 23:14:33'),
(59, 5, 2, 4, 1, 16, 1, 61, 7, '2020-12-18 23:14:33', '2020-12-18 23:14:33'),
(60, 5, 2, 4, 1, 16, 1, 60, 8, '2020-12-18 23:14:33', '2020-12-18 23:14:33'),
(61, 5, 2, 4, 1, 16, 1, 59, 9, '2020-12-18 23:14:33', '2020-12-18 23:14:33'),
(62, 5, 2, 4, 1, 16, 1, 58, 10, '2020-12-18 23:14:33', '2020-12-18 23:14:33'),
(63, 5, 2, 4, 1, 16, 1, 57, 11, '2020-12-18 23:14:33', '2020-12-18 23:14:33'),
(64, 5, 2, 4, 1, 16, 1, 56, 12, '2020-12-18 23:14:33', '2020-12-18 23:14:33'),
(65, 5, 2, 4, 1, 16, 1, 55, 13, '2020-12-18 23:14:33', '2020-12-18 23:14:33'),
(98, 7, 7, 6, 1, 18, 2, 101, 1, '2021-03-03 14:38:12', '2021-03-03 14:38:12'),
(99, 7, 7, 6, 1, 18, 2, 88, 2, '2021-03-03 14:38:12', '2021-03-03 14:38:12'),
(100, 7, 7, 6, 1, 18, 2, 85, 3, '2021-03-03 14:38:12', '2021-03-03 14:38:12'),
(101, 7, 7, 6, 1, 18, 2, 84, 4, '2021-03-03 14:38:12', '2021-03-03 14:38:12'),
(102, 7, 7, 6, 1, 18, 2, 83, 5, '2021-03-03 14:38:12', '2021-03-03 14:38:12'),
(103, 7, 7, 6, 1, 18, 2, 80, 6, '2021-03-03 14:38:12', '2021-03-03 14:38:12'),
(104, 7, 7, 6, 1, 18, 2, 79, 7, '2021-03-03 14:38:12', '2021-03-03 14:38:12'),
(105, 7, 7, 6, 1, 18, 2, 68, 8, '2021-03-03 14:38:12', '2021-03-03 14:38:12'),
(106, 7, 7, 6, 1, 19, 3, 105, 1, '2021-03-03 14:56:49', '2021-03-03 14:56:49'),
(107, 7, 7, 6, 1, 19, 3, 104, 2, '2021-03-03 14:56:49', '2021-03-03 14:56:49'),
(108, 7, 7, 6, 1, 19, 3, 103, 3, '2021-03-03 14:56:49', '2021-03-03 14:56:49'),
(109, 7, 7, 6, 1, 19, 3, 102, 4, '2021-03-03 14:56:49', '2021-03-03 14:56:49'),
(110, 7, 7, 6, 1, 19, 3, 87, 5, '2021-03-03 14:56:49', '2021-03-03 14:56:49'),
(111, 7, 7, 6, 1, 19, 3, 81, 6, '2021-03-03 14:56:49', '2021-03-03 14:56:49'),
(112, 7, 7, 6, 1, 19, 3, 78, 7, '2021-03-03 14:56:49', '2021-03-03 14:56:49'),
(113, 7, 7, 6, 1, 21, 4, 111, 1, '2021-03-03 15:12:51', '2021-03-03 15:12:51'),
(114, 7, 7, 6, 1, 21, 4, 110, 2, '2021-03-03 15:12:51', '2021-03-03 15:12:51'),
(115, 7, 7, 6, 1, 21, 4, 109, 3, '2021-03-03 15:12:51', '2021-03-03 15:12:51'),
(116, 7, 7, 6, 1, 21, 4, 105, 4, '2021-03-03 15:12:51', '2021-03-03 15:12:51'),
(117, 7, 7, 6, 1, 21, 4, 104, 5, '2021-03-03 15:12:51', '2021-03-03 15:12:51'),
(118, 7, 7, 6, 1, 22, 5, 108, 1, '2021-03-03 15:13:19', '2021-03-03 15:13:19'),
(119, 7, 7, 6, 1, 22, 5, 107, 2, '2021-03-03 15:13:19', '2021-03-03 15:13:19'),
(120, 7, 7, 6, 1, 22, 5, 106, 3, '2021-03-03 15:13:19', '2021-03-03 15:13:19'),
(121, 7, 7, 5, 2, 23, 1, 113, 1, '2021-03-03 15:16:27', '2021-03-03 15:16:27'),
(122, 7, 7, 5, 2, 23, 1, 112, 2, '2021-03-03 15:16:27', '2021-03-03 15:16:27'),
(125, 7, 7, 5, 2, 24, 2, 118, 1, '2021-03-03 15:22:18', '2021-03-03 15:22:18'),
(126, 7, 7, 5, 2, 24, 2, 117, 2, '2021-03-03 15:22:18', '2021-03-03 15:22:18'),
(127, 7, 7, 5, 2, 23, 1, 116, 5, '2021-03-03 15:46:43', '2021-03-03 15:46:43'),
(128, 7, 7, 5, 2, 23, 1, 115, 6, '2021-03-03 15:46:43', '2021-03-03 15:46:43'),
(130, 7, 7, 5, 2, 24, 2, 119, 3, '2021-03-03 15:47:22', '2021-03-03 15:47:22'),
(131, 7, 7, 5, 2, 25, 3, 126, 1, '2021-03-03 15:48:02', '2021-03-03 15:48:02'),
(132, 7, 7, 5, 2, 25, 3, 125, 2, '2021-03-03 15:48:02', '2021-03-03 15:48:02'),
(133, 7, 7, 5, 2, 25, 3, 124, 3, '2021-03-03 15:48:02', '2021-03-03 15:48:02'),
(134, 7, 7, 5, 2, 25, 3, 123, 4, '2021-03-03 15:48:02', '2021-03-03 15:48:02'),
(135, 7, 7, 5, 2, 25, 3, 122, 5, '2021-03-03 15:48:02', '2021-03-03 15:48:02'),
(136, 5, 2, 4, 1, 16, 1, 153, 14, '2021-03-27 23:27:07', '2021-03-27 23:27:07'),
(137, 5, 2, 4, 1, 17, 2, 163, 1, '2021-03-27 23:29:29', '2021-03-27 23:29:29'),
(138, 5, 2, 4, 1, 17, 2, 162, 2, '2021-03-27 23:29:29', '2021-03-27 23:29:29'),
(139, 5, 2, 4, 1, 17, 2, 161, 3, '2021-03-27 23:29:29', '2021-03-27 23:29:29'),
(140, 5, 2, 4, 1, 17, 2, 160, 4, '2021-03-27 23:29:29', '2021-03-27 23:29:29'),
(141, 5, 2, 4, 1, 17, 2, 159, 5, '2021-03-27 23:29:29', '2021-03-27 23:29:29'),
(142, 5, 2, 4, 1, 17, 2, 158, 6, '2021-03-27 23:29:29', '2021-03-27 23:29:29'),
(143, 5, 2, 4, 1, 17, 2, 157, 7, '2021-03-27 23:29:29', '2021-03-27 23:29:29'),
(144, 5, 2, 4, 1, 17, 2, 156, 8, '2021-03-27 23:29:29', '2021-03-27 23:29:29'),
(145, 5, 2, 4, 1, 17, 2, 155, 9, '2021-03-27 23:29:29', '2021-03-27 23:29:29'),
(146, 5, 2, 4, 1, 17, 2, 154, 10, '2021-03-27 23:29:29', '2021-03-27 23:29:29'),
(147, 5, 2, 4, 1, 17, 2, 152, 11, '2021-03-27 23:31:30', '2021-03-27 23:31:30'),
(148, 5, 2, 4, 1, 16, 1, 164, 15, '2021-03-27 23:35:05', '2021-03-27 23:35:05'),
(149, 5, 2, 4, 1, 17, 2, 165, 12, '2021-03-27 23:35:22', '2021-03-27 23:35:22'),
(150, 5, 2, 4, 1, 16, 1, 166, 16, '2021-03-27 23:43:12', '2021-03-27 23:43:12'),
(151, 14, 9, 11, 1, 26, 1, 169, 1, '2021-04-08 15:54:59', '2021-04-08 15:54:59'),
(152, 14, 9, 11, 1, 26, 1, 168, 2, '2021-04-08 15:54:59', '2021-04-08 15:54:59'),
(153, 14, 9, 11, 1, 26, 1, 167, 3, '2021-04-08 15:54:59', '2021-04-08 15:54:59'),
(154, 14, 9, 11, 1, 27, 2, 169, 1, '2021-04-08 15:55:18', '2021-04-08 15:55:18'),
(155, 14, 9, 11, 1, 27, 2, 168, 2, '2021-04-08 15:55:18', '2021-04-08 15:55:18'),
(156, 14, 9, 11, 1, 27, 2, 167, 3, '2021-04-08 15:55:18', '2021-04-08 15:55:18'),
(157, 14, 9, 11, 1, 28, 3, 169, 1, '2021-04-08 15:55:39', '2021-04-08 15:55:39'),
(158, 14, 9, 11, 1, 28, 3, 168, 2, '2021-04-08 15:55:39', '2021-04-08 15:55:39'),
(159, 14, 9, 11, 1, 28, 3, 167, 3, '2021-04-08 15:55:39', '2021-04-08 15:55:39'),
(160, 1, 10, 2, 1, 12, 1, 50, 1, '2021-06-03 12:02:37', '2021-06-03 12:02:37'),
(161, 1, 10, 3, 2, 3, 1, 1, 1, '2021-06-03 12:07:32', '2021-06-03 12:07:32'),
(162, 1, 10, 12, 3, 5, 1, 13, 1, '2021-06-03 12:09:09', '2021-06-03 12:09:09'),
(163, 1, 10, 2, 1, 12, 1, 51, 2, '2021-06-03 12:09:41', '2021-06-03 12:09:41');

-- --------------------------------------------------------

--
-- Table structure for table `catalog_list_types`
--

CREATE TABLE `catalog_list_types` (
  `id` int(5) NOT NULL,
  `slug` char(50) DEFAULT NULL,
  `url` varchar(191) DEFAULT NULL,
  `name` varchar(191) DEFAULT NULL,
  `image` varchar(191) DEFAULT NULL,
  `description` text DEFAULT NULL,
  `order` tinyint(4) DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `catalog_list_types`
--

INSERT INTO `catalog_list_types` (`id`, `slug`, `url`, `name`, `image`, `description`, `order`, `created_at`, `updated_at`) VALUES
(1, 'service', 'service', 'Service', 'http://localhost/liat_menu_hotel/images//users/1/subcategory/aneka_sate_1605023178.jpg', NULL, 0, NULL, NULL),
(2, 'complaint', 'complaint', 'Complaint', 'http://localhost/liat_menu_hotel/images//users/1/subcategory/aneka_sapi_kambing_1605023299.jpg', NULL, 1, NULL, NULL),
(3, 'restaurant', 'restaurant', 'Restaurant', 'http://localhost/liat_menu_hotel/images//users/1/subcategory/aneka_ayam_1605023447.jpg', NULL, 2, NULL, NULL),
(4, 'tutorial', 'tutorial', 'Tutorial', 'http://localhost/liat_menu_hotel/images//users/1/subcategory/soto_dan_sup_1605023119.jpg', NULL, 3, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `catalog_types`
--

CREATE TABLE `catalog_types` (
  `id` int(5) NOT NULL,
  `catalog_id` int(11) DEFAULT NULL,
  `catalog_list_type_id` tinytext DEFAULT NULL,
  `label` varchar(191) DEFAULT NULL,
  `description` text DEFAULT NULL,
  `image` varchar(191) DEFAULT NULL,
  `order` tinyint(4) DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `catalog_types`
--

INSERT INTO `catalog_types` (`id`, `catalog_id`, `catalog_list_type_id`, `label`, `description`, `image`, `order`, `created_at`, `updated_at`) VALUES
(16, 1, '3', NULL, NULL, NULL, 0, NULL, NULL),
(17, 1, '1', NULL, NULL, NULL, 1, NULL, NULL),
(18, 1, '2', NULL, NULL, NULL, 2, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `category_name` varchar(255) DEFAULT NULL,
  `category_slug` varchar(255) DEFAULT NULL,
  `show_on_catalog` varchar(50) NOT NULL,
  `category_color` varchar(10) DEFAULT NULL,
  `category_type` enum('Main','Add') NOT NULL DEFAULT 'Main',
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `user_id`, `category_name`, `category_slug`, `show_on_catalog`, `category_color`, `category_type`, `created_at`, `updated_at`) VALUES
(1, 1, 'Makanan', '1-makanan', '', '#000', 'Main', '2020-11-10 08:31:49', '2021-06-03 07:27:29'),
(2, 1, 'Minuman', '1-minuman', '1|10|', '#000', 'Main', '2020-11-10 08:31:59', '2021-06-03 07:27:23'),
(3, 1, 'Kudapan & Jajanan', '1-kudapan-jajanan', '1|10|', '#000', 'Main', '2020-11-10 08:32:09', '2021-06-03 12:06:58'),
(4, 5, 'Menu', '5-menu', '', '#000', 'Main', '2020-12-14 22:56:49', '2020-12-14 22:57:44'),
(5, 7, 'Food', '7-food', '', '#000', 'Main', '2021-01-25 15:56:19', '2021-01-25 15:56:19'),
(6, 7, 'Drink', '7-drink', '', '#000', 'Main', '2021-01-25 16:01:49', '2021-01-25 16:01:49'),
(7, 1, 'Sugar Level', '1-sugar-level', '1|10|', '#000', 'Add', '2021-02-15 05:10:12', '2021-06-02 20:26:26'),
(8, 1, 'Toping', '1-toping', '1|10|', '#000', 'Add', '2021-02-15 05:10:18', '2021-06-02 20:26:26'),
(9, 7, 'Sugar Level', '7-sugar-level', '', '#000', 'Add', '2021-03-03 11:10:18', '2021-03-03 11:10:18'),
(10, 7, 'Syrup', '7-syrup', '', '#000', 'Add', '2021-03-03 14:05:49', '2021-03-03 14:05:49'),
(11, 14, 'Menu', '14-menu', '', '#000', 'Main', '2021-04-08 15:24:13', '2021-04-08 15:53:41'),
(12, 1, 'Makanan Liar', '1-makanan-liar', '1|10|', '#000', 'Main', '2021-06-03 12:08:17', '2021-06-03 12:08:38');

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE `customer` (
  `id` int(11) NOT NULL,
  `catalog_id` int(10) DEFAULT NULL,
  `customer_name` varchar(255) DEFAULT NULL,
  `customer_email` varchar(255) DEFAULT NULL,
  `customer_phone` varchar(15) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `customer`
--

INSERT INTO `customer` (`id`, `catalog_id`, `customer_name`, `customer_email`, `customer_phone`, `created_at`, `updated_at`) VALUES
(1, 1, 'Tukang Cilok', 'jobong.nagreg@gmail.com', NULL, '2021-04-06 15:18:05', '2021-04-06 15:18:17'),
(2, 1, 'Rian', 'Adriansadoyan@gmail.com', NULL, '2021-04-06 15:50:18', '2021-04-06 15:50:22'),
(3, 7, 'Jajaha', 'Akakajakaka', 'Hajajaj', '2021-04-08 10:10:21', '2021-04-08 10:10:26'),
(4, 1, 'Nanang k', 'nanangkoesharwanto@gmail.com', '081217357535', '2021-04-08 13:10:22', '2021-04-08 13:10:35'),
(5, 1, 'nanang', 'nanangk@gmail.com', '9234237846', '2021-04-08 13:13:58', '2021-04-08 13:14:58'),
(6, 1, 'Nama', 'adhaygyagsd', '5678', '2021-04-08 13:31:38', '2021-04-08 13:45:00'),
(7, 1, 'Email', 'Email', 'Email', '2021-04-08 14:04:23', '2021-04-08 14:04:31'),
(8, 1, 'Sanjay', 'san@jay.com', '0867678456456', '2021-05-17 02:00:23', '2021-05-17 02:00:38'),
(9, 1, 'Arig', 'a@rig.com', '08766787867856', '2021-05-18 04:50:02', '2021-05-18 04:50:14'),
(10, 1, 'Arip Man', 'arip@man.com', '089877877887', '2021-05-20 09:04:49', '2021-05-20 09:06:55');

-- --------------------------------------------------------

--
-- Table structure for table `feature`
--

CREATE TABLE `feature` (
  `id` tinyint(2) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `mainfeature_id` tinyint(2) DEFAULT NULL,
  `package_id` tinyint(2) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `feature`
--

INSERT INTO `feature` (`id`, `user_id`, `mainfeature_id`, `package_id`, `created_at`, `updated_at`) VALUES
(50, 1, 1, 3, '2021-02-28 13:30:17', '2021-02-28 13:30:17'),
(51, 1, 2, 3, '2021-02-28 13:30:17', '2021-02-28 13:30:17'),
(52, 1, 3, 3, '2021-02-28 13:34:35', '2021-02-28 13:34:35'),
(53, 1, 4, 3, '2021-02-28 13:34:35', '2021-02-28 13:34:35'),
(54, 1, 5, 3, '2021-02-28 13:35:34', '2021-02-28 13:35:34'),
(55, 1, 6, 3, '2021-02-28 13:35:34', '2021-02-28 13:35:34'),
(56, 1, 7, 3, '2021-02-28 13:36:32', '2021-02-28 13:36:32'),
(57, 2, 1, 1, '2021-03-02 00:54:50', '2021-03-02 00:54:50'),
(58, 2, 2, 1, '2021-03-02 00:54:50', '2021-03-02 00:54:50'),
(59, 2, 3, 1, '2021-03-02 00:54:50', '2021-03-02 00:54:50'),
(60, 2, 4, 1, '2021-03-02 00:54:50', '2021-03-02 00:54:50'),
(61, 2, 5, 1, '2021-03-02 00:54:50', '2021-03-02 00:54:50'),
(62, 2, 6, 1, '2021-03-02 00:54:50', '2021-03-02 00:54:50'),
(63, 2, 7, 1, '2021-03-02 00:54:50', '2021-03-02 00:54:50'),
(64, 2, 1, 2, '2021-04-05 06:38:22', '2021-04-05 06:38:22'),
(65, 2, 2, 2, '2021-04-05 06:38:22', '2021-04-05 06:38:22'),
(66, 2, 3, 2, '2021-04-05 06:38:22', '2021-04-05 06:38:22'),
(67, 2, 4, 2, '2021-04-05 06:38:22', '2021-04-05 06:38:22'),
(68, 2, 5, 2, '2021-04-05 06:38:22', '2021-04-05 06:38:22'),
(69, 2, 6, 2, '2021-04-05 06:38:22', '2021-04-05 06:38:22'),
(70, 2, 7, 2, '2021-04-05 06:38:22', '2021-04-05 06:38:22'),
(71, 2, 8, 2, '2021-04-05 06:38:22', '2021-04-05 06:38:22'),
(72, 2, 9, 2, '2021-04-05 06:38:22', '2021-04-05 06:38:22'),
(73, 2, 10, 2, '2021-04-05 06:38:22', '2021-04-05 06:38:22'),
(74, 2, 11, 2, '2021-04-05 06:38:22', '2021-04-05 06:38:22'),
(75, 2, 12, 2, '2021-04-05 06:38:22', '2021-04-05 06:38:22'),
(76, 2, 13, 2, '2021-04-05 06:38:22', '2021-04-05 06:38:22'),
(77, 2, 14, 2, '2021-04-05 06:38:22', '2021-04-05 06:38:22');

-- --------------------------------------------------------

--
-- Table structure for table `invoice`
--

CREATE TABLE `invoice` (
  `id` int(11) NOT NULL,
  `catalog_id` int(11) DEFAULT NULL,
  `invoice_number` varchar(14) DEFAULT NULL,
  `via` enum('Whatsapp','System','POS') DEFAULT NULL,
  `position` varchar(50) DEFAULT NULL,
  `status` varchar(10) DEFAULT NULL,
  `note_order` text DEFAULT NULL,
  `payment_method` tinyint(2) DEFAULT NULL,
  `transfer_image` varchar(255) DEFAULT NULL,
  `tax` tinyint(4) DEFAULT NULL,
  `pending` enum('N','Y') NOT NULL DEFAULT 'N',
  `amount` double DEFAULT NULL,
  `invoice_type` enum('Temporary','Permanent','Clone') NOT NULL DEFAULT 'Permanent',
  `clone_invoice` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `invoice`
--

INSERT INTO `invoice` (`id`, `catalog_id`, `invoice_number`, `via`, `position`, `status`, `note_order`, `payment_method`, `transfer_image`, `tax`, `pending`, `amount`, `invoice_type`, `clone_invoice`, `created_at`, `updated_at`) VALUES
(1, 1, 'ORD1-00000001', 'System', '1', 'Completed', NULL, 1, NULL, 10, 'Y', NULL, 'Temporary', NULL, '2021-04-06 10:14:58', '2021-04-06 10:16:24'),
(2, 7, 'INV7-00000003', 'System', '2', 'Completed', NULL, 1, '', 10, 'N', 510000, 'Permanent', NULL, '2021-04-06 10:21:13', '2021-04-06 11:26:11'),
(3, 1, 'INV1-00000001', 'POS', NULL, 'Completed', NULL, 1, '', 10, 'N', 50000, 'Permanent', 1, '2021-04-06 10:21:49', '2021-04-06 10:24:11'),
(4, 7, 'INV7-00000003', 'System', '8', 'Checkout', NULL, 1, NULL, 10, 'N', NULL, 'Permanent', NULL, '2021-04-06 10:32:56', '2021-04-06 10:39:30'),
(5, 7, 'INV7-00000005', 'System', '3', 'Checkout', NULL, 1, NULL, 10, 'N', NULL, 'Permanent', NULL, '2021-04-06 11:16:15', '2021-04-06 11:16:28'),
(6, 7, 'INV7-00000006', 'System', '2', 'Checkout', NULL, 1, NULL, 10, 'N', NULL, 'Permanent', NULL, '2021-04-06 11:31:39', '2021-04-06 11:31:59'),
(7, 1, 'INV1-00000004', 'System', NULL, 'Order', NULL, NULL, NULL, NULL, 'N', NULL, 'Permanent', NULL, '2021-04-06 11:34:54', '2021-04-06 11:34:54'),
(8, 1, 'INV1-00000008', 'System', NULL, 'Order', NULL, NULL, NULL, NULL, 'N', NULL, 'Permanent', NULL, '2021-04-06 15:50:50', '2021-04-06 15:50:50'),
(9, 1, 'INV1-00000009', 'System', NULL, 'Order', NULL, NULL, NULL, NULL, 'N', NULL, 'Permanent', NULL, '2021-04-07 11:14:44', '2021-04-07 11:14:44'),
(10, 2, 'INV2-00000001', 'POS', NULL, 'Order', NULL, NULL, NULL, NULL, 'N', NULL, 'Permanent', NULL, '2021-04-07 11:47:11', '2021-04-07 11:47:11'),
(11, 7, 'INV7-00000007', 'System', NULL, 'Order', NULL, NULL, NULL, NULL, 'N', NULL, 'Permanent', NULL, '2021-04-08 10:10:37', '2021-04-08 10:10:37'),
(12, 1, '20210520162039', 'System', NULL, 'Order', NULL, NULL, NULL, NULL, 'N', NULL, 'Permanent', NULL, '2021-05-20 09:20:39', '2021-05-20 09:20:39');

-- --------------------------------------------------------

--
-- Table structure for table `invoiceaddons`
--

CREATE TABLE `invoiceaddons` (
  `id` int(11) NOT NULL,
  `invoiceid` int(11) DEFAULT NULL,
  `invoicedetailid` int(11) DEFAULT NULL,
  `row_group` varchar(14) DEFAULT NULL,
  `single_addon` text DEFAULT NULL,
  `multiple_addon` text DEFAULT NULL,
  `addon_qty` int(5) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `invoiceaddons`
--

INSERT INTO `invoiceaddons` (`id`, `invoiceid`, `invoicedetailid`, `row_group`, `single_addon`, `multiple_addon`, `addon_qty`, `created_at`, `updated_at`) VALUES
(1, 2, 9, '20210406111741', '10-100-5000', NULL, 1, '2021-04-06 11:17:41', '2021-04-06 11:17:41'),
(2, 6, 14, '20210406113146', '9-97-0', NULL, 1, '2021-04-06 11:31:46', '2021-04-06 11:31:46'),
(3, 11, 24, '20210408101048', '10-99-5000', NULL, 1, '2021-04-08 10:10:48', '2021-04-08 10:10:48');

-- --------------------------------------------------------

--
-- Table structure for table `invoicedetail`
--

CREATE TABLE `invoicedetail` (
  `id` int(11) NOT NULL,
  `invoiceid` int(11) DEFAULT NULL,
  `category` varchar(30) DEFAULT NULL,
  `item_id` int(11) DEFAULT NULL,
  `item` varchar(255) DEFAULT NULL,
  `price` double DEFAULT NULL,
  `discount` double DEFAULT NULL,
  `qty` int(3) DEFAULT NULL,
  `note` text DEFAULT NULL,
  `service_type` varchar(20) NOT NULL DEFAULT 'Dine-In',
  `item_status` varchar(10) DEFAULT NULL,
  `clone_data` enum('Y','N') NOT NULL DEFAULT 'N',
  `parent_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `invoicedetail`
--

INSERT INTO `invoicedetail` (`id`, `invoiceid`, `category`, `item_id`, `item`, `price`, `discount`, `qty`, `note`, `service_type`, `item_status`, `clone_data`, `parent_id`, `created_at`, `updated_at`) VALUES
(1, 1, 'Makanan', 19, 'Iga Penyet', 40000, 0, 1, '', 'Dine-In', 'Completed', 'Y', NULL, '2021-04-06 10:14:58', '2021-04-06 10:21:49'),
(2, 1, 'Makanan', 20, 'Tongseng Kambing', 42500, 0, 1, '', 'Dine-In', 'Completed', 'N', NULL, '2021-04-06 10:15:18', '2021-04-06 10:16:13'),
(3, 2, 'Food', 116, 'Beef Cheese Burger', 65000, 0, 1, '', 'Dine-In', 'Completed', 'N', NULL, '2021-04-06 10:21:13', '2021-04-06 10:22:31'),
(4, 3, 'Makanan', 19, 'Iga Penyet', 40000, 0, 1, '', 'Dine-In', 'Completed', 'N', 1, '2021-04-06 10:21:49', '2021-04-06 10:21:49'),
(5, 2, 'Food', 115, 'Spaghetti Carbonara', 65000, 0, 2, '2', 'Dine-In', NULL, 'N', NULL, '2021-04-06 10:25:28', '2021-04-06 11:22:03'),
(6, 4, 'Drink', 101, 'Cappuccino', 35000, 0, 1, '2', 'Dine-In', NULL, 'N', NULL, '2021-04-06 10:32:56', '2021-04-06 10:32:56'),
(7, 5, 'Drink', 101, 'Cappuccino', 35000, 0, 1, '2', 'Dine-In', NULL, 'N', NULL, '2021-04-06 11:16:15', '2021-04-06 11:16:15'),
(8, 2, 'Drink', 101, 'Cappuccino', 35000, 0, 3, '5', 'Dine-In', NULL, 'N', NULL, '2021-04-06 11:16:51', '2021-04-06 11:19:41'),
(9, 2, 'Drink', 83, 'Flat White', 35000, 0, 1, '', 'Dine-In', NULL, 'N', NULL, '2021-04-06 11:17:41', '2021-04-06 11:17:41'),
(10, 2, 'Drink', 104, 'Mineral Water', 20000, 0, 1, '', 'Dine-In', NULL, 'N', NULL, '2021-04-06 11:18:31', '2021-04-06 11:18:31'),
(11, 2, 'Food', 113, 'Chcken Rice Bowl', 45000, 0, 1, '', 'Dine-In', NULL, 'N', NULL, '2021-04-06 11:21:03', '2021-04-06 11:21:03'),
(12, 2, 'Food', 112, 'Beef Bowl Teriyaki', 50000, 0, 1, '', 'Dine-In', NULL, 'N', NULL, '2021-04-06 11:21:29', '2021-04-06 11:21:29'),
(13, 6, 'Drink', 101, 'Cappuccino', 35000, 0, 1, '', 'Dine-In', NULL, 'N', NULL, '2021-04-06 11:31:39', '2021-04-06 11:31:39'),
(14, 6, 'Drink', 85, 'Espresso', 20000, 0, 1, '', 'Dine-In', NULL, 'N', NULL, '2021-04-06 11:31:46', '2021-04-06 11:31:46'),
(15, 7, 'Makanan', 12, 'Sop Buntut', 44000, 0, 1, '', 'Dine-In', NULL, 'N', NULL, '2021-04-06 11:34:54', '2021-04-06 11:34:54'),
(16, 7, 'Makanan', 11, 'Soto Madura', 21500, 0, 1, '', 'Dine-In', NULL, 'N', NULL, '2021-04-06 15:00:54', '2021-04-06 15:00:54'),
(17, 8, 'Makanan', 11, 'Soto Madura', 21500, 0, 3, '', 'Dine-In', NULL, 'N', NULL, '2021-04-06 15:50:50', '2021-04-06 19:39:23'),
(18, 9, 'Minuman', 40, 'Soda Gembira', 14500, 0, 1, '', 'Dine-In', NULL, 'N', NULL, '2021-04-07 11:14:44', '2021-04-07 11:14:44'),
(19, 9, 'Makanan', 11, 'Soto Madura', 21500, 0, 1, '', 'Dine-In', NULL, 'N', NULL, '2021-04-07 11:15:12', '2021-04-07 11:15:12'),
(20, 10, 'Menu', 54, 'Sicura Coffee', 18000, 0, 1, '', 'Dine-In', 'Order', 'N', NULL, '2021-04-07 11:47:11', '2021-04-07 11:47:11'),
(21, 10, 'Menu', 66, 'Latte', 15000, 0, 1, '', 'Dine-In', 'Order', 'N', NULL, '2021-04-07 11:47:14', '2021-04-07 11:47:14'),
(22, 10, 'Menu', 63, 'Americano', 10000, 0, 1, '', 'Dine-In', 'Order', 'N', NULL, '2021-04-07 11:47:17', '2021-04-07 11:47:17'),
(23, 11, 'Drink', 101, 'Cappuccino', 35000, 0, 1, '', 'Dine-In', NULL, 'N', NULL, '2021-04-08 10:10:37', '2021-04-08 10:10:37'),
(24, 11, 'Drink', 83, 'Flat White', 35000, 0, 1, '', 'Dine-In', NULL, 'N', NULL, '2021-04-08 10:10:48', '2021-04-08 10:10:48'),
(25, 12, 'Makanan', 1, 'Sop Buntut', 44000, 0, 2, 'Ga pake kuah', 'Dine-In', 'Order', 'N', NULL, '2021-05-20 09:20:39', '2021-05-20 09:20:39');

-- --------------------------------------------------------

--
-- Table structure for table `invoicerecipe`
--

CREATE TABLE `invoicerecipe` (
  `id` int(11) NOT NULL,
  `reff_id` int(11) DEFAULT NULL,
  `recipe_id` int(11) DEFAULT NULL,
  `item_id` int(11) DEFAULT NULL,
  `recipe_qty` int(5) DEFAULT NULL,
  `note_recipe` varchar(10) DEFAULT NULL,
  `checked` enum('Y','N') NOT NULL DEFAULT 'Y',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `invoicerecipe`
--

INSERT INTO `invoicerecipe` (`id`, `reff_id`, `recipe_id`, `item_id`, `recipe_qty`, `note_recipe`, `checked`, `created_at`, `updated_at`) VALUES
(1, 20, 38, 135, 120, 'Main', 'Y', '2021-04-07 11:47:11', '2021-04-07 11:47:11'),
(2, 20, 39, 136, 9, 'Main', 'Y', '2021-04-07 11:47:11', '2021-04-07 11:47:11'),
(3, 20, 40, 142, 15, 'Main', 'Y', '2021-04-07 11:47:11', '2021-04-07 11:47:11'),
(4, 21, 8, 136, 9, 'Main', 'Y', '2021-04-07 11:47:14', '2021-04-07 11:47:14'),
(5, 21, 9, 135, 120, 'Main', 'Y', '2021-04-07 11:47:14', '2021-04-07 11:47:14'),
(6, 22, 14, 136, 9, 'Main', 'Y', '2021-04-07 11:47:17', '2021-04-07 11:47:17');

-- --------------------------------------------------------

--
-- Table structure for table `items`
--

CREATE TABLE `items` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `items_name` varchar(255) DEFAULT NULL,
  `items_slug` varchar(255) DEFAULT NULL,
  `items_color` varchar(15) DEFAULT NULL,
  `show_on_catalog` varchar(50) NOT NULL,
  `item_image_one` varchar(255) DEFAULT NULL,
  `item_image_two` varchar(255) DEFAULT NULL,
  `item_image_three` varchar(255) DEFAULT NULL,
  `item_image_four` varchar(255) DEFAULT NULL,
  `item_image_primary` varchar(255) DEFAULT NULL,
  `items_description` longtext DEFAULT NULL,
  `items_price` double DEFAULT 0,
  `items_discount` double DEFAULT 0,
  `items_youtube` varchar(255) DEFAULT NULL,
  `ready_stock` enum('Y','N') DEFAULT 'Y',
  `sell` int(11) DEFAULT NULL,
  `item_type` enum('Main','Add','Material') NOT NULL DEFAULT 'Main',
  `item_unit` varchar(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `items`
--

INSERT INTO `items` (`id`, `user_id`, `items_name`, `items_slug`, `items_color`, `show_on_catalog`, `item_image_one`, `item_image_two`, `item_image_three`, `item_image_four`, `item_image_primary`, `items_description`, `items_price`, `items_discount`, `items_youtube`, `ready_stock`, `sell`, `item_type`, `item_unit`, `created_at`, `updated_at`) VALUES
(1, 1, 'Pisang Goreng Tepung', '1-pisang-goreng-tepung', '#000', '1|10|', '/users/1/items/thumbs/pisang_goreng_tepung_1_1.jpg', '/users/1/items/thumbs/pisang_goreng_tepung_1_2.jpg', NULL, NULL, '/users/1/items/thumbs/pisang_goreng_tepung_1_1.jpg', '<p>Klassy Cafe is one of the best <a href=\"https://templatemo.com/tag/restaurant\" target=\"_blank\">restaurant HTML templates</a> with Bootstrap v4.5.2 CSS framework. You can download and feel free to use this website template layout for your restaurant business. You are allowed to use this template for commercial purposes.<br />\r\n<br />\r\nYou are NOT allowed to redistribute the template ZIP file on any template donwnload website. Please contact us for more information.</p>', 22000, 0, 'https://www.youtube.com/watch?v=CdaYvgBl4aU', 'Y', NULL, 'Main', NULL, '2020-11-10 09:05:22', '2021-06-02 12:37:38'),
(2, 1, 'Roti Bakar', '2-roti-bakar', '#000', '1|10|', '/users/1/items/thumbs/roti_bakar_2_1.jpg', NULL, NULL, NULL, '/users/1/items/thumbs/roti_bakar_2_1.jpg', '<p>Klassy Cafe is one of the best <a href=\"https://templatemo.com/tag/restaurant\" target=\"_blank\">restaurant HTML templates</a> with Bootstrap v4.5.2 CSS framework. You can download and feel free to use this website template layout for your restaurant business. You are allowed to use this template for commercial purposes.<br />\r\n<br />\r\nYou are NOT allowed to redistribute the template ZIP file on any template donwnload website. Please contact us for more information.</p>', 18000, 0, 'https://www.youtube.com/watch?v=V5ZC1ojCtyk', 'Y', NULL, 'Main', NULL, '2020-11-10 09:07:46', '2021-06-02 12:37:38'),
(3, 1, 'Batagor', '3-batagor', '#000', '1|10|', '/users/1/items/thumbs/batagor_3_1.jpg', NULL, NULL, NULL, '/users/1/items/thumbs/batagor_3_1.jpg', '<p>Klassy Cafe is one of the best <a href=\"https://templatemo.com/tag/restaurant\" target=\"_blank\">restaurant HTML templates</a> with Bootstrap v4.5.2 CSS framework. You can download and feel free to use this website template layout for your restaurant business. You are allowed to use this template for commercial purposes.<br />\r\n<br />\r\nYou are NOT allowed to redistribute the template ZIP file on any template donwnload website. Please contact us for more information.</p>', 20000, 0, 'https://www.youtube.com/watch?v=FcEVbVrNIyg', 'Y', NULL, 'Main', NULL, '2020-11-10 09:09:10', '2021-06-02 12:37:38'),
(4, 1, 'Siomay', '4-siomay', '#000', '1|10|', '/users/1/items/thumbs/siomay_4_1.jpg', NULL, NULL, NULL, '/users/1/items/thumbs/siomay_4_1.jpg', '<p>Klassy Cafe is one of the best <a href=\"https://templatemo.com/tag/restaurant\" target=\"_blank\">restaurant HTML templates</a> with Bootstrap v4.5.2 CSS framework. You can download and feel free to use this website template layout for your restaurant business. You are allowed to use this template for commercial purposes.<br />\r\n<br />\r\nYou are NOT allowed to redistribute the template ZIP file on any template donwnload website. Please contact us for more information.</p>', 15000, 0, 'https://www.youtube.com/watch?v=skgsn2PKZ1w', 'Y', NULL, 'Main', NULL, '2020-11-10 09:10:39', '2021-06-02 12:37:38'),
(5, 1, 'Singkong Keju', '5-singkong-keju', '#000', '1|10|', '/users/1/items/thumbs/singkong_keju_5_1.jpg', NULL, NULL, NULL, '/users/1/items/thumbs/singkong_keju_5_1.jpg', '<p>Klassy Cafe is one of the best <a href=\"https://templatemo.com/tag/restaurant\" target=\"_blank\">restaurant HTML templates</a> with Bootstrap v4.5.2 CSS framework. You can download and feel free to use this website template layout for your restaurant business. You are allowed to use this template for commercial purposes.<br />\r\n<br />\r\nYou are NOT allowed to redistribute the template ZIP file on any template donwnload website. Please contact us for more information.</p>', 13000, 0, 'https://www.youtube.com/watch?v=skgsn2PKZ1w', 'Y', NULL, 'Main', NULL, '2020-11-10 09:15:13', '2021-06-02 12:37:38'),
(6, 1, 'Tempe Mendoan', '6-tempe-mendoan', '#000', '1|10|', '/users/1/items/thumbs/tempe_mendoan_6_1.jpg', NULL, NULL, NULL, '/users/1/items/thumbs/tempe_mendoan_6_1.jpg', '<p>Klassy Cafe is one of the best <a href=\"https://templatemo.com/tag/restaurant\" target=\"_blank\">restaurant HTML templates</a> with Bootstrap v4.5.2 CSS framework. You can download and feel free to use this website template layout for your restaurant business. You are allowed to use this template for commercial purposes.<br />\r\n<br />\r\nYou are NOT allowed to redistribute the template ZIP file on any template donwnload website. Please contact us for more information.</p>', 14000, 1500, 'https://www.youtube.com/watch?v=VMakfEhU_PY', 'Y', NULL, 'Main', NULL, '2020-11-10 09:16:15', '2021-06-02 12:37:38'),
(7, 1, 'Bakwan', '7-bakwan', '#000', '1|10|', '/users/1/items/thumbs/bakwan_7_1.jpg', NULL, NULL, NULL, '/users/1/items/thumbs/bakwan_7_1.jpg', '<p>Klassy Cafe is one of the best <a href=\"https://templatemo.com/tag/restaurant\" target=\"_blank\">restaurant HTML templates</a> with Bootstrap v4.5.2 CSS framework. You can download and feel free to use this website template layout for your restaurant business. You are allowed to use this template for commercial purposes.<br />\r\n<br />\r\nYou are NOT allowed to redistribute the template ZIP file on any template donwnload website. Please contact us for more information.</p>', 9000, 0, 'https://www.youtube.com/watch?v=VMakfEhU_PY', 'Y', NULL, 'Main', NULL, '2020-11-10 09:18:30', '2021-06-02 12:37:38'),
(8, 1, 'Soto Lamongan', '8-soto-lamongan', '#000', '1|10|', '/users/1/items/thumbs/soto_lamongan_8_1.jpg', '/users/1/items/thumbs/soto_lamongan_8_2.jpg', NULL, NULL, '/users/1/items/thumbs/soto_lamongan_8_1.jpg', '<p>Klassy Cafe is one of the best <a href=\"https://templatemo.com/tag/restaurant\" target=\"_blank\">restaurant HTML templates</a> with Bootstrap v4.5.2 CSS framework. You can download and feel free to use this website template layout for your restaurant business. You are allowed to use this template for commercial purposes.<br />\r\n<br />\r\nYou are NOT allowed to redistribute the template ZIP file on any template donwnload website. Please contact us for more information.</p>', 27000, 0, 'https://www.youtube.com/watch?v=CjOqCeKVBN8', 'Y', NULL, 'Main', NULL, '2020-11-10 09:20:31', '2021-06-02 12:37:38'),
(9, 1, 'Soto Betawi', '9-soto-betawi', '#000', '1|10|', '/users/1/items/thumbs/soto_betawi_9_1.jpg', '/users/1/items/thumbs/soto_betawi_9_2.jpg', NULL, NULL, '/users/1/items/thumbs/soto_betawi_9_1.jpg', '<p>Klassy Cafe is one of the best <a href=\"https://templatemo.com/tag/restaurant\" target=\"_blank\">restaurant HTML templates</a> with Bootstrap v4.5.2 CSS framework. You can download and feel free to use this website template layout for your restaurant business. You are allowed to use this template for commercial purposes.<br />\r\n<br />\r\nYou are NOT allowed to redistribute the template ZIP file on any template donwnload website. Please contact us for more information.</p>', 28000, 3500, 'https://www.youtube.com/watch?v=aZB9g1ohBZU', 'Y', 1, 'Main', NULL, '2020-11-10 09:23:22', '2021-06-02 12:37:38'),
(10, 1, 'Empal Gentong', '10-empal-gentong', '#000', '1|10|', '/users/1/items/thumbs/empal_gentong_10_1.jpg', NULL, NULL, NULL, '/users/1/items/thumbs/empal_gentong_10_1.jpg', '<p>Klassy Cafe is one of the best <a href=\"https://templatemo.com/tag/restaurant\" target=\"_blank\">restaurant HTML templates</a> with Bootstrap v4.5.2 CSS framework. You can download and feel free to use this website template layout for your restaurant business. You are allowed to use this template for commercial purposes.<br />\r\n<br />\r\nYou are NOT allowed to redistribute the template ZIP file on any template donwnload website. Please contact us for more information.</p>', 33000, 0, '', 'Y', NULL, 'Main', NULL, '2020-11-10 09:25:02', '2021-06-02 12:37:38'),
(11, 1, 'Soto Madura', '11-soto-madura', '#000', '1|10|', '/users/1/items/thumbs/soto_madura_11_1.jpg', '/users/1/items/thumbs/soto_madura_11_2.jpg', NULL, NULL, '/users/1/items/thumbs/soto_madura_11_1.jpg', '<p>Klassy Cafe is one of the best <a href=\"https://templatemo.com/tag/restaurant\" target=\"_blank\">restaurant HTML templates</a> with Bootstrap v4.5.2 CSS framework. You can download and feel free to use this website template layout for your restaurant business. You are allowed to use this template for commercial purposes.<br />\r\n<br />\r\nYou are NOT allowed to redistribute the template ZIP file on any template donwnload website. Please contact us for more information.</p>', 21500, 0, '', 'Y', 3, 'Main', NULL, '2020-11-10 09:26:21', '2021-06-02 12:37:38'),
(12, 1, 'Sop Buntut', '12-sop-buntut', '#000', '1|10|', '/users/1/items/thumbs/sop_buntut_12_1.jpg', NULL, NULL, NULL, '/users/1/items/thumbs/sop_buntut_12_1.jpg', '<p>Klassy Cafe is one of the best <a href=\"https://templatemo.com/tag/restaurant\" target=\"_blank\">restaurant HTML templates</a> with Bootstrap v4.5.2 CSS framework. You can download and feel free to use this website template layout for your restaurant business. You are allowed to use this template for commercial purposes.<br />\r\n<br />\r\nYou are NOT allowed to redistribute the template ZIP file on any template donwnload website. Please contact us for more information.</p>', 44000, 0, 'https://www.youtube.com/watch?v=vYjrGxlKjYo', 'Y', 3, 'Main', NULL, '2020-11-10 09:28:14', '2021-06-02 12:37:38'),
(13, 1, 'Sate Ayam', '13-sate-ayam', '#000', '1|10|', '/users/1/items/thumbs/sate_ayam_13_1.jpg', NULL, NULL, NULL, '/users/1/items/thumbs/sate_ayam_13_1.jpg', '<p>Klassy Cafe is one of the best <a href=\"https://templatemo.com/tag/restaurant\" target=\"_blank\">restaurant HTML templates</a> with Bootstrap v4.5.2 CSS framework. You can download and feel free to use this website template layout for your restaurant business. You are allowed to use this template for commercial purposes.<br />\r\n<br />\r\nYou are NOT allowed to redistribute the template ZIP file on any template donwnload website. Please contact us for more information.</p>', 25000, 0, '', 'Y', NULL, 'Main', NULL, '2020-11-10 09:31:37', '2021-06-02 12:37:38'),
(14, 1, 'Sate Sapi', '14-sate-sapi', '#000', '1|10|', '/users/1/items/thumbs/sate_sapi_14_1.png', '/users/1/items/thumbs/sate_sapi_14_2.jpg', NULL, NULL, '/users/1/items/thumbs/sate_sapi_14_1.png', '<p>Klassy Cafe is one of the best <a href=\"https://templatemo.com/tag/restaurant\" target=\"_blank\">restaurant HTML templates</a> with Bootstrap v4.5.2 CSS framework. You can download and feel free to use this website template layout for your restaurant business. You are allowed to use this template for commercial purposes.<br />\r\n<br />\r\nYou are NOT allowed to redistribute the template ZIP file on any template donwnload website. Please contact us for more information.</p>', 30000, 0, 'https://www.youtube.com/watch?v=v691dr13Dtw', 'Y', NULL, 'Main', NULL, '2020-11-10 09:33:43', '2021-06-02 12:37:38'),
(15, 1, 'Sate Kambing', '15-sate-kambing', '#000', '1|10|', '/users/1/items/thumbs/sate_kambing_15_1.jpg', NULL, NULL, NULL, '/users/1/items/thumbs/sate_kambing_15_1.jpg', '<p>Klassy Cafe is one of the best <a href=\"https://templatemo.com/tag/restaurant\" target=\"_blank\">restaurant HTML templates</a> with Bootstrap v4.5.2 CSS framework. You can download and feel free to use this website template layout for your restaurant business. You are allowed to use this template for commercial purposes.<br />\r\n<br />\r\nYou are NOT allowed to redistribute the template ZIP file on any template donwnload website. Please contact us for more information.</p>', 35000, 7500, 'https://www.youtube.com/watch?v=r6iNXKvq1G4', 'Y', NULL, 'Main', NULL, '2020-11-10 09:35:57', '2021-06-02 12:37:38'),
(16, 1, 'Simeut Goreng', '16-simeut-goreng', '#000', '1|10|', '/users/1/items/thumbs/simeut_goreng_16_1.jpg', NULL, NULL, NULL, '/users/1/items/thumbs/simeut_goreng_16_1.jpg', '<p>Klassy Cafe is one of the best <a href=\"https://templatemo.com/tag/restaurant\" target=\"_blank\">restaurant HTML templates</a> with Bootstrap v4.5.2 CSS framework. You can download and feel free to use this website template layout for your restaurant business. You are allowed to use this template for commercial purposes.<br />\r\n<br />\r\nYou are NOT allowed to redistribute the template ZIP file on any template donwnload website. Please contact us for more information.</p>', 9000, 0, NULL, 'N', NULL, 'Main', NULL, '2020-11-10 18:49:56', '2021-06-02 12:37:38'),
(17, 1, 'Tutut Khas Cicalengka', '17-tutut-khas-cicalengka', '#000', '1|10|', '/users/1/items/thumbs/tutut_khas_cicalengka_17_1.JPG', NULL, NULL, NULL, '/users/1/items/thumbs/tutut_khas_cicalengka_17_1.JPG', '<p>Klassy Cafe is one of the best <a href=\"https://templatemo.com/tag/restaurant\" target=\"_blank\">restaurant HTML templates</a> with Bootstrap v4.5.2 CSS framework. You can download and feel free to use this website template layout for your restaurant business. You are allowed to use this template for commercial purposes.<br />\r\n<br />\r\nYou are NOT allowed to redistribute the template ZIP file on any template donwnload website. Please contact us for more information.</p>', 16000, 2000, '', 'Y', NULL, 'Main', NULL, '2020-11-10 18:50:54', '2021-06-02 12:37:38'),
(18, 1, 'Dendeng Balado', '18-dendeng-balado', '#000', '1|10|', '/users/1/items/thumbs/dendeng_balado_18_1.jpg', NULL, NULL, NULL, '/users/1/items/thumbs/dendeng_balado_18_1.jpg', '<p>Klassy Cafe is one of the best <a href=\"https://templatemo.com/tag/restaurant\" target=\"_blank\">restaurant HTML templates</a> with Bootstrap v4.5.2 CSS framework. You can download and feel free to use this website template layout for your restaurant business. You are allowed to use this template for commercial purposes.<br />\r\n<br />\r\nYou are NOT allowed to redistribute the template ZIP file on any template donwnload website. Please contact us for more information.</p>', 45000, 2500, 'https://www.youtube.com/watch?v=5rd_WwD_aOQ', 'Y', NULL, 'Main', NULL, '2020-11-12 10:37:34', '2021-06-02 12:37:38'),
(19, 1, 'Iga Penyet', '19-iga-penyet', '#000', '1|10|', '/users/1/items/thumbs/iga_penyet_19_1.jpg', NULL, NULL, NULL, '/users/1/items/thumbs/iga_penyet_19_1.jpg', '<p>Klassy Cafe is one of the best <a href=\"https://templatemo.com/tag/restaurant\" target=\"_blank\">restaurant HTML templates</a> with Bootstrap v4.5.2 CSS framework. You can download and feel free to use this website template layout for your restaurant business. You are allowed to use this template for commercial purposes.<br />\r\n<br />\r\nYou are NOT allowed to redistribute the template ZIP file on any template donwnload website. Please contact us for more information.</p>', 40000, 0, 'https://www.youtube.com/watch?v=RK1K2bCg4J8&t=3546s', 'Y', 1, 'Main', NULL, '2020-11-12 10:38:57', '2021-06-02 12:37:38'),
(20, 1, 'Tongseng Kambing', '20-tongseng-kambing', '#000', '1|10|', '/users/1/items/thumbs/tongseng_kambing_20_1.jpg', '/users/1/items/thumbs/tongseng_kambing_20_2.jpg', NULL, NULL, '/users/1/items/thumbs/tongseng_kambing_20_1.jpg', '<p>Klassy Cafe is one of the best <a href=\"https://templatemo.com/tag/restaurant\" target=\"_blank\">restaurant HTML templates</a> with Bootstrap v4.5.2 CSS framework. You can download and feel free to use this website template layout for your restaurant business. You are allowed to use this template for commercial purposes.<br />\r\n<br />\r\nYou are NOT allowed to redistribute the template ZIP file on any template donwnload website. Please contact us for more information.</p>', 42500, 0, 'https://www.youtube.com/watch?v=LXb3EKWsInQ', 'Y', NULL, 'Main', NULL, '2020-11-12 10:40:57', '2021-06-02 12:37:38'),
(21, 1, 'Ayam Bakar Kalio', '21-ayam-bakar-kalio', '#000', '1|', '/users/1/items/thumbs/ayam_bakar_kalio_21_1.jpg', NULL, NULL, NULL, '/users/1/items/thumbs/ayam_bakar_kalio_21_1.jpg', '<p>Klassy Cafe is one of the best <a href=\"https://templatemo.com/tag/restaurant\" target=\"_blank\">restaurant HTML templates</a> with Bootstrap v4.5.2 CSS framework. You can download and feel free to use this website template layout for your restaurant business. You are allowed to use this template for commercial purposes.<br />\r\n<br />\r\nYou are NOT allowed to redistribute the template ZIP file on any template donwnload website. Please contact us for more information.</p>', 23000, 0, 'https://www.youtube.com/watch?v=K1QICrgxTjA', 'Y', NULL, 'Main', NULL, '2020-11-12 10:42:56', '2021-06-02 11:15:04'),
(22, 1, 'Ayam Bakar Kalasan', '22-ayam-bakar-kalasan', '#000', '1|', '/users/1/items/thumbs/ayam_bakar_kalasan_22_1.jpg', '/users/1/items/thumbs/ayam_bakar_kalasan_22_2.jpg', NULL, NULL, '/users/1/items/thumbs/ayam_bakar_kalasan_22_1.jpg', '<p>Klassy Cafe is one of the best <a href=\"https://templatemo.com/tag/restaurant\" target=\"_blank\">restaurant HTML templates</a> with Bootstrap v4.5.2 CSS framework. You can download and feel free to use this website template layout for your restaurant business. You are allowed to use this template for commercial purposes.<br />\r\n<br />\r\nYou are NOT allowed to redistribute the template ZIP file on any template donwnload website. Please contact us for more information.</p>', 17500, 0, 'https://www.youtube.com/watch?v=FisOoN7Ynk8&t=2555s', 'Y', NULL, 'Main', NULL, '2020-11-12 10:46:02', '2021-06-02 11:14:59'),
(23, 1, 'Ayam Rica-rica', '23-ayam-rica-rica', '#000', '1|', '/users/1/items/thumbs/ayam_rica_rica_23_1.jpg', NULL, NULL, NULL, '/users/1/items/thumbs/ayam_rica_rica_23_1.jpg', '<p>Klassy Cafe is one of the best <a href=\"https://templatemo.com/tag/restaurant\" target=\"_blank\">restaurant HTML templates</a> with Bootstrap v4.5.2 CSS framework. You can download and feel free to use this website template layout for your restaurant business. You are allowed to use this template for commercial purposes.<br />\r\n<br />\r\nYou are NOT allowed to redistribute the template ZIP file on any template donwnload website. Please contact us for more information.</p>', 15500, 0, 'https://www.youtube.com/watch?v=8zqJP-anCLc', 'Y', NULL, 'Main', NULL, '2020-11-12 10:47:03', '2021-06-02 11:15:09'),
(24, 1, 'Ayam Kremes', '24-ayam-kremes', '#000', '1|', '/users/1/items/thumbs/ayam_kremes_24_1.jpg', NULL, NULL, NULL, '/users/1/items/thumbs/ayam_kremes_24_1.jpg', '<p>Klassy Cafe is one of the best <a href=\"https://templatemo.com/tag/restaurant\" target=\"_blank\">restaurant HTML templates</a> with Bootstrap v4.5.2 CSS framework. You can download and feel free to use this website template layout for your restaurant business. You are allowed to use this template for commercial purposes.<br />\r\n<br />\r\nYou are NOT allowed to redistribute the template ZIP file on any template donwnload website. Please contact us for more information.</p>', 17450, 0, 'https://www.youtube.com/watch?v=9jZ01i92JI8', 'Y', NULL, 'Main', NULL, '2020-11-12 10:48:05', '2021-06-02 11:15:06'),
(25, 1, 'Mie Yamin', '25-mie-yamin', '#000', '1|10|', '/users/1/items/thumbs/mie_yamin_25_1.jpg', NULL, NULL, NULL, '/users/1/items/thumbs/mie_yamin_25_1.jpg', '<p>Klassy Cafe is one of the best <a href=\"https://templatemo.com/tag/restaurant\" target=\"_blank\">restaurant HTML templates</a> with Bootstrap v4.5.2 CSS framework. You can download and feel free to use this website template layout for your restaurant business. You are allowed to use this template for commercial purposes.<br />\r\n<br />\r\nYou are NOT allowed to redistribute the template ZIP file on any template donwnload website. Please contact us for more information.</p>', 20000, 0, 'https://www.youtube.com/watch?v=SMKPKGW083c&t=1332s', 'Y', NULL, 'Main', NULL, '2020-11-12 10:49:53', '2021-06-02 12:37:38'),
(26, 1, 'Bakmi Jawa', '26-bakmi-jawa', '#000', '1|10|', '/users/1/items/thumbs/bakmi_jawa_26_1.jpg', NULL, NULL, NULL, '/users/1/items/thumbs/bakmi_jawa_26_1.jpg', '<p>Klassy Cafe is one of the best <a href=\"https://templatemo.com/tag/restaurant\" target=\"_blank\">restaurant HTML templates</a> with Bootstrap v4.5.2 CSS framework. You can download and feel free to use this website template layout for your restaurant business. You are allowed to use this template for commercial purposes.<br />\r\n<br />\r\nYou are NOT allowed to redistribute the template ZIP file on any template donwnload website. Please contact us for more information.</p>', 24000, 0, 'https://www.youtube.com/watch?v=G52dUQLxPzg', 'Y', NULL, 'Main', NULL, '2020-11-12 10:51:16', '2021-06-02 12:37:38'),
(27, 1, 'Mie Goreng Aceh', '27-mie-goreng-aceh', '#000', '1|10|', '/users/1/items/thumbs/mie_goreng_aceh_27_1.jpg', NULL, NULL, NULL, '/users/1/items/thumbs/mie_goreng_aceh_27_1.jpg', '<p>Klassy Cafe is one of the best <a href=\"https://templatemo.com/tag/restaurant\" target=\"_blank\">restaurant HTML templates</a> with Bootstrap v4.5.2 CSS framework. You can download and feel free to use this website template layout for your restaurant business. You are allowed to use this template for commercial purposes.<br />\r\n<br />\r\nYou are NOT allowed to redistribute the template ZIP file on any template donwnload website. Please contact us for more information.</p>', 19000, 0, 'https://www.youtube.com/watch?v=vX2vsvdq8nw', 'Y', NULL, 'Main', NULL, '2020-11-12 10:52:08', '2021-06-02 12:37:38'),
(28, 1, 'Mie Seafood', '28-mie-seafood', '#000', '1|10|', '/users/1/items/thumbs/mie_seafood_28_1.jpg', NULL, NULL, NULL, '/users/1/items/thumbs/mie_seafood_28_1.jpg', '<p>Klassy Cafe is one of the best <a href=\"https://templatemo.com/tag/restaurant\" target=\"_blank\">restaurant HTML templates</a> with Bootstrap v4.5.2 CSS framework. You can download and feel free to use this website template layout for your restaurant business. You are allowed to use this template for commercial purposes.<br />\r\n<br />\r\nYou are NOT allowed to redistribute the template ZIP file on any template donwnload website. Please contact us for more information.</p>', 21000, 0, 'https://www.youtube.com/watch?v=voprky8BrPw', 'Y', NULL, 'Main', NULL, '2020-11-12 10:53:00', '2021-06-02 12:37:38'),
(29, 1, 'Nasi Campur Bali', '29-nasi-campur-bali', '#000', '1|10|', '/users/1/items/thumbs/nasi_campur_bali_29_1.jpg', NULL, NULL, NULL, '/users/1/items/thumbs/nasi_campur_bali_29_1.jpg', '<p>Klassy Cafe is one of the best <a href=\"https://templatemo.com/tag/restaurant\" target=\"_blank\">restaurant HTML templates</a> with Bootstrap v4.5.2 CSS framework. You can download and feel free to use this website template layout for your restaurant business. You are allowed to use this template for commercial purposes.<br />\r\n<br />\r\nYou are NOT allowed to redistribute the template ZIP file on any template donwnload website. Please contact us for more information.</p>', 36000, 0, 'https://www.youtube.com/watch?v=KTji1hOICEI', 'Y', NULL, 'Main', NULL, '2020-11-12 11:28:39', '2021-06-02 12:37:38'),
(30, 1, 'Nasi Goreng Kampung', '30-nasi-goreng-kampung', '#000', '1|10|', '/users/1/items/thumbs/nasi_goreng_kampung_30_1.jpg', NULL, NULL, NULL, '/users/1/items/thumbs/nasi_goreng_kampung_30_1.jpg', '<p>Klassy Cafe is one of the best <a href=\"https://templatemo.com/tag/restaurant\" target=\"_blank\">restaurant HTML templates</a> with Bootstrap v4.5.2 CSS framework. You can download and feel free to use this website template layout for your restaurant business. You are allowed to use this template for commercial purposes.<br />\r\n<br />\r\nYou are NOT allowed to redistribute the template ZIP file on any template donwnload website. Please contact us for more information.</p>', 17500, 0, 'https://www.youtube.com/watch?v=_RTMLn7rDRw', 'Y', NULL, 'Main', NULL, '2020-11-12 11:29:38', '2021-06-02 12:37:38'),
(31, 1, 'Nasi Goreng Seafood', '31-nasi-goreng-seafood', '#000', '1|10|', '/users/1/items/thumbs/nasi_goreng_seafood_31_1.jpg', NULL, NULL, NULL, '/users/1/items/thumbs/nasi_goreng_seafood_31_1.jpg', '<p>Klassy Cafe is one of the best <a href=\"https://templatemo.com/tag/restaurant\" target=\"_blank\">restaurant HTML templates</a> with Bootstrap v4.5.2 CSS framework. You can download and feel free to use this website template layout for your restaurant business. You are allowed to use this template for commercial purposes.<br />\r\n<br />\r\nYou are NOT allowed to redistribute the template ZIP file on any template donwnload website. Please contact us for more information.</p>', 26000, 0, 'https://www.youtube.com/watch?v=qmN1Gf8rRc8', 'Y', NULL, 'Main', NULL, '2020-11-12 11:30:26', '2021-06-02 12:37:38'),
(32, 1, 'Nasi Kebuli', '32-nasi-kebuli', '#000', '1|10|', '/users/1/items/thumbs/nasi_kebuli_32_1.jpg', NULL, NULL, NULL, '/users/1/items/thumbs/nasi_kebuli_32_1.jpg', '<p>Klassy Cafe is one of the best <a href=\"https://templatemo.com/tag/restaurant\" target=\"_blank\">restaurant HTML templates</a> with Bootstrap v4.5.2 CSS framework. You can download and feel free to use this website template layout for your restaurant business. You are allowed to use this template for commercial purposes.<br />\r\n<br />\r\nYou are NOT allowed to redistribute the template ZIP file on any template donwnload website. Please contact us for more information.</p>', 76500, 13000, 'https://www.youtube.com/watch?v=UJiVARkYs0c', 'Y', NULL, 'Main', NULL, '2020-11-12 11:31:32', '2021-06-02 12:37:38'),
(33, 1, 'Semur Pete', '33-semur-pete', '#000', '1|10|', '/users/1/items/thumbs/semur_pete_33_1.jpg', '/users/1/items/thumbs/semur_pete_33_2.jpg', NULL, NULL, '/users/1/items/thumbs/semur_pete_33_2.jpg', '<p>Klassy Cafe is one of the best <a href=\"https://templatemo.com/tag/restaurant\" target=\"_blank\">restaurant HTML templates</a> with Bootstrap v4.5.2 CSS framework. You can download and feel free to use this website template layout for your restaurant business. You are allowed to use this template for commercial purposes.<br />\r\n<br />\r\nYou are NOT allowed to redistribute the template ZIP file on any template donwnload website. Please contact us for more information.</p>', 18500, 0, 'https://www.youtube.com/watch?v=KcMlPl9jArM', 'Y', NULL, 'Main', NULL, '2020-11-12 11:33:33', '2021-06-02 12:37:38'),
(34, 1, 'Terong Balado', '34-terong-balado', '#000', '1|10|', '/users/1/items/thumbs/terong_balado_34_1.jpg', NULL, NULL, NULL, '/users/1/items/thumbs/terong_balado_34_1.jpg', '<p>Klassy Cafe is one of the best <a href=\"https://templatemo.com/tag/restaurant\" target=\"_blank\">restaurant HTML templates</a> with Bootstrap v4.5.2 CSS framework. You can download and feel free to use this website template layout for your restaurant business. You are allowed to use this template for commercial purposes.<br />\r\n<br />\r\nYou are NOT allowed to redistribute the template ZIP file on any template donwnload website. Please contact us for more information.</p>', 7000, 0, 'https://www.youtube.com/watch?v=CTcoCHKnkT8', 'Y', NULL, 'Main', NULL, '2020-11-12 11:35:49', '2021-06-02 12:37:38'),
(35, 1, 'Awug', '35-awug', '#000', '1|10|', '/users/1/items/thumbs/awug_35_1.jpg', NULL, NULL, NULL, '/users/1/items/thumbs/awug_35_1.jpg', '<p>Klassy Cafe is one of the best <a href=\"https://templatemo.com/tag/restaurant\" target=\"_blank\">restaurant HTML templates</a> with Bootstrap v4.5.2 CSS framework. You can download and feel free to use this website template layout for your restaurant business. You are allowed to use this template for commercial purposes.<br />\r\n<br />\r\nYou are NOT allowed to redistribute the template ZIP file on any template donwnload website. Please contact us for more information.</p>', 10000, 0, 'https://www.youtube.com/watch?v=W6Aw5_kGprU', 'Y', NULL, 'Main', NULL, '2020-11-12 11:37:09', '2021-06-02 12:37:38'),
(36, 1, 'Rajungan Crispy', '36-rajungan-crispy', '#000', '1|10|', '/users/1/items/thumbs/rajungan_crispy_36_1.jpg', NULL, NULL, NULL, '/users/1/items/thumbs/rajungan_crispy_36_1.jpg', '<p>Klassy Cafe is one of the best <a href=\"https://templatemo.com/tag/restaurant\" target=\"_blank\">restaurant HTML templates</a> with Bootstrap v4.5.2 CSS framework. You can download and feel free to use this website template layout for your restaurant business. You are allowed to use this template for commercial purposes.<br />\r\n<br />\r\nYou are NOT allowed to redistribute the template ZIP file on any template donwnload website. Please contact us for more information.</p>', 24000, 4300, 'https://www.youtube.com/watch?v=hjKO0d_umLc', 'Y', NULL, 'Main', NULL, '2020-11-12 11:39:00', '2021-06-02 12:37:38'),
(37, 1, 'Jamur Crispy', '37-jamur-crispy', '#000', '1|10|', '/users/1/items/thumbs/jamur_crispy_37_1.jpg', NULL, NULL, NULL, '/users/1/items/thumbs/jamur_crispy_37_1.jpg', '<p>Klassy Cafe is one of the best <a href=\"https://templatemo.com/tag/restaurant\" target=\"_blank\">restaurant HTML templates</a> with Bootstrap v4.5.2 CSS framework. You can download and feel free to use this website template layout for your restaurant business. You are allowed to use this template for commercial purposes.<br />\r\n<br />\r\nYou are NOT allowed to redistribute the template ZIP file on any template donwnload website. Please contact us for more information.</p>', 6000, 0, 'https://www.youtube.com/watch?v=UYbJNpC4Jt8&t=201s', 'Y', NULL, 'Main', NULL, '2020-11-12 11:39:46', '2021-06-02 12:37:38'),
(38, 1, 'Lemon Tea', '38-lemon-tea', '#000', '1|10|', '/users/1/items/thumbs/lemon_tea_38_1.jpg', NULL, NULL, NULL, '/users/1/items/thumbs/lemon_tea_38_1.jpg', '<p>Klassy Cafe is one of the best <a href=\"https://templatemo.com/tag/restaurant\" target=\"_blank\">restaurant HTML templates</a> with Bootstrap v4.5.2 CSS framework. You can download and feel free to use this website template layout for your restaurant business. You are allowed to use this template for commercial purposes.<br />\r\n<br />\r\nYou are NOT allowed to redistribute the template ZIP file on any template donwnload website. Please contact us for more information.</p>', 15000, 0, 'https://www.youtube.com/watch?v=6O8eZuMmlwk&t=46s', 'Y', NULL, 'Main', NULL, '2020-11-12 11:41:20', '2021-06-02 12:37:38'),
(39, 1, 'Thai Tea', '39-thai-tea', '#000', '1|10|', '/users/1/items/thumbs/thai_tea_39_1.jpg', NULL, NULL, NULL, '/users/1/items/thumbs/thai_tea_39_1.jpg', '<p>Klassy Cafe is one of the best <a href=\"https://templatemo.com/tag/restaurant\" target=\"_blank\">restaurant HTML templates</a> with Bootstrap v4.5.2 CSS framework. You can download and feel free to use this website template layout for your restaurant business. You are allowed to use this template for commercial purposes.<br />\r\n<br />\r\nYou are NOT allowed to redistribute the template ZIP file on any template donwnload website. Please contact us for more information.</p>', 15350, 0, 'https://www.youtube.com/watch?v=XsVJ8PCV-0M', 'Y', NULL, 'Main', NULL, '2020-11-12 11:42:43', '2021-06-02 12:37:38'),
(40, 1, 'Soda Gembira', '40-soda-gembira', '#000', '1|10|', '/users/1/items/thumbs/soda_gembira_40_1.jpg', NULL, NULL, NULL, '/users/1/items/thumbs/soda_gembira_40_1.jpg', '<p>Klassy Cafe is one of the best <a href=\"https://templatemo.com/tag/restaurant\" target=\"_blank\">restaurant HTML templates</a> with Bootstrap v4.5.2 CSS framework. You can download and feel free to use this website template layout for your restaurant business. You are allowed to use this template for commercial purposes.<br />\r\n<br />\r\nYou are NOT allowed to redistribute the template ZIP file on any template donwnload website. Please contact us for more information.</p>', 14500, 0, 'https://www.youtube.com/watch?v=EsPMpFKvZIA', 'Y', NULL, 'Main', NULL, '2020-11-12 11:44:03', '2021-06-02 12:37:38'),
(41, 1, 'Iced Coffee', '41-iced-coffee', '#000', '10|', '/users/1/items/thumbs/iced_coffee_41_1.jpg', NULL, NULL, NULL, '/users/1/items/thumbs/iced_coffee_41_1.jpg', '<p>Klassy Cafe is one of the best <a href=\"https://templatemo.com/tag/restaurant\" target=\"_blank\">restaurant HTML templates</a> with Bootstrap v4.5.2 CSS framework. You can download and feel free to use this website template layout for your restaurant business. You are allowed to use this template for commercial purposes.<br />\r\n<br />\r\nYou are NOT allowed to redistribute the template ZIP file on any template donwnload website. Please contact us for more information.</p>', 26000, 0, 'https://www.youtube.com/watch?v=IUWJ8_lkFAA&t=249s', 'Y', NULL, 'Main', NULL, '2020-11-12 11:45:45', '2021-06-02 12:37:20'),
(42, 1, 'Jus Jeruk', '42-jus-jeruk', '#000', '1|10|', '/users/1/items/thumbs/jus_jeruk_42_1.png', NULL, NULL, NULL, '/users/1/items/thumbs/jus_jeruk_42_1.png', '<p>Klassy Cafe is one of the best <a href=\"https://templatemo.com/tag/restaurant\" target=\"_blank\">restaurant HTML templates</a> with Bootstrap v4.5.2 CSS framework. You can download and feel free to use this website template layout for your restaurant business. You are allowed to use this template for commercial purposes.<br />\r\n<br />\r\nYou are NOT allowed to redistribute the template ZIP file on any template donwnload website. Please contact us for more information.</p>', 13000, 0, 'https://www.youtube.com/watch?v=3mLJCi5pRYM', 'Y', NULL, 'Main', NULL, '2020-11-12 11:47:19', '2021-06-02 12:37:38'),
(43, 1, 'Jus Alpukat', '43-jus-alpukat', '#000', '1|10|', '/users/1/items/thumbs/jus_alpukat_43_1.jpg', NULL, NULL, NULL, '/users/1/items/thumbs/jus_alpukat_43_1.jpg', '<p>Klassy Cafe is one of the best <a href=\"https://templatemo.com/tag/restaurant\" target=\"_blank\">restaurant HTML templates</a> with Bootstrap v4.5.2 CSS framework. You can download and feel free to use this website template layout for your restaurant business. You are allowed to use this template for commercial purposes.<br />\r\n<br />\r\nYou are NOT allowed to redistribute the template ZIP file on any template donwnload website. Please contact us for more information.</p>', 18000, 0, 'https://www.youtube.com/watch?v=dVkK36KOcqs&t=62s', 'Y', NULL, 'Main', NULL, '2020-11-12 11:48:27', '2021-06-02 12:37:38'),
(44, 1, 'Jus Jambu', '44-jus-jambu', '#000', '1|10|', '/users/1/items/thumbs/jus_jambu_44_1.jpg', NULL, NULL, NULL, '/users/1/items/thumbs/jus_jambu_44_1.jpg', '<p>Klassy Cafe is one of the best <a href=\"https://templatemo.com/tag/restaurant\" target=\"_blank\">restaurant HTML templates</a> with Bootstrap v4.5.2 CSS framework. You can download and feel free to use this website template layout for your restaurant business. You are allowed to use this template for commercial purposes.<br />\r\n<br />\r\nYou are NOT allowed to redistribute the template ZIP file on any template donwnload website. Please contact us for more information.</p>', 14000, 0, 'https://www.youtube.com/watch?v=PyFN_FYwqvc', 'Y', NULL, 'Main', NULL, '2020-11-12 11:49:13', '2021-06-02 12:37:38'),
(45, 1, 'Jus Melon', '45-jus-melon', '#000', '1|10|', '/users/1/items/thumbs/jus_melon_45_1.jpg', NULL, NULL, NULL, '/users/1/items/thumbs/jus_melon_45_1.jpg', 'Jus Melon ini berasal dari buah yang masih segar dengan citarasa yang enak dan bermanfaat untuk kesehatan tubuh.', 17600, 0, 'https://www.youtube.com/watch?v=6_P8RUqGQbM&t=42s', 'Y', 3, 'Main', NULL, '2020-11-12 11:50:17', '2021-06-02 12:37:38'),
(46, 1, 'Bajigur', '46-bajigur', '#000', '1|10|', '/users/1/items/thumbs/bajigur_46_1.jpeg', NULL, NULL, NULL, '/users/1/items/thumbs/bajigur_46_1.jpeg', '<p>Klassy Cafe is one of the best <a href=\"https://templatemo.com/tag/restaurant\" target=\"_blank\">restaurant HTML templates</a> with Bootstrap v4.5.2 CSS framework. You can download and feel free to use this website template layout for your restaurant business. You are allowed to use this template for commercial purposes.<br />\r\n<br />\r\nYou are NOT allowed to redistribute the template ZIP file on any template donwnload website. Please contact us for more information.</p>', 9000, 0, 'https://www.youtube.com/watch?v=8jypK2U1AM0', 'Y', 3, 'Main', NULL, '2020-11-12 11:56:52', '2021-06-02 12:37:38'),
(47, 1, 'Kopi Vietnam', '47-kopi-vietnam', '#000', '1|10|', '/users/1/items/thumbs/kopi_vietnam_47_1.jpg', NULL, NULL, NULL, '/users/1/items/thumbs/kopi_vietnam_47_1.jpg', 'Kopi Vietnam atau yang dikenal sebagai Cà phê sữa đá adalah minuman kopi dingin yang berasal dari Vietnam dan dibuat dengan cara menyeduh kopi lalu dicampurkan es batu dan susu kental manis. Proses tersebut menggunakan lebih banyak biji kopi yang telah digiling dan karenanya membutuhkan waktu yang lebih lama dibandingkan dengan sebagian besar proses penyeduhan kopi.', 34000, 0, 'https://www.youtube.com/watch?v=BHqNWkkMzI0&t=135s', 'Y', 14, 'Main', NULL, '2020-11-12 11:57:15', '2021-06-02 12:37:38'),
(48, 1, 'Wedang Jahe', '48-wedang-jahe', '#000', '1|10|', '/users/1/items/thumbs/wedang_jahe_48_1.jpeg', NULL, NULL, NULL, '/users/1/items/thumbs/wedang_jahe_48_1.jpeg', '<p>Klassy Cafe is one of the best <a href=\"https://templatemo.com/tag/restaurant\" target=\"_blank\">restaurant HTML templates</a> with Bootstrap v4.5.2 CSS framework. You can download and feel free to use this website template layout for your restaurant business. You are allowed to use this template for commercial purposes.<br />\r\n<br />\r\nYou are NOT allowed to redistribute the template ZIP file on any template donwnload website. Please contact us for more information.</p>', 12000, 0, 'https://www.youtube.com/watch?v=YhRMjLhYmpc', 'Y', 5, 'Main', NULL, '2020-11-12 11:58:05', '2021-06-02 12:37:38'),
(49, 1, 'Bandrek Abah', '49-bandrek-abah', '#000', '1|10|', '/users/1/items/thumbs/bandrek_abah_49_1.jpg', NULL, NULL, NULL, '/users/1/items/thumbs/bandrek_abah_49_1.jpg', '<p>Klassy Cafe is one of the best <a href=\"https://templatemo.com/tag/restaurant\" target=\"_blank\">restaurant HTML templates</a> with Bootstrap v4.5.2 CSS framework. You can download and feel free to use this website template layout for your restaurant business. You are allowed to use this template for commercial purposes.<br />\r\n<br />\r\nYou are NOT allowed to redistribute the template ZIP file on any template donwnload website. Please contact us for more information.</p>', 13400, 0, 'https://www.youtube.com/watch?v=KL6ZmgbG-z8&t=33s', 'Y', 1, 'Main', NULL, '2020-11-12 11:58:54', '2021-06-02 12:37:38'),
(50, 1, 'Kitkat Banana Smoothies', '50-kitkat-banana-smoothies', '#000', '1|10|', '/users/1/items/thumbs/kitkat_banana_smoothies_50_1.jpg', NULL, NULL, NULL, '/users/1/items/thumbs/kitkat_banana_smoothies_50_1.jpg', '<p>Klassy Cafe is one of the best <a href=\"https://templatemo.com/tag/restaurant\" target=\"_blank\">restaurant HTML templates</a> with Bootstrap v4.5.2 CSS framework. You can download and feel free to use this website template layout for your restaurant business. You are allowed to use this template for commercial purposes.<br />\r\n<br />\r\nYou are NOT allowed to redistribute the template ZIP file on any template donwnload website. Please contact us for more information.</p>', 24600, 0, 'https://www.youtube.com/watch?v=I5B1kxr2TQQ', 'Y', NULL, 'Main', NULL, '2020-11-12 12:01:03', '2021-06-02 12:37:38'),
(51, 1, 'Oreo Banana Smoothies', '51-oreo-banana-smoothies', '#000', '10|', '/users/1/items/thumbs/oreo_banana_smoothies_51_1.jpg', NULL, NULL, NULL, '/users/1/items/thumbs/oreo_banana_smoothies_51_1.jpg', '<p>Klassy Cafe is one of the best <a href=\"https://templatemo.com/tag/restaurant\" target=\"_blank\">restaurant HTML templates</a> with Bootstrap v4.5.2 CSS framework. You can download and feel free to use this website template layout for your restaurant business. You are allowed to use this template for commercial purposes.<br />\r\n<br />\r\nYou are NOT allowed to redistribute the template ZIP file on any template donwnload website. Please contact us for more information.</p>', 19000, 0, 'https://www.youtube.com/watch?v=mA30W2dHQIo', 'Y', NULL, 'Main', NULL, '2020-11-12 12:02:22', '2021-06-03 06:47:38'),
(52, 1, 'Strawberry Smoothies', '52-strawberry-smoothies', '#000', '10|', '/users/1/items/thumbs/strawberry_smoothies_52_1.jpg', NULL, NULL, NULL, '/users/1/items/thumbs/strawberry_smoothies_52_1.jpg', '<p>Klassy Cafe is one of the best <a href=\"https://templatemo.com/tag/restaurant\" target=\"_blank\">restaurant HTML templates</a> with Bootstrap v4.5.2 CSS framework. You can download and feel free to use this website template layout for your restaurant business. You are allowed to use this template for commercial purposes.<br />\n<br />\nYou are NOT allowed to redistribute the template ZIP file on any template donwnload website. Please contact us for more information.</p>', 23000, 0, 'https://www.youtube.com/watch?v=VR90Hdh-JvQ', 'Y', 2, 'Main', NULL, '2020-11-12 12:03:34', '2021-06-03 06:47:27'),
(53, 1, 'Es Campur', '53-es-campur', '#000', '1|', '/users/1/items/thumbs/es_campur_53_1.jpg', NULL, NULL, NULL, '/users/1/items/thumbs/es_campur_53_1.jpg', 'The standard Lorem Ipsum passage, used since the 1500s\n\n\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\"', 20000, 0, 'https://www.youtube.com/watch?v=KxQIcxlMudw', 'Y', 1, 'Main', NULL, '2020-11-12 12:22:11', '2021-06-03 12:10:17'),
(54, 5, 'Sicura Coffee', '54-sicura-coffee', '#000', '', '/users/5/items/thumbs/sicura_coffee_54_1.png', NULL, NULL, NULL, '/users/5/items/thumbs/sicura_coffee_54_1.png', 'Es Kopi Susu dengan Gula Aren Premium', 18000, 0, '', 'Y', 2, 'Main', NULL, '2020-12-14 23:04:59', '2021-03-28 23:39:44'),
(55, 5, 'Rose Latte', '55-rose-latte', '#000', '', '/users/5/items/thumbs/rose_latte_55_1.png', NULL, NULL, NULL, '/users/5/items/thumbs/rose_latte_55_1.png', 'Paduan Espresso, Susu Fullcream, dengan Rose Flavour', 18000, 0, '', 'Y', NULL, 'Main', NULL, '2020-12-16 21:45:03', '2020-12-16 21:45:04'),
(56, 5, 'Irish Cream Latte', '56-irish-cream-latte', '#000', '', '/users/5/items/thumbs/irish_cream_latte_56_1.png', NULL, NULL, NULL, '/users/5/items/thumbs/irish_cream_latte_56_1.png', 'Paduan Espresso, Susu Fullcream, dengan Irish Cream Flavour', 18000, 0, '', 'Y', NULL, 'Main', NULL, '2020-12-16 21:45:49', '2020-12-16 21:45:50'),
(57, 5, 'Butterscotch Latte', '57-butterscotch-latte', '#000', '', '/users/5/items/thumbs/butterscotch_latte_57_1.png', NULL, NULL, NULL, '/users/5/items/thumbs/butterscotch_latte_57_1.png', 'Paduan Espresso, Susu Fullcream, dengan Butterscotch Latte', 18000, 0, '', 'Y', NULL, 'Main', NULL, '2020-12-16 21:46:30', '2020-12-16 21:46:31'),
(58, 5, 'Pink Latte', '58-pink-latte', '#000', '', '/users/5/items/thumbs/pink_latte_58_1.png', NULL, NULL, NULL, '/users/5/items/thumbs/pink_latte_58_1.png', 'Paduan Espresso, dengan Susu Strawberry dan Creamer', 18000, 0, '', 'Y', NULL, 'Main', NULL, '2020-12-16 21:47:22', '2020-12-16 21:47:23'),
(59, 5, 'Vanilla Latte', '59-vanilla-latte', '#000', '', '/users/5/items/thumbs/vanilla_latte_59_1.png', NULL, NULL, NULL, '/users/5/items/thumbs/vanilla_latte_59_1.png', 'Paduan Espresso, Susu Fullcream, dengan Vanilla Flavour', 16000, 0, '', 'Y', 1, 'Main', NULL, '2020-12-16 21:48:01', '2021-03-27 23:45:47'),
(60, 5, 'Caramel Latte', '60-caramel-latte', '#000', '', '/users/5/items/thumbs/caramel_latte_60_1.png', NULL, NULL, NULL, '/users/5/items/thumbs/caramel_latte_60_1.png', 'Paduan Espresso, Susu Fullcream, dengan Caramel Flavour', 16000, 0, '', 'Y', 2, 'Main', NULL, '2020-12-16 21:49:03', '2021-03-28 23:39:44'),
(61, 5, 'Hazelnut Latte', '61-hazelnut-latte', '#000', '', '/users/5/items/thumbs/hazelnut_latte_61_1.png', NULL, NULL, NULL, '/users/5/items/thumbs/hazelnut_latte_61_1.png', 'Paduan Espresso, Susu Fullcream, dengan Hazelnut Flavour', 16000, 0, '', 'Y', 2, 'Main', NULL, '2020-12-16 21:49:47', '2021-03-28 23:39:44'),
(63, 5, 'Americano', '63-americano', '#000', '', '/users/5/items/thumbs/americano_63_1.png', NULL, NULL, NULL, '/users/5/items/thumbs/americano_63_1.png', 'Espresso dengan Es atau Panas', 10000, 0, '', 'Y', 1, 'Main', NULL, '2020-12-17 01:23:22', '2021-03-16 20:16:45'),
(64, 5, 'Kopi Atasan', '64-kopi-atasan', '#000', '', '/users/5/items/thumbs/kopi_atasan_64_1.png', NULL, NULL, NULL, '/users/5/items/thumbs/kopi_atasan_64_1.png', 'Perpaduan Espresso, Susu Fullcream, dengan Krimer Premium yang menciptakan rasa lebihc creamy pada kopi', 16000, 0, '', 'Y', 3, 'Main', NULL, '2020-12-17 01:24:27', '2021-03-29 00:50:00'),
(65, 5, 'Bambino Latte', '65-bambino-latte', '#000', '', '/users/5/items/thumbs/bambino_latte_65_1.png', NULL, NULL, NULL, '/users/5/items/thumbs/bambino_latte_65_1.png', 'Perpaduan Espresso dengan Regal Blended', 20000, 0, '', 'Y', 1, 'Main', NULL, '2020-12-17 01:26:53', '2021-04-02 20:07:58'),
(66, 5, 'Latte', '66-latte', '#330033', '', '/users/5/items/thumbs/latte_66_1.png', NULL, NULL, NULL, '/users/5/items/thumbs/latte_66_1.png', 'Perpaduan Espresso dengan Susu Full Cream', 15000, 0, NULL, 'Y', 12, 'Main', NULL, '2020-12-17 01:29:08', '2021-03-18 23:08:32'),
(68, 7, 'R18 Coffee Signature', '68-r18-coffee-signature', '#000', '', '/users/7/items/thumbs/r18_coffee_signature_68_1.jpg', NULL, NULL, NULL, '/users/7/items/thumbs/r18_coffee_signature_68_1.jpg', 'Espresso Arabica + Non Diary Creamer + Fresh Milk + Palm Sugar From Kudus Central Java', 35000, 0, NULL, 'Y', 1, 'Main', NULL, '2021-01-25 15:45:46', '2021-03-03 16:08:51'),
(69, 7, 'Burger', '69-burger', '#000', '', '/users/7/items/thumbs/burger_69_1.jpg', NULL, NULL, NULL, '/users/7/items/thumbs/burger_69_1.jpg', 'dibuaaaaaaaat diiii oveennnnnnn', 40000, 0, '', 'Y', NULL, 'Main', NULL, '2021-01-25 15:47:05', '2021-01-25 15:47:05'),
(70, 7, 'Pastaku', '70-pastaku', '#000', '', '/users/7/items/thumbs/pastaku_70_1.jpg', NULL, NULL, NULL, '/users/7/items/thumbs/pastaku_70_1.jpg', 'pkeeeee ebangeeeeeeettttttttttt', 45000, 0, '', 'Y', NULL, 'Main', NULL, '2021-01-25 15:47:48', '2021-01-25 15:47:48'),
(71, 7, 'Toast', '71-toast', '#000', '', '/users/7/items/thumbs/toast_71_1.jpg', NULL, NULL, NULL, '/users/7/items/thumbs/toast_71_1.jpg', 'yuhuuuuuuwwwwwww okeeeeeeeeeee', 40000, 0, NULL, 'Y', NULL, 'Main', NULL, '2021-01-25 15:48:34', '2021-03-02 22:27:33'),
(74, 1, 'Less', '74-less', NULL, '1|10|', '/users/1/items/thumbs/less_74_1.jpg', NULL, NULL, NULL, '/users/1/items/thumbs/less_74_1.jpg', '\"Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit...\"', 0, 0, '', 'Y', NULL, 'Add', NULL, '2021-02-15 05:10:46', '2021-06-02 12:37:38'),
(75, 1, 'Normal', '75-normal', NULL, '1|10|', '/users/1/items/thumbs/normal_75_1.jpg', NULL, NULL, NULL, '/users/1/items/thumbs/normal_75_1.jpg', '\"Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit...\"', 0, 0, '', 'Y', NULL, 'Add', NULL, '2021-02-15 05:10:58', '2021-06-02 12:37:38'),
(76, 1, 'Regal', '76-regal', NULL, '1|10|', '/users/1/items/thumbs/regal_76_1.jpg', NULL, NULL, NULL, '/users/1/items/thumbs/regal_76_1.jpg', '\"Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit...\"', 9500, 0, '', 'Y', NULL, 'Add', NULL, '2021-02-15 05:11:17', '2021-06-02 12:37:38'),
(77, 1, 'Jelly', '77-jelly', NULL, '1|10|', '/users/1/items/thumbs/jelly_77_1.jpg', NULL, NULL, NULL, '/users/1/items/thumbs/jelly_77_1.jpg', '\"Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit...\"', 4000, 0, '', 'Y', 3, 'Add', NULL, '2021-02-15 05:11:30', '2021-06-02 12:37:38'),
(78, 7, 'Regular Tea', '78-regular-tea', '#000', '', '/users/7/items/thumbs/regular_tea_78_1.jpg', NULL, NULL, NULL, '/users/7/items/thumbs/regular_tea_78_1.jpg', 'Teh dengan rasa dan arorma melati', 20000, 0, NULL, 'Y', NULL, 'Main', NULL, '2021-03-02 22:31:07', '2021-03-03 14:25:07'),
(79, 7, 'Caffe Latte', '79-caffe-latte', '#000', '', '/users/7/items/thumbs/caffe_latte_79_1.jpg', NULL, NULL, NULL, '/users/7/items/thumbs/caffe_latte_79_1.jpg', 'Latte atau Caffè latte adalah espresso atau kopi yang dicampur dengan susu dan memiliki lapisan busa yang tipis di bagian atasnya.', 35000, 0, NULL, 'Y', 2, 'Main', NULL, '2021-03-02 22:33:10', '2021-03-03 13:59:44'),
(80, 7, 'Soy Coffee Latte', '80-soy-coffee-latte', '#000', '', '/users/7/items/thumbs/soy_coffee_latte_80_1.png', NULL, NULL, NULL, '/users/7/items/thumbs/soy_coffee_latte_80_1.png', 'Soy Coffee Latte sendiri merupakan sebuah minuman kekinian yang dibuat dari susu kedelai dengan espresso yang dicampur dengan foam susu segar atau dikenal dengan (Coffee Latte).', 35000, 0, NULL, 'Y', 2, 'Main', NULL, '2021-03-02 22:34:49', '2021-03-05 13:20:44'),
(81, 7, 'Lychee Tea', '81-lychee-tea', '#000', '', '/users/7/items/thumbs/lychee_tea_81_1.jpg', NULL, NULL, NULL, '/users/7/items/thumbs/lychee_tea_81_1.jpg', 'Teh dengan rasa dan aroma Lychee, ditambah buah Lychee asli.', 30000, 0, NULL, 'Y', NULL, 'Main', NULL, '2021-03-02 22:37:50', '2021-03-03 14:23:11'),
(82, 7, 'Classic Chocolate Milk', '82-classic-chocolate-milk', '#000', '', '/users/7/items/thumbs/classic_chocolate_milk_82_1.jpg', NULL, NULL, NULL, '/users/7/items/thumbs/classic_chocolate_milk_82_1.jpg', 'Susu cokelat adalah susu rasa cokelat manis. Minuman tersebut terbuat dari campuran sirup cokelat dengan susu.', 18000, 0, '', 'Y', NULL, 'Main', NULL, '2021-03-02 22:39:47', '2021-03-02 22:39:47');
INSERT INTO `items` (`id`, `user_id`, `items_name`, `items_slug`, `items_color`, `show_on_catalog`, `item_image_one`, `item_image_two`, `item_image_three`, `item_image_four`, `item_image_primary`, `items_description`, `items_price`, `items_discount`, `items_youtube`, `ready_stock`, `sell`, `item_type`, `item_unit`, `created_at`, `updated_at`) VALUES
(83, 7, 'Flat White', '83-flat-white', '#000', '', '/users/7/items/thumbs/flat_white_83_1.jpg', NULL, NULL, NULL, '/users/7/items/thumbs/flat_white_83_1.jpg', 'Minuman kopi berbasis espresso yang terdiri dari Doppio dengan lapisan mikrofoam', 35000, 0, NULL, 'Y', 3, 'Main', NULL, '2021-03-02 22:46:08', '2021-04-06 11:26:11'),
(84, 7, 'Picollo', '84-picollo', '#000', '', '/users/7/items/thumbs/picollo_84_1.jpg', NULL, NULL, NULL, '/users/7/items/thumbs/picollo_84_1.jpg', 'Mini Cappucino 5oz (Single Ristreto)', 35000, 0, NULL, 'Y', 2, 'Main', NULL, '2021-03-02 22:49:02', '2021-03-05 13:20:44'),
(85, 7, 'Espresso', '85-espresso', '#000', '', '/users/7/items/thumbs/espresso_85_1.jpg', NULL, NULL, NULL, '/users/7/items/thumbs/espresso_85_1.jpg', 'Espreso adalah minuman yang dihasilkan dengan mengekstraksi biji kopi yang sudah digiling dengan menyemburkan air panas di bawah tekanan tinggi.', 20000, 0, NULL, 'Y', 1, 'Main', NULL, '2021-03-02 22:51:22', '2021-03-05 09:32:42'),
(86, 7, 'Tubruk Coffee', '86-tubruk-coffee', '#000', '', '/users/7/items/thumbs/tubruk_coffee_86_1.jpg', NULL, NULL, NULL, '/users/7/items/thumbs/tubruk_coffee_86_1.jpg', 'Kopi Tubruk adalah minuman kopi khas Indonesia yang dibuat dengan menuangkan air panas ke dalam gelas atau teko yang sudah diisi bubuk kopi.', 25000, 0, NULL, 'Y', NULL, 'Main', NULL, '2021-03-02 22:53:34', '2021-03-03 14:13:20'),
(87, 7, 'Lemon Tea', '87-lemon-tea', '#000', '', '/users/7/items/thumbs/lemon_tea_87_1.jpg', NULL, NULL, NULL, '/users/7/items/thumbs/lemon_tea_87_1.jpg', 'Teh dengan rasa dan aroma Lemon dari Lemon asli', 30000, 0, NULL, 'Y', NULL, 'Main', NULL, '2021-03-02 22:57:30', '2021-03-03 14:18:36'),
(88, 7, 'Americano / Long Black', '88-americano-long-black', '#000', '', '/users/7/items/thumbs/americano_long_black_88_1.jpg', NULL, NULL, NULL, '/users/7/items/thumbs/americano_long_black_88_1.jpg', 'Caffè Americano atau Amerikano adalah minuman kopi yang dibuat dengan mencampurkan satu seloki espresso dengan air panas. Air panas yang digunakan dalam minuman ini adalah sebanyak 6 hingga 8 ons.', 30000, 0, NULL, 'Y', 10, 'Main', NULL, '2021-03-02 23:01:38', '2021-03-05 15:11:20'),
(89, 7, 'Kue Balok', '89-kue-balok', '#000', '', '/users/7/items/thumbs/kue_balok_89_1.jpg', NULL, NULL, NULL, '/users/7/items/thumbs/kue_balok_89_1.jpg', 'Kue balok adalah kue yang terbuat dari bahan baku tepung terigu, vanili, telur, susu kental manis, soda kue, margarine dan gula pasir.', 43000, 0, '', 'Y', NULL, 'Main', NULL, '2021-03-02 23:05:17', '2021-03-02 23:05:17'),
(90, 7, 'Macaroni Schotel', '90-macaroni-schotel', '#000', '', '/users/7/items/thumbs/macaroni_schotel_90_1.jpg', NULL, NULL, NULL, '/users/7/items/thumbs/macaroni_schotel_90_1.jpg', 'Makaroni schotel atau terkadang disebut sebagai makaroni schaal adalah sebuah hidangan kaserol makaroni Indonesia yang terbuat dari pasta (makaroni), keju, susu, mentega, daging (terutama daging asap atau ayam), sosis, tuna, telur, bawang bombay, jamur dan terkadang kentang.', 27000, 0, '', 'Y', NULL, 'Main', NULL, '2021-03-02 23:08:18', '2021-03-02 23:08:18'),
(91, 7, 'Spaghetti Carbonara', '91-spaghetti-carbonara', '#000', '', '/users/7/items/thumbs/spaghetti_carbonara_91_1.jpg', NULL, NULL, NULL, '/users/7/items/thumbs/spaghetti_carbonara_91_1.jpg', 'Spaghetti alla carbonara atau carbonara adalah masakan Italia yang berupa spaghetti yang dimasak dengan saus telur, keju dan daging. Carbonara diciptakan dari resep rakyat pedesaan Italia tengah yang berciri sederhana dan apa adanya.', 37500, 0, '', 'Y', NULL, 'Main', NULL, '2021-03-02 23:10:40', '2021-03-02 23:10:40'),
(92, 7, 'Serabi Coklat', '92-serabi-coklat', '#000', '', '/users/7/items/thumbs/serabi_coklat_92_1.jpg', NULL, NULL, NULL, '/users/7/items/thumbs/serabi_coklat_92_1.jpg', 'Serabi merupakan jajanan pasar tradisional yang berasal dari Indonesia. Ada dua jenis serabi, yaitu serabi manis yang menggunakan kinca dan serabi asin dengan atau tanpa taburan oncom yang telah dibumbui di atasnya.', 14100, 0, '', 'Y', NULL, 'Main', NULL, '2021-03-02 23:13:20', '2021-03-02 23:13:21'),
(93, 7, 'Ravioli', '93-ravioli', '#000', '', '/users/7/items/thumbs/ravioli_93_1.jpg', NULL, NULL, NULL, '/users/7/items/thumbs/ravioli_93_1.jpg', 'Ravioli adalah jenis pasta yang terdiri dari isian yang diselimuti adonan pasta tipis. Biasanya disajikan dalam kaldu atau dengan saus, mereka berasal sebagai makanan tradisional dalam masakan Italia.', 54600, 8700, '', 'Y', NULL, 'Main', NULL, '2021-03-02 23:18:10', '2021-03-02 23:18:10'),
(94, 7, 'Blueberry Muffin', '94-blueberry-muffin', '#000', '', '/users/7/items/thumbs/blueberry_muffin_94_1.jpg', NULL, NULL, NULL, '/users/7/items/thumbs/blueberry_muffin_94_1.jpg', 'Here are some adjectives for blueberry muffins: hot, fresh, awesome, fresh, wild, circular, hot, huge, best. You can get the definitions of these adjectives by clicking on them.', 20300, 0, '', 'Y', NULL, 'Main', NULL, '2021-03-02 23:21:12', '2021-03-02 23:21:12'),
(95, 7, 'Crab Farfalle Pasta', '95-crab-farfalle-pasta', '#000', '', '/users/7/items/thumbs/crab_farfalle_pasta_95_1.jpeg', NULL, NULL, NULL, '/users/7/items/thumbs/crab_farfalle_pasta_95_1.jpeg', 'Farfalle adalah jenis pasta yang umumnya dikenal sebagai pasta kupu-kupu. Nama ini berasal dari kata Italia farfalle. Di kota Italia Modena, farfalle dikenal sebagai strichetti. Variasi farfalle yang lebih besar dikenal sebagai farfalloni, sedangkan versi miniatur disebut farfalline.', 75000, 0, '', 'Y', NULL, 'Main', NULL, '2021-03-02 23:23:16', '2021-03-02 23:23:17'),
(96, 7, 'Less', '96-less', NULL, '', '/users/7/items/thumbs/less_96_1.jpg', NULL, NULL, NULL, '/users/7/items/thumbs/less_96_1.jpg', 'Sugar is the generic name for sweet-tasting, soluble carbohydrates, many of which are used in food.', 0, 0, '', 'Y', NULL, 'Add', NULL, '2021-03-03 11:11:32', '2021-03-03 11:11:32'),
(97, 7, 'Medium', '97-medium', NULL, '', '/users/7/items/thumbs/medium_97_1.jpg', NULL, NULL, NULL, '/users/7/items/thumbs/medium_97_1.jpg', 'Sugar is the generic name for sweet-tasting, soluble carbohydrates, many of which are used in food.', 0, 0, '', 'Y', NULL, 'Add', NULL, '2021-03-03 11:11:53', '2021-03-03 11:11:54'),
(98, 7, 'Caramel', '98-caramel', NULL, '', '/users/7/items/thumbs/caramel_98_1.jpg', NULL, NULL, NULL, '/users/7/items/thumbs/caramel_98_1.jpg', 'Sirup dengan rasa dan aroma Caramel', 5000, 0, '', 'Y', NULL, 'Add', NULL, '2021-03-03 14:10:07', '2021-03-03 14:10:07'),
(99, 7, 'Hazelnut', '99-hazelnut', NULL, '', '/users/7/items/thumbs/hazelnut_99_1.jpg', NULL, NULL, NULL, '/users/7/items/thumbs/hazelnut_99_1.jpg', 'Sirup dengan rasa dan aroma Hazelnut', 5000, 0, '', 'Y', NULL, 'Add', NULL, '2021-03-03 14:11:31', '2021-03-03 14:11:31'),
(100, 7, 'Vanilla', '100-vanilla', NULL, '', '/users/7/items/thumbs/vanilla_100_1.jpg', NULL, NULL, NULL, '/users/7/items/thumbs/vanilla_100_1.jpg', 'Sirup dengan rasa dan aroma Vanilla', 5000, 0, '', 'Y', NULL, 'Add', NULL, '2021-03-03 14:12:40', '2021-03-03 14:12:40'),
(101, 7, 'Cappuccino', '101-cappuccino', '#000', '', '/users/7/items/thumbs/cappuccino_101_1.jpg', NULL, NULL, NULL, '/users/7/items/thumbs/cappuccino_101_1.jpg', 'Minuman yang dibuat dari espresso arabica dan susu dengan perbandingan 1/3 espresso arabica, 1/3 susu, dan 1/3 foam.', 35000, 0, '', 'Y', 16, 'Main', NULL, '2021-03-03 14:35:46', '2021-04-06 11:26:11'),
(102, 7, 'Chocolate', '102-chocolate', '#000', '', '/users/7/items/thumbs/chocolate_102_1.jpeg', NULL, NULL, NULL, '/users/7/items/thumbs/chocolate_102_1.jpeg', 'Minuman yang dibuat dari bubuk cacao, condense milk, dan fresh milk', 35000, 0, NULL, 'Y', NULL, 'Main', NULL, '2021-03-03 14:46:22', '2021-03-03 14:47:42'),
(103, 7, 'Matcha Latte', '103-matcha-latte', '#000', '', '/users/7/items/thumbs/matcha_latte_103_1.jpeg', NULL, NULL, NULL, '/users/7/items/thumbs/matcha_latte_103_1.jpeg', 'Minuman yang dibuat dari bubuk matcha, condense milk, dan fresh milk', 35000, 0, NULL, 'Y', 1, 'Main', NULL, '2021-03-03 14:47:32', '2021-03-03 16:42:59'),
(104, 7, 'Mineral Water', '104-mineral-water', '#000', '', '/users/7/items/thumbs/mineral_water_104_1.jpeg', NULL, NULL, NULL, '/users/7/items/thumbs/mineral_water_104_1.jpeg', 'Air Mineral Kemasan Segar', 20000, 0, '', 'Y', 3, 'Main', NULL, '2021-03-03 14:50:48', '2021-04-06 11:26:11'),
(105, 7, 'Sparkling Water', '105-sparkling-water', '#000', '', '/users/7/items/thumbs/sparkling_water_105_1.jpeg', NULL, NULL, NULL, '/users/7/items/thumbs/sparkling_water_105_1.jpeg', 'Air soda atau air berkarbonasi', 45000, 0, NULL, 'N', 1, 'Main', NULL, '2021-03-03 14:52:11', '2021-03-05 14:36:31'),
(106, 7, 'Carlsberg', '106-carlsberg', '#000', '', '/users/7/items/thumbs/carlsberg_106_1.png', NULL, NULL, NULL, '/users/7/items/thumbs/carlsberg_106_1.png', 'Minuman alkohol 5% dengan proses fermentasi', 50000, 0, '', 'Y', 1, 'Main', NULL, '2021-03-03 14:57:08', '2021-03-05 09:43:52'),
(107, 7, 'Smirnoff', '107-smirnoff', '#000', '', '/users/7/items/thumbs/smirnoff_107_1.jpg', NULL, NULL, NULL, '/users/7/items/thumbs/smirnoff_107_1.jpg', 'Minuman alkohol yang mengandung kadar alkohol 4,5%', 50000, 0, '', 'Y', 2, 'Main', NULL, '2021-03-03 14:57:43', '2021-03-05 09:43:52'),
(108, 7, 'Anker', '108-anker', '#000', '', '/users/7/items/thumbs/anker_108_1.jpg', NULL, NULL, NULL, '/users/7/items/thumbs/anker_108_1.jpg', 'Minuman fermentasi dari campuran beras dan biji gandum dengan kadar alkohol 4,5%', 50000, 0, '', 'Y', NULL, 'Main', NULL, '2021-03-03 14:58:12', '2021-03-03 14:58:12'),
(109, 7, 'Blue Ocean', '109-blue-ocean', '#000', '', '/users/7/items/thumbs/blue_ocean_109_1.jpeg', NULL, NULL, NULL, '/users/7/items/thumbs/blue_ocean_109_1.jpeg', 'Sirup Blueberry dengan soda dan fresh lemon', 40000, 0, NULL, 'N', NULL, 'Main', NULL, '2021-03-03 15:00:54', '2021-03-05 14:35:30'),
(110, 7, 'Pink Beach', '110-pink-beach', '#000', '', '/users/7/items/thumbs/pink_beach_110_1.jpeg', NULL, NULL, NULL, '/users/7/items/thumbs/pink_beach_110_1.jpeg', 'Rosella Tea dengan soda', 40000, 0, NULL, 'N', NULL, 'Main', NULL, '2021-03-03 15:01:39', '2021-03-05 14:35:54'),
(111, 7, 'Mojito', '111-mojito', '#000', '', '/users/7/items/thumbs/mojito_111_1.jpeg', NULL, NULL, NULL, '/users/7/items/thumbs/mojito_111_1.jpeg', 'Minuman segar perpaduan lemon dan soda(Mojito)', 40000, 0, NULL, 'N', NULL, 'Main', NULL, '2021-03-03 15:02:11', '2021-03-05 13:53:47'),
(112, 7, 'Beef Bowl Teriyaki', '112-beef-bowl-teriyaki', '#000', '', '/users/7/items/thumbs/beef_bowl_teriyaki_112_1.jpeg', NULL, NULL, NULL, '/users/7/items/thumbs/beef_bowl_teriyaki_112_1.jpeg', 'Nasi dengan beef teriyaki dan telur ceplok', 50000, 0, '', 'Y', 1, 'Main', NULL, '2021-03-03 15:04:24', '2021-04-06 11:26:11'),
(113, 7, 'Chcken Rice Bowl', '113-chcken-rice-bowl', '#000', '', '/users/7/items/thumbs/chcken_rice_bowl_113_1.jpeg', NULL, NULL, NULL, '/users/7/items/thumbs/chcken_rice_bowl_113_1.jpeg', 'Nasi dengan ayam goreng dan telur ceplok', 45000, 0, '', 'Y', 1, 'Main', NULL, '2021-03-03 15:05:44', '2021-04-06 11:26:11'),
(114, 7, 'Spaghetti Aglio Olio', '114-spaghetti-aglio-olio', '#000', '', '/users/7/items/thumbs/spaghetti_aglio_olio_114_1.jpeg', NULL, NULL, NULL, '/users/7/items/thumbs/spaghetti_aglio_olio_114_1.jpeg', 'Pasta Spaghetti dengan bumbu bawang putih, cabai, dan udang.', 65000, 0, '', 'Y', NULL, 'Main', NULL, '2021-03-03 15:14:06', '2021-03-03 15:14:06'),
(115, 7, 'Spaghetti Carbonara', '115-spaghetti-carbonara', '#000', '', '/users/7/items/thumbs/spaghetti_carbonara_115_1.jpeg', NULL, NULL, NULL, '/users/7/items/thumbs/spaghetti_carbonara_115_1.jpeg', 'Pasta Spaghetti dengan saus carbonara dan topping beef bacon', 65000, 0, '', 'Y', 2, 'Main', NULL, '2021-03-03 15:15:50', '2021-04-06 11:26:11'),
(116, 7, 'Beef Cheese Burger', '116-beef-cheese-burger', '#000', '', '/users/7/items/thumbs/beef_cheese_burger_116_1.jpeg', NULL, NULL, NULL, '/users/7/items/thumbs/beef_cheese_burger_116_1.jpeg', 'Burger dengan isian beef dan cheese', 65000, 0, '', 'Y', 1, 'Main', NULL, '2021-03-03 15:16:47', '2021-04-06 11:26:11'),
(117, 7, 'Pempek', '117-pempek', '#000', '', '/users/7/items/thumbs/pempek_117_1.jpeg', NULL, NULL, NULL, '/users/7/items/thumbs/pempek_117_1.jpeg', 'Makanan yang terbuat dari ikan tenggiri  dan berasal dari palembang (fish cake)', 35000, 0, '', 'Y', NULL, 'Main', NULL, '2021-03-03 15:18:18', '2021-03-03 15:18:18'),
(118, 7, 'Toast', '118-toast', '#000', '', '/users/7/items/thumbs/toast_118_1.jpeg', NULL, NULL, NULL, '/users/7/items/thumbs/toast_118_1.jpeg', 'Roti Bakar dengan isian cokelat & keju', 35000, 0, '', 'Y', NULL, 'Main', NULL, '2021-03-03 15:20:49', '2021-03-03 15:20:50'),
(119, 7, 'French Fries', '119-french-fries', '#000', '', '/users/7/items/thumbs/french_fries_119_1.jpeg', NULL, NULL, NULL, '/users/7/items/thumbs/french_fries_119_1.jpeg', 'Kentang Goreng dengan Saus Keju', 35000, 0, NULL, 'Y', NULL, 'Main', NULL, '2021-03-03 15:21:44', '2021-03-03 15:23:39'),
(120, 7, 'Croissant', '120-croissant', '#000', '', '/users/7/items/thumbs/croissant_120_1.jpeg', NULL, NULL, NULL, '/users/7/items/thumbs/croissant_120_1.jpeg', 'Sejenis pastry yang berasal dari prancis yang terbuat dari tepung dan butter', 35000, 0, '', 'Y', NULL, 'Main', NULL, '2021-03-03 15:26:20', '2021-03-03 15:26:21'),
(121, 7, 'Yoghurt Greek', '121-yoghurt-greek', '#000', '', '/users/7/items/thumbs/yoghurt_greek_121_1.jpeg', NULL, NULL, NULL, '/users/7/items/thumbs/yoghurt_greek_121_1.jpeg', 'Yoghurt Greek dengan rasa buah Peach', 35000, 0, '', 'Y', NULL, 'Main', NULL, '2021-03-03 15:27:28', '2021-03-03 15:27:28'),
(122, 7, 'Waflle (Strawberry/Chocolate)', '122-waflle-strawberrychocolate', '#000', '', '/users/7/items/thumbs/waflle_strawberrychocolate_122_1.jpeg', NULL, NULL, NULL, '/users/7/items/thumbs/waflle_strawberrychocolate_122_1.jpeg', 'Makanan dari adonan beragi yang dibuat dengan cetakan. Dipadukan dengan topping Strawberry atau Chocolate', 50000, 0, '', 'Y', NULL, 'Main', NULL, '2021-03-03 15:33:40', '2021-03-03 15:33:40'),
(123, 7, 'Cookies', '123-cookies', '#000', '', '/users/7/items/thumbs/cookies_123_1.jpeg', NULL, NULL, NULL, '/users/7/items/thumbs/cookies_123_1.jpeg', 'Makanan manis yang dipanggang yang terbuat dari tepung gula dan butter. Dengan 3 varian rasa Triple Choco, Chocolate Chip, dan Red Velvet', 20000, 0, '', 'Y', NULL, 'Main', NULL, '2021-03-03 15:36:32', '2021-03-03 15:36:32'),
(124, 7, 'Red Velvet Cake', '124-red-velvet-cake', '#000', '', '/users/7/items/thumbs/red_velvet_cake_124_1.jpeg', NULL, NULL, NULL, '/users/7/items/thumbs/red_velvet_cake_124_1.jpeg', 'Bolu dengan rasa red velvet yang berwarna merah dan dilapisi dengan butter cream', 50000, 0, '', 'Y', NULL, 'Main', NULL, '2021-03-03 15:39:55', '2021-03-03 15:39:55'),
(125, 7, 'Cheese Cake', '125-cheese-cake', '#000', '', '/users/7/items/thumbs/cheese_cake_125_1.jpeg', NULL, NULL, NULL, '/users/7/items/thumbs/cheese_cake_125_1.jpeg', 'Jenis cake yang terbuat dari bahan krim keju.', 50000, 0, '', 'Y', NULL, 'Main', NULL, '2021-03-03 15:41:43', '2021-03-03 15:41:43'),
(126, 7, 'Brownies', '126-brownies', '#000', '', '/users/7/items/thumbs/brownies_126_1.jpeg', NULL, NULL, NULL, '/users/7/items/thumbs/brownies_126_1.jpeg', 'Kue bertekstur lembut dan padat yang memiliki rasa cokelat.', 35000, 0, '', 'Y', NULL, 'Main', NULL, '2021-03-03 15:42:23', '2021-03-03 15:42:23'),
(127, 1, 'Gula', '127-gula', NULL, '1|10|', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'Material', 'Gram', '2021-03-09 16:14:32', '2021-06-02 12:37:38'),
(128, 1, 'Susu', '128-susu', NULL, '1|10|', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'Material', 'Mililiter', '2021-03-09 16:14:44', '2021-06-02 12:37:38'),
(129, 1, 'Melon', '129-melon', NULL, '1|10|', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'Material', 'Gram', '2021-03-09 16:14:57', '2021-06-02 12:37:38'),
(130, 1, 'Kopi Arabica', '130-kopi-arabica', NULL, '1|10|', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'Material', 'Gram', '2021-03-09 16:15:11', '2021-06-02 12:37:38'),
(131, 1, 'Regal', '131-regal', NULL, '1|10|', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'Material', 'PCS', '2021-03-09 16:15:20', '2021-06-02 12:37:38'),
(132, 1, 'Jelly', '132-jelly', NULL, '1|10|', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'Material', 'Gram', '2021-03-09 16:18:17', '2021-06-02 12:37:38'),
(135, 5, 'Susu Full Cream', '135-susu-full-cream', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'Material', 'Mililiter', '2021-03-14 18:52:57', '2021-03-14 18:52:57'),
(136, 5, 'Beans', '136-beans', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'Material', 'Gram', '2021-03-14 18:59:03', '2021-03-14 18:59:03'),
(137, 5, 'Sirup Vanilla', '137-sirup-vanilla', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'Material', 'Mililiter', '2021-03-14 19:02:16', '2021-03-14 19:59:25'),
(138, 5, 'Sirup Leci', '138-sirup-leci', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'Material', 'Mililiter', '2021-03-14 19:05:03', '2021-03-14 19:59:14'),
(139, 5, 'Sirup Caramel', '139-sirup-caramel', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'Material', 'Mililiter', '2021-03-14 19:09:13', '2021-03-14 19:58:59'),
(140, 5, 'Sirup Irish', '140-sirup-irish', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'Material', 'Mililiter', '2021-03-14 19:24:18', '2021-03-14 19:58:37'),
(141, 5, 'Sirup Strawberry', '141-sirup-strawberry', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'Material', 'Mililiter', '2021-03-14 19:24:35', '2021-03-14 19:24:35'),
(142, 5, 'Krimer', '142-krimer', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'Material', 'Gram', '2021-03-14 19:24:48', '2021-03-14 19:24:48'),
(143, 5, 'Powder Cokelat', '143-powder-cokelat', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'Material', 'Gram', '2021-03-14 19:31:39', '2021-03-14 19:31:39'),
(144, 5, 'Powder Matcha', '144-powder-matcha', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'Material', 'Gram', '2021-03-14 19:51:28', '2021-03-14 19:51:28'),
(145, 5, 'SKM', '145-skm', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'Material', 'Mililiter', '2021-03-14 19:51:45', '2021-03-24 22:27:20'),
(146, 5, 'Teh', '146-teh', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'Material', 'Gram', '2021-03-14 19:52:05', '2021-03-17 16:10:34'),
(147, 5, 'Sirup Hazelnut', '147-sirup-hazelnut', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'Material', 'Miligram', '2021-03-24 22:31:25', '2021-03-24 22:31:25'),
(148, 5, 'Sirup Butterscotch', '148-sirup-butterscotch', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'Material', 'Miligram', '2021-03-24 22:52:51', '2021-03-24 22:52:51'),
(149, 5, 'Sirup Irish Cream', '149-sirup-irish-cream', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'N', NULL, 'Material', 'Miligram', '2021-03-24 22:59:51', '2021-03-27 22:29:12'),
(150, 5, 'Sirup Rose', '150-sirup-rose', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'Material', 'Miligram', '2021-03-24 23:24:07', '2021-03-24 23:24:07'),
(151, 5, 'Gula Aren', '151-gula-aren', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'Material', 'Mililiter', '2021-03-24 23:46:02', '2021-03-24 23:46:02'),
(152, 5, 'Teh Tarik', '152-teh-tarik', '#000', '', '/users/5/items/thumbs/teh_tarik_152_1.png', NULL, NULL, NULL, '/users/5/items/thumbs/teh_tarik_152_1.png', NULL, 12000, 0, '', 'Y', 2, 'Main', NULL, '2021-03-27 22:39:01', '2021-03-27 23:45:47'),
(153, 5, 'Kopi Tarik', '153-kopi-tarik', '#000', '', '/users/5/items/thumbs/kopi_tarik_153_1.png', NULL, NULL, NULL, '/users/5/items/thumbs/kopi_tarik_153_1.png', NULL, 12000, 0, '', 'Y', 1, 'Main', NULL, '2021-03-27 22:43:08', '2021-03-29 00:50:00'),
(154, 5, 'Susu Hanna', '154-susu-hanna', '#000', '', '/users/5/items/thumbs/susu_hanna_154_1.png', NULL, NULL, NULL, '/users/5/items/thumbs/susu_hanna_154_1.png', NULL, 12000, 0, '', 'Y', NULL, 'Main', NULL, '2021-03-27 22:44:57', '2021-03-27 22:44:57'),
(155, 5, 'Susu Vanessa', '155-susu-vanessa', '#000', '', '/users/5/items/thumbs/susu_vanessa_155_1.png', NULL, NULL, NULL, '/users/5/items/thumbs/susu_vanessa_155_1.png', NULL, 12000, 0, '', 'Y', 1, 'Main', NULL, '2021-03-27 22:45:36', '2021-04-02 20:07:58'),
(156, 5, 'Susu Ariana', '156-susu-ariana', '#000', '', '/users/5/items/thumbs/susu_ariana_156_1.png', NULL, NULL, NULL, '/users/5/items/thumbs/susu_ariana_156_1.png', NULL, 12000, 0, '', 'Y', 2, 'Main', NULL, '2021-03-27 22:46:27', '2021-04-02 20:07:58'),
(157, 5, 'Susu Carmila', '157-susu-carmila', '#000', '', '/users/5/items/thumbs/susu_carmila_157_1.png', NULL, NULL, NULL, '/users/5/items/thumbs/susu_carmila_157_1.png', NULL, 12000, 0, '', 'Y', 1, 'Main', NULL, '2021-03-27 22:47:04', '2021-04-02 20:07:58'),
(158, 5, 'Leci Tea', '158-leci-tea', '#000', '', '/users/5/items/thumbs/leci_tea_158_1.png', NULL, NULL, NULL, '/users/5/items/thumbs/leci_tea_158_1.png', NULL, 16000, 0, '', 'Y', NULL, 'Main', NULL, '2021-03-27 22:47:46', '2021-03-27 22:47:46'),
(159, 5, 'Roseburry', '159-roseburry', '#000', '', '/users/5/items/thumbs/roseburry_159_1.png', NULL, NULL, NULL, '/users/5/items/thumbs/roseburry_159_1.png', NULL, 22000, 0, '', 'Y', NULL, 'Main', NULL, '2021-03-27 22:49:05', '2021-03-27 22:49:05'),
(160, 5, 'Chocoansel', '160-chocoansel', '#000', '', '/users/5/items/thumbs/chocoansel_160_1.png', NULL, NULL, NULL, '/users/5/items/thumbs/chocoansel_160_1.png', NULL, 22000, 0, '', 'Y', NULL, 'Main', NULL, '2021-03-27 22:49:48', '2021-03-27 22:49:49'),
(161, 5, 'Matcha', '161-matcha', '#000', '', '/users/5/items/thumbs/matcha_161_1.png', NULL, NULL, NULL, '/users/5/items/thumbs/matcha_161_1.png', NULL, 20000, 0, '', 'Y', 1, 'Main', NULL, '2021-03-27 22:51:10', '2021-03-27 23:39:26'),
(162, 5, 'Cioccolato', '162-cioccolato', '#000', '', '/users/5/items/thumbs/cioccolato_162_1.png', NULL, NULL, NULL, '/users/5/items/thumbs/cioccolato_162_1.png', NULL, 18000, 0, '', 'Y', 3, 'Main', NULL, '2021-03-27 22:52:11', '2021-03-28 23:39:44'),
(163, 5, 'Peach Tea', '163-peach-tea', '#000', '', '/users/5/items/thumbs/peach_tea_163_1.png', NULL, NULL, NULL, '/users/5/items/thumbs/peach_tea_163_1.png', NULL, 16000, 0, '', 'Y', 1, 'Main', NULL, '2021-03-27 22:52:42', '2021-04-02 20:07:58'),
(164, 5, 'Lotus Latte', '164-lotus-latte', '#000', '', '/users/5/items/thumbs/lotus_latte_164_1.jpg', NULL, NULL, NULL, '/users/5/items/thumbs/lotus_latte_164_1.jpg', NULL, 22000, 0, '', 'Y', 1, 'Main', NULL, '2021-03-27 23:33:44', '2021-03-27 23:39:26'),
(165, 5, 'Lotus Milk', '165-lotus-milk', '#000', '', '/users/5/items/thumbs/lotus_milk_165_1.jpg', NULL, NULL, NULL, '/users/5/items/thumbs/lotus_milk_165_1.jpg', NULL, 20000, 0, '', 'Y', 1, 'Main', NULL, '2021-03-27 23:34:28', '2021-03-27 23:39:26'),
(166, 5, 'Berrypresso', '166-berrypresso', '#000', '', '/users/5/items/thumbs/berrypresso_166_1.png', NULL, NULL, NULL, '/users/5/items/thumbs/berrypresso_166_1.png', NULL, 20000, 0, '', 'Y', 2, 'Main', NULL, '2021-03-27 23:41:42', '2021-03-27 23:45:47'),
(167, 14, 'Manual Brew (V60)', '167-manual-brew-v60', '#000', '', '/users/14/items/thumbs/manual_brew_v60_167_1.jpg', NULL, NULL, NULL, '/users/14/items/thumbs/manual_brew_v60_167_1.jpg', NULL, 20000, 0, '', 'Y', NULL, 'Main', NULL, '2021-04-08 15:26:35', '2021-04-08 15:26:35'),
(168, 14, 'SS Kopi Susu', '168-ss-kopi-susu', '#000', '', '/users/14/items/thumbs/ss_kopi_susu_168_1.jpg', NULL, NULL, NULL, '/users/14/items/thumbs/ss_kopi_susu_168_1.jpg', NULL, 20000, 0, '', 'Y', NULL, 'Main', NULL, '2021-04-08 15:27:54', '2021-04-08 15:27:54'),
(169, 14, 'Americano/Long Black', '169-americanolong-black', '#000', '', '/users/14/items/thumbs/americanolong_black_169_1.jpg', NULL, NULL, NULL, '/users/14/items/thumbs/americanolong_black_169_1.jpg', NULL, 20000, 0, '', 'Y', NULL, 'Main', NULL, '2021-04-08 15:28:36', '2021-04-08 15:28:36');

-- --------------------------------------------------------

--
-- Table structure for table `items_detail`
--

CREATE TABLE `items_detail` (
  `id` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `item_id` int(11) DEFAULT NULL,
  `addon` int(11) DEFAULT NULL,
  `check_type` enum('Single','Multiple') DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `items_detail`
--

INSERT INTO `items_detail` (`id`, `user_id`, `category_id`, `item_id`, `addon`, `check_type`, `created_at`, `updated_at`) VALUES
(1, 1, 7, 47, 75, 'Single', '2021-02-15 05:12:02', '2021-02-15 05:12:02'),
(2, 1, 7, 47, 74, 'Single', '2021-02-15 05:12:02', '2021-02-15 05:12:02'),
(3, 1, 8, 47, 77, 'Multiple', '2021-02-15 05:12:16', '2021-02-15 05:12:16'),
(4, 1, 8, 47, 76, 'Multiple', '2021-02-15 05:12:16', '2021-02-15 05:12:16'),
(7, 7, 9, 87, 97, 'Single', '2021-03-03 11:13:11', '2021-03-03 11:13:11'),
(8, 7, 9, 87, 96, 'Single', '2021-03-03 11:13:11', '2021-03-03 11:13:11'),
(9, 7, 9, 86, 97, 'Single', '2021-03-03 11:13:20', '2021-03-03 11:13:20'),
(10, 7, 9, 86, 96, 'Single', '2021-03-03 11:13:20', '2021-03-03 11:13:20'),
(11, 7, 9, 85, 97, 'Single', '2021-03-03 11:13:30', '2021-03-03 11:13:30'),
(12, 7, 9, 85, 96, 'Single', '2021-03-03 11:13:30', '2021-03-03 11:13:30'),
(13, 7, 9, 84, 97, 'Single', '2021-03-03 11:13:40', '2021-03-03 11:13:40'),
(14, 7, 9, 84, 96, 'Single', '2021-03-03 11:13:40', '2021-03-03 11:13:40'),
(17, 7, 9, 82, 97, 'Single', '2021-03-03 11:14:01', '2021-03-03 11:14:01'),
(18, 7, 9, 82, 96, 'Single', '2021-03-03 11:14:01', '2021-03-03 11:14:01'),
(19, 7, 9, 81, 97, 'Single', '2021-03-03 11:14:10', '2021-03-03 11:14:10'),
(20, 7, 9, 81, 96, 'Single', '2021-03-03 11:14:10', '2021-03-03 11:14:10'),
(25, 7, 9, 78, 97, 'Single', '2021-03-03 11:14:43', '2021-03-03 11:14:43'),
(26, 7, 9, 78, 96, 'Single', '2021-03-03 11:14:43', '2021-03-03 11:14:43'),
(27, 7, 10, 68, 100, 'Single', '2021-03-03 15:26:15', '2021-03-03 15:26:15'),
(28, 7, 10, 68, 99, 'Single', '2021-03-03 15:26:15', '2021-03-03 15:26:15'),
(29, 7, 10, 68, 98, 'Single', '2021-03-03 15:26:15', '2021-03-03 15:26:15'),
(30, 7, 10, 83, 100, 'Single', '2021-03-03 15:27:04', '2021-03-03 15:27:04'),
(31, 7, 10, 83, 99, 'Single', '2021-03-03 15:27:04', '2021-03-03 15:27:04'),
(32, 7, 10, 83, 98, 'Single', '2021-03-03 15:27:04', '2021-03-03 15:27:04'),
(33, 7, 10, 79, 100, 'Single', '2021-03-03 15:29:27', '2021-03-03 15:29:27'),
(34, 7, 10, 79, 99, 'Single', '2021-03-03 15:29:27', '2021-03-03 15:29:27'),
(35, 7, 10, 79, 98, 'Single', '2021-03-03 15:29:27', '2021-03-03 15:29:27'),
(36, 7, 9, 111, 97, 'Single', '2021-03-03 15:31:18', '2021-03-03 15:31:18'),
(37, 7, 9, 111, 96, 'Single', '2021-03-03 15:31:18', '2021-03-03 15:31:18'),
(38, 7, 9, 110, 97, 'Single', '2021-03-03 15:31:40', '2021-03-03 15:31:40'),
(39, 7, 9, 110, 96, 'Single', '2021-03-03 15:31:40', '2021-03-03 15:31:40'),
(40, 7, 9, 109, 97, 'Single', '2021-03-03 15:32:06', '2021-03-03 15:32:06'),
(41, 7, 9, 109, 96, 'Single', '2021-03-03 15:32:06', '2021-03-03 15:32:06');

-- --------------------------------------------------------

--
-- Table structure for table `items_stock`
--

CREATE TABLE `items_stock` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `catalog` int(10) DEFAULT NULL,
  `item_id` int(11) DEFAULT NULL,
  `stock` decimal(11,1) DEFAULT NULL,
  `notes` text DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `items_stock`
--

INSERT INTO `items_stock` (`id`, `user_id`, `catalog`, `item_id`, `stock`, `notes`, `created_at`, `updated_at`) VALUES
(1, 1, 0, 132, '900.0', NULL, '2021-03-20 01:42:16', '2021-03-20 01:42:16'),
(2, 1, 0, 132, '200.0', NULL, '2021-03-20 01:42:24', '2021-03-20 01:42:24'),
(3, 1, 0, 131, '500.0', NULL, '2021-03-20 01:42:37', '2021-03-20 01:42:37'),
(4, 1, 0, 130, '5000.0', NULL, '2021-03-20 01:42:49', '2021-03-20 01:42:49'),
(5, 1, 0, 129, '4500.0', NULL, '2021-03-20 01:43:03', '2021-03-20 01:43:03'),
(6, 1, 0, 128, '600.0', NULL, '2021-03-20 01:43:15', '2021-03-20 01:43:15'),
(7, 1, 0, 127, '5000.0', NULL, '2021-03-20 01:43:25', '2021-03-20 01:43:25'),
(8, 1, 1, 132, '300.0', NULL, '2021-03-20 01:44:00', '2021-03-20 01:44:00'),
(9, 1, 1, 131, '100.0', NULL, '2021-03-20 01:44:14', '2021-03-20 01:44:14'),
(10, 1, 1, 130, '2000.0', NULL, '2021-03-20 01:44:27', '2021-03-20 01:44:27'),
(11, 1, 1, 129, '2000.0', NULL, '2021-03-20 01:45:23', '2021-03-20 01:45:23'),
(12, 1, 1, 128, '250.0', NULL, '2021-03-20 01:45:41', '2021-03-20 01:45:41'),
(13, 1, 1, 127, '1200.0', NULL, '2021-03-20 01:45:51', '2021-03-20 01:45:51'),
(30, 5, 0, 149, '100.0', NULL, '2021-03-24 23:00:03', '2021-03-24 23:00:03'),
(32, 5, 0, 146, '225.0', NULL, '2021-03-27 21:44:11', '2021-03-27 21:44:11'),
(33, 5, 0, 145, '1000.0', NULL, '2021-03-27 21:49:56', '2021-03-27 21:49:56'),
(34, 5, 0, 144, '253.0', NULL, '2021-03-27 22:17:52', '2021-03-27 22:17:52'),
(35, 5, 0, 143, '210.0', NULL, '2021-03-27 22:18:29', '2021-03-27 22:18:29'),
(36, 5, 0, 142, '180.0', NULL, '2021-03-27 22:20:15', '2021-03-27 22:20:15'),
(37, 5, 0, 141, '123.0', NULL, '2021-03-27 22:21:23', '2021-03-27 22:21:23'),
(38, 5, 0, 137, '550.0', NULL, '2021-03-27 22:22:26', '2021-03-27 22:22:26'),
(39, 5, 0, 139, '340.0', NULL, '2021-03-27 22:23:20', '2021-03-27 22:23:20'),
(40, 5, 0, 140, '315.0', NULL, '2021-03-27 22:23:53', '2021-03-27 22:23:53'),
(41, 5, 0, 138, '550.0', NULL, '2021-03-27 22:24:22', '2021-03-27 22:24:22'),
(42, 5, 0, 135, '6200.0', NULL, '2021-03-27 22:24:44', '2021-03-27 22:24:44'),
(43, 5, 0, 136, '1000.0', NULL, '2021-03-27 22:26:02', '2021-03-27 22:26:02'),
(44, 5, 0, 151, '10.0', NULL, '2021-03-27 22:53:32', '2021-03-27 22:53:32'),
(45, 5, 0, 150, '10.0', NULL, '2021-03-27 22:53:48', '2021-03-27 22:53:48'),
(46, 5, 0, 148, '10.0', NULL, '2021-03-27 22:54:22', '2021-03-27 22:54:22'),
(47, 5, 0, 147, '10.0', NULL, '2021-03-27 22:54:45', '2021-03-27 22:54:45');

-- --------------------------------------------------------

--
-- Table structure for table `mainfeatures`
--

CREATE TABLE `mainfeatures` (
  `id` tinyint(2) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `feature_name` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `mainfeatures`
--

INSERT INTO `mainfeatures` (`id`, `user_id`, `feature_name`, `created_at`, `updated_at`) VALUES
(1, 1, 'Menu Digital', '2021-02-28 09:49:13', '2021-02-28 09:49:13'),
(2, 1, 'Image Slider', '2021-02-28 09:49:13', '2021-02-28 09:49:13'),
(3, 1, 'Video Product', '2021-02-28 09:49:13', '2021-02-28 09:49:13'),
(4, 1, 'GeoLocation', '2021-02-28 09:49:13', '2021-02-28 09:49:13'),
(5, 1, 'Waiter Bell', '2021-02-28 09:49:13', '2021-02-28 09:49:13'),
(6, 1, 'Customer Color Theme', '2021-02-28 09:49:13', '2021-02-28 09:49:13'),
(7, 1, 'Unlimited Product', '2021-02-28 09:49:13', '2021-02-28 09:49:13'),
(8, 1, '5 Account Member', '2021-02-28 09:49:13', '2021-02-28 09:49:13'),
(9, 1, 'Unlimited Add - On Product', '2021-02-28 09:49:13', '2021-02-28 09:49:13'),
(10, 1, 'Aplikasi POS', '2021-02-28 09:49:13', '2021-02-28 09:49:13'),
(11, 1, 'Order Process', '2021-02-28 09:49:13', '2021-02-28 09:49:13'),
(12, 1, 'Order Transaction', '2021-02-28 09:49:13', '2021-02-28 09:49:13'),
(13, 1, 'Payment Gateway', '2021-02-28 09:49:13', '2021-02-28 09:49:13'),
(14, 1, 'Transaction Report', '2021-02-28 09:49:13', '2021-02-28 09:49:13');

-- --------------------------------------------------------

--
-- Table structure for table `member_statuses`
--

CREATE TABLE `member_statuses` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `paket_id` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `expdate` date NOT NULL,
  `status` set('Trial','Pending','Paid','Expire','Banned') COLLATE utf8mb4_unicode_ci NOT NULL,
  `payment` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `member_statuses`
--

INSERT INTO `member_statuses` (`id`, `username`, `paket_id`, `expdate`, `status`, `payment`, `created_at`, `updated_at`) VALUES
(1, '1', '4', '2022-05-04', 'Paid', NULL, '2021-05-04 08:19:59', '2021-05-04 08:20:51'),
(2, '3', '3', '2021-08-02', 'Paid', NULL, '2021-05-04 08:19:59', '2021-05-04 08:20:51'),
(3, '5', '1', '2021-05-18', 'Trial', NULL, '2021-05-04 08:19:59', '2021-05-04 08:20:51'),
(4, '7', '1', '2021-05-18', 'Trial', NULL, '2021-05-04 08:19:59', '2021-05-04 08:20:51'),
(5, '8', '3', '2021-08-02', 'Paid', NULL, '2021-05-04 08:19:59', '2021-05-04 08:20:51'),
(6, '10', '1', '2021-05-18', 'Trial', NULL, '2021-05-04 08:19:59', '2021-05-04 08:20:51'),
(7, '11', '1', '2021-05-18', 'Trial', NULL, '2021-05-04 08:19:59', '2021-05-04 08:20:51'),
(8, '12', '3', '2021-05-04', 'Pending', NULL, '2021-05-04 08:19:59', '2021-05-04 08:20:51'),
(9, '13', '1', '2021-05-18', 'Trial', NULL, '2021-05-04 08:19:59', '2021-05-04 08:20:51'),
(10, '14', '1', '2021-05-18', 'Trial', NULL, '2021-05-04 08:19:59', '2021-05-04 08:20:51'),
(21, '14', '5', '2021-05-12', 'Pending', NULL, '2021-05-09 20:02:41', NULL),
(26, '14', '3', '2021-05-13', 'Pending', NULL, '2021-05-11 04:41:21', NULL),
(27, '14', '4', '2021-05-14', 'Pending', NULL, '2021-05-11 22:56:13', NULL),
(28, '1', '4', '2021-05-21', 'Pending', NULL, '2021-05-18 21:29:48', NULL),
(29, '1', '3', '2021-05-21', 'Pending', NULL, '2021-05-18 21:42:22', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(2, '2021_05_03_145615_create_member_statuses_table', 1),
(7, '2021_05_20_100803_password_resets', 2),
(8, '2021_05_28_170107_create_price_lists_table', 3),
(9, '2021_05_28_170126_create_price_payments_table', 3),
(10, '2021_05_28_170138_create_price_payment_cats_table', 3);

-- --------------------------------------------------------

--
-- Table structure for table `package`
--

CREATE TABLE `package` (
  `id` tinyint(2) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `package_name` varchar(100) DEFAULT NULL,
  `package_slug` varchar(100) DEFAULT NULL,
  `recommended` enum('Y','N') DEFAULT 'N',
  `description` text DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `package`
--

INSERT INTO `package` (`id`, `user_id`, `package_name`, `package_slug`, `recommended`, `description`, `created_at`, `updated_at`) VALUES
(1, 2, 'Free Trial', 'free-trial', 'N', 'PROMO! Free 1 Bulan untuk 1000 Restoran pertama.Klik \'Pesan Sekarang\'', '2020-11-25 08:07:00', '2021-03-02 00:54:50'),
(2, 2, 'Premium', 'premium', 'Y', 'PROMO! Rp.499.000 untuk 100 Restoran pertama.Klik \'Pesan Sekarang\'', '2020-11-25 08:07:26', '2021-04-05 06:38:22'),
(3, 2, 'Standard', 'standard', 'N', NULL, '2020-11-25 08:07:41', '2021-01-24 02:03:53');

-- --------------------------------------------------------

--
-- Table structure for table `package_price`
--

CREATE TABLE `package_price` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `package_id` tinyint(2) DEFAULT NULL,
  `price` double DEFAULT NULL,
  `period` tinyint(2) DEFAULT NULL,
  `unit` varchar(10) DEFAULT NULL,
  `notes` text DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `package_price`
--

INSERT INTO `package_price` (`id`, `user_id`, `package_id`, `price`, `period`, `unit`, `notes`, `created_at`, `updated_at`) VALUES
(1, 2, 1, 0, 14, 'Hari', NULL, '2021-02-24 01:18:01', '2021-02-24 01:18:01'),
(2, 2, 1, 0, 1, 'Bulan', '( 1000 Restoran/Cafe Pertama )', '2021-02-24 01:18:01', '2021-02-24 01:18:01'),
(3, 2, 2, 990000, 3, 'Bulan', NULL, '2021-02-24 01:18:01', '2021-02-24 01:18:01'),
(4, 2, 2, 2990000, 1, 'Tahun', '( Hemat 970 Ribu )', '2021-02-24 01:18:01', '2021-02-24 01:18:01'),
(5, 2, 3, 390000, 3, 'Bulan', NULL, NULL, NULL),
(6, 2, 3, 990000, 1, 'Tahun', NULL, NULL, NULL),
(7, 2, 2, 499000, 3, 'Bulan', '( Untuk 500 Restoran pertama )', '2021-03-20 07:08:38', '2021-03-20 07:08:38');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `price_lists`
--

CREATE TABLE `price_lists` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_item` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_payment` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` double NOT NULL,
  `diskon` double NOT NULL DEFAULT 0,
  `status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `price_lists`
--

INSERT INTO `price_lists` (`id`, `id_item`, `id_payment`, `price`, `diskon`, `status`, `created_at`, `updated_at`) VALUES
(1, '53', '2', 15000, 12000, 'Active', '2021-06-01 00:41:55', '2021-06-01 15:30:10'),
(2, '53', '1', 15000, 0, 'Active', '2021-06-01 00:41:55', '2021-06-01 00:41:55'),
(3, '53', '3', 15000, 14500, 'Active', '2021-06-01 00:41:55', '2021-06-01 00:41:55'),
(6, '53', '4', 15000, 14500, 'Active', '2021-06-01 15:39:03', '2021-06-01 15:39:49'),
(7, '52', '4', 17000, 0, 'Active', '2021-06-01 16:49:20', '2021-06-01 16:49:51'),
(8, '169', '4', 17000, 0, 'Active', '2021-06-01 22:28:18', '2021-06-01 22:28:18'),
(9, '169', '1', 500, 0, 'Active', '2021-06-01 22:44:40', '2021-06-01 23:19:42'),
(10, '169', '2', 17000, 0, 'Active', '2021-06-01 22:47:44', '2021-06-01 23:08:27'),
(11, '169', '3', 14000, 0, 'Active', '2021-06-01 22:49:18', '2021-06-01 23:08:42'),
(12, '171', '4', 17000, 15000, 'Active', '2021-06-03 12:56:21', '2021-06-03 12:57:34'),
(13, '46', '4', 12000, 0, 'Active', '2021-06-03 21:34:01', '2021-06-03 21:34:01'),
(14, '46', '2', 12000, 0, 'Active', '2021-06-03 21:34:13', '2021-06-03 21:34:13');

-- --------------------------------------------------------

--
-- Table structure for table `price_payments`
--

CREATE TABLE `price_payments` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `jenis` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `methode` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_payment_cat` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Not Active',
  `ket` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `price_payments`
--

INSERT INTO `price_payments` (`id`, `jenis`, `methode`, `id_payment_cat`, `status`, `ket`, `created_at`, `updated_at`) VALUES
(1, 'GoPay', '', '2', 'Active', 'Pembayaran menggunakan GoPay Karya Anak Bangsa', '2021-05-28 22:58:51', '2021-05-29 10:51:21'),
(2, 'OvO', '', '2', 'Active', 'Pembayaran menggunakan Ovo Mania', '2021-05-28 23:34:03', '2021-05-29 00:45:18'),
(3, 'Kartu Kredit BCA', '', '1', 'Active', 'Pembayaran menggunakan Kartu Kredit BCA', '2021-05-29 00:43:30', '2021-05-30 20:26:38'),
(4, 'Cash', '', '3', 'Active', 'Pembayaran Cash di Kasir', '2021-05-30 20:28:30', '2021-05-30 20:28:38');

-- --------------------------------------------------------

--
-- Table structure for table `price_payment_cats`
--

CREATE TABLE `price_payment_cats` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nama` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Not Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `price_payment_cats`
--

INSERT INTO `price_payment_cats` (`id`, `nama`, `description`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Credit Card', 'Pembayaran menggunakan kartu kredit Visa Master Card', 'Active', '2021-05-28 20:50:12', '2021-05-28 22:35:17'),
(2, 'Digital Wallet', 'Pembayaran menggunakan digital wallet indonesia', 'Active', '2021-05-28 20:50:12', '2021-05-28 23:35:42'),
(3, 'Cash Payment', 'Pembayaran langsung di petugas Kasir', 'Active', '2021-05-28 20:50:12', '2021-05-28 21:29:30'),
(4, 'Bank Transfer', 'Pembayaran melalui transfer antar bank', 'Active', '2021-05-28 20:51:53', '2021-05-28 22:39:19'),
(5, 'Debit Card', 'Pembayaran menggunakan kartu debit bank lokal', 'Active', '2021-05-28 21:26:48', '2021-05-30 19:53:35'),
(6, 'Ojek Online', 'Transaksi via ojek online', 'Not Active', '2021-05-29 10:38:00', '2021-05-29 10:48:22');

-- --------------------------------------------------------

--
-- Table structure for table `recipe`
--

CREATE TABLE `recipe` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `item_id` int(11) DEFAULT NULL,
  `serving_size` int(5) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `recipe`
--

INSERT INTO `recipe` (`id`, `user_id`, `parent_id`, `item_id`, `serving_size`, `created_at`, `updated_at`) VALUES
(1, 1, 45, 129, 70, '2021-03-20 01:57:04', '2021-03-20 01:57:04'),
(2, 1, 45, 127, 45, '2021-03-20 01:57:15', '2021-03-20 01:57:15'),
(3, 1, 45, 128, 50, '2021-03-20 01:57:30', '2021-03-20 01:57:30'),
(4, 1, 47, 130, 20, '2021-03-20 01:59:02', '2021-03-20 01:59:02'),
(5, 1, 47, 128, 70, '2021-03-20 01:59:23', '2021-03-20 01:59:23'),
(6, 1, 77, 132, 40, '2021-03-20 02:21:32', '2021-03-20 02:21:32'),
(7, 1, 76, 131, 3, '2021-03-20 02:21:48', '2021-03-20 02:21:48'),
(8, 5, 66, 136, 9, '2021-03-24 22:24:52', '2021-03-24 22:24:52'),
(9, 5, 66, 135, 120, '2021-03-24 22:25:12', '2021-03-24 22:43:06'),
(10, 5, 64, 135, 120, '2021-03-24 22:26:36', '2021-03-24 22:47:00'),
(11, 5, 64, 136, 9, '2021-03-24 22:26:49', '2021-03-24 22:26:49'),
(12, 5, 64, 145, 25, '2021-03-24 22:27:04', '2021-03-24 22:27:04'),
(13, 5, 64, 142, 15, '2021-03-24 22:27:57', '2021-03-24 22:27:57'),
(14, 5, 63, 136, 9, '2021-03-24 22:28:47', '2021-03-24 22:28:47'),
(15, 5, 61, 136, 9, '2021-03-24 22:29:38', '2021-03-24 22:29:38'),
(16, 5, 61, 135, 120, '2021-03-24 22:29:52', '2021-03-24 22:48:00'),
(17, 5, 61, 147, 28, '2021-03-24 22:33:29', '2021-03-24 22:33:29'),
(18, 5, 60, 136, 9, '2021-03-24 22:34:11', '2021-03-24 22:34:11'),
(19, 5, 60, 139, 28, '2021-03-24 22:34:32', '2021-03-24 22:34:32'),
(20, 5, 60, 135, 120, '2021-03-24 22:34:43', '2021-03-24 22:48:25'),
(21, 5, 59, 136, 9, '2021-03-24 22:35:21', '2021-03-24 22:35:21'),
(22, 5, 59, 137, 28, '2021-03-24 22:35:47', '2021-03-24 22:35:47'),
(23, 5, 59, 135, 120, '2021-03-24 22:36:06', '2021-03-24 22:48:54'),
(24, 5, 58, 136, 9, '2021-03-24 22:36:52', '2021-03-24 22:36:52'),
(25, 5, 58, 141, 20, '2021-03-24 22:41:25', '2021-03-24 22:41:25'),
(26, 5, 58, 135, 120, '2021-03-24 22:41:45', '2021-03-24 22:41:45'),
(27, 5, 57, 135, 120, '2021-03-24 22:50:34', '2021-03-24 22:50:34'),
(28, 5, 57, 148, 30, '2021-03-24 22:53:24', '2021-03-24 22:53:24'),
(29, 5, 57, 136, 9, '2021-03-24 22:55:15', '2021-03-24 22:55:15'),
(30, 5, 56, 136, 9, '2021-03-24 22:59:08', '2021-03-24 22:59:08'),
(31, 5, 56, 135, 120, '2021-03-24 22:59:19', '2021-03-24 22:59:19'),
(32, 5, 56, 149, 30, '2021-03-24 23:19:51', '2021-03-24 23:19:51'),
(33, 5, 55, 135, 120, '2021-03-24 23:21:04', '2021-03-24 23:21:04'),
(34, 5, 55, 136, 9, '2021-03-24 23:21:25', '2021-03-24 23:21:25'),
(35, 5, 55, 150, 20, '2021-03-24 23:38:50', '2021-03-24 23:39:04'),
(36, 5, 55, 145, 10, '2021-03-24 23:40:47', '2021-03-24 23:40:47'),
(37, 5, 58, 145, 10, '2021-03-24 23:41:26', '2021-03-24 23:41:26'),
(38, 5, 54, 135, 120, '2021-03-24 23:44:13', '2021-03-24 23:44:13'),
(39, 5, 54, 136, 9, '2021-03-24 23:44:43', '2021-03-24 23:44:43'),
(40, 5, 54, 142, 15, '2021-03-24 23:45:23', '2021-03-24 23:45:23'),
(41, 5, 56, 140, 30, '2021-03-27 22:31:23', '2021-03-27 22:31:23'),
(42, 5, 152, 146, 3, '2021-03-27 22:40:46', '2021-03-27 22:40:46'),
(43, 5, 152, 145, 40, '2021-03-27 22:42:22', '2021-03-27 22:42:22'),
(44, 5, 153, 136, 9, '2021-03-27 22:43:31', '2021-03-27 22:43:31'),
(45, 5, 153, 145, 35, '2021-03-27 22:43:44', '2021-03-27 22:43:44'),
(46, 5, 154, 135, 130, '2021-03-27 23:03:14', '2021-03-27 23:03:14'),
(47, 5, 154, 147, 25, '2021-03-27 23:03:29', '2021-03-27 23:03:29'),
(48, 5, 155, 135, 130, '2021-03-27 23:04:37', '2021-03-27 23:04:37'),
(49, 5, 155, 137, 25, '2021-03-27 23:04:49', '2021-03-27 23:04:49'),
(50, 5, 156, 135, 130, '2021-03-27 23:05:36', '2021-03-27 23:05:36'),
(51, 5, 156, 151, 25, '2021-03-27 23:05:50', '2021-03-27 23:05:50'),
(52, 5, 157, 135, 130, '2021-03-27 23:06:11', '2021-03-27 23:06:11'),
(53, 5, 157, 139, 25, '2021-03-27 23:06:24', '2021-03-27 23:06:24'),
(54, 5, 158, 146, 3, '2021-03-27 23:07:15', '2021-03-27 23:07:15'),
(55, 5, 158, 138, 30, '2021-03-27 23:08:27', '2021-03-27 23:08:27'),
(56, 5, 159, 143, 25, '2021-03-27 23:09:04', '2021-03-27 23:09:04'),
(57, 5, 159, 150, 20, '2021-03-27 23:09:17', '2021-03-27 23:09:17'),
(58, 5, 159, 136, 5, '2021-03-27 23:09:29', '2021-03-27 23:09:29'),
(59, 5, 159, 145, 10, '2021-03-27 23:09:41', '2021-03-27 23:09:41'),
(60, 5, 159, 135, 100, '2021-03-27 23:09:53', '2021-03-27 23:09:53'),
(61, 5, 160, 147, 20, '2021-03-27 23:10:22', '2021-03-27 23:10:51'),
(62, 5, 160, 143, 25, '2021-03-27 23:10:37', '2021-03-27 23:10:37'),
(63, 5, 160, 145, 10, '2021-03-27 23:11:06', '2021-03-27 23:11:06'),
(64, 5, 160, 135, 100, '2021-03-27 23:11:19', '2021-03-27 23:11:19'),
(65, 5, 161, 144, 25, '2021-03-27 23:11:52', '2021-03-27 23:12:19'),
(66, 5, 161, 135, 100, '2021-03-27 23:12:40', '2021-03-27 23:12:40'),
(67, 5, 161, 145, 25, '2021-03-27 23:13:04', '2021-03-27 23:13:16'),
(68, 5, 161, 142, 10, '2021-03-27 23:13:27', '2021-03-27 23:13:27'),
(69, 5, 162, 135, 100, '2021-03-27 23:14:08', '2021-03-27 23:14:08'),
(70, 5, 162, 143, 30, '2021-03-27 23:14:20', '2021-03-27 23:14:20'),
(71, 5, 162, 145, 30, '2021-03-27 23:14:35', '2021-03-27 23:14:35'),
(72, 5, 163, 141, 20, '2021-03-27 23:15:01', '2021-03-27 23:16:05'),
(73, 5, 163, 146, 3, '2021-03-27 23:15:46', '2021-03-27 23:15:46'),
(74, 5, 165, 147, 10, '2021-03-27 23:36:35', '2021-03-27 23:36:35'),
(75, 5, 165, 139, 10, '2021-03-27 23:36:49', '2021-03-27 23:36:49'),
(76, 5, 165, 135, 130, '2021-03-27 23:37:10', '2021-03-27 23:37:10'),
(77, 5, 165, 145, 15, '2021-03-27 23:37:38', '2021-03-27 23:37:38'),
(78, 5, 164, 135, 130, '2021-03-27 23:37:56', '2021-03-27 23:37:56'),
(79, 5, 164, 145, 15, '2021-03-27 23:38:05', '2021-03-27 23:38:05'),
(80, 5, 164, 139, 10, '2021-03-27 23:38:20', '2021-03-27 23:38:20'),
(81, 5, 164, 147, 10, '2021-03-27 23:38:29', '2021-03-27 23:38:29'),
(82, 5, 164, 136, 9, '2021-03-27 23:38:38', '2021-03-27 23:38:38'),
(83, 5, 166, 136, 9, '2021-03-27 23:42:22', '2021-03-27 23:42:22'),
(84, 5, 166, 141, 30, '2021-03-27 23:42:39', '2021-03-27 23:42:39');

-- --------------------------------------------------------

--
-- Table structure for table `register`
--

CREATE TABLE `register` (
  `id` int(11) UNSIGNED NOT NULL,
  `invoice` varchar(12) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `package_id` tinyint(2) DEFAULT NULL,
  `package_name` varchar(50) DEFAULT NULL,
  `price` double DEFAULT NULL,
  `voucher_code` varchar(20) DEFAULT NULL,
  `discount` double DEFAULT NULL,
  `confirmation` enum('Y','N') DEFAULT NULL,
  `confirmation_slip` varchar(255) DEFAULT NULL,
  `account_to` text DEFAULT NULL,
  `status` varchar(20) DEFAULT NULL,
  `affiliate` varchar(255) DEFAULT NULL,
  `duration` varchar(50) DEFAULT NULL,
  `expired` date DEFAULT NULL,
  `notes` text DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `register`
--

INSERT INTO `register` (`id`, `invoice`, `user_id`, `package_id`, `package_name`, `price`, `voucher_code`, `discount`, `confirmation`, `confirmation_slip`, `account_to`, `status`, `affiliate`, `duration`, `expired`, `notes`, `created_at`, `updated_at`) VALUES
(1, '', 1, 3, 'Advance', 50000, NULL, NULL, 'Y', NULL, NULL, 'Approved', 'None', NULL, '2021-05-25', NULL, '2020-11-25 09:19:12', '2020-11-25 09:19:12'),
(2, '', 3, 3, 'Advance', 150000, NULL, NULL, 'Y', NULL, NULL, 'Approved', 'None', NULL, '2021-11-25', NULL, '2020-11-25 10:37:19', '2020-11-25 10:37:19'),
(3, '', 5, 3, 'Advance', 150000, NULL, NULL, 'Y', NULL, NULL, 'Approved', 'None', NULL, '2021-12-11', NULL, '2020-12-11 18:22:04', '2021-01-25 07:27:02'),
(4, '', 7, 3, 'Advance', 150000, NULL, NULL, 'Y', NULL, NULL, 'Approved', 'None', NULL, '2022-01-03', NULL, '2021-01-03 15:01:47', '2021-01-25 07:26:51'),
(5, 'INV-00000005', 8, 2, 'Premium Member', 990000, NULL, NULL, 'N', NULL, NULL, 'Order', NULL, '3 Bulan', NULL, NULL, '2021-02-25 14:22:47', '2021-02-25 14:22:47'),
(6, 'REG-00000006', 10, 2, 'Premium', 2990000, NULL, NULL, 'Y', '/assets/img/confirmation/reg_00000006.png', 'Bank BCA<br>Nomor Rekening : 6760438721<br>Atas Nama : Heri Sutejo<br>', 'Approved', NULL, '1 Tahun', '2021-04-20', NULL, '2021-03-14 19:39:21', '2021-03-20 09:14:04'),
(7, 'REG-00000007', 11, 1, 'Free Trial', 0, NULL, NULL, 'Y', NULL, NULL, 'Approved', NULL, '1 Bulan', '2021-04-23', NULL, '2021-03-23 18:39:50', '2021-03-23 18:39:50'),
(8, 'REG-00000008', 12, 1, 'Free Trial', 0, NULL, NULL, 'Y', NULL, NULL, 'Approved', NULL, '1 Bulan', '2021-05-05', NULL, '2021-04-05 12:02:56', '2021-04-05 12:02:56'),
(9, 'REG-00000009', 13, 2, 'Premium', 2990000, NULL, NULL, 'N', NULL, NULL, 'Checkout', NULL, '1 Tahun', NULL, NULL, '2021-04-08 13:23:21', '2021-04-08 13:23:21'),
(10, 'REG-00000010', 14, 2, 'Premium', 2990000, NULL, NULL, 'Y', '/assets/img/confirmation/reg_00000010.png', 'Bank Mandiri<br>Nomor Rekening : 1310013854643<br>Atas Nama : Muhammad Adriansa<br>', 'Approved', NULL, '1 Tahun', '2021-05-08', NULL, '2021-04-08 14:50:31', '2021-04-08 14:54:45');

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE `services` (
  `id` int(5) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `title` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Table structure for table `sliders`
--

CREATE TABLE `sliders` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `sliders_title` varchar(255) DEFAULT NULL,
  `sliders_image` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `sliders`
--

INSERT INTO `sliders` (`id`, `user_id`, `sliders_title`, `sliders_image`, `created_at`, `updated_at`) VALUES
(1, 1, 'Colorfull Food', '/users/1/sliders/colorfull_food_1605060328.jpg', '2020-11-10 09:41:36', '2020-11-10 19:05:29'),
(2, 1, 'Delicious Meat', '/users/1/sliders/delicious_meat_1607264654.jpg', '2020-11-10 09:42:26', '2020-12-06 14:24:14'),
(3, 5, '#CobaAjaDulu', '/users/5/sliders/cobaajadulu_1607961634.jpg', '2020-12-14 23:00:34', '2020-12-14 23:00:34'),
(4, 7, 'slide 1', '/users/7/sliders/slide_1_1611565527.jpg', '2021-01-25 16:05:27', '2021-01-25 16:05:27'),
(5, 7, 'slide 2', '/users/7/sliders/slide_2_1611565542.jpg', '2021-01-25 16:05:42', '2021-01-25 16:05:42'),
(6, 7, 'Pasta', '/users/7/sliders/pasta_1614698540.jpg', '2021-03-02 22:22:20', '2021-03-02 22:22:21'),
(7, 14, 'satu', '/users/14/sliders/satu_1617869201.jpg', '2021-04-08 15:06:40', '2021-04-08 15:06:41'),
(8, 14, 'dua', '/users/14/sliders/dua_1617869235.jpg', '2021-04-08 15:07:15', '2021-04-08 15:07:15');

-- --------------------------------------------------------

--
-- Table structure for table `subcategory`
--

CREATE TABLE `subcategory` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `subcategory_name` varchar(255) DEFAULT NULL,
  `subcategory_slug` varchar(255) DEFAULT NULL,
  `show_on_catalog` varchar(50) NOT NULL,
  `subcategory_image` varchar(255) DEFAULT NULL,
  `subcategory_color` varchar(15) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `subcategory`
--

INSERT INTO `subcategory` (`id`, `user_id`, `subcategory_name`, `subcategory_slug`, `show_on_catalog`, `subcategory_image`, `subcategory_color`, `created_at`, `updated_at`) VALUES
(1, 1, 'Aneka Kudapan Manis', '1-aneka-kudapan-manis', '1|10|', '/users/1/subcategory/aneka_kudapan_manis_1605022913.jpg', '#000', '2020-11-10 08:41:53', '2021-06-02 20:41:53'),
(2, 1, 'Jajanan', '2-jajanan', '1|10|', '/users/1/subcategory/jajanan_1605022970.jpg', '#000', '2020-11-10 08:42:50', '2021-06-02 20:41:53'),
(3, 1, 'Makanan Pembuka', '3-makanan-pembuka', '1|10|', '/users/1/subcategory/makanan_pembuka_1605023064.jpg', '#000', '2020-11-10 08:44:24', '2021-06-02 20:41:53'),
(4, 1, 'Soto dan Sup', '4-soto-dan-sup', '1|10|', '/users/1/subcategory/soto_dan_sup_1605023119.jpg', '#000', '2020-11-10 08:45:19', '2021-06-02 20:41:53'),
(5, 1, 'Aneka Sate', '5-aneka-sate', '1|10|', '/users/1/subcategory/aneka_sate_1605023178.jpg', '#000', '2020-11-10 08:46:18', '2021-06-02 20:41:53'),
(7, 1, 'Aneka Sapi - Kambing', '7-aneka-sapi-kambing', '1|10|', '/users/1/subcategory/aneka_sapi_kambing_1605023299.jpg', '#000', '2020-11-10 08:48:19', '2021-06-02 20:41:53'),
(8, 1, 'Aneka Ayam', '8-aneka-ayam', '1|10|', '/users/1/subcategory/aneka_ayam_1605023447.jpg', '#000', '2020-11-10 08:50:47', '2021-06-02 20:41:53'),
(9, 1, 'Aneka Mie', '9-aneka-mie', '1|10|', '/users/1/subcategory/aneka_mie_1605023509.png', '#000', '2020-11-10 08:51:49', '2021-06-02 20:41:53'),
(10, 1, 'Aneka Nasi', '10-aneka-nasi', '1|10|', '/users/1/subcategory/aneka_nasi_1605023554.jpg', '#000', '2020-11-10 08:52:34', '2021-06-02 20:41:53'),
(11, 1, 'Aneka Sambal', '11-aneka-sambal', '1|10|', '/users/1/subcategory/aneka_sambal_1605023599.jpg', '#000', '2020-11-10 08:53:19', '2021-06-02 20:41:53'),
(12, 1, 'Aneka Es', '12-aneka-es', '1|10|', '/users/1/subcategory/aneka_es_1605023630.jpg', '#000', '2020-11-10 08:53:50', '2021-06-02 20:41:53'),
(13, 1, 'Aneka Jus', '13-aneka-jus', '1|10|', '/users/1/subcategory/aneka_jus_1605023679.jpg', '#000', '2020-11-10 08:54:39', '2021-06-02 20:41:53'),
(14, 1, 'Aneka Minuman Panas', '14-aneka-minuman-panas', '1|10|', '/users/1/subcategory/aneka_minuman_panas_1605023717.jpg', '#000', '2020-11-10 08:55:17', '2021-06-02 20:41:53'),
(15, 1, 'Aneka Minuman Dingin', '15-aneka-minuman-dingin', '1|', '/users/1/subcategory/aneka_minuman_dingin_1622667699.jpg', '#000', '2020-11-10 08:56:00', '2021-06-02 21:01:39'),
(16, 5, 'Coffee', '16-coffee', '', '/users/5/subcategory/coffee_1607961559.jpg', '#000', '2020-12-14 22:59:19', '2020-12-14 22:59:19'),
(17, 5, 'non-coffee', '17-non-coffee', '', '/users/5/subcategory/non_coffee_1608144054.jpg', '#000', '2020-12-17 01:40:54', '2020-12-17 01:40:54'),
(18, 7, 'Coffee', '18-coffee', '', '/users/7/subcategory/coffee_1614758615.jpg', '#000', '2021-01-25 16:21:38', '2021-03-03 15:03:35'),
(19, 7, 'Non Coffee', '19-non-coffee', '', '/users/7/subcategory/non_coffee_1614758472.jpg', '#000', '2021-01-25 16:21:59', '2021-03-03 15:01:12'),
(20, 7, 'Pasta', '20-pasta', '', '/users/7/subcategory/pasta_1614696985.jpg', '#000', '2021-03-02 21:56:25', '2021-03-02 21:56:25'),
(21, 7, 'Fresh Drink (Non Alcohol)', '21-fresh-drink-non-alcohol', '', '/users/7/subcategory/fresh_drink_non_alcohol_1614758671.jpg', '#000', '2021-03-03 13:42:31', '2021-03-03 15:04:31'),
(22, 7, 'Beer', '22-beer', '', '/users/7/subcategory/beer_1614761395.jpg', '#000', '2021-03-03 13:42:54', '2021-03-03 15:49:55'),
(23, 7, 'Main Course', '23-main-course', '', '/users/7/subcategory/main_course_1614758811.jpg', '#000', '2021-03-03 13:43:33', '2021-03-03 15:06:51'),
(24, 7, 'Snack', '24-snack', '', '/users/7/subcategory/snack_1614758852.jpg', '#000', '2021-03-03 13:43:56', '2021-03-03 15:07:32'),
(25, 7, 'Dessert', '25-dessert', '', '/users/7/subcategory/dessert_1614761358.jpg', '#000', '2021-03-03 13:44:13', '2021-03-03 15:49:18'),
(26, 14, 'Coffee', '26-coffee', '', '/users/14/subcategory/coffee_1617870749.jpg', '#000', '2021-04-08 15:32:29', '2021-04-08 15:32:29'),
(27, 14, 'Non Coffee', '27-non-coffee', '', '/users/14/subcategory/non_coffee_1617870788.jpg', '#000', '2021-04-08 15:33:08', '2021-04-08 15:33:08'),
(28, 14, 'Fresh Drink', '28-fresh-drink', '', '/users/14/subcategory/fresh_drink_1617870980.jpg', '#000', '2021-04-08 15:36:20', '2021-04-08 15:36:20');

-- --------------------------------------------------------

--
-- Table structure for table `tutorials`
--

CREATE TABLE `tutorials` (
  `id` int(5) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `title` varchar(100) DEFAULT NULL,
  `description` text DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) NOT NULL,
  `username` varchar(50) DEFAULT '',
  `name` varchar(100) DEFAULT '',
  `password` varchar(255) DEFAULT '',
  `remember_token` varchar(255) DEFAULT NULL,
  `api_token` varchar(20) DEFAULT '',
  `photo` varchar(50) DEFAULT NULL,
  `email` varchar(200) CHARACTER SET latin1 COLLATE latin1_bin DEFAULT '',
  `email_code` varchar(15) DEFAULT NULL,
  `phone` varchar(20) DEFAULT '',
  `address` text DEFAULT NULL,
  `level` enum('Super Admin','User','Member') DEFAULT 'User',
  `active` enum('Y','N') NOT NULL,
  `user_show` enum('Y','N') DEFAULT 'Y',
  `owner` tinyint(4) DEFAULT NULL,
  `last_login` timestamp NULL DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `catalog` int(5) DEFAULT NULL,
  `number_catalog` int(5) DEFAULT NULL,
  `legitimate` enum('Y','N') NOT NULL DEFAULT 'N',
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `name`, `password`, `remember_token`, `api_token`, `photo`, `email`, `email_code`, `phone`, `address`, `level`, `active`, `user_show`, `owner`, `last_login`, `parent_id`, `catalog`, `number_catalog`, `legitimate`, `created_at`, `updated_at`) VALUES
(1, 'administrator', 'Dedeh Darwis', '$2y$10$9dna/q2ZbW1JmCg45XHI8u.7UbvpfRjbY4miBRRbANpDJTWXZ7JyW', 'pjEbhvORcm9Rdro4TCK6tkvbWJRxUIl7mLEqkQgTg9FQQrbbCjChzWWuXfcS', 'Q2dNmbFQEmygNjgwdLy5', 'sakti.jpg', 'saktie.banua@gmail.com', '1247817865249O0', '089798987989878', 'Jl Cigunungku 29', 'Member', 'Y', 'Y', 1, NULL, NULL, NULL, 1, 'Y', '2018-05-23 19:48:16', '2021-06-01 22:21:44'),
(2, 'jobongnagreg', 'Super Admin', '$2y$10$sEh.vhIK.qU99HsSXOhV2.tMKyvEXJLBjWc.AJegk75J5nlIdo4yi', NULL, 'cPrAQ4woS22hRLeFQpaB', NULL, 'jobong.nagreg@gmail.com', NULL, '085624177776', NULL, 'Super Admin', 'Y', 'Y', NULL, NULL, NULL, NULL, NULL, 'N', NULL, '2021-05-09 17:44:18'),
(3, 'yabedi', 'Yabedi', '$2y$10$JnYidDNHROGpiQU0j55Xp.c2XgSJDBZatntqCGn5ydJKcYa1gmdjm', NULL, '', NULL, 'yabedi@gmail.com', NULL, '2234567890', NULL, 'Member', 'Y', 'Y', 1, NULL, NULL, NULL, 1, 'Y', '2020-11-25 10:37:19', '2021-03-15 10:10:52'),
(4, 'jokerslot8888', 'Jujun Junaedi', '$2y$10$vdFov0QeB5XkhS3NZyA7/eI4smu8VNlthB1h2CWMbCQhRsgM6NhDi', NULL, 'LNfeONYHb6VgqjECDkZr', NULL, 'jokerslot8888@gmail.com', NULL, '0822191554422', NULL, 'User', 'Y', 'Y', NULL, NULL, 1, 1, NULL, 'N', '2020-12-07 03:18:21', '2021-05-17 05:17:52'),
(5, 'rian', 'Rian', '$2y$10$6tznBl5poBosFkb5.wW3huT6tg4hjTrYiimIC8ZDPcWf75AW/uw8S', NULL, '', NULL, 'rian@ottimista.id', NULL, '088211976244', NULL, 'Member', 'Y', 'Y', 1, NULL, NULL, 3, 5, 'Y', '2020-12-11 18:22:04', '2021-03-24 17:23:58'),
(6, 'adriansadoyan', 'Rian', '$2y$10$Y/ri/bvziqvTdFyJ2iI4j.XTTfX.0GklbSlvTonNzkpMVAWMS/wf6', NULL, '', NULL, 'adriansadoyan@gmail.com', NULL, '088211976244', NULL, 'User', 'Y', 'Y', NULL, NULL, 5, 2, NULL, 'N', '2020-12-14 23:06:05', '2020-12-14 23:06:05'),
(7, 'vivi', 'vivi', '$2y$10$SX.KKfDfFZ/ku7NKS4RjbujEzTJ76/OgCRP.RuE7292AnV5r7Q1nO', NULL, '', NULL, 'vivi@omio.id', NULL, '08111616467', NULL, 'Member', 'Y', 'Y', 1, NULL, NULL, 5, 1, 'Y', '2021-01-03 15:01:47', '2021-03-15 10:11:02'),
(8, 'cron.orangunung', 'Sakti Tes', '$2y$10$XaDzSoffmb9mZEiwg3YCuORJ.ZWng0aY4kvFrSwUdFA7jpIuPKS76', NULL, '', NULL, 'cron.orangunung@gmail.com', NULL, '085624177777', NULL, 'Member', 'Y', 'Y', 1, NULL, NULL, NULL, 1, 'Y', '2021-02-25 14:22:47', '2021-03-15 10:11:08'),
(9, 'cron.yasin', 'Sakti', '$2y$10$LdV1.fkwXjnr2FxoeZAIneCWmYckXRS11xiWJHWzk8dYegQpjyNIS', NULL, '', NULL, 'cron.yasin@gmail.com', NULL, '082219155447', NULL, 'User', 'Y', 'Y', NULL, NULL, 7, 7, NULL, 'N', '2021-03-03 10:40:30', '2021-03-03 10:40:30'),
(10, 'adriansamulaidisini', 'Muhammad Adriansa', '$2y$10$PvSzhu1fzXNoe90ipGHlpeeUbYUIOUxzAwFbIP8GM/dLdA7zJcOMm', NULL, '', NULL, 'adriansamulaidisini@gmail.com', NULL, '088211976244', NULL, 'Member', 'Y', 'Y', 1, NULL, NULL, NULL, 5, 'Y', '2021-03-14 19:39:21', '2021-03-24 17:23:45'),
(11, 'kronizzz21', 'Michael', '$2y$10$bzf9R3mV0kLXKSGP1mhvQuHBS/TPrewzFpyPPS9TUGrw9/0.Wr2Ye', NULL, '', NULL, 'kronizzz21@gmail.com', NULL, '082157222517', NULL, 'Member', 'Y', 'Y', 1, NULL, NULL, NULL, 1, 'N', '2021-03-23 18:39:49', '2021-03-23 18:39:49'),
(12, 'danisutria.im', 'dani', '$2y$10$kISbZGLcLMdFUMvyiVkp9elegt9v0FdncDcRyEK9KxZwKV8c2k/sC', NULL, '', NULL, 'danisutria.im@gmail.com', NULL, '081380735857', NULL, 'Member', 'Y', 'Y', 1, NULL, NULL, NULL, 1, 'N', '2021-04-05 12:02:56', '2021-04-05 12:02:56'),
(13, 'vivi13', 'Vivi Super Ssnday', '$2y$10$bQ037pa9PyA9hmAv/buK4.haDsqZUMPpB67.pB0/vUW943X5p26Vq', NULL, '', NULL, 'vivi@supersunday.id', NULL, '085624177775', NULL, 'Member', 'Y', 'Y', 1, NULL, NULL, NULL, 1, 'Y', '2021-04-08 13:23:21', '2021-04-08 06:51:25'),
(14, 'supersundaycafe14', 'Darto Mamang', '$2y$10$P/hc6S7WdM7lHKHdQ3cR0evgM2T0sT7/Bs3nMaLvKMpJzGbO4GHkm', NULL, 'WRhfq6mM88bT72MrHkL1', NULL, 'supersundaycafe@gmail.com', NULL, '089787987767', 'Jl Cinunun 144', 'Member', 'Y', 'Y', 1, NULL, NULL, NULL, 1, 'Y', '2021-04-08 14:50:31', '2021-05-10 23:30:02'),
(15, 'ahmad', 'Ahmad Su', '$2y$10$SKKSGUXTDMYG9MEOLyk9.uTb1YhQHB0xNipjPtWF/E0r1I0cvEDce', NULL, '', NULL, 'ahmad@su.com', NULL, '089886283787237', NULL, 'User', 'Y', 'Y', NULL, NULL, 1, 1, NULL, 'N', '2021-04-21 16:37:33', '2021-04-21 16:37:33'),
(16, 'sar', 'Sar Ha', '$2y$10$YMYnPMK9oEjLOPyjVS/AN.Wj2H9H56/idNEQLJgYLYYPIyfLYouPW', NULL, '', NULL, 'sar@ha.com', NULL, '0898876768787', 'Jl Cikujung 55', 'User', 'Y', 'Y', NULL, NULL, 1, 1, NULL, 'N', '2021-04-21 17:04:44', '2021-04-24 07:35:18'),
(17, 'nandar', 'Su Nandar', '$2y$10$esuC9b3jYCEUwd1Sva6YP.EKeDnB354wf3kDtC.GUcR8bpf1iDWeK', '4FJC5qZpOCMzxYKac3kLDCYNW2KcCZvw6VQvCnsBnVWmgPXeIojEHvyHEeaf', 'osPRy30WOuaKOC3nbMJw', NULL, 'nandar@su.com', NULL, '089877373783737', 'Jl Cihunjung 55', 'User', 'Y', '', NULL, NULL, 1, 1, NULL, 'N', '2021-04-22 05:46:36', '2021-05-08 08:32:11'),
(18, 'si', 'Su Ami', '$2y$10$TVU9etT8ZXe.0KGWkZDGM.zNWt0OSIPxXNjQMZlzMZyEiJSFQ507u', NULL, '', NULL, 'si@ami.com', NULL, '0876876765609', NULL, 'User', 'Y', 'Y', NULL, NULL, 1, 1, NULL, 'N', '2021-04-22 06:09:00', '2021-04-22 09:17:17'),
(19, 'kar', 'Kar So', '$2y$10$tevc8RXDlZqTbuiE4ETqceAXYGzdLxNsIbqAncd9I7QWJMpH4NPEy', NULL, 'GBLGUkG7bK0GHPlo7gvV', NULL, 'kar@so.com', NULL, '0876876765609', NULL, 'User', 'Y', 'Y', NULL, NULL, 1, 1, NULL, 'N', '2021-04-22 09:15:10', '2021-05-28 06:22:49'),
(20, 'nga', 'Ngadiem', '$2y$10$AQAkxGCCIXtZo6sNrrCMw.MFkvajCy4y/RU551DKLsIVYSY6PudAW', NULL, 'ZReCGZ0tUm3zfPXDZ9Mu', NULL, 'nga@diem.com', NULL, '08988789877676', NULL, 'User', 'Y', 'Y', NULL, NULL, 14, 9, NULL, 'N', '2021-05-08 08:21:48', '2021-05-08 08:21:48'),
(21, 'pai', 'Painem', '$2y$10$M3Lktrco18KC0C6/WzmWmeQ04JGFpT4ueqNf0UK1pAO7mERCDPhku', NULL, 'yIXCYw5uP0gSo864QTqB', NULL, 'pai@nem.com', NULL, '089898787987', NULL, 'User', 'Y', 'Y', NULL, NULL, 14, 9, NULL, 'N', '2021-05-09 08:45:27', '2021-05-10 22:07:35'),
(22, 'superbwx', 'Arip Man', '$2y$10$rOLFaa6bbiiRxEE5u2BLZOuGlPqT8Vi9hXvkwAjTEzqymyD5izX1i', 'tk5CgHQSYZh5RPNQWdJNyQe7BJkvKUQ3ofSRNT2hpNBntb4UTTCI6kfy85Kr', 'QwBQM8slD7Paml1xtvP3', NULL, 'superbwx@gmail.com', NULL, '0989788998987', NULL, 'User', 'Y', 'Y', NULL, NULL, 1, 1, NULL, 'N', '2021-05-19 07:29:46', '2021-05-30 18:29:37');

-- --------------------------------------------------------

--
-- Table structure for table `user_services`
--

CREATE TABLE `user_services` (
  `id` int(5) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `service_id` int(11) DEFAULT NULL,
  `title` varchar(100) DEFAULT NULL,
  `description` text DEFAULT NULL,
  `handler` varchar(100) DEFAULT NULL,
  `job_result` text DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Table structure for table `voucher`
--

CREATE TABLE `voucher` (
  `id` int(5) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `voucher_code` varchar(20) DEFAULT NULL,
  `voucher_nominal` double DEFAULT NULL,
  `voucher_owner` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `voucher`
--

INSERT INTO `voucher` (`id`, `user_id`, `voucher_code`, `voucher_nominal`, `voucher_owner`, `created_at`, `updated_at`) VALUES
(1, 2, 'HIPMIBANDUNG', 100000, 'Hipmi Bandung', '2021-03-20 10:58:57', '2021-03-20 10:58:57');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bell`
--
ALTER TABLE `bell`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `catalog`
--
ALTER TABLE `catalog`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `catalog_username` (`catalog_username`);

--
-- Indexes for table `catalogdetail`
--
ALTER TABLE `catalogdetail`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `catalog_id` (`catalog_id`);

--
-- Indexes for table `catalog_list_types`
--
ALTER TABLE `catalog_list_types`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `catalog_types`
--
ALTER TABLE `catalog_types`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`id`),
  ADD KEY `catalog_id` (`catalog_id`);

--
-- Indexes for table `feature`
--
ALTER TABLE `feature`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `package_id` (`package_id`),
  ADD KEY `mainfeature_id` (`mainfeature_id`);

--
-- Indexes for table `invoice`
--
ALTER TABLE `invoice`
  ADD PRIMARY KEY (`id`),
  ADD KEY `catalog_id` (`catalog_id`);

--
-- Indexes for table `invoiceaddons`
--
ALTER TABLE `invoiceaddons`
  ADD PRIMARY KEY (`id`),
  ADD KEY `invoicedetailid` (`invoicedetailid`),
  ADD KEY `invoiceid` (`invoiceid`);

--
-- Indexes for table `invoicedetail`
--
ALTER TABLE `invoicedetail`
  ADD PRIMARY KEY (`id`),
  ADD KEY `invoiceid` (`invoiceid`);

--
-- Indexes for table `invoicerecipe`
--
ALTER TABLE `invoicerecipe`
  ADD PRIMARY KEY (`id`),
  ADD KEY `invoicedetailid` (`reff_id`),
  ADD KEY `recipe_id` (`recipe_id`);

--
-- Indexes for table `items`
--
ALTER TABLE `items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `items_detail`
--
ALTER TABLE `items_detail`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `item_id` (`item_id`),
  ADD KEY `category_id` (`category_id`);

--
-- Indexes for table `items_stock`
--
ALTER TABLE `items_stock`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `item_id` (`item_id`);

--
-- Indexes for table `mainfeatures`
--
ALTER TABLE `mainfeatures`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `member_statuses`
--
ALTER TABLE `member_statuses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `package`
--
ALTER TABLE `package`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `package_price`
--
ALTER TABLE `package_price`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `package_id` (`package_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `price_lists`
--
ALTER TABLE `price_lists`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `price_payments`
--
ALTER TABLE `price_payments`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `jenis` (`jenis`);

--
-- Indexes for table `price_payment_cats`
--
ALTER TABLE `price_payment_cats`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `nama` (`nama`);

--
-- Indexes for table `recipe`
--
ALTER TABLE `recipe`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `item_id` (`item_id`),
  ADD KEY `parent_id` (`parent_id`);

--
-- Indexes for table `register`
--
ALTER TABLE `register`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `sliders`
--
ALTER TABLE `sliders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `subcategory`
--
ALTER TABLE `subcategory`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `tutorials`
--
ALTER TABLE `tutorials`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Indexes for table `user_services`
--
ALTER TABLE `user_services`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `voucher`
--
ALTER TABLE `voucher`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `voucher_code` (`voucher_code`),
  ADD KEY `user_id` (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bell`
--
ALTER TABLE `bell`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `catalog`
--
ALTER TABLE `catalog`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `catalogdetail`
--
ALTER TABLE `catalogdetail`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=164;

--
-- AUTO_INCREMENT for table `catalog_list_types`
--
ALTER TABLE `catalog_list_types`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `catalog_types`
--
ALTER TABLE `catalog_types`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `customer`
--
ALTER TABLE `customer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `feature`
--
ALTER TABLE `feature`
  MODIFY `id` tinyint(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=78;

--
-- AUTO_INCREMENT for table `invoice`
--
ALTER TABLE `invoice`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `invoiceaddons`
--
ALTER TABLE `invoiceaddons`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `invoicedetail`
--
ALTER TABLE `invoicedetail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `invoicerecipe`
--
ALTER TABLE `invoicerecipe`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `items`
--
ALTER TABLE `items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=181;

--
-- AUTO_INCREMENT for table `items_detail`
--
ALTER TABLE `items_detail`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- AUTO_INCREMENT for table `items_stock`
--
ALTER TABLE `items_stock`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;

--
-- AUTO_INCREMENT for table `mainfeatures`
--
ALTER TABLE `mainfeatures`
  MODIFY `id` tinyint(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `member_statuses`
--
ALTER TABLE `member_statuses`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `package`
--
ALTER TABLE `package`
  MODIFY `id` tinyint(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `package_price`
--
ALTER TABLE `package_price`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `price_lists`
--
ALTER TABLE `price_lists`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `price_payments`
--
ALTER TABLE `price_payments`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `price_payment_cats`
--
ALTER TABLE `price_payment_cats`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `recipe`
--
ALTER TABLE `recipe`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=85;

--
-- AUTO_INCREMENT for table `register`
--
ALTER TABLE `register`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `services`
--
ALTER TABLE `services`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `sliders`
--
ALTER TABLE `sliders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `subcategory`
--
ALTER TABLE `subcategory`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `tutorials`
--
ALTER TABLE `tutorials`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `user_services`
--
ALTER TABLE `user_services`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `voucher`
--
ALTER TABLE `voucher`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `catalog`
--
ALTER TABLE `catalog`
  ADD CONSTRAINT `catalog_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `catalogdetail`
--
ALTER TABLE `catalogdetail`
  ADD CONSTRAINT `catalogdetail_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `catalogdetail_ibfk_2` FOREIGN KEY (`catalog_id`) REFERENCES `catalog` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `category`
--
ALTER TABLE `category`
  ADD CONSTRAINT `category_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `customer`
--
ALTER TABLE `customer`
  ADD CONSTRAINT `customer_ibfk_1` FOREIGN KEY (`catalog_id`) REFERENCES `catalog` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `feature`
--
ALTER TABLE `feature`
  ADD CONSTRAINT `feature_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `feature_ibfk_2` FOREIGN KEY (`package_id`) REFERENCES `package` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `feature_ibfk_3` FOREIGN KEY (`mainfeature_id`) REFERENCES `mainfeatures` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `invoice`
--
ALTER TABLE `invoice`
  ADD CONSTRAINT `invoice_ibfk_1` FOREIGN KEY (`catalog_id`) REFERENCES `catalog` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `invoiceaddons`
--
ALTER TABLE `invoiceaddons`
  ADD CONSTRAINT `invoiceaddons_ibfk_1` FOREIGN KEY (`invoicedetailid`) REFERENCES `invoicedetail` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `invoiceaddons_ibfk_2` FOREIGN KEY (`invoiceid`) REFERENCES `invoice` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `invoicedetail`
--
ALTER TABLE `invoicedetail`
  ADD CONSTRAINT `invoicedetail_ibfk_1` FOREIGN KEY (`invoiceid`) REFERENCES `invoice` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `invoicerecipe`
--
ALTER TABLE `invoicerecipe`
  ADD CONSTRAINT `invoicerecipe_ibfk_4` FOREIGN KEY (`recipe_id`) REFERENCES `recipe` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `items`
--
ALTER TABLE `items`
  ADD CONSTRAINT `items_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `items_detail`
--
ALTER TABLE `items_detail`
  ADD CONSTRAINT `items_detail_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `items_detail_ibfk_2` FOREIGN KEY (`item_id`) REFERENCES `items` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `items_detail_ibfk_3` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `items_stock`
--
ALTER TABLE `items_stock`
  ADD CONSTRAINT `items_stock_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `items_stock_ibfk_2` FOREIGN KEY (`item_id`) REFERENCES `items` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `mainfeatures`
--
ALTER TABLE `mainfeatures`
  ADD CONSTRAINT `mainfeatures_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `package`
--
ALTER TABLE `package`
  ADD CONSTRAINT `package_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `package_price`
--
ALTER TABLE `package_price`
  ADD CONSTRAINT `package_price_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `package_price_ibfk_2` FOREIGN KEY (`package_id`) REFERENCES `package` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `recipe`
--
ALTER TABLE `recipe`
  ADD CONSTRAINT `recipe_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `recipe_ibfk_2` FOREIGN KEY (`item_id`) REFERENCES `items` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `recipe_ibfk_3` FOREIGN KEY (`parent_id`) REFERENCES `items` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `register`
--
ALTER TABLE `register`
  ADD CONSTRAINT `register_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `sliders`
--
ALTER TABLE `sliders`
  ADD CONSTRAINT `sliders_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `subcategory`
--
ALTER TABLE `subcategory`
  ADD CONSTRAINT `subcategory_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `voucher`
--
ALTER TABLE `voucher`
  ADD CONSTRAINT `voucher_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
