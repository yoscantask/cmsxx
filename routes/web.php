<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/notif/{catalog?}/{invoice?}/{info?}/{status?}', function($catalog,$invoice,$info,$status){
	event(new \App\Events\NotifEvent(['catalog'=>$catalog,'invoice'=>$catalog,'info'=>$info,'status'=>$status,'note'=>'']));
});

// Route::post('/bell', function(Request $request){
// 	event(new \App\Events\NotifEvent(['catalog'=>$request->input('catalog'),'invoice'=>$request->input('invoice'),'info'=>$request->input('info'),'status'=>$request->input('position'),'note'=>'Bell']));
// });
Route::post('/bellnotif', function(Request $request){
	event(new \App\Events\NotifEvent(['catalog'=>$request->input('catalog'),'invoice'=>$request->input('invoice'),'info'=>$request->input('info'),'status'=>$request->input('position'),'note'=>'Bell']));
});
Route::post('/newtransaction', function(Request $request){
	event(new \App\Events\NotifEvent(['catalog'=>$request->input('catalog'),'invoice'=>$request->input('invoice'),'info'=>$request->input('info'),'status'=>$request->input('position'),'note'=>'Front']));
});

Route::group(['prefix'=>'auth'],function () {
	Route::post('/login', 'Auth\LoginController@authLogin');
});

Route::get('/lang', function(Request $request) {	
	\Session::put('locale', $request->bhs);
	return redirect()->back();
});

Auth::routes();
Route::get('/', 'DashboardController@index');
Route::get('/catalog-session/{catalog?}', 'DashboardController@setCatalogSession');
Route::get('/dahsboard-data', 'DashboardController@getData');
Route::get('/logout', 'DashboardController@logout');
Route::get('/email','DashboardController@sendEmail'); 

Route::get('/home', function(){
	return redirect('/');
});

// Bell
Route::group(['prefix'=>'bell'],function () {
	Route::post('/data', 'Master\BellController@getData');
});
Route::resource('bell','Master\BellController');
// End

// Customer
Route::group(['prefix'=>'customer'],function () {
	Route::post('/data', 'Master\CustomerController@getData');
});
Route::resource('customer','Master\CustomerController');
// End

// Category Item
Route::group(['prefix'=>'category'],function () {
	Route::post('/data', 'Master\CategoryController@getData');
	Route::get('/delete/{id?}', 'Master\CategoryController@destroy');
});
Route::resource('category','Master\CategoryController');
// End

// Category Addon
Route::group(['prefix'=>'categoryadd'],function () {
	Route::post('/data', 'Master\CategoryAddController@getData');
	Route::get('/delete/{id?}', 'Master\CategoryAddController@destroy');
});
Route::resource('categoryadd','Master\CategoryAddController');
// End

// User
Route::group(['prefix'=>'user'],function () {
	Route::post('/data', 'Master\UserController@getData');
	Route::get('/delete/{id?}', 'Master\UserController@destroy');
});
Route::resource('user','Master\UserController');
// End

// Subcategory
Route::group(['prefix'=>'subcategory'],function () {
	Route::post('/data', 'Master\SubCategoryController@getData');
	Route::get('/delete/{id?}', 'Master\SubCategoryController@destroy');
});
Route::resource('subcategory','Master\SubCategoryController');
// End

// Items
Route::group(['prefix'=>'items'],function () {
	Route::post('/data', 'Master\ItemsController@getData');
	Route::get('/delete/{id?}', 'Master\ItemsController@destroy');
	Route::any('/addons/{id?}', 'Master\ItemsController@addons');
	Route::any('/addaddons/{id?}', 'Master\ItemsController@addaddons');
	Route::any('/ingredient/{id?}', 'Master\ItemsController@ingredient');
	Route::get('/detailingredient/{id?}', 'Master\ItemsController@detailingredient');
	Route::post('/addingredient/{id?}', 'Master\ItemsController@addingredient');
	Route::post('/updateingredient/{id?}', 'Master\ItemsController@updateingredient');
	Route::get('/gallery/{id?}', 'Master\ItemsController@gallery');
	Route::get('/delete/addon/{id?}/{addons?}', 'Master\ItemsController@deleteaddons');
	Route::get('/delete/ingredient/{id?}/{ingredient?}', 'Master\ItemsController@deleteingredient');
	Route::get('/primaryimage/{id?}/{image?}', 'Master\ItemsController@primaryimage');
	Route::get('/deleteimage/{id?}/{image?}/{position?}', 'Master\ItemsController@deleteimage');
});
Route::resource('items','Master\ItemsController');
//End

// Material
Route::group(['prefix'=>'material'],function () {
	Route::get('/history', 'Master\MaterialController@indexHistory');
	Route::post('/history/data', 'Master\MaterialController@getDataHistory');
	Route::get('/history/detail/{id?}', 'Master\MaterialController@showHistory');
	Route::get('/history/delete/{id?}', 'Master\MaterialController@destroyHistory');
	Route::post('/history/update/{id?}', 'Master\MaterialController@updateHistory');
	Route::post('/storeqty', 'Master\MaterialController@storeqty');
	Route::post('/data', 'Master\MaterialController@getData');
	Route::get('/delete/{id?}', 'Master\MaterialController@destroy');
	Route::get('/detail/{id?}', 'Master\MaterialController@show');
	Route::any('/stock/{id?}', 'Master\MaterialController@stock');
	Route::post('/addstock/{id?}', 'Master\MaterialController@addstock');
	Route::post('/updatestock/{id?}', 'Master\MaterialController@updatestock');
	Route::get('/delete/stock/{id?}/{stock?}', 'Master\MaterialController@deletestock');
});
Route::resource('material','Master\MaterialController');
// End

// Addons
Route::group(['prefix'=>'addons'],function () {
	Route::post('/data', 'Master\AddonsController@getData');
	Route::get('/delete/{id?}', 'Master\AddonsController@destroy');
	Route::any('/serving/{id?}', 'Master\AddonsController@serving');
	Route::get('/detailserving/{id?}', 'Master\AddonsController@detailserving');
	Route::post('/addserving/{id?}', 'Master\AddonsController@addserving');
	Route::post('/updateserving/{id?}', 'Master\AddonsController@updateserving');
	Route::get('/delete/serving/{id?}/{serving?}', 'Master\AddonsController@deleteserving');
});
Route::resource('addons','Master\AddonsController');
// End

// Slider
Route::group(['prefix'=>'sliders'],function () {
	Route::post('/data', 'Master\SlidersController@getData');
	Route::get('/delete/{id?}', 'Master\SlidersController@destroy');
});
Route::resource('sliders','Master\SlidersController');
// End

// Voucher
Route::resource('voucher','Master\VoucherController');
Route::group(['prefix'=>'voucher'],function () {
	Route::post('/data', 'Master\VoucherController@getData');
	Route::get('/delete/{id?}', 'Master\VoucherController@destroy');
	Route::get('/usagevoucher/{code?}', 'Master\VoucherController@usagevoucher');
});
// End

// Package
Route::resource('package','Master\PackageController');
Route::group(['prefix'=>'package'],function () {
	Route::post('/data', 'Master\PackageController@getData');
	Route::get('/delete/{id?}', 'Master\PackageController@destroy');
	Route::any('/price/{id?}', 'Master\PackageController@price');
	Route::get('/pricedetail/{id?}', 'Master\PackageController@pricedetail');
	Route::get('/pricedelete/{id?}', 'Master\PackageController@pricedelete');
	Route::post('/addprice', 'Master\PackageController@addprice');
	Route::post('/updateprice', 'Master\PackageController@updateprice');
});
// End

// Main Features
Route::resource('mainfeatures','Master\MainFeaturesController');
Route::group(['prefix'=>'mainfeatures'],function () {
	Route::post('/data', 'Master\MainFeaturesController@getData');
	Route::get('/delete/{id?}', 'Master\MainFeaturesController@destroy');
});
// End

// Catalog
Route::resource('catalog','CatalogController');
Route::group(['prefix'=>'catalog'],function () {
	Route::post('/data', 'CatalogController@getData');
	Route::get('/delete/{id?}', 'CatalogController@destroy');
	Route::any('/items/{id?}', 'CatalogController@items');
	Route::get('/qrcode/{id?}', 'CatalogController@qrcode');
	Route::any('/additems/{id?}', 'CatalogController@additems');
	Route::get('/position/category/{catalog?}/{category?}', 'CatalogController@getPositionCategory');
	Route::get('/position/subcategory/{catalog?}/{category?}/{subcategory?}', 'CatalogController@getPositionSubCategory');
	Route::any('/change/position/{catalog?}/{me?}/{current?}/{status?}', 'CatalogController@changeStatus');
	Route::get('/delete/element/{catalog?}/{me?}/{type?}', 'CatalogController@deleteElement');
	Route::get('/available/element/{catalog?}/{me?}/{available?}', 'CatalogController@availableElement');
});
// End

Route::group(['prefix'=>'transaction'],function () {
	Route::get('/status/{invoice?}/{status?}', 'TransactionController@updatestatus');
	Route::get('/detail/{invoice?}', 'TransactionController@detail');
	Route::get('/detailpopup/{invoice?}', 'TransactionController@detailpopup');
	Route::get('/delete/item/{id?}', 'TransactionController@deleteitem');
	Route::post('/cancel/order', 'TransactionController@cancelorder');
	Route::get('/index/{status?}', 'TransactionController@index');
	Route::any('/report/{report?}/{status?}/{startdate?}/{enddate?}', 'TransactionController@report');
	Route::get('/print/{inv?}', 'TransactionController@generateStruk');
	Route::get('/printall/{print?}/{getstatus?}/{datestart?}/{dateend?}', 'TransactionController@printAll');
	Route::any('/data', 'TransactionController@getData');
	Route::any('/datamon', 'TransactionController@getDataMon');
	Route::any('/datayear', 'TransactionController@getDataYear');
});

Route::group(['prefix'=>'pos'],function () {
	Route::any('/', 'POSController@index');
	Route::any('/table', 'POSController@getTable');
	Route::any('/tablepending', 'POSController@getTablePending');
	Route::any('/data', 'POSController@getData');
	Route::post('/update', 'POSController@updateData');
	Route::post('/updatecartbackpayemnt', 'POSController@updateCartBackData');
	Route::post('/updateaddons', 'POSController@updatecartaddons');
	Route::post('/checkout', 'POSController@checkoutData');
	Route::post('/completepending', 'POSController@completePending');
	Route::get('/cancel', 'POSController@cancelData');
	Route::get('/clonedata', 'POSController@getCloneData');
	Route::get('/addons/{item?}/{group?}', 'POSController@addons');
	Route::get('/delete/{id?}', 'POSController@deleteData');
	Route::get('/delete-addon/{detail?}/{group?}', 'POSController@deleteaddon');
	Route::get('/clone/{item?}/{detailinvoice?}', 'POSController@clonedata');
	Route::get('/invoicepending/{id?}', 'POSController@showInvoice');
	Route::get('/detailinvoicepending/{id?}', 'POSController@getDataPending');
	Route::get('/edit/{id?}', 'POSController@edit');
	Route::any('/editdata/{id?}', 'POSController@getEditData');
	Route::get('/detail/{id?}', 'POSController@showDetail');
	Route::get('/selectitem/{id?}', 'POSController@showItem');
});

Route::group(['prefix'=>'payment'],function () {
	Route::get('/snap/{total?}','PaymentController@token');
	Route::any('/finish','PaymentController@finishPayment');
});

// Member
Route::group(['prefix'=>'member'],function () {
	Route::post('/data', 'MemberController@getData');
	Route::get('/show/{id?}', 'MemberController@show');
	Route::get('/block/{id?}/{active?}', 'MemberController@block');
});
Route::resource('member','MemberController');
// End

// Register
Route::group(['prefix'=>'register'],function () {
	Route::post('/data/{status?}', 'RegisterController@getData');
	Route::get('/show/{id?}', 'RegisterController@show');
	Route::get('/detail/{id?}', 'RegisterController@detail');
	Route::get('/approve/{id?}', 'RegisterController@approve');
	//Route::get('/block/{id?}/{active?}', 'RegisterController@block');
	Route::post('/reject', 'RegisterController@rejected');
	Route::get('/{status?}', 'RegisterController@index');
});
Route::resource('register','RegisterController');
// End

// Tutorial
Route::group(['prefix'=>'tutorial'],function () {
	Route::post('/data', 'TutorialController@getData');
	Route::get('/delete/{id?}', 'TutorialController@destroy');
});
Route::resource('tutorial','TutorialController');
//End

// Complaint
Route::group(['prefix'=>'complaint'],function () {
	Route::post('/data', 'ComplaintController@getData');
	Route::get('/delete/{id?}', 'ComplaintController@destroy');
});
Route::resource('complaint','ComplaintController');
//End

// User Complaint
Route::group(['prefix'=>'user_complaint'],function () {
	Route::post('/data', 'UserComplaintController@getData');
	Route::get('/delete/{id?}', 'UserComplaintController@destroy');
});
Route::resource('user_complaint','UserComplaintController');
//End

// Service
Route::group(['prefix'=>'service'],function () {
	Route::post('/data', 'ServiceController@getData');
	Route::get('/delete/{id?}', 'ServiceController@destroy');
});
Route::resource('service','ServiceController');
//End

// User Service
Route::group(['prefix'=>'user_service'],function () {
	Route::post('/data', 'UserServiceController@getData');
	Route::get('/delete/{id?}', 'UserServiceController@destroy');
});
Route::resource('user_service','UserServiceController');
//End

Route::get('/viewmenus/{id}', 'ViewMenusController@index');
Route::post('/viewmenus/{id}', 'ViewMenusController@update');
Route::get('/profile', 'ProfileController@index');
Route::post('/profile/{id}', 'ProfileController@update');

// MemberStatus
Route::group(['prefix'=>'memberstatus'],function () {
	Route::post('/data', 'MemberStatusController@getData');
	Route::get('/show/{id?}', 'MemberStatusController@show');
	Route::get('/perpanjang', 'MemberStatusController@perpanjang');
	Route::post('/perpanjang/{id?}', 'MemberStatusController@store');
	//Route::get('/block/{id?}/{active?}', 'MemberStatusController@block');
});
Route::resource('memberstatus','MemberStatusController');
// End

// PriceList
Route::group(['prefix'=>'pricelist'],function () {
	Route::get('/delete/{id?}', 'Price\PriceListController@destroy');
	Route::get('/details/{id?}', 'Price\PriceListController@index');
	Route::get('/data/{id?}', 'Price\PriceListController@getData');
	Route::get('/exportharga', 'Price\PriceListController@create'); // Khusus copy harga
});
Route::resource('pricelist', 'Price\PriceListController');

// PricePayment
Route::get('/pricepayment/delete/{id?}', 'Price\PricePaymentController@destroy');
Route::resource('pricepayment', 'Price\PricePaymentController');

// PricePaymentCat
Route::get('/pricepaymentcat/delete/{id?}', 'Price\PricePaymentCatController@destroy');
Route::resource('pricepaymentcat','Price\PricePaymentCatController');
// EndPrice
