<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use App\Http\Requests;

use App\Models\ItemsDetail;
use App\Models\ItemsStock;
use App\Helper\myFunction;

use Image;
use Input;
use File;
use Auth;

class Items extends Model
{
    protected $table = 'items';

    public static function save_data($request){
    	try {
    	    DB::transaction(function () use ($request) {
                $showoncatalog="";
    	        $id = myFunction::id('items','id');
                $data=$request->all();
                $var=new Items;
                $var->id=$id;
                $var->user_id=Auth::user()->id;
                $var->items_name=trim($data['items_name']);
                $var->items_slug=$id.'-'.Str::slug(trim($data['items_name']),"-");
                $var->items_color=$data['items_color'];
                $var->items_description=$data['items_description'];
                // $var->items_price=trim($data['items_price']);
                // $var->items_discount=trim($data['items_discount']);
                $var->items_youtube=trim($data['items_youtube']);
                //$var->ready_stock=trim($data['ready_stock']);
                $var->item_type=trim($data['item_type']);
                $var->show_on_catalog=trim($data['show_on_catalog']);
                //$var->addons=(!empty($data['addons']))?json_encode(explode(',', $data['addons'])):'';
                $var->save();

                $mainpath = myFunction::pathAsset();
                $subpath = '/users/'.Auth::user()->id.'/items';
                $thumbs = '/users/'.Auth::user()->id.'/items/thumbs';
                $path = $mainpath.$subpath;
                $thumbspath = $mainpath.$thumbs;
                File::isDirectory($path) or File::makeDirectory($path, 0777, true, true);
                File::isDirectory($thumbspath) or File::makeDirectory($thumbspath, 0777, true, true);

                if($request->hasFile('imagefile_one')){
                    $imgfile_one = Image::make($_FILES['imagefile_one']['tmp_name']);

                    $name_one=Str::slug(trim($data['items_name']),"_").'_'.$id.'_1';
                    $extension_one = $request->file('imagefile_one')->getClientOriginalExtension();
                    $imgfile_one->save($path.'/'.$name_one.'.'.$extension_one);

                    //Thumb
                    $imgfile_one_thumb = Image::make($_FILES['imagefile_one']['tmp_name']);
                    $imgfile_one_thumb->fit(650, 650);
                    $name_one_thumb=Str::slug(trim($data['items_name']),"_").'_'.$id.'_1';
                    $extension_one_thumb = $request->file('imagefile_one')->getClientOriginalExtension();
                    $imgfile_one_thumb->save($thumbspath.'/'.$name_one_thumb.'.'.$extension_one_thumb);
                    //End

                    $images_one=$thumbs.'/'.$name_one.'.'.$extension_one;

                    $array_one=['item_image_one'=>$images_one,'item_image_primary'=>$images_one];
                    Items::where('id',$id)->update($array_one);
                }
                if($request->hasFile('imagefile_two')){
                    $imgfile_two = Image::make($_FILES['imagefile_two']['tmp_name']);

                    $name_two=Str::slug(trim($data['items_name']),"_").'_'.$id.'_2';
                    $extension_two = $request->file('imagefile_two')->getClientOriginalExtension();
                    $imgfile_two->save($path.'/'.$name_two.'.'.$extension_two);

                    //Thumb
                    $imgfile_two_thumb = Image::make($_FILES['imagefile_two']['tmp_name']);
                    $imgfile_two_thumb->fit(650, 650);
                    $name_two_thumb=Str::slug(trim($data['items_name']),"_").'_'.$id.'_2';
                    $extension_two_thumb = $request->file('imagefile_two')->getClientOriginalExtension();
                    $imgfile_two_thumb->save($thumbspath.'/'.$name_two_thumb.'.'.$extension_two_thumb);
                    //End

                    $images_two=$thumbs.'/'.$name_two.'.'.$extension_two;

                    $array_two=['item_image_two'=>$images_two];
                    Items::where('id',$id)->update($array_two);
                }
                if($request->hasFile('imagefile_three')){
                    $imgfile_three = Image::make($_FILES['imagefile_three']['tmp_name']);

                    $name_three=Str::slug(trim($data['items_name']),"_").'_'.$id.'_3';
                    $extension_three = $request->file('imagefile_three')->getClientOriginalExtension();
                    $imgfile_three->save($path.'/'.$name_three.'.'.$extension_three);

                    //Thumb
                    $imgfile_three_thumb = Image::make($_FILES['imagefile_three']['tmp_name']);
                    $imgfile_three_thumb->fit(650, 650);
                    $name_three_thumb=Str::slug(trim($data['items_name']),"_").'_'.$id.'_3';
                    $extension_three_thumb = $request->file('imagefile_three')->getClientOriginalExtension();
                    $imgfile_three_thumb->save($thumbspath.'/'.$name_three_thumb.'.'.$extension_three_thumb);
                    //End

                    $images_three=$thumbs.'/'.$name_three.'.'.$extension_three;

                    $array_three=['item_image_three'=>$images_three];
                    Items::where('id',$id)->update($array_three);
                }
                if($request->hasFile('imagefile_four')){
                    $imgfile_four = Image::make($_FILES['imagefile_four']['tmp_name']);

                    $name_four=Str::slug(trim($data['items_name']),"_").'_'.$id.'_4';
                    $extension_four = $request->file('imagefile_four')->getClientOriginalExtension();
                    $imgfile_four->save($path.'/'.$name_four.'.'.$extension_four);

                    //Thumb
                    $imgfile_four_thumb = Image::make($_FILES['imagefile_four']['tmp_name']);
                    $imgfile_four_thumb->fit(650, 650);
                    $name_four_thumb=Str::slug(trim($data['items_name']),"_").'_'.$id.'_4';
                    $extension_four_thumb = $request->file('imagefile_four')->getClientOriginalExtension();
                    $imgfile_four_thumb->save($thumbspath.'/'.$name_four_thumb.'.'.$extension_four_thumb);
                    //End

                    $images_four=$thumbs.'/'.$name_four.'.'.$extension_four;

                    $array_four=['item_image_four'=>$images_four];
                    Items::where('id',$id)->update($array_four);
                }
                
                
    	    });
    	 }
    	catch(\Exception $e) {
    	    return false;
    	}
    	return true;
    }
    public static function save_material($request){
        try {
            DB::transaction(function () use ($request) {
                $id = myFunction::id('items','id');
                $data=$request->all();
                $var=new Items;
                $var->id=$id;
                $var->user_id=Auth::user()->id;
                $var->items_name=trim($data['items_name']);
                $var->items_slug=$id.'-'.Str::slug(trim($data['items_name']),"-");
                $var->ready_stock=trim($data['ready_stock']);
                $var->item_type=trim($data['item_type']);
                $var->item_unit=trim($data['item_unit']);
                $var->save();
            });
         }
        catch(\Exception $e) {
            return false;
        }
        return true;
    }
    public static function update_data($request){
        try {
            DB::transaction(function () use ($request) {
                $data=$request->all();

                $query = Items::where('id',$data['id'])->first();

                $mainpath = myFunction::pathAsset();
                $subpath = '/users/'.Auth::user()->id.'/items';
                $thumbs = '/users/'.Auth::user()->id.'/items/thumbs';
                $path = $mainpath.$subpath;
                $thumbspath = $mainpath.$thumbs;
                File::isDirectory($path) or File::makeDirectory($path, 0777, true, true);
                File::isDirectory($thumbspath) or File::makeDirectory($thumbspath, 0777, true, true);

                if($request->hasFile('imagefile_one')){
                    if(!empty($query['item_image_one']) and File::exists($path.'/'.basename(parse_url($query['item_image_one'])['path']))){
                        unlink($path.'/'.basename(parse_url($data['item_image_one'])['path']));
                        unlink($path.'/thumbs/'.basename(parse_url($query['item_image_one'])['path']));
                    }

                    $imgfile_one = Image::make($_FILES['imagefile_one']['tmp_name']);

                    $name_one=Str::slug(trim($data['items_name']),"_").'_'.$data['id'].'_1';
                    $extension_one = $request->file('imagefile_one')->getClientOriginalExtension();
                    $imgfile_one->save($path.'/'.$name_one.'.'.$extension_one);

                    //Thumb
                    $imgfile_one_thumb = Image::make($_FILES['imagefile_one']['tmp_name']);
                    $imgfile_one_thumb->fit(650, 650);
                    $name_one_thumb=Str::slug(trim($data['items_name']),"_").'_'.$data['id'].'_1';
                    $extension_one_thumb = $request->file('imagefile_one')->getClientOriginalExtension();
                    $imgfile_one_thumb->save($thumbspath.'/'.$name_one_thumb.'.'.$extension_one_thumb);
                    //End

                    $images_one=$thumbs.'/'.$name_one.'.'.$extension_one;

                }else{
                    if(!empty($data['item_image_one'])){

                        $old = $path.'/'.basename(parse_url($data['item_image_one'])['path']);
                        $name_one=Str::slug(trim($data['items_name']),"_").'_'.$data['id'].'_1';
                        $ext=explode(".",basename(parse_url($data['item_image_one'])['path']));
                        $new = $path.'/'.$name_one.'.'.$ext[1];
                        rename($old, $new);

                        $oldthumbs = $thumbspath.'/'.basename(parse_url($data['item_image_one'])['path']);
                        $name_one_thumbs=Str::slug(trim($data['items_name']),"_").'_'.$data['id'].'_1';
                        $ext_thumbs=explode(".",basename(parse_url($data['item_image_one'])['path']));
                        $new_thumbs = $thumbspath.'/'.$name_one_thumbs.'.'.$ext_thumbs[1];
                        rename($oldthumbs, $new_thumbs);

                        $images_one=$thumbs.'/'.$name_one.'.'.$ext[1];
                    }else{
                        $images_one=$data['item_image_one'];
                    }
                }
                if($data['item_image_primary'] == $data['item_image_one']){
                    $primary = $images_one;
                }

                if($request->hasFile('imagefile_two')){
                    if(!empty($query['item_image_two']) and File::exists($path.'/'.basename(parse_url($query['item_image_two'])['path']))){
                        unlink($path.'/'.basename(parse_url($data['item_image_two'])['path']));
                        unlink($path.'/thumbs/'.basename(parse_url($query['item_image_two'])['path']));
                    }

                    $imgfile_two = Image::make($_FILES['imagefile_two']['tmp_name']);

                    $name_two=Str::slug(trim($data['items_name']),"_").'_'.$data['id'].'_2';
                    $extension_two = $request->file('imagefile_two')->getClientOriginalExtension();
                    $imgfile_two->save($path.'/'.$name_two.'.'.$extension_two);

                    //Thumb
                    $imgfile_two_thumb = Image::make($_FILES['imagefile_two']['tmp_name']);
                    $imgfile_two_thumb->fit(650, 650);
                    $name_two_thumb=Str::slug(trim($data['items_name']),"_").'_'.$data['id'].'_2';
                    $extension_two_thumb = $request->file('imagefile_two')->getClientOriginalExtension();
                    $imgfile_two_thumb->save($thumbspath.'/'.$name_two_thumb.'.'.$extension_two_thumb);
                    //End

                    $images_two=$thumbs.'/'.$name_two.'.'.$extension_two;

                }else{
                    if(!empty($data['item_image_two'])){

                        $old = $path.'/'.basename(parse_url($data['item_image_two'])['path']);
                        $name_two=Str::slug(trim($data['items_name']),"_").'_'.$data['id'].'_2';
                        $ext=explode(".",basename(parse_url($data['item_image_two'])['path']));
                        $new = $path.'/'.$name_two.'.'.$ext[1];
                        rename($old, $new);

                        $oldthumbs = $thumbspath.'/'.basename(parse_url($data['item_image_two'])['path']);
                        $name_two_thumbs=Str::slug(trim($data['items_name']),"_").'_'.$data['id'].'_2';
                        $ext_thumbs=explode(".",basename(parse_url($data['item_image_two'])['path']));
                        $new_thumbs = $thumbspath.'/'.$name_two_thumbs.'.'.$ext_thumbs[1];
                        rename($oldthumbs, $new_thumbs);

                        $images_two=$thumbs.'/'.$name_two.'.'.$ext[1];
                    }else{
                        $images_two=$data['item_image_two'];
                    }
                }
                if($data['item_image_primary'] == $data['item_image_two']){
                    $primary = $images_two;
                }

                if($request->hasFile('imagefile_three')){
                    if(!empty($query['item_image_three']) and File::exists($path.'/'.basename(parse_url($query['item_image_three'])['path']))){
                        unlink($path.'/'.basename(parse_url($data['item_image_three'])['path']));
                        unlink($path.'/thumbs/'.basename(parse_url($query['item_image_three'])['path']));
                    }

                    $imgfile_three = Image::make($_FILES['imagefile_three']['tmp_name']);

                    $name_three=Str::slug(trim($data['items_name']),"_").'_'.$data['id'].'_3';
                    $extension_three = $request->file('imagefile_three')->getClientOriginalExtension();
                    $imgfile_three->save($path.'/'.$name_three.'.'.$extension_three);

                    //Thumb
                    $imgfile_three_thumb = Image::make($_FILES['imagefile_three']['tmp_name']);
                    $imgfile_three_thumb->fit(650, 650);
                    $name_three_thumb=Str::slug(trim($data['items_name']),"_").'_'.$data['id'].'_3';
                    $extension_three_thumb = $request->file('imagefile_three')->getClientOriginalExtension();
                    $imgfile_three_thumb->save($thumbspath.'/'.$name_three_thumb.'.'.$extension_three_thumb);
                    //End

                    $images_three=$thumbs.'/'.$name_three.'.'.$extension_three;

                }else{
                    if(!empty($data['item_image_three'])){

                        $old = $path.'/'.basename(parse_url($data['item_image_three'])['path']);
                        $name_three=Str::slug(trim($data['items_name']),"_").'_'.$data['id'].'_3';
                        $ext=explode(".",basename(parse_url($data['item_image_three'])['path']));
                        $new = $path.'/'.$name_three.'.'.$ext[1];
                        rename($old, $new);

                        $oldthumbs = $thumbspath.'/'.basename(parse_url($data['item_image_three'])['path']);
                        $name_three_thumbs=Str::slug(trim($data['items_name']),"_").'_'.$data['id'].'_3';
                        $ext_thumbs=explode(".",basename(parse_url($data['item_image_three'])['path']));
                        $new_thumbs = $thumbspath.'/'.$name_three_thumbs.'.'.$ext_thumbs[1];
                        rename($oldthumbs, $new_thumbs);

                        $images_three=$thumbs.'/'.$name_three.'.'.$ext[1];
                    }else{
                        $images_three=$data['item_image_three'];
                    }
                }
                if($data['item_image_primary'] == $data['item_image_three']){
                    $primary = $images_three;
                }

                if($request->hasFile('imagefile_four')){
                    if(!empty($query['item_image_four']) and File::exists($path.'/'.basename(parse_url($query['item_image_four'])['path']))){
                        unlink($path.'/'.basename(parse_url($data['item_image_four'])['path']));
                        unlink($path.'/thumbs/'.basename(parse_url($query['item_image_four'])['path']));
                    }

                    $imgfile_four = Image::make($_FILES['imagefile_four']['tmp_name']);

                    $name_four=Str::slug(trim($data['items_name']),"_").'_'.$data['id'].'_4';
                    $extension_four = $request->file('imagefile_four')->getClientOriginalExtension();
                    $imgfile_four->save($path.'/'.$name_four.'.'.$extension_four);

                    //Thumb
                    $imgfile_four_thumb = Image::make($_FILES['imagefile_four']['tmp_name']);
                    $imgfile_four_thumb->fit(650, 650);
                    $name_four_thumb=Str::slug(trim($data['items_name']),"_").'_'.$data['id'].'_4';
                    $extension_four_thumb = $request->file('imagefile_four')->getClientOriginalExtension();
                    $imgfile_four_thumb->save($thumbspath.'/'.$name_four_thumb.'.'.$extension_four_thumb);
                    //End

                    $images_four=$thumbs.'/'.$name_four.'.'.$extension_four;

                }else{
                    if(!empty($data['item_image_four'])){

                        $old = $path.'/'.basename(parse_url($data['item_image_four'])['path']);
                        $name_four=Str::slug(trim($data['items_name']),"_").'_'.$data['id'].'_4';
                        $ext=explode(".",basename(parse_url($data['item_image_four'])['path']));
                        $new = $path.'/'.$name_four.'.'.$ext[1];
                        rename($old, $new);

                        $oldthumbs = $thumbspath.'/'.basename(parse_url($data['item_image_four'])['path']);
                        $name_four_thumbs=Str::slug(trim($data['items_name']),"_").'_'.$data['id'].'_4';
                        $ext_thumbs=explode(".",basename(parse_url($data['item_image_four'])['path']));
                        $new_thumbs = $thumbspath.'/'.$name_four_thumbs.'.'.$ext_thumbs[1];
                        rename($oldthumbs, $new_thumbs);

                        $images_four=$thumbs.'/'.$name_four.'.'.$ext[1];
                    }else{
                        $images_four=$data['item_image_four'];
                    }
                }
                if($data['item_image_primary'] == $data['item_image_four']){
                    $primary = $images_four;
                }
                $array_one=['items_name'=>$data['items_name'],
                        'items_slug'=>$data['id'].'-'.Str::slug(trim($data['items_name']),"-"),
                        'items_color'=>$data['items_color'],
                        'show_on_catalog'=>$data['show_on_catalog'],
                        'items_description'=>$data['items_description'],
                        'item_image_one'=>$images_one,
                        'item_image_two'=>$images_two,
                        'item_image_three'=>$images_three,
                        'item_image_four'=>$images_four,
                        'item_image_primary'=>$primary,
                        'show_on_catalog'=>trim($data['show_on_catalog']),
                        // 'items_price'=>$data['items_price'],
                        // 'items_discount'=>$data['items_discount'],
                        'items_youtube'=>$data['items_youtube'],
                        //'ready_stock'=>$data['ready_stock'],
                    ];
                Items::where('id',$data['id'])->update($array_one);
            });
         }
        catch(\Exception $e) {
            return false;
        }
        return true;
    }
    public static function update_material($request){
        try {
            DB::transaction(function () use ($request) {
                $data=$request->all();

                $array_one=['items_name'=>$data['items_name'],
                        'items_slug'=>$data['id'].'-'.Str::slug(trim($data['items_name']),"-"),
                        'ready_stock'=>$data['ready_stock'],
                        'item_unit'=>$data['item_unit']
                    ];
                Items::where('id',$data['id'])->update($array_one);
                
            });
         }
        catch(\Exception $e) {
            return false;
        }
        return true;
    }
    public static function delete_data($id){
        try {
            DB::transaction(function () use ($id) {
            	$query = Items::where('id',$id)->first();

            	$mainpath = myFunction::pathAsset();
            	$subpath = '/users/'.Auth::user()->id.'/items';
            	$path = $mainpath.$subpath;

            	if(!empty($query['item_image_one']) and File::exists($path.'/'.basename(parse_url($query['item_image_one'])['path']))){
            	    unlink($path.'/'.basename(parse_url($query['item_image_one'])['path']));
                    unlink($path.'/thumbs/'.basename(parse_url($query['item_image_one'])['path']));
            	}
                if(!empty($query['item_image_two']) and File::exists($path.'/'.basename(parse_url($query['item_image_two'])['path']))){
                    unlink($path.'/'.basename(parse_url($query['item_image_two'])['path']));
                    unlink($path.'/thumbs/'.basename(parse_url($query['item_image_two'])['path']));
                }
                if(!empty($query['item_image_three']) and File::exists($path.'/'.basename(parse_url($query['item_image_three'])['path']))){
                    unlink($path.'/'.basename(parse_url($query['item_image_three'])['path']));
                    unlink($path.'/thumbs/'.basename(parse_url($query['item_image_three'])['path']));
                }
                if(!empty($query['item_image_four']) and File::exists($path.'/'.basename(parse_url($query['item_image_four'])['path']))){
                    unlink($path.'/'.basename(parse_url($query['item_image_four'])['path']));
                    unlink($path.'/thumbs/'.basename(parse_url($query['item_image_four'])['path']));
                }
                Items::where('id',$id)->delete();
            });
         }
        catch(\Exception $e) {
            return false;
        }
        return true;
    }
    public static function delete_data_material($id){
        try {
            DB::transaction(function () use ($id) {
                $query = ItemsStock::where('item_id',$id)->first();
                if(!empty($query)){
                    Items::where('id',$id)->update(['ready_stock'=>'N']);
                }else{
                    Items::where('id',$id)->delete();
                }
            });
         }
        catch(\Exception $e) {
            return false;
        }
        return true;
    }
    public static function primary_image($id,$image){
        try {
            DB::transaction(function () use ($id,$image) {
                $mainpath = myFunction::pathAsset();
                $subpath = '/users/'.Auth::user()->id.'/items';
                $path = $mainpath.$subpath;

                $query = Items::where('id',$id)->update(['item_image_primary'=>$subpath.'/thumbs/'.$image]);
            });
         }
        catch(\Exception $e) {
            return false;
        }
        return true;
    }
    public static function delete_image($id,$image,$position){
        try {
            DB::transaction(function () use ($id,$image,$position) {
                $query = Items::where('id',$id)->first();

                $mainpath = myFunction::pathAsset();
                $subpath = '/users/'.Auth::user()->id.'/items';
                $path = $mainpath.$subpath;

                if(!empty($query['item_image_'].$position) and File::exists($path.'/'.$image)){
                    unlink($path.'/'.$image);
                    unlink($path.'/thumbs/'.$image);
                }

                $query = Items::where('id',$id)->update(['item_image_'.$position=>'']);
            });
         }
        catch(\Exception $e) {
            return false;
        }
        return true;
    }
}
