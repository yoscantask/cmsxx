<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use App\Http\Requests;
use App\Helper\myFunction;

use Image;
use Input;
use File;
use Auth;

class UserService extends Model
{
    public function customer(){
        return $this->belongsTo('App\Models\Customer', 'customer_id');
	}

    public static function update_data($request){
        try {
            DB::transaction(function () use ($request) {
                $data=$request->all();

                $query = UserService::where('id',$data['id'])->first();

                $mainpath = myFunction::pathAsset();
                $subpath = '/users/'.Auth::user()->id.'/items';
                $thumbs = '/users/'.Auth::user()->id.'/items/thumbs';
                $path = $mainpath.$subpath;
                $thumbspath = $mainpath.$thumbs;
                File::isDirectory($path) or File::makeDirectory($path, 0777, true, true);
                File::isDirectory($thumbspath) or File::makeDirectory($thumbspath, 0777, true, true);

                if($request->hasFile('image')){
                    if(!empty($query['image']) and File::exists($path.'/'.basename(parse_url($query['image'])['path']))){
                        @unlink($path.'/'.basename(parse_url($data['image'])['path']));
                        @unlink($path.'/thumbs/'.basename(parse_url($query['image'])['path']));
                    }

                    $imgfile_one = Image::make($_FILES['image']['tmp_name']);

                    $name_one=Str::slug(trim($data['handler']),"_").'_'.$data['id'].'_1';
                    $extension_one = $request->file('image')->getClientOriginalExtension();
                    $imgfile_one->save($path.'/'.$name_one.'.'.$extension_one);

                    //Thumb
                    $imgfile_one_thumb = Image::make($_FILES['image']['tmp_name']);
                    $imgfile_one_thumb->fit(650, 650);
                    $name_one_thumb=Str::slug(trim($data['handler']),"_").'_'.$data['id'].'_1';
                    $extension_one_thumb = $request->file('image')->getClientOriginalExtension();
                    $imgfile_one_thumb->save($thumbspath.'/'.$name_one_thumb.'.'.$extension_one_thumb);
                    //End

                    $images_one=$thumbs.'/'.$name_one.'.'.$extension_one;

                }else{
                    if(!empty($data['item_image'])){

                        $old = $path.'/'.basename(parse_url($data['item_image'])['path']);
                        $name_one=Str::slug(trim($data['handler']),"_").'_'.$data['id'].'_1';
                        $ext=explode(".",basename(parse_url($data['item_image'])['path']));
                        $new = $path.'/'.$name_one.'.'.$ext[1];
                        rename($old, $new);

                        $oldthumbs = $thumbspath.'/'.basename(parse_url($data['item_image'])['path']);
                        $name_one_thumbs=Str::slug(trim($data['handler']),"_").'_'.$data['id'].'_1';
                        $ext_thumbs=explode(".",basename(parse_url($data['item_image'])['path']));
                        $new_thumbs = $thumbspath.'/'.$name_one_thumbs.'.'.$ext_thumbs[1];
                        rename($oldthumbs, $new_thumbs);

                        $images_one=$thumbs.'/'.$name_one.'.'.$ext[1];
                    }else{
                        $images_one=$data['item_image'];
                    }
                }

                $handled_on = \Carbon\Carbon::createFromFormat('m-d-Y H:i', $data['handled_on']);

                $array_one=['handler'=>$data['handler'],
                        'job_result'=>$data['job_result'],
                        'handled_on'=>$handled_on,
                        'job_image'=>$images_one,
                    ];

                UserService::where('id',$data['id'])->update($array_one);
            });
         }
        catch(\Exception $e) {
            return false;
        }
        return true;
    }

    public static function delete_data($id){
        try {
            DB::transaction(function () use ($id) {
                $query = UserService::where('id',$id)->first();

            	$mainpath = myFunction::pathAsset();
            	$subpath = '/users/'.Auth::user()->id.'/items';
            	$path = $mainpath.$subpath;

            	if(!empty($query['image']) and File::exists($path.'/'.basename(parse_url($query['image'])['path']))){
            	    @unlink($path.'/'.basename(parse_url($query['image'])['path']));
                    @unlink($path.'/thumbs/'.basename(parse_url($query['image'])['path']));
            	}

                UserService::where('id',$id)->delete();
            });
         }
        catch(\Exception $e) {
            return false;
        }
        return true;
    }
}
