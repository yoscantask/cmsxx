<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use App\Http\Requests;
use App\Models\CatalogType;

use App\Helper\myFunction;

use Image;
use Input;
use File;
use Auth;
use Hash;

class Catalog extends Model
{
    protected $table = 'catalog';

    public function catalog_type(){
        return $this->hasMany('App\Models\CatalogType', 'catalog_id');
	}

    public static function save_data($request){
    	try {
    	    DB::transaction(function () use ($request) {
    	        $id = myFunction::id('catalog','id');
                $data=$request->all();

                $var=new Catalog;
                $var->id=$id;
                $var->user_id=Auth::user()->id;
                $var->domain=trim($data['domain']);
                $var->catalog_username=Str::slug(trim($data['catalog_username']),"");
                $var->catalog_title=$data['catalog_title'];
                $var->catalog_tagline=$data['catalog_tagline'];
                $var->layout=$data['layout'];
                $var->background_header_color=$data['background_color'];
                $var->sliders=(!empty($data['sliders']))?json_encode(explode(',', $data['sliders'])):'';
                $var->theme_color=$data['theme_color'];
                $var->show_detail=$data['show_detail'];
                $var->lat=$data['lat'];
                $var->long=$data['long'];
                $var->catalog_address=$data['catalog_address'];
                $var->distance=$data['distance'];
                $var->show_catalog=$data['show_catalog'];
                $var->phone_contact=$data['phone_contact'];
                $var->email_contact=$data['email_contact'];
                $var->catalog_password=Hash::make($data['catalog_password']);
                $var->feature=$data['feature'];
                $var->customer_data=$data['customer_data'];

                if($data['feature']=='Full'){
                    $var->checkout_type=$data['checkout_type'];
                    $var->wa_show_cart=$data['wa_show_cart'];
                    $var->wa_show_item=$data['wa_show_item'];
                    $var->wa_number=$data['wa_number'];
                    $var->steps=(!empty($data['steps']))?json_encode(explode(',', $data['steps'])):'';
                    $var->advance_payment=$data['advance_payment'];
                    $var->transfer_payment=$data['transfer_payment'];
                    $var->bank_info=$data['bank_info'];
                    $var->payment_gateway=$data['payment_gateway'];
                    $var->tax=$data['tax'];
                    $var->client_key=$data['client_key'];
                    $var->server_key=$data['server_key'];
                }
                $var->save();

                if($request->hasFile('logo')){
                    $mainpath = myFunction::pathAsset();
                    $subpath = '/users/'.Auth::user()->id.'/catalog';
                    $path = $mainpath.$subpath;

                    File::isDirectory($path) or File::makeDirectory($path, 0777, true, true);

                    $logofile = Image::make($_FILES['logo']['tmp_name']);
                    $logofile->resize(120, null, function ($constraint) {
                        $constraint->aspectRatio();
                    });

                    $name=Str::slug(trim($data['catalog_title']),"_").'_'.time();
                    $extension = $request->file('logo')->getClientOriginalExtension();
                    $logofile->save($path.'/'.$name.'.'.$extension);
                    $images=$subpath.'/'.$name.'.'.$extension;

                    $array=['catalog_logo'=>$images];
                    Catalog::where('id',$id)->update($array);
                }

                if($request->hasFile('catalogbg')){
                    $mainpath = myFunction::pathAsset();
                    $subpath = '/users/'.Auth::user()->id.'/catalog';
                    $path = $mainpath.$subpath;

                    File::isDirectory($path) or File::makeDirectory($path, 0777, true, true);

                    $headerbg = Image::make($_FILES['catalogbg']['tmp_name']);
                    $headerbg->resize(1920, null, function ($constraint) {
                        $constraint->aspectRatio();
                    });

                    $name='header_'.Str::slug(trim($data['catalog_title']),"_").'_'.time();
                    $extension = $request->file('catalogbg')->getClientOriginalExtension();
                    $headerbg->save($path.'/'.$name.'.'.$extension);
                    $images=$subpath.'/'.$name.'.'.$extension;

                    $array=['background_header_image'=>$images];
                    Catalog::where('id',$id)->update($array);
                }
    	    });
    	 }
    	catch(\Exception $e) {
    	    return false;
    	}
    	return true;
    }
    public static function update_data($request){
    	try {
    	    DB::transaction(function () use ($request) {
    	        $data=$request->all();
    	        $query = Catalog::where('id',$data['id'])->first();
    	        $mainpath = myFunction::pathAsset();
                $subpath = '/users/'.Auth::user()->id.'/catalog';
                $path = $mainpath.$subpath;
                File::isDirectory($path) or File::makeDirectory($path, 0777, true, true);

    	        if($request->hasFile('logo')){
                    if(!empty($query['catalog_logo']) and File::exists($path.'/'.basename(parse_url($query['catalog_logo'])['path']))){
                        unlink($path.'/'.basename(parse_url($data['catalog_logo'])['path']));
                    }

                    $imgfile = Image::make($_FILES['logo']['tmp_name']);
                    $imgfile->resize(120, null, function ($constraint) {
                        $constraint->aspectRatio();
                    });
                    $name=Str::slug(trim($data['catalog_title']),"_").'_'.time();
                    $extension = $request->file('logo')->getClientOriginalExtension();
                    $imgfile->save($path.'/'.$name.'.'.$extension);
                    $logo=$subpath.'/'.$name.'.'.$extension;
                }else{
                    if(!empty($data['catalog_logo'])){
                        $old = $path.'/'.basename(parse_url($data['catalog_logo'])['path']);
                        $name=Str::slug(trim($data['catalog_title']),"_").'_'.time();
                        $ext=explode(".",basename(parse_url($data['catalog_logo'])['path']));
                        $new = $path.'/'.$name.'.'.$ext[1];
                        @rename($old, $new);
                        $logo=$subpath.'/'.$name.'.'.$ext[1];
                    }else{
                        $logo=$data['catalog_logo'];
                    }
                }
                
                if($request->hasFile('catalogbg')){
                    if(!empty($query['background_header_image']) and File::exists($path.'/'.basename(parse_url($query['background_header_image'])['path']))){
                        unlink($path.'/'.basename(parse_url($data['background_header_image'])['path']));
                    }

                    $headerbg = Image::make($_FILES['catalogbg']['tmp_name']);
                    $headerbg->resize(1920, null, function ($constraint) {
                        $constraint->aspectRatio();
                    });
                    $name='header_'.Str::slug(trim($data['catalog_title']),"_").'_'.time();
                    $extension = $request->file('catalogbg')->getClientOriginalExtension();
                    $headerbg->save($path.'/'.$name.'.'.$extension);
                    $backgroundheader=$subpath.'/'.$name.'.'.$extension;
                }else{
                    if(!empty($data['background_header_image'])){
                        $old = $path.'/'.basename(parse_url($data['background_header_image'])['path']);
                        $name='header_'.Str::slug(trim($data['catalog_title']),"_").'_'.time();
                        $ext=explode(".",basename(parse_url($data['background_header_image'])['path']));
                        $new = $path.'/'.$name.'.'.$ext[1];
                        rename($old, $new);
                        $backgroundheader=$subpath.'/'.$name.'.'.$ext[1];
                    }else{
                        $backgroundheader=$data['background_header_image'];
                    }
                }

                $temp_pay_opt = !empty($data['pay_opt']) ? explode(',', $data['pay_opt']) : [];
                $is_any_delivery_option = false;

                if($data['feature']=='Full' && $data['advance_payment'] != "Y"){
                    if($temp_pay_opt){
                        foreach ($temp_pay_opt as $key => $value) {
                            if($value < 3){
                                unset($temp_pay_opt[$key]);
                            }
                        }
                    }
                }
                
                if(!in_array(1, $temp_pay_opt)){
                    $data['delivery_option'] = null;
                }
                
    	        $array=	[
    	        			'catalog_type'=>$data['catalog_type'],
    	        			'domain'=>$data['domain'],
    	        			'catalog_username'=>Str::slug(trim($data['catalog_username']),""),
    	        			'catalog_title'=>$data['catalog_title'],
    	        			'catalog_logo'=>$logo,
                            'catalog_tagline'=>$data['catalog_tagline'],
    	        			'background_header_color'=>$data['background_color'],
    	        			'background_header_image'=>$backgroundheader,
    	        			'sliders'=>(!empty($data['sliders']))?json_encode(explode(',', $data['sliders'])):'',
    	        			'payment_option'=>(!empty($temp_pay_opt))?json_encode($temp_pay_opt):'',
    	        			'layout'=>$data['layout'],
                            'theme_color'=>$data['theme_color'],
                            'show_detail'=>$data['show_detail'],
                            'lat'=>$data['lat'],
                            'long'=>$data['long'],
                            'catalog_address'=>$data['catalog_address'],
                            'distance'=>$data['distance'],
                            'show_catalog'=>$data['show_catalog'],
                            'phone_contact'=>$data['phone_contact'],
                            'email_contact'=>$data['email_contact'],
                            'catalog_password'=>Hash::make($data['catalog_password']),
                            'feature'=>$data['feature'],
                            'customer_data'=>$data['customer_data'],
    	    			];
    	        Catalog::where('id',$data['id'])->update($array);

                if($data['feature']=='Full'){
                    $arrayadvance= [
                        'checkout_type'=>$data['checkout_type'],
                        'wa_show_cart'=>$data['wa_show_cart'],
                        'wa_show_item'=>$data['wa_show_item'],
                        'wa_number'=>$data['wa_number'],
                        'steps'=>(!empty($data['steps']))?json_encode(explode(',', $data['steps'])):'',
                        'advance_payment'=>$data['advance_payment'],
                        'transfer_payment'=>$data['transfer_payment'],
                        'delivery_option'=>$data['delivery_option'],
                        'bank_info'=>$data['bank_info'],
                        'payment_gateway'=>$data['payment_gateway'],
                        'tax'=>$data['tax'],
                        'client_key'=>$data['client_key'],
                        'server_key'=>$data['server_key'],
                    ];
                    Catalog::where('id',$data['id'])->update($arrayadvance);
                }

                $catalog_list = (!empty($data['catalog_list']))?(explode(',', $data['catalog_list'])):'';

                if($data['catalog_type'] == 2){
                    if($catalog_list){

                        CatalogType::where('catalog_id', $data['id'])->delete();

                        $insert_type = [];
                        $order = 0;
                        foreach ($catalog_list as $key => $value) {
                            if($value > 0){
                                $insert_type[] = [
                                    'catalog_id' => $data['id'],
                                    'catalog_list_type_id' => $value,
                                    'order' => $order
                                    // 'label'
                                    // 'description'
                                    // 'image'
                                ];
                                $order++;
                            }
                        }
                        
                        if($insert_type){
                            CatalogType::insert($insert_type);
                        }
                    }
                }
    	    });
    	 }
    	catch(\Exception $e) {
    	    return false;
    	}
    	return true;
    }
    public static function delete_data($id){
        try {
            DB::transaction(function () use ($id) {
            	$query = Catalog::where('id',$id)->first();

            	$mainpath = myFunction::pathAsset();
            	$subpath = '/users/'.Auth::user()->id.'/catalog';
            	$path = $mainpath.$subpath;

            	if(!empty($query['catalog_logo']) and File::exists($path.'/'.basename(parse_url($query['catalog_logo'])['path']))){
            	    unlink($path.'/'.basename(parse_url($query['catalog_logo'])['path']));
            	}
            	if(!empty($query['background_header_image']) and File::exists($path.'/'.basename(parse_url($query['background_header_image'])['path']))){
            	    unlink($path.'/'.basename(parse_url($query['background_header_image'])['path']));
            	}
                Catalog::where('id',$id)->delete();
            });
         }
        catch(\Exception $e) {
            return false;
        }
        return true;
    }
}
