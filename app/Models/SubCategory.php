<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use App\Http\Requests;

use App\Helper\myFunction;

use Image;
use Input;
use File;
use Auth;

class SubCategory extends Model
{
    protected $table = 'subcategory';

    public static function save_data($request){
    	try {
    	    DB::transaction(function () use ($request) {
    	        $id = myFunction::id('subcategory','id');
                $data=$request->all();
                $var=new SubCategory;
                $var->id=$id;
                $var->user_id=Auth::user()->id;
                $var->subcategory_name=trim($data['subcategory_name']);
                $var->show_on_catalog=trim($data['show_on_catalog']);
                $var->subcategory_slug=$id.'-'.Str::slug(trim($data['subcategory_name']),"-");
                $var->subcategory_color=$data['subcategory_color'];
                $var->save();

                if($request->hasFile('imagefile')){
                    $mainpath = myFunction::pathAsset();
                    $subpath = '/users/'.Auth::user()->id.'/subcategory';
                    $path = $mainpath.$subpath;

                    File::isDirectory($path) or File::makeDirectory($path, 0777, true, true);

                    $imgfile = Image::make($_FILES['imagefile']['tmp_name']);
                    $imgfile->fit(350, 350);

                    $name=Str::slug(trim($data['subcategory_name']),"_").'_'.time();
                    $extension = $request->file('imagefile')->getClientOriginalExtension();
                    $imgfile->save($path.'/'.$name.'.'.$extension);
                    $images=$subpath.'/'.$name.'.'.$extension;

                    $array=['subcategory_image'=>$images];
                    SubCategory::where('id',$id)->update($array);
                }
    	    });
    	 }
    	catch(\Exception $e) {
    	    return false;
    	}
    	return true;
    }
    public static function update_data($request){
        try {
            DB::transaction(function () use ($request) {
                $data=$request->all();

                $query = SubCategory::where('id',$data['id'])->first();

                $mainpath = myFunction::pathAsset();
                $subpath = '/users/'.Auth::user()->id.'/subcategory';
                $path = $mainpath.$subpath;
                File::isDirectory($path) or File::makeDirectory($path, 0777, true, true);

                if($request->hasFile('imagefile')){
                    if(!empty($query['subcategory_image']) and File::exists($path.'/'.basename(parse_url($query['subcategory_image'])['path']))){
                        unlink($path.'/'.basename(parse_url($data['subcategory_image'])['path']));
                    }
                    
                    $imgfile = Image::make($_FILES['imagefile']['tmp_name']);
                    $imgfile->fit(350, 350);
                    $name=Str::slug(trim($data['subcategory_name']),"_").'_'.time();
                    $extension = $request->file('imagefile')->getClientOriginalExtension();
                    $imgfile->save($path.'/'.$name.'.'.$extension);
                    $image=$subpath.'/'.$name.'.'.$extension;
                }else{
                    if(!empty($data['subcategory_image'])){
                        $old = $path.'/'.basename(parse_url($data['subcategory_image'])['path']);
                        $name=Str::slug(trim($data['subcategory_name']),"_").'_'.time();
                        $ext=explode(".",basename(parse_url($data['subcategory_image'])['path']));
                        $new = $path.'/'.$name.'.'.$ext[1];
                        rename($old, $new);
                        $image=$subpath.'/'.$name.'.'.$ext[1];
                    }else{
                        $image=$data['subcategory_image'];
                    }
                }

                $array=['subcategory_name'=>$data['subcategory_name'],
                        'show_on_catalog'=>$data['show_on_catalog'],
                        'subcategory_slug'=>$data['id'].'-'.Str::slug(trim($data['subcategory_name']),"-"),
                        'subcategory_color'=>$data['subcategory_color'],
                    	'subcategory_image'=>$image];
                SubCategory::where('id',$data['id'])->update($array);
            });
         }
        catch(\Exception $e) {
            return false;
        }
        return true;
    }
    public static function delete_data($id){
        try {
            DB::transaction(function () use ($id) {
            	$query = SubCategory::where('id',$id)->first();

            	$mainpath = myFunction::pathAsset();
            	$subpath = '/users/'.Auth::user()->id.'/subcategory';
            	$path = $mainpath.$subpath;

            	if(!empty($query['subcategory_image']) and File::exists($path.'/'.basename(parse_url($query['subcategory_image'])['path']))){
            	    unlink($path.'/'.basename(parse_url($query['subcategory_image'])['path']));
            	}
                SubCategory::where('id',$id)->delete();
            });
         }
        catch(\Exception $e) {
            return false;
        }
        return true;
    }
}
