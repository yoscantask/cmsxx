<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PricePayment extends Model
{
    protected $table = "price_payments";
    public $timestamps = true;
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'nama', 'id_payment_cat', 'created_at', 'update_at', 'methode', 'status', 'ket'
	];
}
