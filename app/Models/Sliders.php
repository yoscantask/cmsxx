<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use App\Http\Requests;

use App\Helper\myFunction;

use Image;
use Input;
use File;
use Auth;

class Sliders extends Model
{
    protected $table = 'sliders';

    public static function save_data($request){
    	try {
    	    DB::transaction(function () use ($request) {
    	        $id = myFunction::id('sliders','id');
                $data=$request->all();
                $var=new Sliders;
                $var->id=$id;
                $var->user_id=Auth::user()->id;
                $var->sliders_title=trim($data['sliders_title']);
                $var->save();

                if($request->hasFile('imagefile')){
                    $mainpath = myFunction::pathAsset();
                    $subpath = '/users/'.Auth::user()->id.'/sliders';
                    $path = $mainpath.$subpath;

                    File::isDirectory($path) or File::makeDirectory($path, 0777, true, true);

                    $imgfile = Image::make($_FILES['imagefile']['tmp_name']);
                    $imgfile->fit(1920, 1280);

                    $name=Str::slug(trim($data['sliders_title']),"_").'_'.time();
                    $extension = $request->file('imagefile')->getClientOriginalExtension();
                    $imgfile->save($path.'/'.$name.'.'.$extension);
                    $images=$subpath.'/'.$name.'.'.$extension;

                    $array=['sliders_image'=>$images];
                    Sliders::where('id',$id)->update($array);
                }
    	    });
    	 }
    	catch(\Exception $e) {
    	    return false;
    	}
    	return true;
    }
    public static function update_data($request){
        try {
            DB::transaction(function () use ($request) {
                $data=$request->all();

                $query = Sliders::where('id',$data['id'])->first();

                $mainpath = myFunction::pathAsset();
                $subpath = '/users/'.Auth::user()->id.'/sliders';
                $path = $mainpath.$subpath;
                File::isDirectory($path) or File::makeDirectory($path, 0777, true, true);

                if($request->hasFile('imagefile')){
                    if(!empty($query['sliders_image']) and File::exists($path.'/'.basename(parse_url($query['sliders_image'])['path']))){
                        unlink($path.'/'.basename(parse_url($data['sliders_image'])['path']));
                    }
                    
                    $imgfile = Image::make($_FILES['imagefile']['tmp_name']);
                    $imgfile->fit(1920, 1280);
                    $name=Str::slug(trim($data['sliders_title']),"_").'_'.time();
                    $extension = $request->file('imagefile')->getClientOriginalExtension();
                    $imgfile->save($path.'/'.$name.'.'.$extension);
                    $image=$subpath.'/'.$name.'.'.$extension;
                }else{
                    if(!empty($data['sliders_image'])){
                        $old = $path.'/'.basename(parse_url($data['sliders_image'])['path']);
                        $name=Str::slug(trim($data['sliders_title']),"_").'_'.time();
                        $ext=explode(".",basename(parse_url($data['sliders_image'])['path']));
                        $new = $path.'/'.$name.'.'.$ext[1];
                        rename($old, $new);
                        $image=$subpath.'/'.$name.'.'.$ext[1];
                    }else{
                        $image=$data['sliders_image'];
                    }
                }

                $array=['sliders_title'=>$data['sliders_title'],
                    	'sliders_image'=>$image];
                Sliders::where('id',$data['id'])->update($array);
            });
         }
        catch(\Exception $e) {
            return false;
        }
        return true;
    }
    public static function delete_data($id){
        try {
            DB::transaction(function () use ($id) {
            	$query = Sliders::where('id',$id)->first();

            	$mainpath = myFunction::pathAsset();
            	$subpath = '/users/'.Auth::user()->id.'/sliders';
            	$path = $mainpath.$subpath;

            	if(!empty($query['sliders_image']) and File::exists($path.'/'.basename(parse_url($query['sliders_image'])['path']))){
            	    unlink($path.'/'.basename(parse_url($query['sliders_image'])['path']));
            	}
                Sliders::where('id',$id)->delete();
            });
         }
        catch(\Exception $e) {
            return false;
        }
        return true;
    }
}
