<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class PricePaymentCat extends Model
{
    protected $table = "price_payment_cats";
    public $timestamps = true;
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'nama', 'description', 'created_at', 'update_at', 'status'
	];
}
