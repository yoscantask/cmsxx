<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

use App\User;

use Auth;
use Session;

class ProfileController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
		app()->setLocale(Session::get('locale'));
        $email = Auth::user()->email; $id = Auth::id();
        $data['titlepage']='Profile';
		$data['maintitle']=__('lang.myprofile');
		$data['id']=$id;
		$data['check']="index";
        $data['biodata'] = DB::table('users')
            ->where('id', '=', $id, 'OR', 'email', '=', $email)->first();
        return view('profile', $data);
    }

    public function update(Request $request, $id) {
        if ($request->password == null && $request->oldpassword == null) {
            $updatepro = DB::table('users')
                ->where('id', '=', $id)
                ->update(
                [
                    'name' => $request->name,
                    'phone' => $request->phone,
                    'address' => $request->address
                ]
                );
                if($updatepro) {
                    $data['check']="Update Profile Berhasil";
                } else {
                    $data['check']="Update Profile Gagal";
                }
            } elseif ($request->oldpassword != null) {
                $user = User::where('email', '=', Auth::user()->email)->first();
                
                if(\Hash::check($request->oldpassword, $user->password)) {
                    $data['check']="Password Lama Sesuai, Silahkan Input Password Baru Anda";
                }else{
                    $data['check']="Password Salah";
                }

            } elseif ($request->password != null) {
                $updatepass = DB::table('users')
                ->where('id', '=', $id)
                ->update([
                    'password' => \Hash::make($request->password),
                ]);
                if($updatepass) {
                    $data['check']="Update Password Berhasil";
                } else {
                    $data['check']="Update Password Gagal";
                }   
        }        
        $email = Auth::user()->email; $id = Auth::id();
        $data['titlepage']='Profile';
        $data['maintitle']=__('lang.myprofile');
        $data['id']=$id;
        $data['biodata'] = DB::table('users')
            ->where('id', '=', $id, 'OR', 'email', '=', $email)->first();
        return view('profile', $data);
    }
}
