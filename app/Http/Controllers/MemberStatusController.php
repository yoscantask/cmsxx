<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Models\MemberStatus;

use Auth;
use Carbon\Carbon;
use Session;

class MemberStatusController extends Controller
{
  public function __construct()
  {
      $this->middleware('auth');
  }
    public function store(Request $request, $id) {
        $check = MemberStatus::where('username', '=', $id)
        ->where('status', '=', 'Pending')
        ->where('paket_id', '=', $request->paket)
        ->get();
        
        if (!$check->isEmpty()) {
            $data['titlepage']='Perpanjang Member';
            $data['maintitle']='Perpanjang Member';
            $data['username']=Auth::user()->username;
            $data['id']=Auth::user()->id;
            $data['check']='Your Renewel Member was Rejected, Please finish your previous request';
            $data['paket'] = DB::table('package_price')
                ->join('package', 'package_price.package_id', '=', 'package.id')
                ->select('package_price.id', 'package.package_name', 'package_price.price', 'package_price.notes', 'package_price.period', 'package_price.unit')
                ->where('package_price.package_id', '!=', '1')
                ->orderBy('package_price.package_id','asc')
                ->get();
            return view('pages.memberstatus.perpanjang',$data);
        } else {
            $create = DB::table('member_statuses')->insert([
                'username' => $id,
                'paket_id' => $request->paket,
                'expdate' => NOW()->addDays(2),
                'status' => 'Pending',
                'created_at' => NOW()
            ]);
            return redirect(route('memberstatus.index'));
        }
    }

    public function perpanjang() {
        app()->setLocale(Session::get('locale'));
        $data['titlepage']='Perpanjang Member';
        $data['maintitle']='Perpanjang Member';
        $data['username']=Auth::user()->username;
        $data['id']=Auth::user()->id;
        $data['check']=null;
        $data['paket'] = DB::table('package_price')
            ->join('package', 'package_price.package_id', '=', 'package.id')
            ->select('package_price.id', 'package.package_name', 'package_price.price', 'package_price.notes', 'package_price.period', 'package_price.unit')
            ->where('package_price.package_id', '!=', '1')
            ->orderBy('package_price.package_id','asc')
            ->get();
        return view('pages.memberstatus.perpanjang',$data);
    }

    public function index(Request $request) {
        app()->setLocale(Session::get('locale'));
        $data['titlepage']=__('lang.history').' Member';
        $data['maintitle']=__('lang.history').' Member';
        return view('pages.memberstatus.data',$data);
    }
    
    public function getData(Request $request) {
        if(Auth::user()->level == 'Super Admin') {
            $columns = ['username','paket_id','status'];
            $keyword = trim($request->input('searchfield'));
            $query = MemberStatus::join('users', 'users.id', '=', 'member_statuses.username')
                            ->join('package_price', 'package_price.id', '=', 'member_statuses.paket_id')
                            ->join('package', 'package_price.package_id', '=', 'package.id')
                            ->where(function($result) use ($keyword,$columns){
                                foreach($columns as $column)
                                {
                                    if($keyword != ''){
                                        $result->orWhere('users.'.$columns ,'LIKE','%'.$keyword.'%');
                                    }
                                }
                            })
                            ->select('member_statuses.id', 'users.username', 'package.package_name', 'member_statuses.status', 'member_statuses.expdate', 'member_statuses.created_at', 
                            'package_price.period', 'package_price.unit', 'package_price.price')
                            ->orderBy('id','desc');
            $data['request'] = $request->all();
            $data['getData'] = $query->paginate(10);
            $data['pagination'] = $data['getData']->links();
        } elseif (Auth::user()->level == 'Member') {
            $query = MemberStatus::join('users', 'users.id', '=', 'member_statuses.username')
                    ->join('package_price', 'package_price.id', '=', 'member_statuses.paket_id')
                    ->join('package', 'package_price.package_id', '=', 'package.id')
                    ->where('member_statuses.username', '=', Auth::user()->id)
                    ->select('member_statuses.id', 'users.username', 'package.package_name', 'member_statuses.status', 'member_statuses.expdate', 'member_statuses.created_at', 
                    'package_price.period', 'package_price.unit', 'package_price.price')
                    ->orderBy('member_statuses.id','desc');
            $data['getData'] = $query->paginate(10);
            $data['pagination'] = $data['getData']->links();            
        }

        $querypen = MemberStatus::whereDate('expdate','<=', NOW())
        ->where('status', 'Pending')
        ->get();

        $querypaid = MemberStatus::whereDate('expdate','<=', NOW())
        ->where('status', 'Paid')
        ->get();

        $querystat = $querypen->union($querypaid);

        foreach ($querystat as $item) {
            DB::table('member_statuses')
            ->where('id', $item->id)
            ->update([
                'status' => 'Expire',
                'updated_at' => NOW()
            ]);
        }

        return view('pages.memberstatus.table',$data);
    }

    public function show($id){
        $query = MemberStatus::join('users', 'users.id', '=', 'member_statuses.username')
                    ->join('package_price', 'package_price.id', '=', 'member_statuses.paket_id')
                    ->join('package', 'package_price.package_id', '=', 'package.id')
                    ->where('member_statuses.id', '=', $id)
                    ->select('member_statuses.id', 'users.username', 'package.package_name', 'member_statuses.status', 'member_statuses.expdate', 'member_statuses.created_at', 
                    'package_price.period', 'package_price.unit', 'package_price.price')
        ->first();
        return $query;
    }
    public function update(Request $request, $id) {

    }
    public function block($id,$active)
    {
        if(MemberStatus::block_user($id,$active)){
            $status='success';
            $message='Your request was successful.';
        }else{
            $status='error';
            $message='Oh snap! something went wrong.';
        }
        $notif=['status'=>$status,'message'=>$message];
        return response()->json($notif);
    }
}
