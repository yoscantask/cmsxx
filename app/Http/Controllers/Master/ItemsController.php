<?php

namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Models\PriceList;
use App\Models\Items;
use App\Models\ItemsDetail;
use App\Models\Category;
use App\Models\Recipe;
use App\Models\ItemsStock;
use App\User;

use Auth;
use Session;

class ItemsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $data['categoryadd'] = Category::where('user_id',Auth::user()->id)->where('category_type','Add')->get();
        // $data['addons'] = Items::where('user_id',Auth::user()->id)->where('item_type','Add')->orderBy('id','desc')->get();
        $data['titlepage']='Items';
        $data['maintitle']='Manage Items';
        return view('pages.master.items.data',$data);
    }

    public function getData(Request $request){
        $columns = ['items_name'];
        $keyword = trim($request->input('searchfield'));
        $query = Items::where('user_id',Auth::user()->id)
                        ->where('item_type','Main')
                        ->where(function($result) use ($keyword,$columns){
                            foreach($columns as $column)
                            {
                                if($keyword != ''){
                                    $result->orWhere($column,'LIKE','%'.$keyword.'%');
                                }
                            }
                        })
                        ->orderBy('id','desc');
        $data['request'] = $request->all();
        $data['getData'] = $query->paginate(10);
        $data['pagination'] = $data['getData']->links();
        return view('pages.master.items.table',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      // $status='error';
      // $message=$request->show_on_catalog;
        $this->validate($request, [
            'items_name' => 'required|min:3',
            'imagefile_one' => 'required|max:1000|mimes:jpeg,jpg,png',
            //'items_description' => 'required|min:20',
            'items_youtube' => 'nullable|url',
            'items_price' => 'required|numeric|min:500|not_in:499',
            'items_discount' => 'required|numeric|min:0|not_in:-1',
        ]);
        if ($request->hasFile('imagefile_two')) {
            $this->validate($request, [
                'imagefile_two' => 'required|max:1000|mimes:jpeg,jpg,png'
            ]);
        }
        elseif ($request->hasFile('imagefile_three')) {
            $this->validate($request, [
                'imagefile_three' => 'required|max:1000|mimes:jpeg,jpg,png'
            ]);
        }
        elseif ($request->hasFile('imagefile_four')) {
            $this->validate($request, [
                'imagefile_four' => 'required|max:1000|mimes:jpeg,jpg,png'
            ]);
        }
        if(Items::save_data($request)){
            // $status='success';
            // $message='Your request was successful.';
            
            $ceklastid = Items::latest('id')->first();

            $create = DB::table('price_lists')->insert(
              [
                  'id_item' => $ceklastid->id,
                  'id_payment' => '4',
                  'price' => $request->items_price,
                  'diskon' => $request->items_discount,
                  'created_at' => NOW(),
                  'updated_at' => NOW()
              ]
            );
            
            if($create){
                $status='success';
                $message=__('lang.create').' data '.__('lang.success');
            }else{
                $status='error';
                $message=__('lang.create').' data '.__('lang.failed');
            }
        }else{
            $status='error';
            $message='Oh snap! something went wrong.';
        }
        $notif=['status'=>$status,'message'=>$message];
        return response()->json($notif);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $query=Items::select('items.*', 'price_lists.price', 'price_lists.diskon')
                    ->leftJoin('price_lists','price_lists.id_item','=','items.id')
                    ->where('items.id',$id)
                    ->where('price_lists.id_payment', '=', '4')
                    ->first();
        
        if ($query) {} else {
          $query = Items::where('id', $id)->first();
        }
        //$query['price'] = PriceList::where('id_item', '=', $id)->skip(1)->first();
        return $query;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {        
      $request->input('checkCatalog');
        $this->validate($request, [
            'items_name' => 'required|min:3',
            //'items_description' => 'required|min:20',
            // 'items_price' => 'required|numeric|min:500|not_in:499',
            // 'items_discount' => 'required|numeric|min:0|not_in:-1',
            'items_youtube' => 'nullable|url',
        ]);
        if ($request->hasFile('imagefile_one')) {
            $this->validate($request, [
                'imagefile_one' => 'required|max:1000|mimes:jpeg,jpg,png'
            ]);
        }
        elseif ($request->hasFile('imagefile_two')) {
            $this->validate($request, [
                'imagefile_two' => 'required|max:1000|mimes:jpeg,jpg,png'
            ]);
        }
        elseif ($request->hasFile('imagefile_three')) {
            $this->validate($request, [
                'imagefile_three' => 'required|max:1000|mimes:jpeg,jpg,png'
            ]);
        }
        elseif ($request->hasFile('imagefile_four')) {
            $this->validate($request, [
                'imagefile_four' => 'required|max:1000|mimes:jpeg,jpg,png'
            ]);
        }
        if(Items::update_data($request)){
            $status='success';
            $message='Your request was successful.';
        }else{
            $status='error';
            $message='Oh snap! something went wrong.';
        }
        $notif=['status'=>$status,'message'=>$message];
        return response()->json($notif);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(Items::delete_data($id)){
            $status='success';
            $message='Your request was successful.';
        }else{
            $status='error';
            $message='Oh snap! something went wrong.';
        }
        $notif=['status'=>$status,'message'=>$message];
        return response()->json($notif);
    }

    public function gallery($id){
        $data['getData'] = $this->show($id);
        return view('pages.master.items.gallery',$data);
    }

    public function primaryimage($id,$image){
        if(Items::primary_image($id,$image)){
            $status='success';
            $message='Your request was successful.';
        }else{
            $status='error';
            $message='Oh snap! something went wrong.';
        }
    }
    public function deleteimage($id,$image,$position){
        if(Items::delete_image($id,$image,$position)){
            $status='success';
            $message='Your request was successful.';
        }else{
            $status='error';
            $message='Oh snap! something went wrong.';
        }
    }
    public function addons(Request $request,$id=null){
      $data['item']=$this->show($id);
      if($request->isMethod('post')){
        $data['detail'] = ItemsDetail::select('items_detail.*',
                        'category.category_name',
                        )
                        ->leftJoin('category','items_detail.category_id','=','category.id')
                        ->where('item_id',$id)
                        ->groupBy('category_id')
                        ->get();
        return view('pages.master.items.tableaddons',$data);
      }else{
        $data['titlepage']='Manage Add Ons';
        $data['maintitle']='Manage Add Ons';
        return view('pages.master.items.addons',$data);
      }
    }
    public function addaddons($id=null, Request $request){
        if($request->isMethod('post')){
          $this->validate($request, [
            'category_id' => 'required',
          ]);
          if(ItemsDetail::save_data($request)){
            $status='success';
            $message='Your request was successful.';
          }else{
            $status='error';
            $message='Oh snap! something went wrong.';
          }
          $notif=['status'=>$status,'message'=>$message];
          return response()->json($notif);
        }else{
          $data['itemid']=$id;
          $data['category'] = Category::where('user_id',Auth::user()->id)->where('category_type','Add')->get();
          $data['addons'] = Items::where('user_id',Auth::user()->id)->where('item_type','Add')->orderBy('id','desc')->get();
          return view('pages.master.items.formaddons',$data);
        }
    }
    public function deleteaddons($item=null, $addon=null){
      if(ItemsDetail::delete_data($addon)){
        $status='success';
        $message='Your request was successful.';
      }else{
        $status='error';
        $message='Oh snap! something went wrong.';
      }
      $notif=['status'=>$status,'message'=>$message];
      return response()->json($notif);
    }
    public function checkaddons($item,$addons)
    {
        $query = ItemsDetail::where('item_id',$item)->get();
        if(count((array)$query) > 0){
            foreach($query as $value){
              if($value['addon'] == $addons){
                return 1;
              }
            }
        }else{
            return 0;
        }
    }
    public function ingredient(Request $request,$id=null){
      $child = User::select('id')->where('parent_id',Auth::user()->id)->get();
      $userdata = array_merge(json_decode($child,true),[['id'=>Auth::user()->id]]);
      $data['detail'] = Recipe::select('recipe.*',
                          'items.items_name',
                          'items.item_unit',
                      )
                      ->leftJoin('items','recipe.item_id','=','items.id')
                      ->where('parent_id',$id)
                      ->where('items.ready_stock','Y')
                      ->get();
      $data['item']=$this->show($id);
      $data['material'] = ItemsStock::select('items_stock.*','items.id as itemid','items.items_name')
                                ->leftJoin('items','items_stock.item_id','items.id')
                                ->whereIn('items_stock.user_id',$userdata)
                                ->where('items_stock.catalog',Session::get('catalogsession'))
                                ->where('items.item_type','Material')
                                ->where('items.ready_stock','Y')
                                ->groupBy('items_stock.item_id')
                                ->orderBy('items.items_name','asc')
                                ->get();
      if($request->isMethod('post')){
        return view('pages.master.items.tableingredient',$data);
      }else{
        $data['titlepage']='Manage Ingredient';
        $data['maintitle']='Manage Ingredient';
        return view('pages.master.items.ingredient',$data);
      }
    }
    public function detailingredient($id=null){
      $query = Recipe::select('recipe.*',
                          'items.items_name',
                          'items.item_unit',
                      )
                      ->leftJoin('items','recipe.item_id','=','items.id')
                      ->where('recipe.id',$id)
                      ->first();
      return $query;
    }
    public function addingredient($id=null, Request $request){
      $this->validate($request, [
        'item_id' => 'required',
        'serving_size' => 'numeric|min:1|max:100000',
      ]);
      if(Recipe::save_data($request)){
        $status='success';
        $message='Your request was successful.';
      }else{
        $status='error';
        $message='Oh snap! something went wrong.';
      }
      $notif=['status'=>$status,'message'=>$message];
      return response()->json($notif);
    }
    public function updateingredient($id=null, Request $request){
      $this->validate($request, [
        'item_id' => 'required',
        'serving_size' => 'numeric|min:1|max:100000',
      ]);
      if(Recipe::update_data($request)){
        $status='success';
        $message='Your request was successful.';
      }else{
        $status='error';
        $message='Oh snap! something went wrong.';
      }
      $notif=['status'=>$status,'message'=>$message];
      return response()->json($notif);
    }
    public function deleteingredient($item=null, $addon=null){
      if(Recipe::delete_data($addon)){
        $status='success';
        $message='Your request was successful.';
      }else{
        $status='error';
        $message='Oh snap! something went wrong.';
      }
      $notif=['status'=>$status,'message'=>$message];
      return response()->json($notif);
    }
}
