<?php

namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Models\Items;
use App\Models\ItemsStock;
use App\User;

use Auth;
use getData;
use Session;
use Image;
use Input;
use File;

class MaterialController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['titlepage']=(Session::get('catalogsession')=='All')?'Warehouse':getData::getCatalogSession('catalog_title').' Stock';
        $data['maintitle']='';
        $data['page']="Stock";
        return view('pages.master.material.data',$data);
    }
    public function getData(Request $request){
        if(Auth::user()->level == 'User'){
            $parent = User::find(Auth::user()->id);
            $child = User::select('id')->where('parent_id',$parent['parent_id'])->get();
            $userdata = array_merge(json_decode($child,true),[['id'=>$parent['parent_id']]]);
        }else{
            $child = User::select('id')->where('parent_id',Auth::user()->id)->get();
            $userdata = array_merge(json_decode($child,true),[['id'=>Auth::user()->id]]);
        }
        $columns = ['items_name'];
        $keyword = trim($request->input('searchfield'));
        $query = Items::whereIn('user_id',$userdata)
                      ->where('item_type','Material')
                      ->where(function($result) use ($keyword,$columns){
                          foreach($columns as $column)
                          {
                              if($keyword != ''){
                                  $result->orWhere($column,'LIKE','%'.$keyword.'%');
                              }
                          }
                      })
                      ->orderBy('id','desc');
        $data['request'] = $request->all();
        $data['getData'] = $query->paginate(10);
        $data['pagination'] = $data['getData']->links();
        return view('pages.master.material.table',$data);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeqty(Request $request)
    {
        $id = Auth::user()->id;
        $upload = $id."-".time()."-TanpaNota";
        $timenow = NOW();

        if($request->hasFile('upload')){
            $image = $request->file('upload');
            $destinationPath = 'images/users/'.$id.'/nota/';
            $name = $id.'-nota-'.time().'.'.$image->getClientOriginalExtension();

            $upimg = $image->move(public_path($destinationPath), $name);
            $upload = $destinationPath.$name;

            if($upload){
                $status='success';
                $message='Your upload was successful.';
            }else{
                $status='error';
                $message='Your upload something went wrong.';
            }
        }
        
        $explode = explode(",", $request->arraydata);
        $catalog = (Session::get('catalogsession')=='All')?0:Session::get('catalogsession');
        foreach($explode as $itm) {
            $exp = explode("|", $itm);
            $insert = DB::table('items_stock')->insert([
                'user_id' => $id,
                'catalog' => $catalog,
                'item_id' => $exp[0],
                'stock' => $exp[1],
                'nota' => $upload,
                'notes' => $exp[2],
                'created_at' => $timenow,
                'updated_at' => $timenow
            ]);           
        }

        if($insert){
            $status='success';
            $message='Your request was successful.';
        }else{
            $status='error';
            $message='Your request something went wrong.';
        }
        
        $notif=['status'=>$status,'message'=>$message];
        return response()->json($notif);
    }
    
    public function indexHistory()
    {
        $data['titlepage']="History Material ".(Session::get('catalogsession')=='All')?'Warehouse':getData::getCatalogSession('catalog_title').' Stock';
        $data['maintitle']='History Material';
        $data['page']="Stock";
        
        return view('pages.master.material.datahistory',$data);
    }

    public function getDataHistory(Request $request)
    {
        $catalog = (Session::get('catalogsession')=='All')?0:Session::get('catalogsession');

        $querynotnull = ItemsStock::select('*', DB::raw('DATE(created_at) as date'))
        ->where('catalog',$catalog)
        ->whereNotNull('nota')
        ->groupBy('nota');
        // ->orderBy('id','desc');

        $querynull = ItemsStock::select('*', DB::raw('DATE(created_at) as date'))
        ->where('catalog',$catalog)                       
        ->whereNull('nota')
        ->groupBy('date');
        // ->orderBy('id','desc');
                
        foreach ($querynotnull->get() as $list) {
                $data['perNota'][$list->nota] = ItemsStock::select('items_stock.id', 'items_stock.stock', 'items_stock.notes', 'items.items_name', 'items.item_unit')
                ->leftJoin('items', 'items.id', '=', 'items_stock.item_id')
                ->where('nota', $list->nota)
                ->get();                
        } 
        
        foreach ($querynull->get() as $list) {
                $data['perNota'][$list->date] = ItemsStock::select('items_stock.id', 'items_stock.stock', 'items_stock.notes', 'items.items_name', 'items.item_unit')
                ->leftJoin('items', 'items.id', '=', 'items_stock.item_id')
                ->whereDate('items_stock.created_at', $list->date)
                ->get();                
        }

        $query = $querynotnull->union($querynull)->orderBy('id','desc');
        // dump($query->get());dump($data);

        $data['request'] = $request->all();
        $data['getData'] = $query->paginate(10);
        $data['pagination'] = $data['getData']->links();
        return view('pages.master.material.tablehistory',$data);
    }

    public function showHistory($id)
    {
        $query = ItemsStock::select('items_stock.id', 'items_stock.stock', 'items_stock.notes', 'items.items_name', 'items.item_unit')
        ->leftJoin('items', 'items.id', '=', 'items_stock.item_id')
        ->where('items_stock.id', $id)
        ->first();
        return $query;
    }

    public function updateHistory(Request $request, $id=null)
    {
        if ($id == 0) {
            $update = DB::table('items_stock')
            ->where('id', $request->id)
            ->update(
                [
                    'stock' => $request->stock,
                    'notes' => $request->notes,
                    'updated_at' => NOW()
                ]
            );
        }

        if($update){
            $status='success';
            $message='Your request was successful.';
        }else{
            $status='error';
            $message='Oh snap! something went wrong.';
        }
        $notif=['status'=>$status,'message'=>$message];
        return response()->json($notif);
    }

    public function destroyHistory($id)
    {
        $query = ItemsStock::where('id', $id)->delete();

        if (!$query) {
            $query = ItemsStock::where('nota', $id)->delete();
        }

        if($query){
            $status='success';
            $message='Your request was successful.';
        }else{
            $status='error';
            $message='Oh snap! something went wrong.';
        }
        $notif=['status'=>$status,'message'=>$message];
        return response()->json($notif);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'items_name' => 'required|min:4',
            'item_unit' => 'required',
        ]);
        if(Items::save_material($request)){
            $status='success';
            $message='Your request was successful.';
        }else{
            $status='error';
            $message='Oh snap! something went wrong.';
        }
        $notif=['status'=>$status,'message'=>$message];
        return response()->json($notif);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $query=ItemsStock::find($id);
        return $query;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'items_name' => 'required|min:3',
            'item_unit' => 'required',
        ]);
        if(Items::update_material($request)){
            $status='success';
            $message='Your request was successful.';
        }else{
            $status='error';
            $message='Oh snap! something went wrong.';
        }
        $notif=['status'=>$status,'message'=>$message];
        return response()->json($notif);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(Items::delete_data_material($id)){
            $status='success';
            $message='Your request was successful.';
        }else{
            $status='error';
            $message='Oh snap! something went wrong.';
        }
        $notif=['status'=>$status,'message'=>$message];
        return response()->json($notif);
    }
    public function stock(Request $request,$id=null){
      $data['item']=Items::find($id);
      if($request->isMethod('post')){
        if(Auth::user()->level == 'User'){
            $parent = User::find(Auth::user()->id);
            $child = User::select('id')->where('parent_id',$parent['parent_id'])->get();
            $user = array_merge(json_decode($child,true),[['id'=>$parent['parent_id']]]);
        }else{
            $child = User::select('id')->where('parent_id',Auth::user()->id)->get();
            $user = array_merge(json_decode($child,true),[['id'=>Auth::user()->id]]);
        }
        $columns = ['name','stock','notes'];
        $catalog = (Session::get('catalogsession')=='All')?0:Session::get('catalogsession');
        $keyword = trim($request->input('searchfield'));
        $query = ItemsStock::select('items_stock.*',
                            'users.name'
                        )
                        ->leftJoin('users','items_stock.user_id','=','users.id')
                        ->whereIn('user_id',$user)
                        ->where('item_id',$id)
                        ->where('items_stock.catalog',$catalog)
                        ->where(function($result) use ($keyword,$columns){
                            foreach($columns as $column)
                            {
                                if($keyword != ''){
                                    $result->orWhere($column,'LIKE','%'.$keyword.'%');
                                }
                            }
                        })
                        ->orderBy('id','desc');
        $data['request'] = $request->all();
        $data['getData'] = $query->paginate(10);
        $data['pagination'] = $data['getData']->links();
        return view('pages.master.material.tablestock',$data);
      }else{
        $data['titlepage']='Manage Stock '.$data['item']['items_name'];
        $data['maintitle']='Manage Stock '.$data['item']['items_name'];
        $data['page']="Stock";
        return view('pages.master.material.stock',$data);
      }
    }
    public function addstock($id=null, Request $request){
        $this->validate($request, [
          'stock' => 'required|numeric|min:1|max:100000',
        ]);
        $allbranchstock = ItemsStock::where('item_id',$request->input('item_id'))
                            ->where('catalog','>',0)
                            ->sum('stock');
        $centralstock = getData::countStock($request->input('item_id'),0);
        if(($allbranchstock+$request->input('stock')) > $centralstock && Session::get('catalogsession')!='All'){
          $status='error';
          $message='Oh snap! Qty you entered is too large.';
        }else{
          if(ItemsStock::save_data($request)){
            $status='success';
            $message='Your request was successful.';
          }else{
            $status='error';
            $message='Oh snap! something went wrong.';
          }
        }
        $notif=['status'=>$status,'message'=>$message];
        return response()->json($notif);
    }
    public function updatestock($id=null, Request $request){
        $this->validate($request, [
          'stock' => 'required|numeric|min:1|max:100000',
        ]);
        if(ItemsStock::update_data($request)){
          $status='success';
          $message='Your request was successful.';
        }else{
          $status='error';
          $message='Oh snap! something went wrong.';
        }
        $notif=['status'=>$status,'message'=>$message];
        return response()->json($notif);
    }
    public function deletestock($item=null, $addon=null){
        if(ItemsStock::delete_data($addon)){
          $status='success';
          $message='Your request was successful.';
        }else{
          $status='error';
          $message='Oh snap! something went wrong.';
        }
       $notif=['status'=>$status,'message'=>$message];
       return response()->json($notif);
    }
}
