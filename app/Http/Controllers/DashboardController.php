<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Helper\myFunction;

use App\Events\NotifEvent;
use App\Models\Invoice;
use App\Models\Catalog;
use App\Models\CatalogDetail;
use App\Models\Register;
use App\Models\Items;
use App\Models\InvoiceDetail;
use App\User;

use Auth;
use Session;
use getData;
use Mail;

class DashboardController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth');
	}
	public function index(){
		$data['titlepage']='Dashboard';
		$data['maintitle']='Welcome';
		if(Auth::user()->level == 'Super Admin'){
			$data['countowners'] = User::where('owner',1)->where('active','Y')->get();
			$data['countcatalog'] = Catalog::get();
			$data['countitems'] = Items::get();
			$data['counttransaction'] = Invoice::where('status','<>','Order')->get();
			$data['catalog'] = Catalog::select('catalog.*','users.name')
										->leftJoin('users','catalog.user_id','=','users.id')
										->orderBy('catalog.id','desc')
										->take(10)
										->get();
			$data['registration'] = Register::select('register.*','users.name')
										->leftJoin('users','register.user_id','=','users.id')
										->whereIn('status', ['Order', 'Confirmation', 'Approved'])
										->orderBy('register.id','desc')
										->take(10)
										->get();
			return view('pages.dashboard.superadmin',$data);
		}else{
			return view('pages.dashboard.wrap',$data);
		}
	}
	public function getData(Request $request){
		if(Auth::user()->owner == 1){
			if(Session::get('catalogsession') == 'All'){
				$catalog = Catalog::where('user_id',Auth::user()->id)->orderBy('id','asc')->first();
			}else{
				$catalog = Catalog::where('id',Session::get('catalogsession'))->where('user_id',Auth::user()->id)->orderBy('id','asc')->first();
			}
		}else{
			$catalog = Catalog::where('id',Auth::user()->catalog)->orderBy('id','asc')->first();
		}
		$data['latestactivity'] = Invoice::select('invoice.*','catalog.catalog_title','catalog.catalog_logo','catalog.catalog_username','catalog.domain')
								->leftJoin('catalog','invoice.catalog_id','=','catalog.id')
								->where('invoice.catalog_id',$catalog['id'])
								->where('invoice.invoice_type','Permanent')
								->orderBy('id','desc')
								->take(10)
								->get();
		$data['hotitems'] = CatalogDetail::select('catalogdetail.*',
													'category.category_name',
													'items.items_name',
													'items.item_image_primary')
										->leftJoin('category','catalogdetail.category_id','=','category.id')
										->leftJoin('items','catalogdetail.item','=','items.id')
										->where('catalog_id',$catalog['id'])
										->where('items.sell','>',0)
										->take(6)
										->orderBy('items.sell','desc')
										->get();
		$data['checkout'] = Invoice::where('invoice.catalog_id',$catalog['id'])->where('status','Checkout')->where('invoice.invoice_type','Permanent')->get();
		$data['approve'] = Invoice::where('invoice.catalog_id',$catalog['id'])->where('status','Approve')->where('invoice.invoice_type','Permanent')->get();
		$data['process'] = Invoice::where('invoice.catalog_id',$catalog['id'])->where('status','Process')->where('invoice.invoice_type','Permanent')->get();
		$data['delivered'] = Invoice::where('invoice.catalog_id',$catalog['id'])->where('status','Delivered')->where('invoice.invoice_type','Permanent')->get();
		$data['completed'] = Invoice::where('invoice.catalog_id',$catalog['id'])->where('status','Completed')->where('invoice.invoice_type','Permanent')->get();
		$data['cancel'] = Invoice::where('invoice.catalog_id',$catalog['id'])->where('status','Cancel')->where('invoice.invoice_type','Permanent')->get();
		return view('pages.dashboard.data',$data);
	}
	public function setCatalogSession($catalog){
		Session::forget('catalogsession');
		Session::put('catalogsession',$catalog);
	}
	public function logout(){
		Session::forget('catalogsession');
		Auth::logout();
    	return redirect('/login');
	}
	public function sendEmail(){
        $invoice = Invoice::where('id',4)->first(); // Jangan dibawa

        $item = InvoiceDetail::where('invoiceid', $invoice['id'])
            ->groupBy('category')
            ->orderBy('id')
            ->get();
        $disp = array(
            'email'=>getData::getCatalogSession('email_contact'),
            'name'=>getData::getCatalogSession('catalog_title'),
            'namapengirim'=>'LiatMenu Support',
            'emailpengirim'=>'support@liatmenu.id',
            'subject'=>"Checkout Invoice ".$invoice['invoice_number']
        );
        $content = array(
            'invoice' => $invoice,
            'item' => $item
        );
        Mail::send('pages.email.transaction', $content, function($message) use ($disp)
        {
            $message->from($disp['emailpengirim'], $disp['namapengirim']);
            $message->to($disp['email'], $disp['name'])->subject($disp['subject']);
            // $message->to('nanangkoesharwanto@gmail.com', 'nanang yoscan')->subject($disp['subject']);
        });
    }
}
