<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Catalog;
use App\Models\Category;
use App\Models\Sliders;
use App\Models\SubCategory;
use App\Models\Items;
use App\Models\CatalogDetail;
use App\Models\Register;
use App\Models\CatalogListType;
use App\Models\CatalogType;

use Auth;
use Dompdf\Dompdf;

class CatalogController extends Controller
{
  public function __construct()
  {
    $this->middleware('auth');
  }
  public function index(Request $request){
    $data['titlepage']='Catalog';
    $data['maintitle']='Manage Catalog';
    $data['total'] = Catalog::where('user_id',Auth::user()->id)->count();
    return view('pages.catalog.data',$data);
  }
  public function getData(Request $request){
    $columns = ['catalog_username'];
    $keyword = trim($request->input('searchfield'));
    $query = Catalog::where('user_id',Auth::user()->id)
                    ->where(function($result) use ($keyword,$columns){
                        foreach($columns as $column)
                        {
                            if($keyword != ''){
                                $result->orWhere($column,'LIKE','%'.$keyword.'%');
                            }
                        }
                    })
                    ->orderBy('id','desc');
    $data['request'] = $request->all();
    $data['getData'] = $query->paginate(10);
    $data['pagination'] = $data['getData']->links();
    return view('pages.catalog.table',$data);      
  }
  public function create(){
    $data['package']=Register::select('register.*','package.*')
                              ->leftJoin('package','register.package_id','=','package.id')
                              ->where('register.user_id',Auth::user()->id)
                              ->first();
    $data['sliders'] = Sliders::where('user_id',Auth::user()->id)->orderBy('id','desc')->get();
    return view('pages.catalog.form',$data);
  }
  public function store(Request $request)
  {
    // if(count((array)$request->input('items')) > 0){

    // }else{
    // 	$status='error';
    // 	$message='Please select item(s).';
    // }
    $this->validate($request, [
      'domain' => 'required',
      'catalog_username' => 'required|min:3|unique:catalog,catalog_username',
      'catalog_title' => 'required|min:3',
      'logo' => 'required|max:1000|mimes:jpeg,jpg,png',
      'distance' => 'required|numeric',
      'phone_contact' => 'required',
      'email_contact' => 'required',
      'catalog_password' => 'required|min:5',
      'feature' => 'required',
    ]);
    if($request->input('feature')=='Full'){
      $this->validate($request, [
        // 'wa_number' => 'required|min:10|numeric',
        // 'wa_show_item' => 'required',
        // 'wa_show_cart' => 'required',
        'checkout_type' => 'required',
        'tax' => 'required|numeric',
      ]);
    }
    if ($request->hasFile('catalogbg')) {
      $this->validate($request, [
        'catalogbg' => 'required|max:1000|mimes:jpeg,jpg,png'
      ]);
    }
    if(Catalog::save_data($request)){
      $status='success';
      $message='Your request was successful.';
    }else{
      $status='error';
      $message='Oh snap! something went wrong.';
    }
    $notif=['status'=>$status,'message'=>$message];
    return response()->json($notif);
  }
  public function show($id)
  {
    $query=Catalog::where('id',$id)->first();
    return $query;
  }
  public function edit($id)
  {
    $data['package']=Register::select('register.*','package.*')
                              ->leftJoin('package','register.package_id','=','package.id')
                              ->where('register.user_id',Auth::user()->id)
                              ->first();
    $data['getData'] = $this->show($id);
    $data['sliders'] = Sliders::where('user_id',Auth::user()->id)->orderBy('id','desc')->get();
    $data['catalog_list'] = CatalogListType::all();
    $data['catalog_type'] = CatalogType::where('catalog_id', $id)->orderBy('order')->pluck('catalog_list_type_id', 'order');
    return view('pages.catalog.form',$data);
  }
  public function update(Request $request, $id)
  {
    $this->validate($request, [
      'domain' => 'required',
      'catalog_username' => 'required|min:3',
      'catalog_title' => 'required|min:3',
      'distance' => 'required|numeric',
      'phone_contact' => 'required',
      'email_contact' => 'required',
      'catalog_password' => 'nullable|min:5',
      'feature' => 'required',
    ]);
    if($request->input('feature')=='Full'){
      $this->validate($request, [
        // 'wa_number' => 'required|min:10|numeric',
        // 'wa_show_item' => 'required',
        // 'wa_show_cart' => 'required',
        'checkout_type' => 'required',
        'tax' => 'required|numeric',
      ]);
    }
    if ($request->hasFile('logo')) {
      $this->validate($request, [
        'logo' => 'required|max:1000|mimes:jpeg,jpg,png'
      ]);
    }
    if ($request->hasFile('catalogbg')) {
      $this->validate($request, [
        'catalogbg' => 'required|max:1000|mimes:jpeg,jpg,png'
      ]);
    }
    if(Catalog::update_data($request)){
      $status='success';
      $message='Your request was successful.';
    }else{
      $status='error';
      $message='Oh snap! something went wrong.';
    }
    $notif=['status'=>$status,'message'=>$message];
    return response()->json($notif);
  }
  public function destroy($id)
  {
    if(Catalog::delete_data($id)){
      $status='success';
      $message='Your request was successful.';
    }else{
      $status='error';
      $message='Oh snap! something went wrong.';
    }
    $notif=['status'=>$status,'message'=>$message];
    return response()->json($notif);
  }
  public function items(Request $request,$id=null){ 
    $data['catalog']=$this->show($id);
    if($request->isMethod('post')){
      $data['catalogid'] = $id;
      $data['detail'] = CatalogDetail::select('catalogdetail.*',
                                        'category.category_name',
                                        'subcategory.subcategory_name'
                                      )
                                      ->leftJoin('category','catalogdetail.category_id','=','category.id')
                                      ->leftJoin('subcategory','catalogdetail.subcategory_id','=','subcategory.id')
                                      ->where('catalog_id',$id)
                                      ->orderBy('category_position')
                                      ->groupBy('category_id')
                                      ->get();
      return view('pages.catalog.tableitems',$data);
    }else{
      $data['titlepage']='Catalog Items';
      $data['maintitle']='Manage Catalog Items';
      return view('pages.catalog.items',$data);
    }
  }
  public function additems($id=null, Request $request){
    if($request->isMethod('post')){
      $this->validate($request, [
        'category_id' => 'required',
      ]);
      if(CatalogDetail::save_data($request)){
        $status='success';
        $message='Your request was successful.';
      }else{
        $status='error';
        $message='Oh snap! something went wrong.';
      }
      $notif=['status'=>$status,'message'=>$message];
      return response()->json($notif);
    }else{
      $data['catalogid']=$id;
      $data['category'] = Category::where('user_id',Auth::user()->id)->where('category_type','Main')->where('show_on_catalog', 'LIKE','%'.$id.'|%')->orderBy('id','desc')->get();
      $data['subcategory'] = SubCategory::where('user_id',Auth::user()->id)->where('show_on_catalog', 'LIKE','%'.$id.'|%')->orderBy('id','desc')->get();
      $data['items'] = Items::where('user_id',Auth::user()->id)->where('item_type','Main')->where('show_on_catalog', 'LIKE','%'.$id.'|%')->orderBy('id','desc')->get();
      return view('pages.catalog.formitems',$data);
    }
  }
  public function getPositionCategory($catalog=null,$category=null){
    $getpostion = CatalogDetail::where('catalog_id',$catalog)->where('category_id',$category)->orderBy('category_id','desc')->first();
    if($getpostion){
      return $getpostion['category_position'];
    }else{
      $maxposition = CatalogDetail::where('catalog_id',$catalog)->max('category_position');
      return $maxposition+1;
    }
  }
  public function getPositionSubCategory($catalog=null,$category=null,$subcategory=null){
    $getpostion = CatalogDetail::where('catalog_id',$catalog)->where('category_id',$category)->where('subcategory_id',$subcategory)->orderBy('subcategory_id','desc')->first();
    if($getpostion){
      return $getpostion['subcategory_position'];
    }else{
      $maxposition = CatalogDetail::where('catalog_id',$catalog)->where('category_id',$category)->max('subcategory_position');
      return $maxposition+1;
    }
  }
  public function changeStatus(Request $request,$catalog=null,$me=null,$current=null,$status=null){
    if($request->isMethod('post')){
      if(CatalogDetail::change_position($request)){
        $status='success';
        $message='Your request was successful.';
      }else{
        $status='error';
        $message='Oh snap! something went wrong.';
      }
      $notif=['status'=>$status,'message'=>$message];
      return response()->json($notif);
    }else{
      $data['catalog']=$catalog;
      $data['status']=$status;
      $data['current']=$current;
      $data['me']=$me;
      if($status == 'Category'){
        $maxposition = CatalogDetail::where('catalog_id',$catalog)->max('category_position');
        $data['position']=$maxposition;
      }
      elseif($status == 'SubCategory'){
        $data['getData'] = CatalogDetail::where('catalog_id',$catalog)->where('subcategory_id',$me)->first();
        //$data['category'] = Category::where('user_id',Auth::user()->id)->orderBy('id','desc')->get();
        $maxposition = CatalogDetail::where('catalog_id',$catalog)->where('category_id',$data['getData']['category_id'])->max('subcategory_position');
        $data['position']=$maxposition;
      }
      elseif($status == 'Item'){
        $data['getData'] = CatalogDetail::where('id',$me)->first();
        $maxposition = CatalogDetail::where('catalog_id',$data['getData']['catalog_id'])->where('category_id',$data['getData']['category_id'])->where('subcategory_id',$data['getData']['subcategory_id'])->max('item_position');
        $data['position']=$maxposition;
      }
      return view('pages.catalog.position',$data);
    }
  }
  public function deleteElement($catalog=null,$me=null,$type=null){
    if(CatalogDetail::delete_element($catalog,$me,$type)){
      $status='success';
      $message='Your request was successful.';
    }else{
      $status='error';
      $message='Oh snap! something went wrong.';
    }
    $notif=['status'=>$status,'message'=>$message];
    return response()->json($notif);
  }
  public function availableElement($catalog=null,$me=null,$available=null){
    if(CatalogDetail::available_element($catalog,$me,$available)){
      $status='success';
      $message='Your request was successful.';
    }else{
      $status='error';
      $message='Oh snap! something went wrong.';
    }
    $notif=['status'=>$status,'message'=>$message];
    return response()->json($notif);
  }
  public function qrcode($id=null){
    $data['catalog'] = Catalog::where('id',$id)->first();
    $view = view('pages.catalog.qrcode',$data);
    return $view;
    // instantiate and use the dompdf class
    $dompdf = new Dompdf();
    $dompdf->loadHtml($view->render());
    
    // (Optional) Setup the paper size and orientation
    $dompdf->setPaper('A4', 'landscape');
    
    // Render the HTML as PDF
    $dompdf->render();
    
    // Output the generated PDF to Browser
    $dompdf->stream();
    
    // $pdf = PDF::loadView('pages.catalog.qrcode', $data);
    // return $pdf->download('invoice.pdf');
  }
}
