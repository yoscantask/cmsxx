<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

use App\User;

use Auth;
use Session;

class ViewMenusController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request, $id) {
        app()->setLocale(Session::get('locale'));
        $catalog = DB::table('catalog')
					->where('id', $id)
					->orWhere('email_contact', $id)
					->first();
        $data['titlepage']='View Menus';
		$data['maintitle']=__('lang.setting').' '.__('lang.viewmenus').' : ' . $catalog->catalog_title;
		$data['id']=$catalog->id;
        $data['view_subcat'] = $catalog->view_subcat;
        $data['view_item'] = $catalog->view_item;

        return view('viewmenus',$data);
    }

    public function update(Request $request, $id) {
        DB::table('catalog')
            ->where('id', '=', $request->id, 'OR', 'email_contact', '=', $request->id)
            ->update(
            [
                'view_subcat' => $request->view_subcat,
                'view_item' => $request->view_item
            ]
        );
        
        $status='success';
        $message='Your request was successful.';
        $notif=['status'=>$status,'message'=>$message];

        // return redirect('viewmenus/'.$id);
        return redirect()->route('catalog.index')->with($notif);
    
    }
}
