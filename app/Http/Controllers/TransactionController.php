<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

use App\Models\Catalog;
use App\Models\Invoice;
use App\Models\InvoiceDetail;

use Auth;
use Session;

class TransactionController extends Controller
{
    public function __construct()
	{
	    $this->middleware('auth');
	}
    public function index($getstatus=null) 
    {
        app()->setLocale(Session::get('locale'));
        $icon="lnr-printer";
        if($getstatus == 'checkout'){
    		$status = 'Checkout';
    		$icon = 'pe-7s-next-2';
    	}
    	elseif($getstatus == 'approve'){
    		$status = 'Approve';
    		$icon = 'lnr-select';
    	}
    	elseif($getstatus == 'process'){
    		$status = 'Process';
    		$icon = 'lnr-hourglass';
    	}
    	elseif($getstatus == 'delivered'){
    		$status = 'Delivered';
    		$icon = 'lnr-location';
    	}
    	elseif($getstatus == 'completed'){
    		$status = 'Completed';
    		$icon = 'lnr-checkmark-circle';
    	}
    	elseif($getstatus == 'cancel'){
    		$status = 'Cancel';
    		$icon = 'lnr-cross-circle';
    	}
        $data['titlepage']=__('lang.'.$getstatus);
        if ($getstatus == 'all') {$data['maintitle']=__('lang.transactionreport');} 
        else {$data['maintitle']=__('lang.transactionreport')." ".__('lang.'.$getstatus);}       
        $data['status']=$getstatus;
        $data['icon']=$icon;
        return view('pages.transaction.data',$data);
    }
	public function getData(Request $request)
    {
        $columns = ['invoice_number'];
        $keyword = trim($request->input('searchfield'));
        $status = $getstatus = trim($request->input('status'));
        $datestart = trim($request->input('datestart'));
        $dateend = trim($request->input('dateend'));
        $print = trim($request->input('print'));

    	if($getstatus == 'checkout'){
    		$status = 'Checkout';
    		$icon = 'pe-7s-next-2';
    	}
    	elseif($getstatus == 'approve'){
    		$status = 'Approve';
    		$icon = 'lnr-select';
    	}
    	elseif($getstatus == 'process'){
    		$status = 'Process';
    		$icon = 'lnr-hourglass';
    	}
    	elseif($getstatus == 'delivered'){
    		$status = 'Delivered';
    		$icon = 'lnr-location';
    	}
    	elseif($getstatus == 'completed'){
    		$status = 'Completed';
    		$icon = 'lnr-checkmark-circle';
    	}
    	elseif($getstatus == 'cancel'){
    		$status = 'Cancel';
    		$icon = 'lnr-cross-circle';
    	}
    	elseif($getstatus == 'order'){
    		$status = 'Order';
    		$icon = 'lnr-cross-circle';
    	}
        if(Session::get('catalogsession') == 'All'){
            $catalog = Catalog::orderBy('id','desc')->first();
        }else{
            $catalog = Catalog::where('id',Session::get('catalogsession'))->orderBy('id','desc')->first();
        }

        if ($getstatus != 'all') {
            $query = Invoice::where('catalog_id',$catalog['id'])
            ->where('status',$status)
            ->where(function($result) use ($status){
                if($status == 'Completed'){
                    $result->where('pending','N');
                }
            })
            ->where(function($result) use ($keyword,$columns){
                foreach($columns as $column)
                {
                    if($keyword != ''){
                        $result->orWhere($column,'LIKE','%'.$keyword.'%');
                    }
                }
            })
            ->orderBy('id','desc');
            if (strtotime($dateend) !== false && strtotime($datestart) !== false) {     
                $query = $query->whereBetween('created_at',[$datestart,$dateend]);
            }
        } else {         
            $query = Invoice::where('catalog_id',$catalog['id'])
            ->where(function($result) use ($keyword,$columns){
                foreach($columns as $column)
                {
                    if($keyword != ''){
                        $result->orWhere($column,'LIKE','%'.$keyword.'%');
                    }
                }
            })
            ->orderBy('id','asc');
            if (strtotime($dateend) !== false && strtotime($datestart) !== false) {       
                $query = $query->whereBetween('created_at',[$datestart,$dateend]);
            }
        }    
        $data['print'] = $print;
        $data['request'] = $request->all();
        $data['getData'] = $query->paginate(10);
        $data['pagination'] = $data['getData']->links();
        
        return view('pages.transaction.table',$data);
    }
    public function detail(Request $request,$invoice)
    {
    	$data['invoice']=Invoice::where('invoice_number',$invoice)->first();
    	$data['item'] =  InvoiceDetail::where('invoiceid',$data['invoice']['id'])->groupBy('category')->orderBy('id')->get();
    	$data['titlepage']='Detail order '.$invoice;
        $data['maintitle']='Detail order '.$invoice;
    	return view('pages.transaction.detail',$data);
    }    
    public function detailpopup($invoice)
    {
        $data['invoice']=Invoice::where('invoice_number',$invoice)->first();
        $data['item'] =  InvoiceDetail::where('invoiceid',$data['invoice']['id'])->where('clone_data','N')->groupBy('category')->orderBy('id')->get();
        return view('pages.transaction.detailpopup',$data);
    }
    public function deleteitem($id=null)
    {
    	if(InvoiceDetail::delete_data($id)){
    	    $status='success';
    	    $message='Your request was successful.';
    	}else{
    	    $status='error';
    	    $message='Oh snap! something went wrong.';
    	}
    	return ['status'=>$status,'message'=>$message];
    }
    public function cancelorder(Request $request)
    {
    	if(Invoice::cancel_order($request)){
    	    $status='success';
    	    $message='Your request was successful.';
    	}else{
    	    $status='error';
    	    $message='Oh snap! something went wrong.';
    	}
    	return ['status'=>$status,'message'=>$message];
    }
    public function updatestatus($invoice=null,$status=null)
    {
        if(Invoice::update_status($invoice,$status)){
            $ch = curl_init(); 
            curl_setopt($ch, CURLOPT_URL, \URL::to('/notif/'.$invoice.'/'.$status));
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
            $output = curl_exec($ch); 
            curl_close($ch);

            $status='success';
            $message='Your request was successful.';
        }else{
            $status='error';
            $message='Oh snap! something went wrong.';
        }
        return ['status'=>$status,'message'=>$message];
    }
    public function report(Request $request,$status=null,$start=null,$end=null)
    {
        if(Session::get('catalogsession') == 'All'){
            $catalog = Catalog::orderBy('id','desc')->first();
        }else{
            $catalog = Catalog::where('id',Session::get('catalogsession'))->orderBy('id','desc')->first();
        }
        if($request->has('status')){
            $status = $request->status;
        }else{
            $status = $status;
        }
        if($request->has('start')){
            $start = $request->start;
        }else{
            $start = $start;
        }
        if($request->has('end')){
            $end = $request->end;
        }else{
            $end = $end;
        }

        if($status == 'All') {
            $query = Invoice::where('catalog_id',$catalog['id'])->where('created_at','>=',$start.' 00:00:00')->where('created_at','<=',$end.' 23:59:59')->where('invoice_type','Permanent')->where('status','<>','Order')->orderBy('invoice_number');
            if(Session::get('catalogsession') == 'All'){
                $periodday = Invoice::select('invoice.*','catalog.catalog_title','catalog.catalog_logo','catalog.catalog_username','catalog.domain')
                                    ->leftJoin('catalog','invoice.catalog_id','=','catalog.id')
                                    ->where('invoice_type','Permanent')
                                    ->where('status','<>','Order')
                                    ->where('catalog.user_id',Auth::user()->id)
                                    ->where('invoice.created_at','>=',$start.' 00:00:00')->where('invoice.created_at','<=',$end.' 23:59:59')
                                    ->groupBy(DB::raw('Date(invoice.created_at)'))
                                    ->orderBy('invoice_number');
                $periodmonth = Invoice::select('invoice.*','catalog.catalog_title','catalog.catalog_logo','catalog.catalog_username','catalog.domain')
                                    ->leftJoin('catalog','invoice.catalog_id','=','catalog.id')
                                    ->where('invoice_type','Permanent')
                                    ->where('status','<>','Order')
                                    ->where('catalog.user_id',Auth::user()->id)
                                    ->where('invoice.created_at','>=',$start.' 00:00:00')->where('invoice.created_at','<=',$end.' 23:59:59')
                                    ->groupBy(DB::raw('Month(invoice.created_at)'))
                                    ->orderBy('invoice_number');
            }else{
                $periodday = Invoice::where('catalog_id',$catalog['id'])
                                        ->where('invoice_type','Permanent')
                                        ->where('status','<>','Order')
                                        ->where('created_at','>=',$start.' 00:00:00')
                                        ->where('created_at','<=',$end.' 23:59:59')
                                        ->groupBy(DB::raw('Date(created_at)'))
                                        ->orderBy('invoice_number');
                $periodmonth = Invoice::where('catalog_id',$catalog['id'])
                                        ->where('invoice_type','Permanent')
                                        ->where('status','<>','Order')
                                        ->where('created_at','>=',$start.' 00:00:00')
                                        ->where('created_at','<=',$end.' 23:59:59')
                                        ->groupBy(DB::raw('Month(created_at)'))
                                        ->orderBy('invoice_number');
            }
            
            $itemDay = InvoiceDetail::select('invoicedetail.*','invoice.invoice_number')
                                ->leftJoin('invoice','invoicedetail.invoiceid','invoice.id')
                                ->where('invoice.invoice_type','Permanent')
                                ->where('invoice.catalog_id',$catalog['id'])
                                ->where('invoice.created_at','>=',$start.' 00:00:00')
                                ->where('invoice.created_at','<=',$end.' 23:59:59')
                                ->groupBy(DB::raw('Date(invoicedetail.created_at)'),'item_id')
                                ->orderBy('invoicedetail.id');
            $itemMonth = InvoiceDetail::select('invoicedetail.*','invoice.invoice_number')
                                ->leftJoin('invoice','invoicedetail.invoiceid','invoice.id')
                                ->where('invoice.invoice_type','Permanent')
                                ->where('invoice.catalog_id',$catalog['id'])
                                ->where('invoice.created_at','>=',$start.' 00:00:00')
                                ->where('invoice.created_at','<=',$end.' 23:59:59')
                                ->groupBy(DB::raw('Month(invoicedetail.created_at)'),'item_id')
                                ->orderBy('invoicedetail.id');
        }else{
            $query = Invoice::where('catalog_id',$catalog['id'])->where('status',$status)->where('invoice.invoice_type','Permanent')->where('created_at','>=',$start.' 00:00:00')->where('created_at','<=',$end.' 23:59:59')->orderBy('invoice_number');
            $periodday = Invoice::where('catalog_id',$catalog['id'])
                                    ->where('invoice.invoice_type','Permanent')
                                    ->where('status',$status)
                                    ->where('created_at','>=',$start.' 00:00:00')
                                    ->where('created_at','<=',$end.' 23:59:59')
                                    ->groupBy(DB::raw('Date(created_at)'))
                                    ->orderBy('invoice_number');
            $periodmonth = Invoice::where('catalog_id',$catalog['id'])
                                    ->where('invoice.invoice_type','Permanent')
                                    ->where('status',$status)
                                    ->where('created_at','>=',$start.' 00:00:00')
                                    ->where('created_at','<=',$end.' 23:59:59')
                                    ->groupBy(DB::raw('Month(created_at)'))
                                    ->orderBy('invoice_number');
            $itemDay = InvoiceDetail::select('invoicedetail.*','invoice.invoice_number')
                                ->leftJoin('invoice','invoicedetail.invoiceid','invoice.id')
                                ->where('invoice.invoice_type','Permanent')
                                ->where('invoice.catalog_id',$catalog['id'])
                                ->where('invoice.status',$status)
                                ->where('invoice.created_at','>=',$start.' 00:00:00')
                                ->where('invoice.created_at','<=',$end.' 23:59:59')
                                ->groupBy(DB::raw('Date(invoicedetail.created_at)'),'item_id')
                                ->orderBy('invoicedetail.id');
            $itemMonth = InvoiceDetail::select('invoicedetail.*','invoice.invoice_number')
                                ->leftJoin('invoice','invoicedetail.invoiceid','invoice.id')
                                ->where('invoice.invoice_type','Permanent')
                                ->where('invoice.catalog_id',$catalog['id'])
                                ->where('invoice.status',$status)
                                ->where('invoice.created_at','>=',$start.' 00:00:00')
                                ->where('invoice.created_at','<=',$end.' 23:59:59')
                                ->groupBy(DB::raw('Month(invoicedetail.created_at)'),'item_id')
                                ->orderBy('invoicedetail.id');
        }
        if($request->has('searchfield')){
            $query->where('invoice_number', 'LIKE', '%'.$request->searchfield.'%');
            $periodday->where('invoice_number', 'LIKE', '%'.$request->searchfield.'%');
            $periodmonth->where('invoice_number', 'LIKE', '%'.$request->searchfield.'%');
        }
        $data['request'] = $request->all();
        $data['getData'] = $query->paginate(10);
        $data['getDay'] = $periodday->get();
        $data['getMonth'] = $periodmonth->get();
        $data['itemDay'] = $itemDay->get();
        $data['itemMonth'] = $itemMonth->get();
        $data['pagination'] = $data['getData']->appends(['searchfield'=>($request->searchfield == '')?"":$request->searchfield])->links();
        $data['titlepage']='Report Transaction '.$status;
        $data['maintitle']='Report Transaction '.$status;
        $data['status']=$status;
        $data['start']=$start;
        $data['end']=$end;
        return view('pages.transaction.report',$data);
    }
    public function generateStruk($inv)
    {
        $data['invoice'] = Invoice::where('id', $inv)->first();
        $data['item'] = InvoiceDetail::where('invoiceid', $data['invoice']['id'])
            ->groupBy('category')
            ->orderBy('id')
            ->get();
        return view('pages.transaction.print', $data);
    }
    public function printAll($report='daily', $getstatus='all', $datestart=null, $dateend=null) {
        $data['status'] = 'all';
        if(Session::get('catalogsession') == 'All'){
            $catalog = Catalog::orderBy('id','desc')->first();
        }else{
            $catalog = Catalog::where('id',Session::get('catalogsession'))->orderBy('id','desc')->first();
        }

    	if($report == 'daily') {
            if ($getstatus != 'all') {
                $data['status'] = $getstatus;
                $query = Invoice::where('catalog_id',$catalog['id'])
                ->where('status',$getstatus)
                ->where(function($result) use ($getstatus){
                    if($getstatus == 'Completed'){
                        $result->where('pending','N');
                    }
                });
                $cekperiode = Invoice::where('catalog_id',$catalog['id'])
                ->where('status',$getstatus)
                ->where(function($result) use ($getstatus){
                    if($getstatus == 'Completed'){
                        $result->where('pending','N');
                    }
                });
                if (strtotime($dateend) !== false && strtotime($datestart) !== false) {     
                    $query = $query->whereBetween('created_at',[$datestart,$dateend." 23:59:59"]);
                    $cekperiode = $cekperiode->whereBetween('created_at',[$datestart,$dateend." 23:59:59"]);
                }
            } else {         
                $query = Invoice::where('catalog_id',$catalog['id']);
                $cekperiode = Invoice::where('catalog_id',$catalog['id']);
                if (strtotime($dateend) !== false && strtotime($datestart) !== false) {       
                    $query = $query->whereBetween('created_at',[$datestart,$dateend." 23:59:59"]);
                    $cekperiode = $cekperiode->whereBetween('created_at',[$datestart,$dateend." 23:59:59"]);
                }   
            }    

            $cekperiodestart = $cekperiode->first();
            $cekperiodeend = $cekperiode->orderBy('id','desc')->first();
            $data['periode'] = date('d M Y', strtotime($cekperiodestart->created_at))." - ".date('d M Y', strtotime($cekperiodeend->created_at));

            $data['report'] = $report;
            $data['getData'] = $query->get();
            $data['summary'] = $query->select(DB::raw('SUM(amount) AS total'))->first();
        }

        if($report == 'month') {
            if ($getstatus != 'all') {
                $data['status'] = $getstatus;
                $query = Invoice::select(
                    DB::raw('COUNT(id) AS totrans'), 
                    DB::raw('SUM(amount) AS total'), 
                    DB::raw("DATE_FORMAT(created_at,'%d %M %Y') as tanggal"),
                    DB::raw("DATE_FORMAT(created_at,'%Y-%m-%d') as tgl")
                )
                ->where('catalog_id',$catalog['id'])
                ->groupBy('tanggal')
                ->orderBy('created_at', 'ASC')
                ->where('status',$getstatus)
                ->where(function($result) use ($getstatus){
                    if($getstatus == 'Completed'){
                        $result->where('pending','N');
                    }
                });
                
                $cekperiode = Invoice::select('created_at')
                ->where('status',$getstatus)
                ->where(function($result) use ($getstatus){
                    if($getstatus == 'Completed'){
                        $result->where('pending','N');
                    }
                });
    
                if (strtotime($dateend) !== false && strtotime($datestart) !== false) {     
                    $query = $query->whereBetween('created_at',[$datestart,$dateend." 23:59:59"]);
                    $cekperiode = $cekperiode->whereBetween('created_at',[$datestart,$dateend." 23:59:59"]);
                }
                
                foreach ($query->get() as $tgl) {
                    $data['perDay'][$tgl->tgl] = Invoice::where('status',$getstatus)
                        ->whereDate('created_at', $tgl->tgl)
                        ->where('catalog_id',$catalog['id'])
                        ->get();
                }
            } else {         
                $query = Invoice::select(
                    DB::raw('COUNT(id) AS totrans'), 
                    DB::raw('SUM(amount) AS total'), 
                    DB::raw("DATE_FORMAT(created_at,'%d %M %Y') as tanggal"),
                    DB::raw("DATE_FORMAT(created_at,'%Y-%m-%d') as tgl")
                )
                ->where('catalog_id',$catalog['id'])
                ->groupBy('tanggal')
                ->orderBy('created_at', 'ASC');

                $cekperiode = Invoice::select('created_at');
        
                if (strtotime($dateend) !== false && strtotime($datestart) !== false) {     
                    $query = $query->whereBetween('created_at',[$datestart,$dateend." 23:59:59"]);
                    $cekperiode = $cekperiode->whereBetween('created_at',[$datestart,$dateend." 23:59:59"]);
                }
                
                foreach ($query->get() as $tgl) {
                    $data['perDay'][$tgl->tgl] = Invoice::whereDate('created_at', $tgl->tgl)
                    ->where('catalog_id',$catalog['id'])
                    ->get();
                }

            }    

            $cekperiodestart = $cekperiode->first();
            $cekperiodeend = $cekperiode->orderBy('id', 'DESC')->first();
            $data['periode'] = date('d M Y', strtotime($cekperiodestart->created_at))." - ".date('d M Y', strtotime($cekperiodeend->created_at));

            $data['report'] = $report;
            $data['getData'] = $query->get();
        }

        if($report == 'monthx') {
            if ($getstatus != 'all') {
                $data['status'] = $getstatus;
                $query = Invoice::select(
                    DB::raw('COUNT(id) AS totrans'), 
                    DB::raw('SUM(amount) AS total'), 
                    DB::raw("DATE_FORMAT(created_at,'%M %Y') as tanggal"),
                    DB::raw("DATE_FORMAT(created_at,'%Y-%m') as tgl")
                )
                ->where('catalog_id',$catalog['id'])
                ->groupBy('tanggal')
                ->orderBy('created_at', 'ASC')
                ->where('status',$getstatus)
                ->where(function($result) use ($getstatus){
                    if($getstatus == 'Completed'){
                        $result->where('pending','N');
                    }
                });
                
                $cekperiode = Invoice::select('created_at');
    
                if (strtotime($dateend) !== false && strtotime($datestart) !== false) {   
                    $datestart = date('Y-m-01', strtotime($datestart));
                    $dateend = date('Y-m-01', strtotime("+1 months", strtotime($dateend)));    
                    $query = $query->whereBetween('created_at',[$datestart,$dateend." 00:00:00"]);
                    $cekperiode = $cekperiode->whereBetween('created_at',[$datestart,$dateend." 00:00:00"]);
                }
                
                foreach ($query->get() as $tgl) {
                    $dateAwal = date($tgl->tgl."-1 00:00:00");
                    $dateAkhir = date('Y-m-d H:i:s', strtotime("+1 months", strtotime($dateAwal)));
                    
                    $data['perDay'][$tgl->tgl] = Invoice::select(
                        DB::raw('COUNT(id) AS totrans'), 
                        DB::raw('SUM(amount) AS total'), 
                        DB::raw("DATE_FORMAT(created_at,'%d %M %Y') as tanggal"),
                        DB::raw("DATE_FORMAT(created_at,'%Y-%m-%d') as tgl")
                    )
                    ->where('catalog_id',$catalog['id'])
                    ->groupBy('tanggal')
                    ->orderBy('created_at', 'ASC')
                    ->whereBetween('created_at',[$dateAwal,$dateAkhir])
                    ->where('status',$getstatus)
                    ->where(function($result) use ($getstatus){
                        if($getstatus == 'Completed'){
                            $result->where('pending','N');
                        }
                    })
                    ->get();
                }
            } else {         
                $query = Invoice::select(
                    DB::raw('COUNT(id) AS totrans'), 
                    DB::raw('SUM(amount) AS total'), 
                    DB::raw("DATE_FORMAT(created_at,'%M %Y') as tanggal"),
                    DB::raw("DATE_FORMAT(created_at,'%Y-%m') as tgl")
                )
                ->where('catalog_id',$catalog['id'])
                ->groupBy('tanggal')
                ->orderBy('created_at', 'ASC');

                $cekperiode = Invoice::select('created_at');
        
                if (strtotime($dateend) !== false && strtotime($datestart) !== false) {   
                    $datestart = date('Y-m-01', strtotime($datestart));
                    $dateend = date('Y-m-01', strtotime("+1 months", strtotime($dateend)));  
                    $query = $query->whereBetween('created_at',[$datestart,$dateend." 00:00:00"]);
                    $cekperiode = $cekperiode->whereBetween('created_at',[$datestart,$dateend." 00:00:00"]);
                }
                
                foreach ($query->get() as $tgl) {
                    $dateAwal = date($tgl->tgl."-1 00:00:00");
                    $dateAkhir = date('Y-m-d H:i:s', strtotime("+1 months", strtotime($dateAwal)));
                    
                    $data['perDay'][$tgl->tgl] = Invoice::select(
                        DB::raw('COUNT(id) AS totrans'), 
                        DB::raw('SUM(amount) AS total'), 
                        DB::raw("DATE_FORMAT(created_at,'%d %M %Y') as tanggal"),
                        DB::raw("DATE_FORMAT(created_at,'%Y-%m-%d') as tgl")
                    )
                    ->where('catalog_id',$catalog['id'])
                    ->groupBy('tanggal')
                    ->orderBy('created_at', 'ASC')
                    ->whereBetween('created_at',[$dateAwal,$dateAkhir])
                    ->get();
                }

            }    

            $cekperiodestart = $cekperiode->first();
            $cekperiodeend = $cekperiode->orderBy('id', 'DESC')->first();
            if (date('M Y', strtotime($cekperiodestart->created_at)) == date('M Y', strtotime($cekperiodeend->created_at))) {
                $data['periode'] = date('M Y', strtotime($cekperiodestart->created_at));
            } else { 
                $data['periode'] = date('M Y', strtotime($cekperiodestart->created_at))." - ".date('M Y', strtotime($cekperiodeend->created_at));
            }

            $data['report'] = $report;
            $data['getData'] = $query->get();
        }

        if($report == 'year') {
            if ($getstatus != 'all') {
                $data['status'] = $getstatus;
                $query = Invoice::select(
                    DB::raw('COUNT(id) AS totrans'), 
                    DB::raw('SUM(amount) AS total'), 
                    DB::raw("DATE_FORMAT(created_at,'%Y') as tanggal"),
                    DB::raw("DATE_FORMAT(created_at,'%Y') as tgl")
                )
                ->where('catalog_id',$catalog['id'])
                ->groupBy('tanggal')
                ->orderBy('created_at', 'ASC')
                ->where('status',$getstatus)
                ->where(function($result) use ($getstatus){
                    if($getstatus == 'Completed'){
                        $result->where('pending','N');
                    }
                });
                
                $cekperiode = Invoice::select('created_at');
    
                if (strtotime($dateend) !== false && strtotime($datestart) !== false) {     
                    $datestart = date('Y-01-01', strtotime($datestart));
                    $dateend = date('Y-01-01', strtotime("+1 years", strtotime($dateend)));  
                    $query = $query->whereBetween('created_at',[$datestart,$dateend." 00:00:00"]);
                    $cekperiode = $cekperiode->whereBetween('created_at',[$datestart,$dateend." 00:00:00"]);
                }
                
                foreach ($query->get() as $tgl) {
                    $dateAwal = ($tgl->tgl)."-01-01 00:00:00";
                    $dateAkhir = date('Y-m-d H:i:s', strtotime("+1 years", strtotime($dateAwal)));
                    
                    $data['perDay'][$tgl->tgl] = Invoice::select(
                        DB::raw('COUNT(id) AS totrans'), 
                        DB::raw('SUM(amount) AS total'), 
                        DB::raw("DATE_FORMAT(created_at,'%M %Y') as tanggal"),
                        DB::raw("DATE_FORMAT(created_at,'%Y-%m') as tgl")
                    )
                    ->where('catalog_id',$catalog['id'])
                    ->groupBy('tanggal')
                    ->orderBy('created_at', 'ASC')
                    ->whereBetween('created_at',[$dateAwal,$dateAkhir])
                    ->where('status',$getstatus)
                    ->where(function($result) use ($getstatus){
                        if($getstatus == 'Completed'){
                            $result->where('pending','N');
                        }
                    })
                    ->get();
                }
            } else {         
                $query = Invoice::select(
                    DB::raw('COUNT(id) AS totrans'), 
                    DB::raw('SUM(amount) AS total'), 
                    DB::raw("DATE_FORMAT(created_at,'%Y') as tanggal"),
                    DB::raw("DATE_FORMAT(created_at,'%Y') as tgl")
                )
                ->where('catalog_id',$catalog['id'])
                ->groupBy('tanggal')
                ->orderBy('created_at', 'ASC');

                $cekperiode = Invoice::select('created_at');
        
                if (strtotime($dateend) !== false && strtotime($datestart) !== false) {    
                    $datestart = date('Y-01-01', strtotime($datestart));
                    $dateend = date('Y-01-01', strtotime("+1 years", strtotime($dateend)));  
                    $query = $query->whereBetween('created_at',[$datestart,$dateend." 00:00:00"]);
                    $cekperiode = $cekperiode->whereBetween('created_at',[$datestart,$dateend." 00:00:00"]);
                }
                
                foreach ($query->get() as $tgl) {
                    $dateAwal = ($tgl->tgl)."-01-01 00:00:00";
                    $dateAkhir = date('Y-m-d H:i:s', strtotime("+1 years", strtotime($dateAwal)));
                    
                    $data['perDay'][$tgl->tgl] = Invoice::select(
                        DB::raw('COUNT(id) AS totrans'), 
                        DB::raw('SUM(amount) AS total'), 
                        DB::raw("DATE_FORMAT(created_at,'%M %Y') as tanggal"),
                        DB::raw("DATE_FORMAT(created_at,'%Y-%m') as tgl")
                    )
                    ->where('catalog_id',$catalog['id'])
                    ->groupBy('tanggal')
                    ->orderBy('created_at', 'ASC')
                    ->whereBetween('created_at',[$dateAwal,$dateAkhir])
                    ->get();
                }

            }    

            $cekperiodestart = $cekperiode->first();
            $cekperiodeend = $cekperiode->orderBy('id', 'DESC')->first();

            if (date('Y', strtotime($cekperiodestart->created_at)) == date('Y', strtotime($cekperiodeend->created_at))) {
                $data['periode'] = date('Y', strtotime($cekperiodestart->created_at));
            } else { 
                $data['periode'] = date('Y', strtotime($cekperiodestart->created_at))." - ".date('Y', strtotime($cekperiodeend->created_at));
            }

            $data['report'] = $report;
            $data['getData'] = $query->get();
        }
        return view('pages.transaction.tableprint', $data);
    }
    public function getDataMon(Request $request)
    {
        $status = $getstatus = trim($request->input('status'));
        $dateS = trim($request->input('datestart'));
        $dateE = trim($request->input('dateend'))." 23:59:59";
        $type = $request->type;

        if(Session::get('catalogsession') == 'All'){
            $catalog = Catalog::orderBy('id','desc')->first();
        }else{
            $catalog = Catalog::where('id',Session::get('catalogsession'))->orderBy('id','desc')->first();
        }

        if ($type == 'daily') {
            if ($getstatus != 'all') {
                $query = Invoice::select(
                    DB::raw('COUNT(id) AS totrans'), 
                    DB::raw('SUM(amount) AS total'), 
                    DB::raw("DATE_FORMAT(created_at,'%d %M %Y') as tanggal"),
                    DB::raw("DATE_FORMAT(created_at,'%Y-%m-%d') as tgl")
                )
                ->where('catalog_id',$catalog['id'])
                ->groupBy('tanggal')
                ->orderBy('created_at', 'ASC')
                ->where('status',$status)
                ->where(function($result) use ($status){
                    if($status == 'Completed'){
                        $result->where('pending','N');
                    }
                });
    
                if (strtotime($dateS) !== false && strtotime($dateE) !== false) {  
                    $query->whereBetween('created_at',[$dateS,$dateE]);     
                }
                
                foreach ($query->get() as $tgl) {
                    $data['perDay'][$tgl->tgl] = Invoice::where('status',$status)
                        ->where(function($result) use ($status){
                            if($status == 'Completed'){
                                $result->where('pending','N');
                            }
                        })
                        ->whereDate('created_at', $tgl->tgl)
                        ->where('catalog_id',$catalog['id'])
                        ->get();
                }
            } else { 
                $query = Invoice::select(
                    DB::raw('COUNT(id) AS totrans'), 
                    DB::raw('SUM(amount) AS total'), 
                    DB::raw("DATE_FORMAT(created_at,'%d %M %Y') as tanggal"),
                    DB::raw("DATE_FORMAT(created_at,'%Y-%m-%d') as tgl")
                )
                ->where('catalog_id',$catalog['id'])
                ->groupBy('tanggal')
                ->orderBy('created_at', 'ASC');
        
                if (strtotime($dateS) !== false && strtotime($dateE) !== false) {  
                    $query->whereBetween('created_at',[$dateS,$dateE]);     
                }
                
                foreach ($query->get() as $tgl) {
                    $data['perDay'][$tgl->tgl] = Invoice::whereDate('created_at', $tgl->tgl)
                    ->where('catalog_id',$catalog['id'])
                    ->get();
                }
            }
        }

        if ($type == 'monthly') {
            if ($getstatus != 'all') {
                $query = Invoice::select(
                    DB::raw('COUNT(id) AS totrans'), 
                    DB::raw('SUM(amount) AS total'), 
                    DB::raw("DATE_FORMAT(created_at,'%M %Y') as tanggal"),
                    DB::raw("DATE_FORMAT(created_at,'%Y-%m') as tgl")
                )
                ->where('catalog_id',$catalog['id'])
                ->groupBy('tanggal')
                ->orderBy('created_at', 'ASC')
                ->where('status',$status)
                ->where(function($result) use ($status){
                    if($status == 'Completed'){
                        $result->where('pending','N');
                    }
                });
    
                if (strtotime($dateS) !== false && strtotime($dateE) !== false) {
                    $dateS = date('Y-m', strtotime($dateS))."-01";
                    $dateE = date('Y-m-d', strtotime("+1 months", strtotime($dateE))); 
                    $query->whereBetween('created_at',[$dateS,$dateE]);     
                }
                
                foreach ($query->get() as $tgl) {
                    $dateAwal = date($tgl->tgl."-1 00:00:00");
                    $dateAkhir = date('Y-m-d H:i:s', strtotime("+1 months", strtotime($dateAwal)));
                    
                    $data['perDay'][$tgl->tgl] = Invoice::select(
                        DB::raw('COUNT(id) AS totrans'), 
                        DB::raw('SUM(amount) AS total'), 
                        DB::raw("DATE_FORMAT(created_at,'%d %M %Y') as tanggal"),
                        DB::raw("DATE_FORMAT(created_at,'%Y-%m-%d') as tgl")
                    )
                    ->where('catalog_id',$catalog['id'])
                    ->groupBy('tanggal')
                    ->orderBy('created_at', 'ASC')
                    ->whereBetween('created_at',[$dateAwal,$dateAkhir])
                    ->where('status',$status)
                    ->where(function($result) use ($status){
                        if($status == 'Completed'){
                            $result->where('pending','N');
                        }
                    })
                    ->get();
                }
            } else { 
                $query = Invoice::select(
                    DB::raw('COUNT(id) AS totrans'), 
                    DB::raw('SUM(amount) AS total'), 
                    DB::raw("DATE_FORMAT(created_at,'%M %Y') as tanggal"),
                    DB::raw("DATE_FORMAT(created_at,'%Y-%m') as tgl")
                )
                ->where('catalog_id',$catalog['id'])
                ->groupBy('tanggal')
                ->orderBy('created_at', 'ASC');
        
                if (strtotime($dateS) !== false && strtotime($dateE) !== false) {  
                    $dateS = date('Y-m', strtotime($dateS))."-01";
                    $dateE = date('Y-m-d', strtotime("+1 months", strtotime($dateE))); 
                    $query->whereBetween('created_at',[$dateS,$dateE]);     
                }
                
                foreach ($query->get() as $tgl) {
                    $dateAwal = date($tgl->tgl."-1 00:00:00");
                    $dateAkhir = date('Y-m-d H:i:s', strtotime("+1 months", strtotime($dateAwal)));
                    
                    $data['perDay'][$tgl->tgl] = Invoice::select(
                        DB::raw('COUNT(id) AS totrans'), 
                        DB::raw('SUM(amount) AS total'), 
                        DB::raw("DATE_FORMAT(created_at,'%d %M %Y') as tanggal"),
                        DB::raw("DATE_FORMAT(created_at,'%Y-%m-%d') as tgl")
                    )
                    ->where('catalog_id',$catalog['id'])
                    ->groupBy('tanggal')
                    ->orderBy('created_at', 'ASC')
                    ->whereBetween('created_at',[$dateAwal,$dateAkhir])
                    ->get(); 
                }
            }
        }

        if ($type == 'yearly') {
            if ($getstatus != 'all') {
                $query = Invoice::select(
                    DB::raw('COUNT(id) AS totrans'), 
                    DB::raw('SUM(amount) AS total'), 
                    DB::raw("DATE_FORMAT(created_at,'%Y') as tanggal"),
                    DB::raw("DATE_FORMAT(created_at,'%Y') as tgl")
                )
                ->where('catalog_id',$catalog['id'])
                ->groupBy('tanggal')
                ->orderBy('created_at', 'ASC')
                ->where('status',$status)
                ->where(function($result) use ($status){
                    if($status == 'Completed'){
                        $result->where('pending','N');
                    }
                });
    
                if (strtotime($dateS) !== false && strtotime($dateE) !== false) {  
                    $dateS = date('Y', strtotime($dateS))."-01-01";
                    $dateE = date('Y-01-01', strtotime("+1 years", strtotime($dateE))); 
                    $query->whereBetween('created_at',[$dateS,$dateE]);     
                }
                
                foreach ($query->get() as $tgl) {
                    $dateAwal = date($tgl->tgl."-1 00:00:00");
                    $dateAkhir = date('Y-m-d H:i:s', strtotime("+12 months", strtotime($dateAwal)));
                    
                    $data['perDay'][$tgl->tgl] = Invoice::select(
                        DB::raw('COUNT(id) AS totrans'), 
                        DB::raw('SUM(amount) AS total'), 
                        DB::raw("DATE_FORMAT(created_at,'%M %Y') as tanggal"),
                        DB::raw("DATE_FORMAT(created_at,'%Y-%m') as tgl")
                    )
                    ->where('catalog_id',$catalog['id'])
                    ->groupBy('tanggal')
                    ->orderBy('created_at', 'ASC')
                    ->whereBetween('created_at',[$dateAwal,$dateAkhir])
                    ->where('status',$status)
                    ->where(function($result) use ($status){
                        if($status == 'Completed'){
                            $result->where('pending','N');
                        }
                    })
                    ->get();
                }
            } else { 
                $query = Invoice::select(
                    DB::raw('COUNT(id) AS totrans'), 
                    DB::raw('SUM(amount) AS total'), 
                    DB::raw("DATE_FORMAT(created_at,'%Y') as tanggal"),
                    DB::raw("DATE_FORMAT(created_at,'%Y') as tgl")
                )
                ->where('catalog_id',$catalog['id'])
                ->groupBy('tanggal')
                ->orderBy('created_at', 'ASC');
        
                if (strtotime($dateS) !== false && strtotime($dateE) !== false) {  
                    $dateS = date('Y', strtotime($dateS))."-01-01";
                    $dateE = date('Y-01-01', strtotime("+1 years", strtotime($dateE))); 
                    $query->whereBetween('created_at',[$dateS,$dateE]);     
                }
                
                foreach ($query->get() as $tgl) {
                    $dateAwal = date($tgl->tgl."-1 00:00:00");
                    $dateAkhir = date('Y-m-d H:i:s', strtotime("+12 months", strtotime($dateAwal)));
                    
                    $data['perDay'][$tgl->tgl] = Invoice::select(
                        DB::raw('COUNT(id) AS totrans'), 
                        DB::raw('SUM(amount) AS total'), 
                        DB::raw("DATE_FORMAT(created_at,'%M %Y') as tanggal"),
                        DB::raw("DATE_FORMAT(created_at,'%Y-%m') as tgl")
                    )
                    ->where('catalog_id',$catalog['id'])
                    ->groupBy('tanggal')
                    ->orderBy('created_at', 'ASC')
                    ->whereBetween('created_at',[$dateAwal,$dateAkhir])
                    ->get(); 
                }
            }
        }
        
        $data['type'] = $type;
        $data['request'] = $request->all();
        $data['getData'] = $query->paginate(10);
        $data['pagination'] = $data['getData']->links();
        return view('pages.transaction.tablemon',$data);
    }
    public function getDataYear(Request $request)
    {
        $columns = ['invoice_number'];
        $keyword = trim($request->input('searchfield'));
        $getstatus = trim($request->input('status'));
        $datestart = trim($request->input('datestart'));
        $dateend = trim($request->input('dateend'));
        $print = trim($request->input('print'));

    	if($getstatus == 'checkout'){
    		$status = 'Checkout';
    		$icon = 'pe-7s-next-2';
    	}
    	elseif($getstatus == 'approve'){
    		$status = 'Approve';
    		$icon = 'lnr-select';
    	}
    	elseif($getstatus == 'process'){
    		$status = 'Process';
    		$icon = 'lnr-hourglass';
    	}
    	elseif($getstatus == 'delivered'){
    		$status = 'Delivered';
    		$icon = 'lnr-location';
    	}
    	elseif($getstatus == 'completed'){
    		$status = 'Completed';
    		$icon = 'lnr-checkmark-circle';
    	}
    	elseif($getstatus == 'cancel'){
    		$status = 'Cancel';
    		$icon = 'lnr-cross-circle';
    	}
    	elseif($getstatus == 'order'){
    		$status = 'Order';
    		$icon = 'lnr-cross-circle';
    	}
        if(Session::get('catalogsession') == 'All'){
            $catalog = Catalog::orderBy('id','desc')->first();
        }else{
            $catalog = Catalog::where('id',Session::get('catalogsession'))->orderBy('id','desc')->first();
        }

        if ($getstatus != 'all') {
            $query = Invoice::where('catalog_id',$catalog['id'])
            ->where('status',$status)
            ->where(function($result) use ($status){
                if($status == 'Completed'){
                    $result->where('pending','N');
                }
            })
            ->where(function($result) use ($keyword,$columns){
                foreach($columns as $column)
                {
                    if($keyword != ''){
                        $result->orWhere($column,'LIKE','%'.$keyword.'%');
                    }
                }
            })
            ->orderBy('id','desc');
            if (strtotime($dateend) !== false && strtotime($datestart) !== false) {     
                $query = $query->whereBetween('created_at',[$datestart,$dateend]);
            }
        } else {         
            $query = Invoice::where('catalog_id',$catalog['id'])
            ->where(function($result) use ($keyword,$columns){
                foreach($columns as $column)
                {
                    if($keyword != ''){
                        $result->orWhere($column,'LIKE','%'.$keyword.'%');
                    }
                }
            })
            ->orderBy('id','asc');
            if (strtotime($dateend) !== false && strtotime($datestart) !== false) {       
                $query = $query->whereBetween('created_at',[$datestart,$dateend]);
            }
        }
    
        $data['request'] = $request->all();
        $data['getData'] = $query->paginate(10);
        $data['pagination'] = $data['getData']->links();
        return view('pages.transaction.table',$data);
    }
}
