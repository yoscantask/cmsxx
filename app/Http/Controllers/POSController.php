<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Models\Catalog;
use App\Models\CatalogDetail;
use App\Models\Invoice;
use App\Models\InvoiceDetail;
use App\Models\Items;
use App\Models\ItemsDetail;
use App\Models\InvoiceAddons;
use App\Models\PriceList;

use Auth;
use Session;
use getData;

class POSController extends Controller
{
    public function __construct()
	{
	    $this->middleware('auth');
	}
	public function index()
    {
        // getData::getCatalogSession('advance_payment')
        $data['titlepage']='Point of Sales';
        $data['maintitle']='Point of Sales';
        return view('pages.pos.index',$data);
    }
    public function getTable(Request $request){
        $columns = ['items_name','category_name'];
        $keyword = trim($request->input('searchfield'));
        $id_payment = trim($request->input('id_payment'));

        $query = CatalogDetail::select('catalogdetail.*','items.*','category.category_name', 'price_lists.price', 'price_lists.diskon')
                                ->leftJoin('items','catalogdetail.item','=','items.id')
                                ->leftJoin('category','catalogdetail.category_id','=','category.id')
                                ->leftJoin('price_lists','items.id','=','price_lists.id_item')
                                ->where('price_lists.id_payment', '=', $id_payment)
                                ->where('catalogdetail.catalog_id',getData::getCatalogSession('id'))
                                //->where('ready_stock','Y')
                                ->where(function($result) use ($keyword,$columns){
                                    foreach($columns as $column)
                                    {
                                        if($keyword != ''){
                                            $result->orWhere($column,'LIKE','%'.$keyword.'%');
                                        }
                                    }
                                });
        $data['request'] = $request->all();
        $data['getData'] = $query->paginate(10);
        $data['pagination'] = $data['getData']->appends(['searchfield'=>($request->searchfield == '')?"":$request->searchfield])->links();
        return view('pages.pos.table',$data);
    }
    public function getTablePending(Request $request){
        if(Session::get('catalogsession') == 'All'){
            $catalog = Catalog::orderBy('id','desc')->first();
        }else{
            $catalog = Catalog::where('id',Session::get('catalogsession'))->orderBy('id','desc')->first();
        }
        $query = Invoice::where('catalog_id',$catalog['id'])
                        ->where('status','Completed')
                        ->where('pending','Y')
                        //->where('payment_method','>',0)
                        ->orderBy('id');
        if($request->has('searchinvoice')){
            $query->where('invoice_number', 'LIKE', '%'.$request->searchinvoice.'%');
        }
        $data['request'] = $request->all();
        $data['getData'] = $query->paginate(10);
        $data['pagination'] = $data['getData']->appends(['searchinvoice'=>($request->searchinvoice == '')?"":$request->searchinvoice])->links();
        return view('pages.pos.tableinvoice',$data);
    }
    public function getData(Request $request)
    {
        if($request->isMethod('post')){
            if(getData::getCatalogSession('advance_payment') == 'Y'){
                if(Invoice::save_data($request)){
                    $status='success';
                    $message='Your request was successful.';
                }else{
                    $status='error';
                    $message='Oh snap! something went wrong.';
                }
            }else{
                if(Invoice::save_data_last($request)){
                    $status='success';
                    $message='Your request was successful.';
                }else{
                    $status='error';
                    $message='Oh snap! something went wrong.';
                }
            }
            
            $notif=['status'=>$status,'message'=>$message];
            return response()->json($notif);
        }else{
            $data['id_payment'] = trim($request->input('id_payment'));
            $data['invoice'] = Invoice::where('invoice_number', Session::get('cartInvoice'))->where('device_session',Session::get('device_session'))->first();
            if(!empty($data['invoice'])){
                $selectinvoiceitem = InvoiceDetail::where('invoiceid', $data['invoice']['id'])->get();
                foreach ($selectinvoiceitem as $item) {
                    $getprice = PriceList::where('id_item', $item->item_id)->where('id_payment', $data['id_payment'])->first();
                    $update = DB::table('invoicedetail')
                        ->where('id', $item->id)
                        ->update(
                        [
                            'price' => $getprice->price,
                            'discount' => $getprice->diskon,
                            'updated_at' => NOW()
                        ]
                    );
                }

                $data['item'] = InvoiceDetail::where('invoiceid', $data['invoice']['id'])
                    ->groupBy('category')
                    ->orderBy('id')
                    ->get();
                    if(getData::getCatalogSession('advance_payment') == 'Y'){
                        return view('pages.pos.transaction_front',$data);
                    }else{
                        return view('pages.pos.transaction_back',$data);
                    }
            }else{
                $data['item']=[];
            }
        }
    }
    public function getEditData(Request $request,$id)
    {
        if($request->isMethod('post')){
            if(getData::getCatalogSession('advance_payment') == 'Y'){
                if(Invoice::save_data($request)){
                    $status='success';
                    $message='Your request was successful.';
                }else{
                    $status='error';
                    $message='Oh snap! something went wrong.';
                }
            }else{
                if(Invoice::save_data_last($request)){
                    $status='success';
                    $message='Your request was successful.';
                }else{
                    $status='error';
                    $message='Oh snap! something went wrong.';
                }
            }
            
            $notif=['status'=>$status,'message'=>$message];
            return response()->json($notif);
        }else{
            $data['invoice'] = Invoice::where('id', $id)->first();
            if(!empty($data['invoice'])){
                $data['item'] = InvoiceDetail::where('invoiceid', $data['invoice']['id'])
                                                ->groupBy('category')
                                                ->orderBy('id')
                                                ->get();
            }else{
                $data['item']=[];
            }
            return view('pages.pos.transaction_edit',$data);
        }
    }
    public function updateData(Request $request)
    {
        if(InvoiceDetail::update_data($request)){
            $status='success';
            $message='Your request was successful.';
        }else{
            $status='error';
            $message='Oh snap! something went wrong.';
        }
        $notif=['status'=>$status,'message'=>$message];
        return response()->json($notif);
    }
    public function updateCartBackData(Request $request)
    {
        if(InvoiceDetail::update_data_back_payment($request)){
            $status='success';
            $message='Your request was successful.';
        }else{
            $status='error';
            $message='Oh snap! something went wrong.';
        }
        $notif=['status'=>$status,'message'=>$message];
        return response()->json($notif);
    }
    public function checkoutData(Request $request)
    {
        $this->validate($request, [
            'amount' => 'required|numeric',
            //'position' => 'required',
        ]);
        if($request->input('payment_method')==2){
            $this->validate($request, [
                'imagefile' => 'required|max:1000|mimes:jpeg,jpg,png'
            ]);
        }
        if(Invoice::checkout_data($request)){
            Session::forget('cartInvoice');
            $status='success';
            $message='Your request was successful.';
        }else{
            $status='error';
            $message='Oh snap! something went wrong.';
        }
        $notif=['status'=>$status,'message'=>$message];
        return response()->json($notif);
    }
    public function completePending(Request $request)
    {
        $this->validate($request, [
            'amount' => 'required|numeric',
        ]);
        if($request->input('payment_method')==2){
            $this->validate($request, [
                'imagefile' => 'required|max:1000|mimes:jpeg,jpg,png'
            ]);
        }
        if(Invoice::complete_pending($request)){
            Session::forget('cloneOrder');
            $status='success';
            $message='Your request was successful.';
        }else{
            $status='error';
            $message='Oh snap! something went wrong.';
        }
        $notif=['status'=>$status,'message'=>$message];
        return response()->json($notif);
    }
    public function cancelData()
    {
        if(Invoice::delete_data()){
            Session::forget('cartInvoice');
            $status='success';
            $message='Your request was successful.';
        }else{
            $status='error';
            $message='Oh snap! something went wrong.';
        }
        $notif=['status'=>$status,'message'=>$message];
        return response()->json($notif);
    }
    public function deleteData($id=null)
    {
        if(InvoiceDetail::delete_data($id)){
            $status='success';
            $message='Your request was successful.';
        }else{
            $status='error';
            $message='Oh snap! something went wrong.';
        }
        $notif=['status'=>$status,'message'=>$message];
        return response()->json($notif);
    }
    public function deleteaddon($detail = null,$group = null)
    {
        if (InvoiceAddons::delete_addon($detail,$group)) {
            $status = 'success';
            $message = 'Your request was successful.';
        } else {
            $status = 'error';
            $message = 'Oh snap! something went wrong.';
        }
        return ['status' => $status, 'message' => $message];
    }
    public function getCloneData(){
        $data['invoice'] = Invoice::where('invoice_number', Session::get('cloneOrder'))->first();
        $data['item'] = InvoiceDetail::where('invoiceid', $data['invoice']['id'])
                        ->groupBy('category')
                        ->orderBy('id')
                        ->get();
        if($data['item']->count() > 0){
            
            return view('pages.pos.transaction_split',$data);
        }else{
            return "";
        }
    }
    public function clonedata($item = null,$detailinvoice = null)
    {
        if(!Session::has('cloneOrder')){
            Session::put('cloneOrder',date('YmdHis'));
        }
        $invoice = Session::get('cloneOrder');
        if (InvoiceDetail::clone_data($item,$detailinvoice,$invoice)) {
            $status = 'success';
            $message = 'Your request was successful.';
        } else {
            $status = 'error';
            $message = 'Oh snap! something went wrong.';
        }
        return ['status' => $status, 'message' => $message];
    }
    public function showInvoice($id)
    {
        $data = Invoice::find($id);
        return $data;
    }
    public function edit($id)
    {
        $data['invoice'] = $this->showInvoice($id);
        $data['titlepage']='Order Number '.$data['invoice']['invoice_number'];
        $data['maintitle']='Order Number '.$data['invoice']['invoice_number'];
        return view('pages.pos.edit',$data);
    }
    public function showDetail($id)
    {
        $data = InvoiceDetail::select('invoicedetail.*', 'items.item_image_one')
            ->where('invoicedetail.id', '=', $id)
            ->leftJoin('items','invoicedetail.item_id','=','items.id')
            ->first();
        return $data;
    }
    public function getDataPending($id)
    {
        $data['invoice'] = Invoice::where('id', $id)->first();
        if(!empty($data['invoice'])){
            $data['item'] = InvoiceDetail::where('invoiceid', $data['invoice']['id'])
                ->groupBy('category')
                ->orderBy('id')
                ->get();
        }else{
            $data['item']=[];
        }
        return view('pages.pos.transaction_pending',$data);
    }
    public function showItem($id)
    {
        $data['invoice'] = Invoice::where('id', $id)->first();
        if(!empty($data['invoice'])){
            $data['item'] = InvoiceDetail::where('invoiceid', $data['invoice']['id'])
                ->wherenotIn('item_status',['Order','Checkout'])
                ->groupBy('category')
                ->orderBy('id')
                ->get();
        }else{
            $data['item']=[];
        }
        return view('pages.pos.select_item',$data);
    }
    public function addons($item,$group)
    {
        $data['addons'] = ItemsDetail::select('items_detail.*',
                            'category.category_name',
                            'items.items_name',
                            'items.items_price')
                            ->leftJoin('items','items_detail.addon','=','items.id')
                            ->leftJoin('category','items_detail.category_id','=','category.id')
                            ->where('items.item_type','Add')
                            ->where('items.ready_stock','Y')
                            ->where('item_id',$item)
                            ->groupBy('category_id')
                            ->get();
        $data['item']=$item;
        $data['group']=$group;
        return view('pages.pos.addons', $data);
    }
    public function updatecartaddons(Request $request)
    {
        if (InvoiceAddons::update_data($request)) {
            $status = 'success';
            $message = 'Your request was successful.';
        } else {
            $status = 'error';
            $message = 'Oh snap! something went wrong.';
        }
        return ['status' => $status, 'message' => $message];
    }
}
