<?php

namespace App\Http\Controllers\Price;

use App\Models\PricePayment;
use App\Models\PriceList;
use App\Models\Items;
use App\Models\Invoice;
use App\Models\Catalog;
use App\Models\MemberStatus;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use Auth;
use Carbon\Carbon;
use Illuminate\Validation\Rule;
use Session;

class PriceListController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        if (Auth::user()->level == 'Member') {
            app()->setLocale(Session::get('locale'));
            $namamenu = Items::where('id',$id)->first();
            $data['titlepage']=__('lang.manage').' '.__('lang.payment');
            $data['maintitle']=__('lang.manage').' '.__('lang.payment');
            $data['createnew']=__('lang.createnew');
            $data['judul']=$namamenu;
            return view('pages.price.list.data',$data);
        } else {return redirect()->back();}
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // $getitems = Items::where('item_type', 'Main')->get();
        // $getidpayment = PricePayment::all();
        // foreach ($getitems as $item) {
        // foreach ($getidpayment as $idpay) {
        //     $cekdata = PriceList::where('id_item', $item->id)->where('id_payment', $idpay->id)->first();
        //     $diskon = 1000;
        //     if ($idpay->id == '3') {$diskon = 2000;}
        //     if (!$cekdata) {
        //         $create = DB::table('price_lists')->insert(
        //             [
        //                 'id_item' => $item->id,
        //                 'id_payment' => $idpay->id,
        //                 'price' => $item->items_price,
        //                 'diskon' => $diskon,
        //                 'created_at' => NOW(),
        //                 'updated_at' => NOW()
        //             ]
        //         );
        //     } else {
        //         $update = DB::table('price_lists')
        //             ->where('id', $item->id)
        //             ->update(
        //             [
        //                 'price' => $item->items_price,
        //                 'diskon' => $diskon,
        //                 'updated_at' => NOW()
        //             ]
        //         );
        //     }
        // }
        // }
        $querypen = MemberStatus::whereDate('expdate','<=', NOW())
        ->where('status', 'Pending')
        ->get();

        $querypaid = MemberStatus::whereDate('expdate','<=', NOW())
        ->where('status', 'Paid')
        ->get();

        return $query = $querypen->union($querypaid);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (Auth::user()->level == 'Member') {
            if ($request->price) {
                $this->validate($request, [
                    'price' => 'required|numeric|min:500|not_in:499',
                    'diskon' => 'required|numeric',
                ]);

                $check = PriceList::where('id_item', '=', $request->id_item)
                                    ->where('id_payment', '=', $request->id_payment)
                                    ->first();
                
                if ($check) {
                    $status='error';
                    $message='Metode Payment yg anda pilih sudah ada, silahkan pilih yang lain';
                    $notif=['status'=>$status,'message'=>$message];
                    return response()->json($notif);
                } else {
                    if ($request->price > 400 && $request->diskon >= 0) {
                        $create = DB::table('price_lists')->insert(
                            [
                                'id_item' => $request->id_item,
                                'id_payment' => $request->id_payment,
                                'price' => $request->price,
                                'diskon' => $request->diskon,
                                'created_at' => NOW(),
                                'updated_at' => NOW()
                            ]
                        );
                        if($create){
                            $status='success';
                            $message=__('lang.create').' data '.__('lang.success');
                        }else{
                            $status='error';
                            $message=__('lang.create').' data '.__('lang.failed');
                        }
                    } else {
                        $status='error';
                        $message='Masukkan harga dengan benar';
                    }        
                    $notif=['status'=>$status,'message'=>$message];
                    return response()->json($notif);
                }
            } else {
                $id = $request->id;
                $query = PriceList::select('price_lists.*', 'price_payments.jenis', 'price_payments.ket', 'price_payment_cats.nama')
                            ->leftJoin('price_payments','price_lists.id_payment','=','price_payments.id')
                            ->leftJoin('price_payment_cats','price_payment_cats.id','=','price_payments.id_payment_cat')
                            ->where('id_item', '=', $id)
                            ->orderBy('updated_at','desc');
                $data['request'] = $request->all();
                $data['getData'] = $query->paginate(10);
                $data['pagination'] = $data['getData']->links();
                return view('pages.price.list.table',$data);
            }
        } else {return redirect()->back();}
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $query = PriceList::where('id', $id)->first();
        return $query;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (Auth::user()->level == 'Member') {
            $this->validate($request, [
                'price' => 'required|numeric|min:500|not_in:499',
                'diskon' => 'required|numeric',
            ]);
            if ($request->price > 400 && $request->diskon >= 0) {
                $update = DB::table('price_lists')
                    ->where('id', $request->id)
                    ->update(
                    [
                        'price' => $request->price,
                        'diskon' => $request->diskon,
                        'updated_at' => NOW()
                    ]
                );

                if($update){
                    $status='success';
                    $message=__('lang.update').' data '.__('lang.success');
                }else{
                    $status='error';
                    $message=__('lang.update').' data '.__('lang.failed');
                }
            } else {
                $status='error';
                $message='Masukkan harga dengan benar';
            }
            $notif=['status'=>$status,'message'=>$message];
            return response()->json($notif);
        } else {
            $status='error';
            $message='Akses ditolak';
            $notif=['status'=>$status,'message'=>$message];
            return response()->json($notif);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Auth::user()->level == 'Member') {
            $check = DB::table('price_lists')
                    ->select('status')
                    ->where('id', $id)
                    ->first();
            if ($check->status == 'Active') {$statux = 'Not Active';}
            if ($check->status == 'Not Active') {$statux = 'Active';}
            
            $delete = DB::table('price_lists')
                    ->where('id', $id)
                    ->update(
                        [
                            'status' => $statux,
                            'updated_at' => NOW(),
                        ]
                    );

            if($delete){
                $status='success';
                $message=__('lang.yrws');
            }else{
                $status='error';
                $message=__('lang.yrwserror');
            }
            $notif=['status'=>$status,'message'=>$message];
            return response()->json($notif);
        }
        else {return redirect()->back();}
    }
}
