<?php

namespace App\Http\Controllers\Price;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Models\PricePayment;

use Auth;
use Carbon\Carbon;
use Illuminate\Validation\Rule;
use Session;

class PricePaymentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        app()->setLocale(Session::get('locale'));
        $data['titlepage']=__('lang.list').' Payment Gateway';
        $data['maintitle']=__('lang.list').' Payment Gateway';
        return view('pages.price.payment.data',$data);
        // $query = PricePayment::select('price_payments.*', 'price_payment_cats.nama')
        //                     ->leftJoin('price_payment_cats','price_payments.id_payment_cat','=','price_payment_cats.id')
        //                     ->get();
        // dump($query);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {
        if (Auth::user()->level == 'Super Admin') {
            if ($request->jenis || $request->ket) {   
                $this->validate($request, [
                    'jenis' => 'required|min:3',
                    'ket' => 'required|min:3',
                ]);

                $create = DB::table('price_payments')->insert(
                    [
                        'jenis' => $request->jenis,
                        'id_payment_cat' => $request->id_payment_cat,
                        'ket' => $request->ket,
                        'methode' => $request->methode,
                        'created_at' => NOW(),
                        'updated_at' => NOW()
                    ]
                );
    
                if($create){
                    $status='success';
                    $message=__('lang.change').' data '.__('lang.success').$request->jenis;
                }else{
                    $status='error';
                    $message=__('lang.change').' data '.__('lang.failed').$request->jenis;
                }
                $notif=['status'=>$status,'message'=>$message];
                return response()->json($notif);
            } else {
                $columns = ['nama', 'jenis'];
                $keyword = trim($request->input('searchfield'));
                $query = PricePayment::select('price_payments.*', 'price_payment_cats.nama')
                            ->leftJoin('price_payment_cats','price_payments.id_payment_cat','=','price_payment_cats.id')
                            ->where(function($result) use ($keyword,$columns){
                                foreach($columns as $column)
                                {
                                    if($keyword != ''){
                                        $result->orWhere($column,'LIKE','%'.$keyword.'%');
                                    }
                                }
                            })
                            ->orderBy('id','desc');
                $data['request'] = $request->all();
                $data['getData'] = $query->paginate(10);
                $data['pagination'] = $data['getData']->links();
                return view('pages.price.payment.table',$data);    
            }
        } else {return redirect()->back();}
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $query=PricePayment::select('price_payments.*', 'price_payment_cats.nama')
                    ->leftJoin('price_payment_cats','price_payments.id_payment_cat','=','price_payment_cats.id')
                    ->where('price_payments.id', $id)
                    ->first();
        return $query;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $m)
    {
        if (Auth::user()->level == 'Super Admin') {
            $this->validate($request, [
                'jenis' => 'required|min:3',
                'ket' => 'required|min:3',
            ]);

            $update = DB::table('price_payments')
                ->where('id', $request->id)
                ->update(
                [
                    'jenis' => $request->jenis,
                    'id_payment_cat' => $request->id_payment_cat,
                    'ket' => $request->ket,
                    'methode' => $request->methode,
                    'updated_at' => NOW(),
                ]
            );

            if($update){
                $status='success';
                $message=__('lang.change').' data '.__('lang.success');
            }else{
                $status='error';
                $message=__('lang.change').' data '.__('lang.failed');
            }
            $notif=['status'=>$status,'message'=>$message];
            return response()->json($notif);
        }
        else {return redirect()->back();}
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Auth::user()->level == 'Super Admin') {
            $check = DB::table('price_payments')
                    ->select('status')
                    ->where('id', $id)
                    ->first();
            if ($check->status == 'Active') {$statux = 'Not Active';}
            if ($check->status == 'Not Active') {$statux = 'Active';}
            
            $delete = DB::table('price_payments')
                    ->where('id', $id)
                    ->update(
                        [
                            'status' => $statux,
                            'updated_at' => NOW(),
                        ]
                    );

            if($delete){
                $status='success';
                $message=__('lang.yrws');
            }else{
                $status='error';
                $message=__('lang.yrwserror');
            }
            $notif=['status'=>$status,'message'=>$message];
            return response()->json($notif);
        }
        else {return redirect()->back();}
    }
}
