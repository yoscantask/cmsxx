@extends('layouts.main')
@section('content')
<div class="app-page-title">
    <div class="page-title-wrapper">
        <div class="page-title-heading">
            <div class="page-title-icon">
                <i class="lnr-printer icon-gradient bg-ripe-malin"> </i>
            </div>
            <div>
                {{ $maintitle }}
                <div class="page-title-subheading">This dashboard was created as an example of the flexibility that Architect offers.</div>
            </div>
        </div>
        <div class="page-title-actions">
            @if(!empty($request['searchfield']))
                <a href="{{ url('/transaction/'.$status) }}" class="btn-shadow btn btn-info btn-sm"><i class="icon lnr-sync"></i> Refresh</a>
            @endif
        </div>
    </div>
</div>

<div class="tabs-animation">
    <div class="row">
        <div class="col-md-12">
            <div class="main-card mb-3 card">
                
                    <div class="card-body">
                        <form id="myForm" method="POST">
                            {!! csrf_field() !!}
                            <div class="row">
                                <div class="col-lg-2 col-md-3 col-sm-6">
                                    <input type="text" class="form-control" name="searchfield" placeholder="Search Invoice..." value="{{ (!empty($request['searchfield']))?$request['searchfield']:'' }}">
                                </div>
                                <div class="col-lg-2 col-md-3 col-sm-6">
                                    <select id="status" name="status" class="custom-select" onchange="filterData()">
                                        <option value="All">All</option>
                                        @if(getData::checkStepTransaction('Approve',Session::get('catalogsession')))
                                            <option value="Approve">Approve</option>
                                        @endif
                                        @if(getData::checkStepTransaction('Process',Session::get('catalogsession')))
                                            <option value="Process">Process</option>
                                        @endif
                                        @if(getData::checkStepTransaction('Delivered',Session::get('catalogsession')))
                                            <option value="Delivered">Delivered</option>
                                        @endif
                                        <option value="Completed">Completed</option>
                                        <option value="Cancel">Cancel</option>
                                    </select>
                                </div>
                                <div class="col-lg-2 col-md-3 col-sm-6">
                                    <div class="input-group mb-3">
                                        <div class="datepicker date input-group p-0">
                                            <input type="text" id="start" name="start" class="form-control" readonly value="{{ $start }}">
                                            <div class="input-group-append">
                                                <button class="btn btn-outline-primary" type="button"><i class="ion-android-calendar" style="font-size: 1rem"></i></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-2 col-md-3 col-sm-6">
                                    <div class="input-group mb-3">
                                        <div class="datepicker date input-group p-0">
                                            <input type="text" id="end" name="end" class="form-control" readonly value="{{ $end }}">
                                            <div class="input-group-append">
                                                <button class="btn btn-outline-primary" type="button"><i class="ion-android-calendar" style="font-size: 1rem"></i></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-2 col-md-6 col-sm-6">
                                    <button type="button" class="btn btn-info btn-block" style="padding: 9px 10px" onclick="filterData()">Search</button>
                                </div>
                                <div class="col-lg-2 col-md-6 col-sm-6">
                                    <a href="{{ url('/transaction/report/All/'.date('Y-m-d').'/'.date('Y-m-d')) }}" style="padding: 9px 10px" class="btn btn-danger btn-block">Reset</a>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="card-header {{ (Session::get('catalogsession')=='All')?'d-none':'' }}">
                        <ul class="nav nav-justified">
                            <li class="nav-item"><a data-toggle="tab" href="#tab-eg7-0" class="nav-link {{ (Session::get('catalogsession')=='All')?'d-none':'active' }}">General</a></li>
                            <li class="nav-item"><a data-toggle="tab" href="#tab-eg7-1" class="nav-link">Period</a></li>
                        </ul>
                    </div>
                    <div class="card-body">
                        <div class="tab-content">
                            <div class="tab-pane {{ (Session::get('catalogsession')=='All')?'d-none':'active' }}" id="tab-eg7-0" role="tabpanel">
                                {{-- <div class="table-responsive">
                                    <table class="align-middle mb-0 table table-borderless table-striped table-hover">
                                        <thead>
                                            <tr>
                                                <th class="text-center" nowrap>No</th>
                                                @if(Session::get('catalogsession') == 'All')
                                                    <th>Catalog</th>
                                                @endif
                                                <th>Invoice Number</th>
                                                <th class="text-center" nowrap>Transaction Via</th>
                                                <th class="text-center" nowrap>Table / Room</th>
                                                <th class="text-center" nowrap>Payment Method</th>
                                                <th class="text-center" nowrap>Status</th>
                                                <th class="text-right d-none" nowrap>Total</th>
                                                <th class="text-center d-none" nowrap>Tax</th>
                                                <th class="text-right" nowrap>Payment</th>
                                                <th class="text-center" nowrap>Actions</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($getData as $key=>$value)
                                            <tr>
                                                <th class="text-center text-muted" nowrap>{{ $getData->firstItem() + $key }}</th>
                                                @if(Session::get('catalogsession') == 'All')
                                                    <td nowrap>
                                                        <div class="widget-content p-0">
                                                            <div class="widget-content-wrapper">
                                                                <div class="widget-content-left mr-3">
                                                                    <div class="widget-content-left">
                                                                        <img width="40" class="rounded" src="{{ myFunction::getProtocol().$value['catalog_logo'].'?'.time() }}" alt="" />
                                                                    </div>
                                                                </div>
                                                                <div class="widget-content-left flex2">
                                                                    <div class="widget-heading">{{ $value['catalog_title'] }}</div>
                                                                    <div class="widget-subheading opacity-7">
                                                                        <a href="https://{{ $value['catalog_username'].'.'.$value['domain'] }}" target="_blank" style="color: #888; text-decoration: none;">
                                                                            https://{{ $value['catalog_username'].'.'.$value['domain'] }}
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                @endif
                                                <td nowrap>
                                                    <div class="widget-content p-0">
                                                        <div class="widget-content-wrapper">
                                                            <div class="widget-content-left flex2">
                                                                <div class="widget-heading">{{ $value['invoice_number'] }}</div>
                                                                <div class="widget-subheading opacity-7">Created : {{ Date::fullDate($value['created_at']) }}</div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td class="text-center" nowrap>
                                                    @if($value['via'] == 'System')
                                                        Regular
                                                    @else
                                                        {{ $value['via'] }}
                                                    @endif
                                                </td>
                                                <td class="text-center" nowrap>
                                                    {{ $value['position'] }}
                                                </td>
                                                <td class="text-center" nowrap>
                                                    {{ myFunction::payment_type($value['payment_method']) }}
                                                </td>
                                                <td class="text-center" nowrap>
                                                    {{ $value['status'] }}
                                                </td>
                                                <td class="text-right d-none" nowrap>
                                                    {{ number_format(getData::getTotal($value['id'])) }}
                                                </td>
                                                <td class="text-center d-none" nowrap>
                                                    @if($value['tax'] > 0)
                                                        {{ $value['tax'].' %' }}
                                                    @else
                                                        -
                                                    @endif
                                                </td>
                                                <td class="text-right" nowrap>
                                                    @php
                                                        $tax = (getData::getTotal($value['id'])*$value['tax'])/100;
                                                    @endphp
                                                    {{ number_format(getData::getTotal($value['id'])+$tax) }}
                                                </td>
                                                <td class="text-center" nowrap>
                                                    <a href="javascript:void(0)" class=" btn-hover-shine btn btn-primary btn-shadow btn-sm" onclick="loadDetail('{{ $value['invoice_number'] }}')">
                                                        Detail
                                                    </a>
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div> --}}
                                <div class="table-responsive">
                                    <table class="table" style="width: 99%">
                                      <thead>
                                        <tr style="font-size: 12px">
                                            <th scope="col">No</th>
                                            @if(Session::get('catalogsession') == 'All')
                                            <th>Catalog</th>
                                            @endif
                                          <th scope="col" style="text-align: center">Invoice Number</th>
                                          <th scope="col" style="text-align: center">Status</th>
                                          <th scope="col" style="text-align: center">Total</th>
                                          <th scope="col" style="text-align: center">Actions</th>
                                        </tr>
                                      </thead>
                                      <tbody id="accordion" class="accordion-wrapper mb-3">
                                            @foreach ($getData as $key=>$value)  
                                            @if (($getData->firstItem() + $key) % 2 == 1)
                                            <tr data-toggle="collapse" data-target="#collapse{{ $value['id'] }}" aria-expanded="true" aria-controls="collapse{{ $value['id'] }}" class="btn-outline-secondary accordion-toggle collapsed">                     
                                            @else
                                            <tr data-toggle="collapse" data-target="#collapse{{ $value['id'] }}" aria-expanded="true" aria-controls="collapse{{ $value['id'] }}" class="btn-light accordion-toggle collapsed"> 
                                            @endif
                                            <td class="text-center" nowrap>{{ $getData->firstItem() + $key }}</td>
                                            @if(Session::get('catalogsession') == 'All')
                                            <td class="large-screen" nowrap>
                                                <div class="large-screen widget-content p-0">
                                                    <div class="widget-content-wrapper">
                                                        <div class="widget-content-left mr-3">
                                                            <div class="widget-content-left">
                                                                <img width="40" class="rounded" src="{{ myFunction::getProtocol().$value['catalog_logo'].'?'.time() }}" alt="" />
                                                            </div>
                                                        </div>
                                                        <div class="widget-content-left flex2">
                                                            <div class="widget-heading">{{ $value['catalog_title'] }}</div>
                                                            <div class="widget-subheading opacity-7">
                                                                <a href="https://{{ $value['catalog_username'].'.'.$value['domain'] }}" target="_blank" style="color: #888; text-decoration: none;">
                                                                    https://{{ $value['catalog_username'].'.'.$value['domain'] }}
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                            @endif
                                            <td nowrap>
                                                <div class="widget-content p-0">
                                                    <div class="widget-content-wrapper">
                                                        <div class="widget-content-left flex2">
                                                            <div class="small-screen widget-heading" style="font-size: 10px">{{ $value['invoice_number'] }}</div>
                                                            <div class="large-screen widget-heading">{{ $value['invoice_number'] }}</div>
                                                            <div class="small-screen" style="font-size: 8px">{{ Date::fullDate($value['created_at']) }}</div>
                                                            <div class="large-screen widget-subheading opacity-8">Created : {{ Date::fullDate($value['created_at']) }}</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                            <td class="text-center" nowrap>
                                                <div class="large-screen">{{ $value['status'] }}</div>
                                                <div class="small-screen" style="font-size: 10px">{{ $value['status'] }}</div>
                                            </td>                                     
                                            <td class="text-center" nowrap>
                                                @php
                                                    $tax = (getData::getTotal($value['id'])*$value['tax'])/100;
                                                @endphp
                                                <div class="large-screen">{{ number_format(getData::getTotal($value['id'])+$tax) }}</div>
                                                <div class="small-screen" style="font-size: 10px">{{ number_format(getData::getTotal($value['id'])+$tax) }}</div>
                                            </td>
                                            <td class="text-center" nowrap>
                                                <a style="font-size: 12px" href="javascript:void(0)" class=" btn-hover-shine btn btn-primary btn-shadow btn-sm" onclick="loadDetail('{{ $value['invoice_number'] }}')">
                                                    Detail
                                                </a>
                                            </td>             
                                          </tr>
                                
                                          <tr class="hide-table-padding">
                                              <td></td>
                                              <td colspan="3">
                                                <div data-parent="#accordion" id="collapse{{ $value['id'] }}" class="ml-2 collapse in p-3">
                                                    <div class="row">
                                                        <div class="col-6" nowrap>Transaction Via</div>
                                                        <div class="col-4" nowrap>{{ $value['via'] }}</div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-6" nowrap>Table / Room</div>
                                                        <div class="col-4" nowrap>{{ $value['position'] }}</div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-6" nowrap>Payment Method</div>
                                                        <div class="col-4" nowrap>{{ myFunction::payment_type($value['payment_method']) }}</div>
                                                    </div>
                                                        
                                                  
                                                </div></td>
                                          </tr>
                                          @endforeach
                                      </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="tab-pane {{ (Session::get('catalogsession')=='All')?'active':'' }}" id="tab-eg7-1" role="tabpanel">
                                <ul class="nav nav-tabs">
                                    <li class="nav-item"><a data-toggle="tab" href="#tab-day" class="nav-link active">Day</a></li>
                                    <li class="nav-item"><a data-toggle="tab" href="#tab-month" class="nav-link">Month</a></li>
                                </ul>
                                <div class="tab-content">
                                    <div class="tab-pane active" id="tab-day" role="tabpanel">
                                        <div class="table-responsive">
                                            <table class="align-middle mb-0 table table-borderless table-striped table-hover">
                                                <thead>
                                                    <tr>
                                                        <th nowrap>Date</th>
                                                        <th class="text-center" nowrap>Transaction</th>
                                                        <th class="text-right" nowrap>Profit</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($getDay as $getDay)
                                                    <tr>
                                                        <td nowrap>{{ Date::myDate($getDay['created_at']) }}</td>
                                                        <td class="text-center" nowrap>{{ getData::getCountTransactionDay($getDay['created_at']) }}</td>
                                                        <td class="text-right" nowrap>{{ number_format(getData::getTotalTransactionDay($getDay['created_at'])) }}</td>
                                                    </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="tab-month" role="tabpanel">
                                        <div class="table-responsive">
                                            <table class="align-middle mb-0 table table-borderless table-striped table-hover">
                                                <thead>
                                                    <tr>
                                                        <th nowrap>Month</th>
                                                        <th class="text-center" nowrap>Transaction</th>
                                                        <th class="text-right" nowrap>Income</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($getMonth as $getMonth)
                                                    @php
                                                        $date = $getMonth['created_at'];
                                                        $month = explode("-",$date)[1];
                                                        $year = explode("-",$date)[0];
                                                    @endphp
                                                    <tr>
                                                        <td nowrap>{{ Date::month($month).' '.$year }}</td>
                                                        <td class="text-center" nowrap>{{ getData::getCountTransactionMonth($month,$year) }}</td>
                                                        <td class="text-right" nowrap>{{ number_format(getData::getTotalTransactionMonth($month,$year)) }}</td>
                                                    </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="d-block justify-content-center card-footer">
                        <nav class="mt-3">
                            {!! $pagination !!}
                        </nav>
                    </div>
                
            </div>
        </div>
    </div>
</div>
@endsection

@section('modal')
<div class="modal fade" id="modalData" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="titleModal"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div id="modalContent" class="modal-body">
                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" onclick="printInvoice()">Print</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
@endsection

@section('customjs')
    <script type="text/javascript">
        $(document).ready(function() {
            $("#status").val("{{ $status }}");
            $('.datepicker').datepicker({
                clearBtn: true,
                useCurrent:true,
                autoclose:true,
                endDate:'0d',
                format: "yyyy-mm-dd"
            });
        });
        function filterData(){
            $("#myForm").attr('action', "{{ url('/transaction/report') }}"+'/'+$("#status").val()+'/'+$("#start").val()+'/'+$("#end").val());
            $("#myForm").submit();
        }
        function loadDetail(invoice){
            $("#modalData").modal('show');
            $("#titleModal").html("Detail order : "+invoice);
            $.ajax({
                url: "{{ url('/transaction/detailpopup') }}"+'/'+invoice,
                type: 'GET',
            })
            .done(function(data) {
                $("#modalContent").html(data)
            })
            .fail(function() {
                Swal.fire("Ops!", "Load data failed.", "error");
            });
        }
        function printInvoice(){
            inv = $("#invoice_data").val();
            $("#prints").attr("src", "{{ url('/transaction/print') }}"+'/'+inv);
            
            // window.open("{{ url('/transaction/print') }}"+'/'+inv);
        }
    </script>
    
<style>
	.large-screen {}
		@media only screen and (max-width: 770px) {
		.large-screen {display: none;}
		}
		@media only screen and (min-width: 770px) {
		.large-screen {display: block;}
		}
	
	.small-screen {}
		@media only screen and (max-width: 770px) {
		.small-screen {display: block;}
		}
		@media only screen and (min-width: 770px) {
		.small-screen {display: none;}
		}

        tr.hide-table-padding td {
        padding: 0;
    }
</style>
@endsection