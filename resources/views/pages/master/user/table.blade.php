<!--
<div class="table-responsive">
    <table class="align-middle mb-0 table table-borderless table-striped table-hover">
        <thead>
            <tr>
                <th class="pt-4 pb-4 text-center" nowrap>#</th>
                <th class="pt-4 pb-4" nowrap>Name</th>
                <th class="pt-4 pb-4" nowrap>Catalog</th>
                <th class="pt-4 pb-4" nowrap>Email</th>
                <th class="pt-4 pb-4" nowrap>Phone</th>
                <th class="pt-4 pb-4" class="text-center" nowrap>Actions</th>
            </tr>
        </thead>
        <tbody>
            @foreach($getData as $key=>$value)
            <tr>
                <th class="text-center text-muted" nowrap>{{ $getData->firstItem() + $key }}</th>
                <td nowrap>
                    <div class="widget-content p-0">
                        <div class="widget-content-wrapper">
                            <div class="widget-content-left flex2">
                                <div class="widget-heading">{{ $value['name'] }}</div>
                                <div class="widget-subheading opacity-7">Created : {{ Date::fullDate($value['created_at']) }}</div>
                            </div>
                        </div>
                    </div>
                </td>
                <td nowrap>
                    {{ $value['catalog_title'] }}
                </td>
                <td nowrap>
                    {{ $value['email'] }}
                </td>
                <td nowrap>
                    {{ $value['phone'] }}
                </td>
                <td class="text-center" nowrap>
                    <div role="group" class="btn-group-sm btn-group btn-group-toggle">
                        <a href="javascript:void(0)" data-id="{{ $value['id'] }}" class="passlink btn btn-warning">Password</a>
                        <a href="javascript:void(0)" data-id="{{ $value['id'] }}" class="editlink btn btn-info">Edit</a>
                        <a href="javascript:void(0)" data-id="{{ $value['id'] }}" class="deletelink btn btn-danger">Delete</a>
                    </div>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
-->
<div class="table-responsive">
    <table class="table" style="width: 99%">
      <thead>
        <tr>
          <th scope="col">#</th>
          <th scope="col"><div class="large-screen">Name</div><div class="small-screen">Name</div></th>
          <th scope="col" style="text-align: center">Action</th>
        </tr>
      </thead>
      <tbody id="accordion" class="accordion-wrapper mb-3">{{--  Edit accordion --}}
        @foreach ($getData as $key=>$value)  
        @if (($getData->firstItem() + $key) % 2 == 1)
        <tr data-toggle="collapse" data-target="#collapse{{ $value['id'] }}" aria-expanded="false" aria-controls="collapse{{ $value['id'] }}" class="btn-outline-secondary accordion-toggle collapsed"> {{--  Edit collapse --}}                       
        @else
        <tr data-toggle="collapse" data-target="#collapse{{ $value['id'] }}" aria-expanded="false" aria-controls="collapse{{ $value['id'] }}" class="btn-light accordion-toggle collapsed"> {{--  Edit collapse --}}                       
        @endif
        <td class="text-center" nowrap>{{ $getData->firstItem() + $key }}</td>
              <td>{{ $value['name'] }}</td>
              <td class="text-center" nowrap>
                <div role="group" class="btn-group-sm btn-group btn-group-toggle">
                    <a href="javascript:void(0)" data-id="{{ $value['id'] }}" class="passlink btn btn-warning">Password</a>
                    <a href="javascript:void(0)" data-id="{{ $value['id'] }}" class="editlink btn btn-info">Edit</a>
                    <a href="javascript:void(0)" data-id="{{ $value['id'] }}" class="deletelink btn btn-danger">Delete</a>
                </div>
            </td>
		  </tr>

          <tr class="hide-table-padding">
              <td></td>
              <td colspan="3">
                <div data-parent="#accordion" id="collapse{{ $value['id'] }}" class="ml-2 collapse in p-3">{{--  Edit collapse --}}
                    <div class="row">
                      <div class="col-4">Created</div>
                      <div class="col-14">{{ Date::fullDate($value['created_at']) }}</div>
                  </div>
                  <div class="row">
                      <div class="col-4">Catalog</div>
                      <div class="col-14">{{ $value['catalog_title'] }}</div>
                  </div>
                  <div class="row">
                    <div class="col-4">Email</div>
                    <div class="col-14">{{ $value['email'] }}</div>
				  </div>
                  <div class="row">
                    <div class="col-4" nowrap>Phone</div>
                    <div class="col-14">{{ $value['phone'] }}</div>
                  </div>		
              </div></td>
          </tr>
          @endforeach
      </tbody>
    </table>
</div>

<div class="d-block justify-content-center card-footer">
    <nav class="mt-3">
        {!! $pagination !!}
    </nav>
</div>

<style>
	.large-screen {}
		@media only screen and (max-width: 770px) {
		.large-screen {display: none;}
		}
		@media only screen and (min-width: 770px) {
		.large-screen {display: block;}
		}
	
	.small-screen {}
		@media only screen and (max-width: 770px) {
		.small-screen {display: block;}
		}
		@media only screen and (min-width: 770px) {
		.small-screen {display: none;}
		}

        tr.hide-table-padding td {
        padding: 0;
    }
</style>