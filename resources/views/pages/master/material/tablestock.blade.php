<!--
<div class="table-responsive">
	<table class="align-middle mb-0 table table-borderless table-striped table-hover">
	    <thead>
	        <tr>
	            <th class="pt-4 pb-4" nowrap>Last Update</th>
	            <th class="pt-4 pb-4" nowrap>Updated by</th>
	            <th class="pt-4 pb-4 text-right" nowrap>Stock Input</th>
	            <th class="pt-4 pb-4 text-center" nowrap>Unit</th>
	            <th class="pt-4 pb-4" nowrap>Notes</th>
	            <th class="pt-4 pb-4 text-center" nowrap>Actions</th>
	        </tr>
	    </thead>
	    <tbody>
	        @foreach($getData as $key => $value)
	        <tr>
	            <td nowrap>{{ Date::fullDate($value['created_at']) }}</td>
	            <td nowrap>{{ $value['name'] }}</td>
	            <td class="text-right" nowrap>{{ $value['stock'] }}</td>
	            <td class="text-center" nowrap>{{ $item['item_unit'] }}</td>
	            <td nowrap>{{ $value['notes'] }}</td>
	            <td class="text-center" nowrap>
	            	<a href="javascript:void(0)" data-id="{{ $item['id'] }}" data-addon="{{ $value['id'] }}" class="deletelink btn btn-danger">Delete</a>
	            </td>
	        </tr>
	        @endforeach
	    </tbody>
	</table>
</div>
-->
{{-- <div class="table-responsive">
    <table class="table" style="width: 100%">
      <thead>
        <tr>
          <th scope="col">#</th>
          <th scope="col"><div class="large-screen">Last Update</div><div class="small-screen">Last Update</div></th>
          <th scope="col"><div class="large-screen">Stock</div><div class="small-screen">Stock</div></th>
          <th scope="col" style="text-align: center">Action</th>
        </tr>
      </thead>
      <tbody>
          @foreach ($getData as $value)                          
          <tr class="accordion-toggle collapsed" id="accordion{{ $value['id'] }}" data-toggle="collapse" data-parent="#accordion{{ $value['id'] }}" href="#coll{{ $value['id'] }}">
            
              <td class="expand-button"></td>
              <td>{{ Date::fullDate($value['created_at']) }}</td>
			  <td>{{ $value['stock'] }} {{ $item['item_unit'] }}</td>
			  <td class="text-center" nowrap>
				<a href="javascript:void(0)" data-id="{{ $item['id'] }}" data-addon="{{ $value['id'] }}" class="deletelink btn btn-danger">Delete</a>
			  </td>
		  </tr>

          <tr class="hide-table-padding">
              <td></td>
              <td colspan="3">
              <div id="coll{{ $value['id'] }}" class="collapse in p-3">
                  <div class="row">
                      <div class="col-6">Update By</div>
                      <div class="col-14" nowrap>{{ $value['name'] }}</div>
                  </div>
                  <div class="row">
                    <div class="col-6" nowrap>Stock Input</div>
                    <div class="col-14">{{ $value['stock'] }} {{ $item['item_unit'] }}</div>
				  </div>
				  @if ($value['notes'] != "")
                  <div class="row">
                    <div class="col-6" nowrap>Notes</div>
                    <div class="col-14">{{ $value['notes'] }} {{ $item['item_unit'] }}</div>
                  </div>					  
				  @endif
              </div></td>
          </tr>
          @endforeach
      </tbody>
    </table>
</div> --}}

<div class="table-responsive">{{app()->setLocale(Session::get('locale'))}}
    <table class="table" style="width: 99%">
      <thead>
        <tr>
          <th scope="col" class="text-center" nowrap>No</th>
          <th scope="col"><div class="large-screen">{{ __('lang.datetimeaddstock') }}</div><div class="small-screen" style="text-align: center">{{ __('lang.datetimeaddstock') }}</div></th>
          <th scope="col" nowrap>Stock</th>
          <th scope="col" class="text-center" nowrap>Action</th>
        </tr>
      </thead>
      <tbody id="accordion" class="accordion-wrapper mb-3">{{--  Edit accordion --}}

          @foreach ($getData as $key=>$value)      
            @if (($getData->firstItem() + $key) % 2 == 1)
            <tr data-toggle="collapse" data-target="#collapse{{ $value['id'] }}" aria-expanded="true" aria-controls="collapse{{ $value['id'] }}" class="btn-outline-secondary accordion-toggle collapsed"> {{--  Edit collapse --}}                       
            @else
            <tr data-toggle="collapse" data-target="#collapse{{ $value['id'] }}" aria-expanded="true" aria-controls="collapse{{ $value['id'] }}" class="btn-light accordion-toggle collapsed"> {{--  Edit collapse --}}                       
            @endif
              <td class="text-center" nowrap>{{ $getData->firstItem() + $key }}</td>
              <td>{{ Date::fullDate($value['created_at']) }}</td>
			  <td>{{ number_format($value['stock']) }} {{ $item['item_unit'] }}</td>
			  <td class="text-center" nowrap>
				<a href="javascript:void(0)" data-id="{{ $item['id'] }}" data-addon="{{ $value['id'] }}" class="deletelink btn btn-danger">Delete</a>
			  </td>
            </tr>

            <tr class="hide-table-padding">
              <td></td>
              <td colspan="3">
                <div data-parent="#accordion" id="collapse{{ $value['id'] }}" class="ml-2 collapse in p-3">
                  @php
                    $pos = strpos($value['nota'], "TanpaNota");
                    $poss = strpos($value['nota'], "Tanpa-Nota");
                  @endphp
                  @if ($pos === false && $poss === false && $value['nota'] != null)
                    <div class="small-screen image-area"><img href="javascript:void(0)" data-id="{{asset($value['nota'])}}" src="{{asset($value['nota'])}}" alt="" class="imglink btn img-fluid rounded shadow mx-auto mb-2" style="max-height: 150px; text-align: left;"></div>
                  @endif
                  <div class="row">
                    @if ($pos === false && $poss === false && $value['nota'] != null)
                    <div class="large-screen image-area"><img href="javascript:void(0)" data-id="{{asset($value['nota'])}}" src="{{asset($value['nota'])}}" alt="" class="imglink btn img-fluid rounded shadow mx-auto mb-2" style="max-height: 150px; text-align: left;"></div>
                    <div class="col ml-3">
                    @else
                    <div class="col">
                    @endif
                      <h6><strong>Details</strong></h6>
                      <div class="row">
                        <div class="col-6">Update By</div>
                        <div class="col-14" nowrap>{{ $value['name'] }}</div>
                        </div>
                        <div class="row">
                        <div class="col-6" nowrap>Stock Input</div>
                        <div class="col-14">{{ $value['stock'] }} {{ $item['item_unit'] }}</div>
                        </div>
                        @if ($value['notes'] != "")
                        <div class="row">
                        <div class="col-6" nowrap>Notes</div>
                        <div class="col-14">{{ $value['notes'] }}</div>
                        </div>					  
                        @endif
                      </div>
                    </div>
                  </div>            
                </td>
              </tr>            
              @endforeach
              <hr>

      </tbody>
    </table>
</div>

<div class="d-block justify-content-center card-footer">
    <nav class="mt-3">
        {!! $pagination !!}
    </nav>
</div>

<div class="d-block justify-content-center card-footer">
    <nav class="mt-3">
        {!! $pagination !!}
    </nav>
</div>

<style>
	.large-screen {}
		@media only screen and (max-width: 770px) {
		.large-screen {display: none;}
		}
		@media only screen and (min-width: 770px) {
		.large-screen {display: block;}
		}
	
	.small-screen {}
		@media only screen and (max-width: 770px) {
		.small-screen {display: block;}
		}
		@media only screen and (min-width: 770px) {
		.small-screen {display: none;}
		}

        tr.hide-table-padding td {
        padding: 0;
    }
    
    .expand-button {
        position: relative;
    }
    
    .accordion-toggle .expand-button:after {
        position: absolute;
        left:.75rem;
        top: 50%;
        transform: translate(0, -50%);
        content: '-';
    }
    .accordion-toggle.collapsed .expand-button:after {content: '+';}
</style>