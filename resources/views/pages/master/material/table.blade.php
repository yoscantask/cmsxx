<!--
<div class="table-responsive">
    <table class="align-middle mb-0 table table-borderless table-striped table-hover">
        <thead>
            <tr>
                <th class="pt-4 pb-4  text-center" nowrap>#</th>
                <th class="pt-4 pb-4 " nowrap>Material Name</th>
                <th class="pt-4 pb-4  text-right" nowrap>{{ (Session::get('catalogsession')=='All')?'Warehouse Stock':'Total Stock' }}</th>
                @if((Session::get('catalogsession')=='All'))
                    <th class="pt-4 pb-4  text-right" nowrap>Total Stock in Catalog</th>
                @endif
                @if((Session::get('catalogsession')!='All'))
                    <th class="pt-4 pb-4  text-right" nowrap>Stock Used</th>
                @endif
                <th class="pt-4 pb-4  text-right" nowrap>Stock Available</th>
                
                <th class="pt-4 pb-4  text-center" nowrap>Unit</th>
                <th class="pt-4 pb-4  text-center" nowrap>Actions</th>
            </tr>
        </thead>
        <tbody>
            @foreach($getData as $key=>$value)
            <tr>
                <form class="mb-0" id="delete_form_{{ $value['id'] }}" action="{{ route('material.destroy', $value['id']) }}" method="post">
                    @csrf
                    @method('delete')
                    <th class="text-center text-muted" nowrap>{{ $getData->firstItem() + $key }}</th>
                    <td nowrap>
                        <div class="widget-content p-0">
                            <div class="widget-content-wrapper">
                                <div class="widget-content-left flex2">
                                    <div class="widget-heading">{{ $value['items_name'] }}</div>
                                    <div class="widget-subheading opacity-7">Created : {{ Date::fullDate($value['created_at']) }}</div>
                                </div>
                            </div>
                        </div>
                    </td>
                    <td class="text-right" nowrap>
                        {{ myFunction::formatNumber(getData::countStock($value['id'],(Session::get('catalogsession')=='All')?0:Session::get('catalogsession')),1) }}
                    </td>
                    @if((Session::get('catalogsession')=='All'))
                        <td class="text-right" nowrap>
                            {{ myFunction::formatNumber(getData::countStockBranch($value['id']),1) }}
                        </td>
                    @endif
                    @if((Session::get('catalogsession')!='All'))
                        <td class="text-right" nowrap>{{ myFunction::formatNumber(getData::countStockBranchUsed($value['id']),1) }}</td>
                    @endif
                    <td class="text-right" nowrap>
                        @if((Session::get('catalogsession')=='All'))
                            {{ myFunction::formatNumber(getData::countStock($value['id'],(Session::get('catalogsession')=='All')?0:Session::get('catalogsession'))-getData::countStockBranch($value['id']),1) }}
                        @else
                            {{ myFunction::formatNumber(getData::countStockBranch($value['id']) - getData::countStockBranchUsed($value['id']),1) }}
                        @endif
                    </td>
                    <td class="text-center" nowrap>
                        {{ $value['item_unit'] }}
                    </td>
                    <td class="text-center" nowrap>
                        <div role="group" class="btn-group-sm btn-group btn-group-toggle">
                            <a href="{{ url('/material/stock/'.$value['id']) }}" class="btn btn-primary">
                                Manage {{ (Session::get('catalogsession')=='All')?'Warehouse':'' }} Stock
                            </a>
                            @if((Session::get('catalogsession')=='All'))
                            <a href="javascript:void(0)" data-id="{{ $value['id'] }}" class="editlink btn btn-info">Edit</a>
                            <a href="javascript:void(0)" data-id="{{ $value['id'] }}" class="deletelink btn btn-danger">Delete</a>
                            @endif
                        </div>
                    </td>
                </form>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
-->
<div class="table-responsive">{{app()->setLocale(Session::get('locale'))}}
    <table class="table" style="width: 99%">
      <thead>
        <tr>
          <th scope="col">#</th>
          <th scope="col"><div class="large-screen">{{ __('lang.materialname') }}</div>
            <div class="small-screen" style="text-align: center">Material Name</div></th>
          <th scope="col">Stock</th>
          <th scope="col">Action</th>
        </tr>
      </thead>
      <tbody id="accordion" class="accordion-wrapper mb-3">{{--  Edit accordion --}}
          @foreach ($getData as $key=>$value)      
          @if (($getData->firstItem() + $key) % 2 == 1)
          <tr data-toggle="collapse" data-target="#collapse{{ $value['id'] }}" aria-expanded="true" aria-controls="collapse{{ $value['id'] }}" class="btn-outline-secondary accordion-toggle collapsed"> {{--  Edit collapse --}}                       
          @else
          <tr data-toggle="collapse" data-target="#collapse{{ $value['id'] }}" aria-expanded="true" aria-controls="collapse{{ $value['id'] }}" class="btn-light accordion-toggle collapsed"> {{--  Edit collapse --}}                       
          @endif
          <form class="mb-0" id="delete_form_{{ $value['id'] }}" action="{{ route('material.destroy', $value['id']) }}" method="post">
                @csrf
                @method('delete')
                <td class="text-center" nowrap>{{ $getData->firstItem() + $key }}</td>
              <td>
                <div class="widget-content p-0">
                    <div class="widget-content-wrapper">
                        <div class="widget-content-left flex2">
                            <div class="widget-heading">{{ $value['items_name'] }}</div>
                            <div class="large-screen widget-subheading opacity-7">Created : {{ Date::fullDate($value['created_at']) }}</div>
                            <div class="small-screen widget-subheading opacity-9">{{ Date::fullDate($value['created_at']) }}</div>
                        </div>
                    </div>
                </div>
              </td>
              <td>
                @if((Session::get('catalogsession')=='All'))
                {{ myFunction::formatNumber(getData::countStock($value['id'],(Session::get('catalogsession')=='All')?0:Session::get('catalogsession'))-getData::countStockBranch($value['id']),1) }}
                @else
                    {{ myFunction::formatNumber(getData::countStockBranch($value['id']) - getData::countStockBranchUsed($value['id']),1) }}
                @endif
                {{ $value['item_unit'] }}
              </td>
              <td>
                <div role="group" class="btn-group-sm btn-group btn-group-toggle">
                    <a href="{{ url('/material/stock/'.$value['id']) }}" class="btn btn-primary">
                        {{ __('lang.manage') }} {{ (Session::get('catalogsession')=='All')?'Warehouse':'' }} Stock
                    </a>
                    @if((Session::get('catalogsession')=='All'))
                    <a href="javascript:void(0)" data-id="{{ $value['id'] }}" class="editlink btn btn-info">Edit</a>
                    <a href="javascript:void(0)" data-id="{{ $value['id'] }}" class="deletelink btn btn-danger">Delete</a>
                    @endif
                </div>
              </td></form></tr>

          <tr class="hide-table-padding">
              <td></td>
              <td colspan="3">
                <div data-parent="#accordion" id="collapse{{ $value['id'] }}" class="ml-2 collapse in p-3">{{--  Edit collapse --}}
                    <div class="row">
                      <div class="col-6">{{ (Session::get('catalogsession')=='All')?'Warehouse Stock':'Total Stock' }}</div>
                    <div class="col-14" nowrap>
                        {{ myFunction::formatNumber(getData::countStock($value['id'],(Session::get('catalogsession')=='All')?0:Session::get('catalogsession')),1) }}
                    </div>
                  </div>
                  @if((Session::get('catalogsession')=='All'))
                    <div class="row">
                        <div class="col-6" nowrap>Total Stock</div>
                        <div class="col-14">{{ myFunction::formatNumber(getData::countStockBranch($value['id']),1) }}</div>
                    </div>
                  @endif
                  @if((Session::get('catalogsession')!='All'))
                    <div class="row">
                        <div class="col-6" nowrap>Stock {{ __('lang.used') }}</div>
                        <div class="col-14">{{ myFunction::formatNumber(getData::countStockBranchUsed($value['id']),1) }}</div>
                    </div>
                  @endif
                  <div class="row">
                    <div class="col-6" nowrap>Stock {{ __('lang.avaiable') }}</div>
                    <div class="col-14">
                        @if((Session::get('catalogsession')=='All'))
                            {{ myFunction::formatNumber(getData::countStock($value['id'],(Session::get('catalogsession')=='All')?0:Session::get('catalogsession'))-getData::countStockBranch($value['id']),1) }}
                        @else
                            {{ myFunction::formatNumber(getData::countStockBranch($value['id']) - getData::countStockBranchUsed($value['id']),1) }}
                        @endif
                        {{ $value['item_unit'] }}
                    </div>
                  </div>
              </div></td>
          </tr>
          @endforeach
      </tbody>
    </table>
</div>

<div class="d-block justify-content-center card-footer">
    <nav class="mt-3">
        {!! $pagination !!}
    </nav>
</div>

<style>
	.large-screen {}
		@media only screen and (max-width: 770px) {
		.large-screen {display: none;}
		}
		@media only screen and (min-width: 770px) {
		.large-screen {display: block;}
		}
	
	.small-screen {}
		@media only screen and (max-width: 770px) {
		.small-screen {display: block;}
		}
		@media only screen and (min-width: 770px) {
		.small-screen {display: none;}
		}

        tr.hide-table-padding td {
        padding: 0;
    }
    
    .expand-button {
        position: relative;
    }
    
    .accordion-toggle .expand-button:after {
        position: absolute;
        left:.75rem;
        top: 50%;
        transform: translate(0, -50%);
        content: '-';
    }
    .accordion-toggle.collapsed .expand-button:after {content: '+';}
</style>