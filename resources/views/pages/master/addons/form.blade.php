<div id="wrapVue">
    <form id="myForm" @submit.prevent="submitForm" method="post" onsubmit="return false;" enctype="multipart/form-data">
        <input type="hidden" id="id" name="id" class="form-control"/>
        <input type="hidden" id="item_image_one" name="item_image_one" class="form-control"/>
        <input type="hidden" id="item_image_two" name="item_image_two" class="form-control"/>
        <input type="hidden" id="item_image_three" name="item_image_three" class="form-control"/>
        <input type="hidden" id="item_image_four" name="item_image_four" class="form-control"/>
        <input type="hidden" id="item_image_primary" name="item_image_primary" class="form-control"/>
        <div class="modal fade" id="modalForm" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-xl" role="document">
                <div class="modal-content">
                    <!-- Form Loader -->
                    <div class="formLoader">
                        <div class="jumper">
                            <div></div>
                            <div></div>
                            <div></div>
                        </div>
                    </div>
                    <!-- End -->
                    <div class="modal-header">
                        <h5 class="modal-title" id="titleModal"></h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div id="modalContent" class="modal-body">
                        <div class="row">
                            <div class="col-md-5">
                                <div class="position-relative form-group">
                                    <label>Add On Name <sup class="text-danger">* (Required)</sup></label>
                                    <input type="text" id="items_name" name="items_name" class="form-control"/>
                                    <div v-if="formErrors['items_name']" class="errormsg alert alert-danger mt-1">
                                      @{{ formErrors['items_name'][0] }}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="position-relative form-group">
                                    <label>Price <sup class="text-danger">* (Required)</sup></label>
                                    <input type="text" id="items_price" name="items_price" class="form-control"/>
                                    <span class="text-danger">{{ $errors->first('items_price') }}</span>
                                    <div v-if="formErrors['items_price']" class="errormsg alert alert-danger mt-1">
                                      @{{ formErrors['items_price'][0] }}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="position-relative form-group">
                                    <label>Image</label>
                                    <input type="file" id="imagefile_one" name="imagefile_one" class="form-control"/>
                                    <div v-if="formErrors['imagefile_one']" class="errormsg alert alert-danger mt-1">
                                      @{{ formErrors['imagefile_one'][0] }}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 d-none">
                                <div class="position-relative form-group">
                                    <label>Available</label>
                                    <select id="ready_stock" name="ready_stock" class="form-control">
                                      <option value="Y">
                                        Yes
                                      </option>
                                      <option value="N">
                                        No
                                      </option>
                                    </select>
                                    <span v-if="formErrors['ready_stock']" class="errormsg">@{{ formErrors['ready_stock'][0] }}</span>
                                </div>
                            </div>
                        </div>
                        <div class="position-relative form-group">
                            <label for="myLabel" class="">Description</label>
                            <textarea id="items_description" name="items_description" class="form-control" rows="10"></textarea>
                            <div v-if="formErrors['items_description']" class="errormsg alert alert-danger mt-1">
                              @{{ formErrors['items_description'][0] }}
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save Data</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
<script type="text/javascript">
    new Vue({
        el: "#wrapVue",
        data() {
            return {
                csrf: "",
                formErrors: {},
                notif: [],
            };
        },
        mounted: function () {
            this.csrf = "{{ csrf_token() }}";
            let self = this;
        },
        methods: {
            submitForm: function (e) {
                submitForm();
                $('.errormsg').hide();
                var form = e.target || e.srcElement;
                if($("#id").val() > 0){
                  var action = "{{ route('addons.update',0) }}";
                  var put = form.querySelector('input[name="_method"]').value;
                }else{
                  var action = "{{ route('addons.store') }}";
                  var put = '';
                }
                var csrfToken = "{{ csrf_token() }}";

                let datas = new FormData();
                datas.append("id", $("#id").val());
                datas.append("item_image_one", $("#item_image_one").val());
                datas.append("item_image_two", $("#item_image_two").val());
                datas.append("item_image_three", $("#item_image_three").val());
                datas.append("item_image_four", $("#item_image_four").val());
                datas.append("item_image_primary", $("#item_image_primary").val());
                datas.append("items_name", $("#items_name").val());
                datas.append("items_price", $("#items_price").val());
                datas.append("items_discount", 0);
                //datas.append("ready_stock", $("#ready_stock").val());
                datas.append('imagefile_one', document.getElementById('imagefile_one').files[0]);
                datas.append("items_description", $("#items_description").val());
                datas.append("items_youtube", '');
                datas.append("items_color", '');
                datas.append("item_type", 'Add');

                axios.post(action, datas, {
                        headers: {
                            "X-CSRF-TOKEN": csrfToken,
                            "X-HTTP-Method-Override": put,
                            Accept: "application/json",
                        },
                    })
                    .then((response) => {
                        let self = this;
                        var notif = response.data;
                        var getstatus = notif.status;
                        if (getstatus == "success") {
                            $("#modalForm").modal("hide");
                            afterSubmitForm();
                            loadView();
                        }else{
                            afterSubmitForm();
                            toastr.error(notif.message);
                        }
                    })
                    .catch((error) => {
                        afterSubmitForm();
                        $('.errormsg').show();
                        this.formErrors = error.response.data.errors;
                    });
                
            },
        },
    });
</script>