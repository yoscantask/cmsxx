{{-- <div class="table-responsive" style="width: 100%">
    <table class="align-middle mb-0 table table-borderless table-striped table-hover">
        <thead>
            <tr>
                <th class="pt-4 pb-4 text-center" nowrap>#</th>
                <th class="pt-4 pb-4">Sub Category Name</th>
                <th class="pt-4 pb-4 text-center">Font Color</th>
                <th class="pt-4 pb-4 text-center">Actions</th>
            </tr>
        </thead>
        <tbody>
            @foreach($getData as $key=>$value)
            <tr>
                <form class="mb-0" id="delete_form_{{ $value['id'] }}" action="{{ route('subcategory.destroy', $value['id']) }}" method="post">
                    @csrf
                    @method('delete')
                    <th class="text-center text-muted" nowrap>
                        <div class="large-screen">{{ $getData->firstItem() + $key }}</div>
                        <div class="small-screen" style="font-size: 9px">{{ $getData->firstItem() + $key }}</div>
                    </th>
                    <td nowrap>
                        <div class="widget-content p-0">
                            <div class="widget-content-wrapper">
                                <div class="widget-content-left mr-3">
                                    <div class="widget-content-left">
                                        <a href="javascript:void(0)" data-featherlight="{{ myFunction::getProtocol().$value['subcategory_image'].'?'.time() }}">
                                            <img width="40" class="rounded" src="{{ myFunction::getProtocol().$value['subcategory_image'].'?'.time() }}" alt="" />
                                        </a>
                                    </div>
                                </div>
                                <div class="widget-content-left flex2">
                                    <div class="large-screen widget-heading">{{ $value['subcategory_name'] }}</div>
                                    <div class="small-screen" style="font-size: 10px">{{ $value['subcategory_name'] }}</div>
                                    <div class="large-screen widget-subheading opacity-7">Created : {{ Date::fullDate($value['created_at']) }}</div>
                                    <div class="small-screen opacity-8" style="font-size: 8px">{{ Date::fullDate($value['created_at']) }}</div>
                                </div>
                            </div>
                        </div>
                    </td>
                    <td class="text-center" nowrap>
                        <div class="badge badge-warning" style="background: {{ $value['subcategory_color'] }};color:{{ $value['subcategory_color'] }}">.</div><br>{{$value['show_on_catalog']}}
                    </td>
                    <td class="text-center" nowrap>
                        <div role="group" class="large-screen btn-group-sm btn-group btn-group-toggle">
                            <a href="javascript:void(0)" data-id="{{ $value['id'] }}" class="editlink btn btn-info">Edit</a>
                            <a href="javascript:void(0)" data-id="{{ $value['id'] }}" class="deletelink btn btn-danger">Delete</a>
                        </div>
                        <div role="group" class="small-screen btn-group-sm btn-group btn-group-toggle">
                            <a href="javascript:void(0)" data-id="{{ $value['id'] }}" class="editlink btn btn-info" style="font-size: 9px">Edit</a>
                            <a href="javascript:void(0)" data-id="{{ $value['id'] }}" class="deletelink btn btn-danger" style="font-size: 9px">Delete</a>
                        </div>
                    </td>
                </form>
            </tr>
            @endforeach
        </tbody>
    </table>
</div> --}}
{{app()->setLocale(Session::get('locale'))}}
<div class="table-responsive">
    <table class="table" style="width: 99%">
      <thead>
        <tr>
          <th scope="col">#</th>
          <th scope="col">Sub {{ __('lang.catname') }}</th>
          <th scope="col" style="text-align: center">{{ __('lang.fontcolor') }}</th>
          <th scope="col" style="text-align: center">Action</th>
        </tr>
      </thead>
      <tbody id="accordion" class="accordion-wrapper mb-3">{{--  Edit accordion --}}
        @foreach ($getData as $key=>$value)  
        @if (($getData->firstItem() + $key) % 2 == 1)
        <tr data-toggle="collapse" data-target="#collapse{{ $value['id'] }}" aria-expanded="false" aria-controls="collapse{{ $value['id'] }}" class="btn-outline-secondary accordion-toggle collapsed"> {{--  Edit collapse --}}                       
        @else
        <tr data-toggle="collapse" data-target="#collapse{{ $value['id'] }}" aria-expanded="false" aria-controls="collapse{{ $value['id'] }}" class="btn-light accordion-toggle collapsed"> {{--  Edit collapse --}}                       
        @endif
          <td class="text-center" nowrap>{{ $getData->firstItem() + $key }}</td>
          <td>
                <div class="widget-content p-0">
                    <div class="widget-content-wrapper">
                        <div class="widget-content-left flex2">
                            <div class="widget-heading">{{ $value['subcategory_name'] }}</div>
                            <div class="large-screen widget-subheading opacity-7">Created : {{ Date::fullDate($value['created_at']) }}</div>
                            <div class="small-screen widget-subheading opacity-7">{{ Date::fullDate($value['created_at']) }}</div>
                        </div>
                    </div>
                </div>
            </td>
            <td class="text-center" nowrap>
                <div class="badge badge-warning" style="background: {{ $value['subcategory_color'] }};color:{{ $value['subcategory_color'] }}">.</div>
            </td>
            <td style="text-align: center">
                <div role="group" class="large-screen btn-group-sm btn-group btn-group-toggle">
                    <a href="javascript:void(0)" data-id="{{ $value['id'] }}" class="editlink btn btn-info">Edit</a>
                    <a href="javascript:void(0)" data-id="{{ $value['id'] }}" class="deletelink btn btn-danger">Delete</a>
                </div>
                <div role="group" class="small-screen btn-group-sm btn-group btn-group-toggle">
                    <a href="javascript:void(0)" data-id="{{ $value['id'] }}" class="editlink btn btn-info" style="font-size: 9px; width: 50px">Edit</a>
                    <a href="javascript:void(0)" data-id="{{ $value['id'] }}" class="deletelink btn btn-danger" style="font-size: 9px; width: 50px">Delete</a>
                </div>
            </td>              
          </tr>

          <tr class="hide-table-padding">
              <td></td>
              <td colspan="3">
                <div data-parent="#accordion" id="collapse{{ $value['id'] }}" class="ml-2 collapse in p-3">{{--  Edit collapse --}}
                    @if ($value['show_on_catalog'])
                <strong>{{ __('lang.subcatdesk') }}</strong>
                <div>
                @foreach (explode('|', $value['show_on_catalog']) as $item)
                @foreach (getData::selectCatalog($item) as $items)
                * {{ $items['catalog_title'] }}.<br>
                @endforeach
                @endforeach 
                </div>               
                @else
                <strong class="text-danger">{{ __('lang.subcatdesknot') }}</strong>    
                @endif                 
                
              </div></td>
          </tr>
          @endforeach
      </tbody>
    </table>
</div>

<div class="d-block justify-content-center card-footer">
    <nav class="mt-3">
        {!! $pagination !!}
    </nav>
</div>


<style>
	.large-screen {}
		@media only screen and (max-width: 600px) {
		.large-screen {display: none;}
		}
		@media only screen and (min-width: 600px) {
		.large-screen {display: block;}
	}

	.small-screen {}
		@media only screen and (max-width: 600px) {
		.small-screen {display: block;}
		}
		@media only screen and (min-width: 600px) {
		.small-screen {display: none;}
		}
	}
</style>