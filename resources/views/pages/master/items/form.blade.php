<div id="wrapVue">{{app()->setLocale(Session::get('locale'))}}
    <form id="myForm" @submit.prevent="submitForm" method="post" onsubmit="return false;" enctype="multipart/form-data">
        <input type="hidden" id="id" name="id" class="form-control"/>
        <input type="hidden" id="item_image_one" name="item_image_one" class="form-control"/>
        <input type="hidden" id="item_image_two" name="item_image_two" class="form-control"/>
        <input type="hidden" id="item_image_three" name="item_image_three" class="form-control"/>
        <input type="hidden" id="item_image_four" name="item_image_four" class="form-control"/>
        <input type="hidden" id="item_image_primary" name="item_image_primary" class="form-control"/>
        <div class="modal fade" id="modalForm" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-xl" role="document">
                <div class="modal-content">
                    <!-- Form Loader -->
                    <div class="formLoader">
                        <div class="jumper">
                            <div></div>
                            <div></div>
                            <div></div>
                        </div>
                    </div>
                    <!-- End -->
                    <div class="modal-header">
                        <h5 class="modal-title" id="titleModal"></h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div id="modalContent" class="modal-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="position-relative form-group">
                                    <label>{{ __('lang.itemname') }} <sup class="text-danger">* (Required)</sup></label>
                                    <input type="text" id="items_name" name="items_name" class="form-control"/>
                                    <div v-if="formErrors['items_name']" class="errormsg alert alert-danger mt-1">
                                      @{{ formErrors['items_name'][0] }}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3" id="priceset" name="priceset">
                                <div class="position-relative form-group">
                                    <label>{{ __('lang.price') }} <sup class="text-danger">* (Required)</sup></label>
                                    <input type="text" id="items_price" name="items_price" class="form-control"/>
                                    <div v-if="formErrors['items_price']" class="errormsg alert alert-danger mt-1">
                                        @{{ formErrors['items_price'][0] }}
                                      </div>
                                </div>
                            </div>
                            <div class="col-md-3" id="pricesetdis" name="pricesetdis">
                                <div class="position-relative form-group">
                                    <label>{{ __('lang.discount') }} <sup class="text-danger">* (Optional)</sup></label>
                                    <input type="text" id="items_discount" name="items_discount" class="form-control" value="0" />
                                    <div v-if="formErrors['items_discount']" class="errormsg alert alert-danger mt-1">
                                        @{{ formErrors['items_discount'][0] }}
                                      </div>
                                </div>
                            </div>
                            <div class="col-md-2 d-none">
                                <div class="position-relative form-group">
                                    <label>Available Item</label>
                                    <select id="ready_stock" name="ready_stock" class="form-control">
                                      <option value="Y">
                                        Yes
                                      </option>
                                      <option value="N">
                                        No
                                      </option>
                                    </select>
                                    <span v-if="formErrors['ready_stock']" class="errormsg">@{{ formErrors['ready_stock'][0] }}</span>
                                </div>
                            </div>
                        </div>
                        
                        <div class="row mb-3">
                            <div class="col-md-2">
                                <div class="position-relative form-group">
                                    <label>Show on Catalog <sup class="text-danger">* (Required)</sup></label>
                                    @foreach (getData::cekCatalog(Auth::user()->id) as $item)
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="form-control custom-control-input" name="checkCatalog[]"  id="checkCatalog{{ $item['id'] }}" value="{{ $item['id'] }}">
                                        <label class="custom-control-label" for="checkCatalog{{ $item['id'] }}">{{ $item['catalog_title'] }}</label>
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                            <div class="col-md-10">
                                <label for="myLabel" class="">Description</label>
                                <textarea id="items_description" name="items_description" class="form-control" style="width: 100%; min-height: 100px"></textarea>
                                <div v-if="formErrors['items_description']" class="errormsg alert alert-danger mt-1">
                                  @{{ formErrors['items_description'][0] }}
                                </div>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-md-6">
                                <div class="position-relative form-group">
                                    <label>Image 1 <sup class="text-danger">* (Required)</sup></label>
                                    <input type="file" id="imagefile_one" name="imagefile_one" class="form-control"/>
                                    <div v-if="formErrors['imagefile_one']" class="errormsg alert alert-danger mt-1">
                                      @{{ formErrors['imagefile_one'][0] }}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="position-relative form-group">
                                    <label>Image 2</label>
                                    <input type="file" id="imagefile_two" name="imagefile_two" class="form-control"/>
                                    <div v-if="formErrors['imagefile_two']" class="errormsg alert alert-danger mt-1">
                                      @{{ formErrors['imagefile_two'][0] }}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="position-relative form-group">
                                    <label>Image 3</label>
                                    <input type="file" id="imagefile_three" name="imagefile_three" class="form-control"/>
                                    <div v-if="formErrors['imagefile_three']" class="errormsg alert alert-danger mt-1">
                                      @{{ formErrors['imagefile_three'][0] }}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="position-relative form-group">
                                    <label>Image 4</label>
                                    <input type="file" id="imagefile_four" name="imagefile_four" class="form-control"/>
                                    <div v-if="formErrors['imagefile_four']" class="errormsg alert alert-danger mt-1">
                                      @{{ formErrors['imagefile_four'][0] }}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="position-relative form-group">
                                    <label>Youtube URL</label>
                                    <input type="text" id="items_youtube" name="items_youtube" class="form-control"/>
                                    <div v-if="formErrors['items_youtube']" class="errormsg alert alert-danger mt-1">
                                      @{{ formErrors['items_youtube'][0] }}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="position-relative form-group">
                                    <label>Font Color</label>
                                    <input id="items_color" name="items_color" type="text" class="colorpicker form-control" value='#000' readonly />
                                    <div v-if="formErrors['items_color']" class="errormsg alert alert-danger mt-1">
                                      @{{ formErrors['items_color'][0] }}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <a href="" id="linkeditharga" type="button" class="btn btn-success">Edit Harga</a>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save Data</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
<script type="text/javascript">    
    new Vue({
        el: "#wrapVue",
        data() {
            return {
                csrf: "",
                formErrors: {},
                notif: [],
            };
        },
        mounted: function () {
            this.csrf = "{{ csrf_token() }}";
            let self = this;
        },
        methods: {
            submitForm: function (e) {
                submitForm();
                $('.errormsg').hide();
                var form = e.target || e.srcElement;
                if($("#id").val() > 0){
                  var action = "{{ route('items.update',0) }}";
                  var put = form.querySelector('input[name="_method"]').value;
                }else{
                  var action = "{{ route('items.store') }}";
                  var put = '';
                }
                var csrfToken = "{{ csrf_token() }}";

                var arraddons=[];
                $("input:checkbox[name*=items]:checked").each(function(){
                    arraddons.push($(this).val());
                });

                let datas = new FormData();
                datas.append("id", $("#id").val());
                datas.append("item_image_one", $("#item_image_one").val());
                datas.append("item_image_two", $("#item_image_two").val());
                datas.append("item_image_three", $("#item_image_three").val());
                datas.append("item_image_four", $("#item_image_four").val());
                datas.append("item_image_primary", $("#item_image_primary").val());
                datas.append("items_name", $("#items_name").val());
                datas.append("items_price", $("#items_price").val());
                datas.append("items_discount", $("#items_discount").val());
                //datas.append("ready_stock", $("#ready_stock").val());
                datas.append('imagefile_one', document.getElementById('imagefile_one').files[0]);
                datas.append('imagefile_two', document.getElementById('imagefile_two').files[0]);
                datas.append('imagefile_three', document.getElementById('imagefile_three').files[0]);
                datas.append('imagefile_four', document.getElementById('imagefile_four').files[0]);
                datas.append("items_description", $("#items_description").val());
                datas.append("items_youtube", $("#items_youtube").val());
                datas.append("items_color", $("#items_color").val());
                datas.append("item_type", 'Main');

                //Nyatuin data checkbox show on catalog
                var showoncatalog = "";              
                var arr = $('input[name="checkCatalog[]"]:checked').map(function(){
                    return $(this).val();
                }).get();

                arr.forEach(element => {
                    showoncatalog = showoncatalog + element + "|";
                });
                
                datas.append("show_on_catalog", showoncatalog);
                //End Nyatuin

                axios.post(action, datas, {
                        headers: {
                            "X-CSRF-TOKEN": csrfToken,
                            "X-HTTP-Method-Override": put,
                            Accept: "application/json",
                        },
                    })
                    .then((response) => {
                        let self = this;
                        var notif = response.data;
                        var getstatus = notif.status;
                        if (getstatus == "success") {
                            $("#modalForm").modal("hide");
                            afterSubmitForm();
                            loadView();
                            toastr.success(notif.message);
                        }else{
                            afterSubmitForm();
                            toastr.error(notif.message);
                        }
                    })
                    .catch((error) => {
                        afterSubmitForm();
                        $('.errormsg').show();
                        this.formErrors = error.response.data.errors;
                    });
                
            },
            isChecked:function(addons) {
                $(document).on('show.bs.modal', '#modalForm', function () {
                    setTimeout(function(){ 
                        $.ajax({
                            url: "{{ url('/items/checkaddons') }}"+'/'+$("#id").val()+'/'+addons,
                            type: 'GET',
                        })
                        .done(function(data) {
                            if(data == 1){
                                $("#item"+addons).prop('checked', true);
                            }else{
                                $("#item"+addons).prop('checked', false);
                            }
                        })
                        .fail(function() {
                            console.log("error");
                        });
                    }, 1000);
                });
            },
        },
    });
</script>