<!--
<div class="table-responsive">
    <table class="align-middle mb-0 table table-borderless table-striped table-hover">
        <thead>
            <tr>
                <th class="pt-4 pb-4 text-center" nowrap>#</th>
                <th class="pt-4 pb-4" nowrap>Item Name</th>
                <th class="pt-4 pb-4 text-right" nowrap>Price</th>
                <th class="pt-4 pb-4 text-center" nowrap>Font Color</th>
                <th class="pt-4 pb-4 text-center" nowrap>Available</th>
                <th class="pt-4 pb-4 text-center" nowrap>Actions</th> 
            </tr>
        </thead>
        <tbody>
            @foreach($getData as $key=>$value)
            <tr>
                <form class="mb-0" id="delete_form_{{ $value['id'] }}" action="{{ route('items.destroy', $value['id']) }}" method="post">
                    @csrf
                    @method('delete')
                    <th class="text-center text-muted" nowrap>{{ $getData->firstItem() + $key }}</th>
                    <td nowrap>
                        <div class="widget-content p-0">
                            <div class="widget-content-wrapper">
                                <div class="widget-content-left mr-3">
                                    <div class="widget-content-left">
                                        <a href="javascript:void(0)" onclick="loadGallery('{{ $value['id'] }}')">
                                            <img width="40" class="rounded" src="{{ myFunction::getProtocol().$value['item_image_primary'].'?'.time() }}" alt="" />
                                        </a>
                                    </div>
                                </div>
                                <div class="widget-content-left flex2">
                                    <div class="widget-heading">{{ $value['items_name'] }}</div>
                                    <div class="widget-subheading opacity-7">Created : {{ Date::fullDate($value['created_at']) }}</div>
                                </div>
                            </div>
                        </div>
                    </td>
                    <td class="text-right" nowrap>
                        @if($value['items_discount'] > 0)
                            <span class="text-danger" style="text-decoration: line-through;">{{ number_format($value['items_price']) }}</span>
                            {{ number_format($value['items_price']-$value['items_discount']) }}
                        @else
                            {{ number_format($value['items_price']) }}
                        @endif
                    </td>
                    <td class="text-center" nowrap>
                        <div class="badge badge-warning" style="background: {{ $value['items_color'] }};color:{{ $value['items_color'] }}">.</div>
                    </td>
                    <td class="text-center" nowrap>
                        {{ $value['ready_stock'] }}
                    </td>
                    <td class="text-center" nowrap>
                        <div role="group" class="btn-group-sm btn-group btn-group-toggle">
                            <a href="{{ url('/items/ingredient/'.$value['id']) }}" class="btn btn-shadow btn-primary">Ingredient</a>
                            <a href="{{ url('/items/addons/'.$value['id']) }}" class="btn btn-shadow btn-primary">Add Ons</a>
                            <a href="javascript:void(0)" data-id="{{ $value['id'] }}" class="editlink btn btn-shadow btn-info">Edit</a>
                            <a href="javascript:void(0)" data-id="{{ $value['id'] }}" class="deletelink btn btn-shadow btn-danger">Delete</a>
                        </div>
                    </td>
                </form>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>-->
{{app()->setLocale(Session::get('locale'))}}
<div class="table-responsive">
    <table class="table" style="width: 99%">
      <thead>
        <tr>
          <th scope="col" style="text-align: center">#</th>
          <th scope="col"><div class="small-screen" style="text-align: center">{{ __('lang.itemname') }}</div><div class="large-screen">{{ __('lang.itemname') }}</div></th>
          <th scope="col" style="text-align: center">{{ __('lang.price') }}</th>
          <th scope="col" style="text-align: center">{{ __('lang.avaiable') }}</th>
        </tr>
      </thead>
      <tbody id="accordion" class="accordion-wrapper mb-3">{{--  Edit accordion --}}
          @foreach ($getData as $key=>$value)  
          @if (($getData->firstItem() + $key) % 2 == 1)
          <tr data-toggle="collapse" data-target="#collapse{{ $value['id'] }}" aria-expanded="true" aria-controls="collapse{{ $value['id'] }}" class="btn-outline-secondary accordion-toggle collapsed"> {{--  Edit collapse --}}                       
          @else
          <tr data-toggle="collapse" data-target="#collapse{{ $value['id'] }}" aria-expanded="true" aria-controls="collapse{{ $value['id'] }}" class="btn-light accordion-toggle collapsed"> {{--  Edit collapse --}}                       
          @endif
            <td class="text-center" nowrap>{{ $getData->firstItem() + $key }}</td>
            <td>
                <div class="widget-content p-0">
                    <div class="widget-content-wrapper">
                        <div class="widget-content-left mr-3">
                            <div class="widget-content-left">
                                <a href="javascript:void(0)" onclick="loadGallery('{{ $value['id'] }}')">
                                    <img width="40" class="rounded" src="{{ myFunction::getProtocol().$value['item_image_primary'].'?'.time() }}" alt="" />
                                </a>
                            </div>
                        </div>
                        <div class="widget-content-left flex2">
                            <div class="widget-heading">{{ $value['items_name'] }}</div>
                            <div class="large-screen widget-subheading opacity-7">Created : {{ Date::fullDate($value['created_at']) }}</div>
                            <div class="small-screen widget-subheading opacity-7">{{ Date::fullDate($value['created_at']) }}</div>
                        </div>
                    </div>
                </div>
            </td>
            <td style="text-align: center">
                @foreach (getData::checkHargaCash($value['id']) as $check)
                @if($check['diskon'] > 0)
                    Rp <span class="text-danger" style="text-decoration: line-through;">{{ number_format($check['price']) }}</span>
                    {{ number_format($check['price']-$check['diskon']) }}
                @else
                    Rp {{ number_format($check['price']) }}
                @endif
                @endforeach
                {{-- @if($value['items_discount'] > 0)
                    <span class="text-danger" style="text-decoration: line-through;">{{ number_format($value['items_price']) }}</span>
                    {{ number_format($value['items_price']-$value['items_discount']) }}
                @else
                    {{ number_format($value['items_price']) }}
                @endif --}}
            </td>
            <td style="text-align: center">
                @if ($value['ready_stock'] == 'Y')
                    Yes
                @else
                    No
                @endif
            </td>              
          </tr>

          <tr class="hide-table-padding">
              <td></td>
              <td colspan="3">
              <div data-parent="#accordion" id="collapse{{ $value['id'] }}" class="ml-2 collapse in p-3">{{--  Edit collapse --}}
                @if ($value['show_on_catalog'])
                <strong>{{ __('lang.showon') }} Catalog</strong>
                <div>
                @foreach (explode('|', $value['show_on_catalog']) as $item)
                @foreach (getData::selectCatalog($item) as $items)
                * {{ $items['catalog_title'] }}.<br>
                @endforeach
                @endforeach 
                </div>               
                @else
                <strong class="text-danger">{{ __('lang.itemdesknot') }}</strong>    
                @endif
                <hr>                    
                @if (count(getData::checkPriceList($value['id'])) == 0)
                <strong>Silahkan tekan tombol Edit Harga untuk menentukan harga item ini</strong>
                <hr>
                @endif
                @foreach (getData::checkPriceList($value['id']) as $item) 
                    @if ($item['price'] != 0)
                        <strong>{{ __('lang.payment') }} {{ $item['jenis'] }}</strong>
                        <br><sup class="text-danger">{{ $item['ket'] }}</sup>
                        <div class="row">
                            <div class="col-6" nowrap>{{ __('lang.price') }}</div>
                            <div class="col-4" nowrap>Rp {{ number_format($item['price']) }}</div>
                        </div>
                        @if ($item['diskon'] != 0)
                            <div class="row">
                                <div class="col-6" nowrap>{{ __('lang.discount') }}</div>
                                <div class="col-4" nowrap>Rp {{ number_format($item['diskon']) }}</div>
                            </div>     
                        @endif
                        <div class="row">
                            <div class="col-6" nowrap>Status</div>
                            <div class="col-4" nowrap>{{ $item['status'] }}</div>
                        </div> 
                        <hr>
                    @endif               
                @endforeach
                  <strong>Font Color</strong>
                  <div class="row">
                    <div class="mt-1 ml-3 badge badge-warning" style="background: {{ $value['items_color'] }};color:{{ $value['items_color'] }}">.</div>
                  </div>
                  <div class="mt-3 row">
                    <div role="group" class="ml-3 btn-group-sm btn-group btn-group-toggle" nowarp>
                            <a style="font-size: 11px" href="{{ url('/items/ingredient/'.$value['id']) }}" class="btn btn-shadow btn-primary">Ingredient</a>
                            <a style="font-size: 11px" href="{{ url('/items/addons/'.$value['id']) }}" class="btn btn-shadow btn-primary">Add Ons</a>
                            <a style="font-size: 11px" href="{{ url('/pricelist/details/'.$value['id']) }}" class="btn btn-shadow btn-success">Edit Harga</a>
                            <a style="font-size: 11px" href="javascript:void(0)" data-id="{{ $value['id'] }}" class="editlink btn btn-shadow btn-info">Edit</a>
                            <a style="font-size: 11px" href="javascript:void(0)" data-id="{{ $value['id'] }}" class="deletelink btn btn-shadow btn-danger">Delete</a>
                    </div>
                  </div>
              </div></td>
          </tr>
          @endforeach
      </tbody>
    </table>
</div>

<div class="d-block justify-content-center card-footer">
    <nav class="mt-3">
        {!! $pagination !!}
    </nav>
</div>

<style>
	.large-screen {}
		@media only screen and (max-width: 770px) {
		.large-screen {display: none;}
		}
		@media only screen and (min-width: 770px) {
		.large-screen {display: block;}
		}
	
	.small-screen {}
		@media only screen and (max-width: 770px) {
		.small-screen {display: block;}
		}
		@media only screen and (min-width: 770px) {
		.small-screen {display: none;}
		}

        tr.hide-table-padding td {
        padding: 0;
    }
</style>