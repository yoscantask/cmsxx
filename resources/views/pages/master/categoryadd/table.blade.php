<div class="table-responsive">
    <table class="align-middle mb-0 table table-borderless table-striped table-hover">
        <thead>
            <tr>
                <th class="pt-4 pb-4 text-center" nowrap>#</th>
                <th class="pt-4 pb-4" nowrap><div class="large-screen">Category Name</div><div class="small-screen" style="text-align: center">Category Name</div></th>
                <th class="pt-4 pb-4 text-center">Font Color</th>
                <th class="pt-4 pb-4 text-center" nowrap>Actions</th>
            </tr>
        </thead>
        <tbody>
          @foreach ($getData as $key=>$value)  
          @if (($getData->firstItem() + $key) % 2 == 1)
          <tr class="bg-outline-secondary">           
          @else
          <tr class="bg-light">           
          @endif
                <form class="mb-0" id="delete_form_{{ $value['id'] }}" action="{{ route('categoryadd.destroy', $value['id']) }}" method="post">
                    @csrf
                    @method('delete')
                    <th class="text-center text-muted" nowrap>{{ $getData->firstItem() + $key }}</th>
                    <td nowrap>
                        <div class="widget-content p-0">
                            <div class="widget-content-wrapper">
                                <div class="widget-content-left flex2">
                                    <div class="widget-heading">{{ $value['category_name'] }}</div>
                                    <div class="large-screen widget-subheading opacity-7">Created : {{ Date::fullDate($value['created_at']) }}</div>
                                    <div class="small-screen widget-subheading opacity-8" style="font-size: 9px">{{ Date::fullDate($value['created_at']) }}</div>
                                </div>
                            </div>
                        </div>
                    </td>
                    <td class="text-center" nowrap>
                        <div class="badge badge-warning" style="background: {{ $value['category_color'] }};color:{{ $value['category_color'] }}">.</div>
                    </td>
                    <td class="text-center" nowrap>
                        <div role="group" class="btn-group-sm btn-group btn-group-toggle">
                            <a href="javascript:void(0)" data-id="{{ $value['id'] }}" class="editlink btn btn-shadow btn-info">Edit</a>
                            <a href="javascript:void(0)" data-id="{{ $value['id'] }}" class="deletelink btn btn-shadow btn-danger">Delete</a>
                        </div>
                    </td>
                </form>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
<div class="d-block justify-content-center card-footer">
    <nav class="mt-3">
        {!! $pagination !!}
    </nav>
</div>


<style>
	.large-screen {}
		@media only screen and (max-width: 600px) {
		.large-screen {display: none;}
		}
		@media only screen and (min-width: 600px) {
		.large-screen {display: block;}
	}

	.small-screen {}
		@media only screen and (max-width: 600px) {
		.small-screen {display: block;}
		}
		@media only screen and (min-width: 600px) {
		.small-screen {display: none;}
		}
	}
</style>