<div class="row">
    {{app()->setLocale(Session::get('locale'))}}
    <div class="col-lg-12 col-xl-12">
        <div class="main-card mb-3 card">
            <div class="grid-menu grid-menu-3col">
                <div class="large-screen"><div class="no-gutters row">
                    <div class="col-md-4">
                        <a href="{{ url('/transaction/checkout') }}" style="color: #495057; text-decoration: none;">
                            <div class="widget-chart widget-chart-hover">
                                <div class="icon-wrapper rounded-circle">
                                    <div class="icon-wrapper-bg bg-secondary"></div>
                                    <i class="pe-7s-next-2"></i>
                                </div>
                                <div class="widget-numbers">{{ count($checkout) }}</div>
                                <div class="widget-subheading">Checkout</div>
                            </div>
                        </a>
                    </div>
                    <div class="col-md-4">
                        <a href="{{ url('/transaction/approve') }}" style="color: #495057; text-decoration: none;">
                            <div class="widget-chart widget-chart-hover">
                                <div class="icon-wrapper rounded-circle">
                                    <div class="icon-wrapper-bg bg-primary"></div>
                                    <i class="lnr-select"></i>
                                </div>
                                <div class="widget-numbers">{{ count($approve) }}</div>
                                <div class="widget-subheading">Approve</div>
                            </div>
                        </a>
                    </div>
                    <div class="col-md-4">
                        <a href="{{ url('/transaction/process') }}" style="color: #495057; text-decoration: none;">
                            <div class="widget-chart widget-chart-hover">
                                <div class="icon-wrapper rounded-circle">
                                    <div class="icon-wrapper-bg bg-info"></div>
                                    <i class="lnr-hourglass"></i>
                                </div>
                                <div class="widget-numbers">{{ count($process) }}</div>
                                <div class="widget-subheading">Process</div>
                            </div>
                        </a>
                    </div>
                    <div class="col-md-4">
                        <a href="{{ url('/transaction/delivered') }}" style="color: #495057; text-decoration: none;">
                            <div class="widget-chart widget-chart-hover br-br">
                                <div class="icon-wrapper rounded-circle">
                                    <div class="icon-wrapper-bg bg-warning"></div>
                                    <i class="lnr-location"></i>
                                </div>
                                <div class="widget-numbers">{{ count($delivered) }}</div>
                                <div class="widget-subheading">Delivered</div>
                            </div>
                        </a>
                    </div>
                    <div class="col-md-4">
                        <a href="{{ url('/transaction/completed') }}" style="color: #495057; text-decoration: none;">
                            <div class="widget-chart widget-chart-hover br-br">
                                <div class="icon-wrapper rounded-circle">
                                    <div class="icon-wrapper-bg bg-success"></div>
                                    <i class="lnr-checkmark-circle"></i>
                                </div>
                                <div class="widget-numbers">{{ count($completed) }}</div>
                                <div class="widget-subheading">Completed</div>
                            </div>
                        </a>
                    </div>
                    <div class="col-md-4">
                        <a href="{{ url('/transaction/cancel') }}" style="color: #495057; text-decoration: none;">
                            <div class="widget-chart widget-chart-hover br-br">
                                <div class="icon-wrapper rounded-circle">
                                    <div class="icon-wrapper-bg bg-danger"></div>
                                    <i class="lnr-cross-circle"></i>
                                </div>
                                <div class="widget-numbers">{{ count($cancel) }}</div>
                                <div class="widget-subheading">Cancel</div>
                            </div>
                        </a>
                    </div>
                </div></div>
                <div class="small-screen"><div class="m-1 no-gutters row">
                    <div class="col">
                        <a href="{{ url('/transaction/checkout') }}" style="color: #495057; text-decoration: none;">
                            <div class="widget-chart widget-chart-hover">
                                <div class="icon-wrapper rounded-circle">
                                    <div class="icon-wrapper-bg bg-secondary"></div>
                                    <i class="pe-7s-next-2"></i>
                                </div>
                                <div class="widget-numbers">{{ count($checkout) }}</div>
                                <div class="widget-subheading">Checkout</div>
                            </div>
                        </a>
                    </div>
                    <div class="col">
                        <a href="{{ url('/transaction/approve') }}" style="color: #495057; text-decoration: none;">
                            <div class="widget-chart widget-chart-hover">
                                <div class="icon-wrapper rounded-circle">
                                    <div class="icon-wrapper-bg bg-primary"></div>
                                    <i class="lnr-select"></i>
                                </div>
                                <div class="widget-numbers">{{ count($approve) }}</div>
                                <div class="widget-subheading">Approve</div>
                            </div>
                        </a>
                    </div>
                    <div class="col">
                        <a href="{{ url('/transaction/process') }}" style="color: #495057; text-decoration: none;">
                            <div class="widget-chart widget-chart-hover">
                                <div class="icon-wrapper rounded-circle">
                                    <div class="icon-wrapper-bg bg-info"></div>
                                    <i class="lnr-hourglass"></i>
                                </div>
                                <div class="widget-numbers">{{ count($process) }}</div>
                                <div class="widget-subheading">Process</div>
                            </div>
                        </a>
                    </div>
                    <div class="col">
                        <a href="{{ url('/transaction/delivered') }}" style="color: #495057; text-decoration: none;">
                            <div class="widget-chart widget-chart-hover br-br">
                                <div class="icon-wrapper rounded-circle">
                                    <div class="icon-wrapper-bg bg-warning"></div>
                                    <i class="lnr-location"></i>
                                </div>
                                <div class="widget-numbers">{{ count($delivered) }}</div>
                                <div class="widget-subheading">Delivered</div>
                            </div>
                        </a>
                    </div>
                    <div class="col">
                        <a href="{{ url('/transaction/completed') }}" style="color: #495057; text-decoration: none;">
                            <div class="widget-chart widget-chart-hover br-br">
                                <div class="icon-wrapper rounded-circle">
                                    <div class="icon-wrapper-bg bg-success"></div>
                                    <i class="lnr-checkmark-circle"></i>
                                </div>
                                <div class="widget-numbers">{{ count($completed) }}</div>
                                <div class="widget-subheading">Completed</div>
                            </div>
                        </a>
                    </div>
                    <div class="col">
                        <a href="{{ url('/transaction/cancel') }}" style="color: #495057; text-decoration: none;">
                            <div class="widget-chart widget-chart-hover br-br">
                                <div class="icon-wrapper rounded-circle">
                                    <div class="icon-wrapper-bg bg-danger"></div>
                                    <i class="lnr-cross-circle"></i>
                                </div>
                                <div class="widget-numbers">{{ count($cancel) }}</div>
                                <div class="widget-subheading">Cancel</div>
                            </div>
                        </a>
                    </div>
                </div></div>
            </div>
        </div>
    </div>
</div>

<div class="main-card mb-3 card" style="width: 100%">
    <div class="card-header">
        <div class="large-screen card-header-title font-size-lg text-capitalize font-weight-normal">{{ __('lang.graphyear') }}</div>
        <div class="small-screen card-header-title font-size-sm text-capitalize font-weight-normal">{{ __('lang.graphyear') }}</div>
    </div>
    <canvas class="p-3" id="lineChartYears"></canvas>
</div>

<div class="main-card mb-3 card" style="width: 100%">
    <div class="card-header">
        <div class="large-screen card-header-title font-size-lg text-capitalize font-weight-normal">{{ __('lang.graphmont') }}</div>
        <div class="small-screen card-header-title font-size-sm text-capitalize font-weight-normal">{{ __('lang.graphmont') }}</div>
    </div>
    <canvas class="p-3" id="lineChartDays"></canvas>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="main-card mb-3 card">
            <div class="card-header">
                {{app()->setLocale(Session::get('locale'))}}
                <div class="card-header-title font-size-lg text-capitalize font-weight-normal">{{ __('lang.lastorder') }}</div>
            </div>
            
            <div class="table-responsive">
                <table class="table" style="width: 100%">
                  <thead>
                    <tr>
                      <th scope="col">No</th>
                      <th scope="col">Order ID</th>
                      <th scope="col">Order Via</th>
                      <th scope="col"><div class="ml-4">Status</div></th>
                    </tr>
                  </thead>
                  <tbody>
                      @foreach ($latestactivity as $latestactivity)                          
                      <tr class="accordion-toggle collapsed" id="accordion{{ $latestactivity['id'] }}" data-toggle="collapse" data-parent="#accordion{{ $latestactivity['id'] }}" href="#coll{{ $latestactivity['id'] }}">
                          <td class="expand-button"></td>
                          <td>{{ $latestactivity['invoice_number'] }}<br>{{ Date::fullDate($latestactivity['created_at']) }}</td>
                          <td>{{ $latestactivity['via'] }}</td>
                          <td><div class="badge badge-{{ myFunction::colorStatus($latestactivity['status']) }}" style="width: 100px;">{{ __('lang.'.$latestactivity['status']) }}</div></td>              
                      </tr>
  
                      <tr class="hide-table-padding">
                          <td></td>
                          <td colspan="3">
                          <div id="coll{{ $latestactivity['id'] }}" class="collapse in p-3">
                              <div class="row">
                                <div class="col-5" nowrap>Order ID</div>
                                <div class="col-6">{{ $latestactivity['invoice_number'] }}</div>
                              </div>
                              <div class="row">
                                <div class="col-5" nowrap>Time</div>
                                <div class="col-6">{{ Date::fullDate($latestactivity['created_at']) }}</div>
                              </div>
                              <div class="row">
                                <div class="col-5" nowrap>Catalog</div>
                                <div class="col-6">
                                    <div class="widget-heading">{{ $latestactivity['catalog_title'] }}</div>
                                    <div class="widget-subheading opacity-8">
                                        <a href="https://{{ $latestactivity['catalog_username'].'.'.$latestactivity['domain'] }}" target="_blank" style="color: #888; text-decoration: none;">
                                            https://{{ $latestactivity['catalog_username'].'.'.$latestactivity['domain'] }}
                                        </a>
                                    </div>
                                    <div class="widget-content-left mr-3">
                                        <div class="widget-content-left">
                                            <img width="40" class="rounded" src="{{ myFunction::getProtocol().$latestactivity['catalog_logo'].'?'.time() }}" alt="" />
                                        </div>
                                    </div>
                                </div>
                              </div>
                              <div class="row">
                                <div class="col-5" nowrap>Order Via</div>
                                <div class="col-6">{{ $latestactivity['via'] }}</div>
                              </div>
                              <div class="row">
                                <div class="col-5" nowrap>Total</div>
                                <div class="col-6">Rp {{ number_format(getData::getTotalInvoice($latestactivity['id'])) }}</div>
                              </div>
                              <div class="row">
                                <div class="col-5" nowrap>Status</div>
                                <div class="col-6" nowrap>{{ __('lang.'.$latestactivity['status']) }}</div>
                              </div>
                              <div class="mt-3">
                                <a href="javascript:void(0)" class="btn btn-outline-primary btn-sm" onclick="loadDetail('{{ $latestactivity['invoice_number'] }}')">
                                    Detail
                                </a>
                              </div>
                          </div></td>
                      </tr>
                      @endforeach
                  </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@if(count($hotitems) > 0)
<div class="row d-none">
    <div class="col-md-12">
        <div class="main-card mb-3 card">
            <div class="card-header">
                <div class="card-header-title font-size-lg text-capitalize font-weight-normal">Hot Items</div>
            </div>
            <div class="card-body">
                <div class="row">
                    @foreach($hotitems as $hotitems)
                    <div class="col-sm-6 col-md-4 col-xl-2">
                        <div class="card-shadow-primary card-border text-white mb-3 card bg-focus">
                            <div class="dropdown-menu-header">
                                <div class="dropdown-menu-header-inner bg-focus">
                                    <div class="menu-header-content">
                                        <div class="avatar-icon-wrapper mb-3 avatar-icon-xl">
                                            <div class="avatar-icon">
                                                <a href="javascript:void(0)" data-featherlight="{{ myFunction::getProtocol().$hotitems['item_image_primary'].'?'.time() }}">
                                                    <img src="{{ myFunction::getProtocol().str_replace('/thumbs','',$hotitems['item_image_primary']) }}" alt="{{ $hotitems['items_name'] }}" />
                                                </a>
                                            </div>
                                        </div>
                                        <div>
                                            <h5 class="menu-header-title" style="font-size: .8rem">{{ $hotitems['items_name'] }}</h5>
                                            <h6 class="menu-header-subtitle">{{ $hotitems['category_name'] }}</h6>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
@endif

<script>
    var ctxLyear = document.getElementById("lineChartYears").getContext('2d');
    var ctxLmonth = document.getElementById("lineChartDays").getContext('2d');
    var thisyear = "{{ getData::getYear() }}";
    var thismont = "{{ getData::getMonth(0) }}";
    var prevmont = "{{ getData::getMonth(1) }}";
    // var arr1 = [65, 59, 80, 81, 56, 55, 40, 59, 80, 81, 56, 55, 40];
    // var arr2 = [81,56,55,40,59, 80, 81, 56, 55, 40];
    var thisyears = JSON.parse("{{json_encode(getData::getMonthSales(0))}}");
    var oneyearlater = JSON.parse("{{json_encode(getData::getMonthSales(1))}}");
    var thismonth = JSON.parse("{{json_encode(getData::getDaySales(0))}}");
    var onemonthlater = JSON.parse("{{json_encode(getData::getDaySales(1))}}");
    var myLineChartYears = new Chart(ctxLyear, {
        type: 'line',
        data: {
            labels: ["January", "February", "March", "April", "May", "June", "July", "Agustus", "September", "Oktober", "November", "Desember"],
            datasets: [
                {
                    label: "Sales " + (thisyear-1) + "      ",
                    data: oneyearlater,
                    backgroundColor: [
                        'rgba(105, 0, 132, .2)',
                    ],
                    borderColor: [
                        'rgba(200, 99, 132, .7)',
                    ],
                    borderWidth: 1
                },
                {
                    label: "Sales " + thisyear,
                    data: thisyears,
                    backgroundColor: [
                        'rgba(0, 137, 132, .2)',
                    ],
                    borderColor: [
                        'rgba(0, 10, 130, .7)',
                    ],
                    borderWidth: 2
                }
            ]
        },
        options: {
            responsive: true
        }
    });
    var myLineChartMonths = new Chart(ctxLmonth, {
        type: 'line',
        data: {
            labels: [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31],
            datasets: [
                {
                    label: "Sales " + prevmont + "      ",
                    data: onemonthlater,
                    backgroundColor: [
                        'rgba(105, 0, 132, .2)',
                    ],
                    borderColor: [
                        'rgba(200, 99, 132, .7)',
                    ],
                    borderWidth: 1
                },
                {
                    label: "Sales " + thismont,
                    data: thismonth,
                    backgroundColor: [
                        'rgba(0, 137, 132, .2)',
                    ],
                    borderColor: [
                        'rgba(0, 10, 130, .7)',
                    ],
                    borderWidth: 2
                }
            ]
        },
        options: {
            responsive: true
        }
    });


</script>

<style>
	.large-screen {}
		@media only screen and (max-width: 770px) {
		.large-screen {display: none;}
		}
		@media only screen and (min-width: 770px) {
		.large-screen {display: block;}
		}
	
	.small-screen {}
		@media only screen and (max-width: 770px) {
		.small-screen {display: block;}
		}
		@media only screen and (min-width: 770px) {
		.small-screen {display: none;}
		}

        tr.hide-table-padding td {
        padding: 0;
    }
    
    .expand-button {
        position: relative;
    }
    
    .accordion-toggle .expand-button:after {
        position: absolute;
        left:.75rem;
        top: 50%;
        transform: translate(0, -50%);
        content: '-';
    }
    .accordion-toggle.collapsed .expand-button:after {content: '+';}
</style>