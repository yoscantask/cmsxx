<div class="table-responsive">{{app()->setLocale(Session::get('locale'))}}
    <table class="align-middle mb-0 table table-borderless table-striped table-hover">
        <thead>
            <tr>
                <!--<th class="text-center" nowrap></th>-->
                <th nowrap>{{ __('lang.name') }} {{ __('lang.items') }}</th>
                <th class="text-right" nowrap>{{ __('lang.price') }} {{ __('lang.items') }}</th>
                <th class="text-center" nowrap>{{ __('lang.additem') }}</th>
            </tr>
        </thead>
        <tbody>
            @foreach($getData as $key=>$value)
            <tr>
                <!--<th class="text-center text-muted" nowrap>{{ $getData->firstItem() + $key }}</th>-->
                <td nowrap>
                    <div class="widget-content p-0">
                        <div class="widget-content-wrapper">{{ $getData->firstItem() + $key }}
                            <div class="widget-content-left mr-3">
                                <div class="widget-content-left">
                                    <a href="javascript:void(0)" onclick="loadGallery('{{ $value['id'] }}')">
                                        <img width="40" class="rounded" src="{{ myFunction::getProtocol().$value['item_image_primary'].'?'.time() }}" alt="" />
                                    </a>
                                </div>
                            </div>
                            <div class="widget-content-left flex2">
                                <div class="widget-heading">{{ $value['items_name'] }}</div>
                                <div class="widget-subheading opacity-8" style="font-size: 10px">
                                    {{ $value['category_name'] }}
                                </div>
                            </div>
                        </div>
                    </div>
                </td>
                <td class="text-right" nowrap>                    
                    @if($value['diskon'] > 0)
                        <span class="text-danger" style="text-decoration: line-through;">Rp. {{ number_format($value['price']) }}</span><br>
                        Rp. {{ number_format($value['price']-$value['diskon']) }}
                    @else
                    Rp. {{ number_format($value['price']) }}
                    @endif
                </td>
                <td class="text-center" nowrap>
                    @if((getData::getAddons($value['id'])->count() > 0))
                        <a href="javascript:void(0)" class="btn-icon btn-icon-only btn-hover-shine btn btn-primary btn-shadow" onclick="addItemCustom('{{ $value['item'] }}','{{ $value['category_name'] }}','{{ $value['price'] }}','{{ $value['diskon'] }}','{{ $value['items_name'] }}','{{ asset('images/'.$value['item_image_one']) }}')">
                            <i class="lnr-plus-circle btn-icon-wrapper"></i>
                        </a>
                    @else
                        <a href="javascript:void(0)" class="btn-icon btn-icon-only btn-hover-shine btn btn-primary btn-shadow" onclick="addItem('{{ $value['item'] }}','{{ $value['category_name'] }}','{{ $value['price'] }}','{{ $value['diskon'] }}','{{ $value['items_name'] }}','{{ asset('images/'.$value['item_image_one']) }}')">
                            <i class="lnr-plus-circle btn-icon-wrapper"></i>
                        </a>
                    @endif
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
<nav class="mt-3">
    {!! $pagination !!}
</nav>