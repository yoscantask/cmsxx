<div class="table-responsive">
	<table class="table">
		<tbody>
			<tr>
				<td style="border-top:none" colspan="2"><b>Customer Data</b></td>
			</tr>
			<tr>
				<td style="border-top:none">Name</td><td style="border-top:none">: {{ $myData['name'] }}</td>
			</tr>
			<tr>
				<td style="border-top:none">Email</td><td style="border-top:none">: {{ $myData['email'] }}</td>
			</tr>
			<tr>
				<td style="border-top:none">Phone</td><td style="border-top:none">: {{ $myData['phone'] }}</td>
			</tr>
			<tr>
				<td style="border-top:none" colspan="2"><b>Transaction Data</b></td>
			</tr>
			<tr>
				<td style="border-top:none">Status</td><td style="border-top:none">: {{ $myData['status'] }}</td>
			</tr>
			<tr>
				<td style="border-top:none">Invoice Number</td><td style="border-top:none">: {{ $myData['invoice'] }}</td>
			</tr>
			<tr>
				<td style="border-top:none">Register Date</td><td style="border-top:none">: {{ Date::fullDate($myData['created_at']) }}</td>
			</tr>
			<tr>
				<td style="border-top:none">Package Name</td><td style="border-top:none">: {{ $myData['package_name'] }}</td>
			</tr>
			<tr>
				<td style="border-top:none">Duration</td><td style="border-top:none">: {{ $myData['duration'] }}</td>
			</tr>
			@if(!empty($myData['expired']))
			<tr>
				<td style="border-top:none">Expired</td><td style="border-top:none">: {{ Date::myDate($myData['expired']) }}</td>
			</tr>
			@endif
			@if(!empty($myData['discount']))
			<tr>
				<td style="border-top:none">Voucher Code</td><td style="border-top:none">: {{ $myData['voucher_code'] }}</td>
			</tr>
			<tr>
				<td style="border-top:none">Discount</td><td style="border-top:none">: <sup>Rp.</sup> {{ number_format($myData['discount']) }}</td>
			</tr>
			@endif
			<tr>
				<td style="border-top:none">Package Price</td><td style="border-top:none">: <sup>Rp.</sup> {{ number_format($myData['price']-$myData['discount']) }}</td>
			</tr>
			@if($myData['status']=='Rejected')
			<tr>
				<td style="border-top:none">Notes</td><td style="border-top:none">: {{ $myData['notes'] }}</td>
			</tr>
			@endif
			@if($myData['confirmation']=='Y')
			<tr>
				<td style="border-top:none" colspan="2"><b>Payment Data</b></td>
			</tr>
			<tr>
				<td style="border-top:none" class="align-top">Payment To</td><td style="border-top:none;" class="text-info align-top">{!! $myData['account_to'] !!}</td>
			</tr>
			<tr>
				<td style="border-top:none" colspan="2">
					<a href="javascript:void(0)" data-featherlight="https://yoscan.id{{ $myData['confirmation_slip'] }}">
						<img src="https://yoscan.id{{ $myData['confirmation_slip'] }}" class="img-fluid">
					</a>
				</td>
			</tr>
			@endif
		</tbody>
	</table>
</div>