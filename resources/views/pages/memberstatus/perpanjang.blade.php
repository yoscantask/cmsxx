@extends('layouts.main')
@section('content')
<div class="app-page-title">
    <div class="page-title-wrapper">
        <div class="page-title-heading">
            <div class="page-title-icon">
                <i class="pe-7s-menu icon-gradient bg-ripe-malin"> </i>
            </div>
            <div>
                {{ $maintitle }}
                <div class="page-title-subheading">This dashboard was created as an example of the flexibility that Architect offers.</div>
            </div>
        </div>
    </div>
</div>

<div id="profileVue">
    <div class="tabs-animation">
        <div class="row">
            <div class="col-md-12"> 
                <div class="main-card mb-3 card" style="min-height: 280px;">
                  @include('blocks.skeleton') 
                  <form action="{{ url('/memberstatus/perpanjang').'/'.$id}}" method="post">
                    @csrf
                    <div class="g-3 col mt-3" style="max-width: 600px">
                      @if ($check != null)
                        <p class="text-center" style="color: red">{{$check}}</p>
                      @endif
                      <div class="mt-3">
                        <label for="username" class="form-label">Username</label>
                        <input value="{{$username}}" disabled type="username" class="form-control" name="username">
                        <input value="pending" hidden class="form-control" id="status">
                      </div>
                      <div class="mt-3">
                        <label for="paket" class="form-label">Paket Member</label>
                        <select id="paket" name="paket" class="custom-select" aria-label="Default select example">
                          <option selected>{{ __('lang.select')}} {{ __('lang.package')}}</option>
                          @foreach ($paket as $item)
                          <option 
                          value={{$item->id}}>
                          {{$item->package_name}} = Rp {{number_format($item->price)}} / {{$item->period}} {{ __('lang.'.$item->unit)}}</option>
                          @endforeach
                        </select>
                      </div>
                    </div>
                    <div class="col mt-2">
                      <button style="width: 100px" type="submit" id="btnSave" class="mt-2 btn btn-primary">Submit</button>
                      <a href="{{ route('memberstatus.index') }}" style="width: 100px" type="button" id="btnCancel" class="mt-2 btn btn-primary">Cancel</a>
                    </div>
                  </form>

                </div>
            </div>
        </div>
    </div>
</div>

@endsection
