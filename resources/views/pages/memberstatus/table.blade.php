<!--<div class="table-responsive">
    <table class="align-middle mb-0 table table-borderless table-striped table-hover">
        <thead>
            <tr>
                <th class="pt-4 pb-4 text-center" nowrap>No</th>
                <th class="pt-4 pb-4" nowrap>Register Date</th>
                <th class="pt-4 pb-4" nowrap>Username</th>
                <th class="pt-4 pb-4" nowrap>{{ __('lang.package')}}</th>
                <th class="pt-4 pb-4" nowrap>{{ __('lang.price')}}</th>
                <th class="pt-4 pb-4" nowrap>Exp Date</th>
                <th class="pt-4 pb-4" nowrap>Status</th>
                <th class="pt-4 pb-4 text-center" nowrap>Actions</th>
            </tr>
        </thead>
        <tbody>
            @foreach($getData as $key=>$value)
            <tr>
                <th class="text-center text-muted" nowrap>{{ $getData->firstItem() + $key }}</th>
                <td nowrap>{{ Date::fullDate($value['created_at']) }}</td>
                <td nowrap>{{ $value['username'] }}</td>
                <td nowrap>{{ $value['package_name'] }} / {{ $value['period'] }} {{ $value['unit'] }}</td>
                <td nowrap>Rp {{ number_format($value['price']) }}</td>
                <td nowrap>{{ $value['expdate'] }}</td>
                <td nowrap>{{ $value['status'] }}</td>
                <td class="text-center" nowrap>
                    <div role="group" class="btn-group-sm btn-group btn-group-toggle">
                        <a href="javascript:void(0)" data-id="{{ $value['id'] }}" class="editlink btn btn-shadow btn-info">Details</a>                        
                    </div>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
-->
<div class="table-responsive">
    <table class="table" style="width: 99%">
      <thead>
        <tr>
          <th scope="col">#</th>
          @if (Auth::user()->level == 'Super Admin')
          <th scope="col">Username</th>              
          @else
          <th scope="col">{{ __('lang.package')}}</th>              
          @endif
          <th scope="col">Status</th>
          <th scope="col" style="text-align: center">Action</th>
        </tr>
      </thead>
      <tbody id="accordion" class="accordion-wrapper mb-3">{{--  Edit accordion --}}
        @foreach ($getData as $key=>$value)  
        @if (($getData->firstItem() + $key) % 2 == 1)
        <tr data-toggle="collapse" data-target="#collapse{{ $value['id'] }}" aria-expanded="false" aria-controls="collapse{{ $value['id'] }}" class="btn-outline-secondary accordion-toggle collapsed"> {{--  Edit collapse --}}                       
        @else
        <tr data-toggle="collapse" data-target="#collapse{{ $value['id'] }}" aria-expanded="false" aria-controls="collapse{{ $value['id'] }}" class="btn-light accordion-toggle collapsed"> {{--  Edit collapse --}}                       
        @endif
              <td class="text-center" nowrap>{{ $getData->firstItem() + $key }}</td>
              @if (Auth::user()->level == 'Super Admin')
              <td>{{ $value['username'] }}</td>
              @else                  
              <td><div class="large-screen">{{ $value['package_name'] }} / {{ $value['period'] }} {{ $value['unit'] }}</div><div class="small-screen">{{ $value['package_name'] }} / <br> {{ $value['period'] }} {{ $value['unit'] }}</div></td>
              @endif
              <td>{{ $value['status'] }}</td>
              <td class="text-center" nowrap>
                <div role="group" class="btn-group-sm btn-group btn-group-toggle">
                    <a href="javascript:void(0)" data-id="{{ $value['id'] }}" class="editlink btn btn-info">Details</a>                        
                </div>
            </td>
		  </tr>

          <tr class="hide-table-padding">
              <td></td>
              <td colspan="3">
                <div data-parent="#accordion" id="collapse{{ $value['id'] }}" class="ml-2 collapse in p-3">{{--  Edit collapse --}}
                    <div class="row">
                      <div class="col-5">Register Date</div>
                      <div class="col-14">{{ Date::fullDate($value['created_at']) }}</div>
                  </div>
                  @if (Auth::user()->level == 'Super Admin')
                  <div class="row">
                      <div class="col-5">Status</div>
                      <div class="col-14">{{ $value['status'] }}</div>  
                  </div>                  
                  @else                      
                  <div class="row">
                      <div class="col-5">Username</div>
                      <div class="col-14">{{ $value['username'] }}</div>
                  </div>
                  @endif
                  <div class="row">
                    <div class="col-5">{{ __('lang.package')}}</div>
                    <div class="col-14">{{ $value['package_name'] }} / {{ $value['period'] }} {{ $value['unit'] }}</div>
				  </div>
                  <div class="row">
                    <div class="col-5">{{ __('lang.price')}}</div>
                    <div class="col-14">Rp {{ number_format($value['price']) }}</div>
                  </div>
                  <div class="row">
                    <div class="col-5">Expire Date</div>
                    <div class="col-14">{{ $value['expdate'] }}</div>
                  </div>		
              </div></td>
          </tr>
          @endforeach
      </tbody>
    </table>
</div>

<div class="d-block justify-content-center card-footer">
    <nav class="mt-3">
        {!! $pagination !!}
    </nav>
</div>

<style>
	.large-screen {}
		@media only screen and (max-width: 770px) {
		.large-screen {display: none;}
		}
		@media only screen and (min-width: 770px) {
		.large-screen {display: block;}
		}
	
	.small-screen {}
		@media only screen and (max-width: 770px) {
		.small-screen {display: block;}
		}
		@media only screen and (min-width: 770px) {
		.small-screen {display: none;}
		}

        tr.hide-table-padding td {
        padding: 0;
    }
</style>