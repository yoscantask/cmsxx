<div id="wrapVue">
    <form id="myForm" @submit.prevent="submitForm" method="post" onsubmit="return false;" enctype="multipart/form-data">
        <div class="modal-body">
            <div class="position-relative form-group">
                <div class="row">
                    <div class="col-md-3">
                        <label>Select Category</label>
                        <select id="category_id" name="category_id" class="custom-select" onchange="getPosition()">
                            <option value="">Select</option>
                            @foreach($category as $vcategory)
                                <option value="{{ $vcategory['id'] }}">{{ $vcategory['category_name'] }}</option>
                            @endforeach
                        </select>
                        <div v-if="formErrors['category_id']" class="errormsg alert alert-danger mt-1">
                          @{{ formErrors['category_id'][0] }}
                        </div>
                    </div>
                    <div class="col-md-3">
                        <label>Category Position</label>
                        <input type="number" id="category_position" name="category_position" class="form-control" min="1">
                        <div v-if="formErrors['category_position']" class="errormsg alert alert-danger mt-1">
                          @{{ formErrors['category_position'][0] }}
                        </div>
                    </div>
                    <div class="col-md-3">
                        <label>Select Sub Category</label>
                        <select id="subcategory_id" name="subcategory_id" class="custom-select" onchange="getPositionSub()">
                            <option value="0">Select</option>
                            @foreach($subcategory as $vsubcategory)
                                <option value="{{ $vsubcategory['id'] }}">{{ $vsubcategory['subcategory_name'] }}</option>
                            @endforeach
                        </select>
                        <div v-if="formErrors['subcategory_id']" class="errormsg alert alert-danger mt-1">
                          @{{ formErrors['subcategory_id'][0] }}
                        </div>
                    </div>
                    <div class="col-md-3">
                        <label>Sub Category Position</label>
                        <input type="number" id="subcategory_position" name="subcategory_position" class="form-control" min="1">
                        <div v-if="formErrors['subcategory_position']" class="errormsg alert alert-danger mt-1">
                          @{{ formErrors['subcategory_position'][0] }}
                        </div>
                    </div>
                </div>
            </div>
            <div class="position-relative form-group">
                <label>Select Items</label>
                <div class="row">
                    @foreach($items as $vitems)
                        @if (!getData::cekShowItem($catalogid, $vitems['id']))
                        <div class="col-md-2 mb-3">
                            <img src="{{ //myFunction::getProtocol().
                            asset('images/'. //tambahan
                            $vitems['item_image_primary'].'?'.time()) }}" class="img-fluid">
                            <div class="custom-checkbox custom-control mt-2">
                                <input type="checkbox" id="item{{ $vitems['id'] }}" name="items[]" class="custom-control-input" value="{{ $vitems['id'] }}" />
                                <label class="custom-control-label" for="item{{ $vitems['id'] }}" style="font-size: 12px;">{{ $vitems['items_name'] }}</label>
                            </div>
                        </div>     
                        @endif
                    @endforeach
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Save Data</button>
        </div>
    </form>
</div>

<script type="text/javascript">
    new Vue({
        el: "#wrapVue",
        data() {
            return {
                csrf: "",
                formErrors: {},
                notif: [],
            };
        },
        mounted: function () {
            this.csrf = "{{ csrf_token() }}";
            let self = this;
            $(document).ready(function() {
                $("#myForm")[0].reset();
            });
        },
        methods: {
            submitForm: function (e) {
                submitForm();
                $('.errormsg').hide();
                var form = e.target || e.srcElement;
                var action = "{{ url('/catalog/additems/'.$catalogid) }}";
                var csrfToken = "{{ csrf_token() }}";

                var arritems=[];
                $("input:checkbox[name*=items]:checked").each(function(){
                    arritems.push($(this).val());
                });
                if(arritems.length < 1){
                    Swal.fire("Ops!", "Please select item(s).", "error");
                    afterSubmitForm();
                    return false;
                }

                let datas = new FormData();
                datas.append("catalog_id", "{{ $catalogid }}");
                datas.append("category_id", $("#category_id").val());
                datas.append("category_position", $("#category_position").val());
                datas.append("subcategory_id", $("#subcategory_id").val());
                datas.append("subcategory_position", $("#subcategory_position").val());
                datas.append("items", arritems);

                axios.post(action, datas, {
                        headers: {
                            "X-CSRF-TOKEN": csrfToken,
                            Accept: "application/json",
                        },
                    })
                    .then((response) => {
                        let self = this;
                        var notif = response.data;
                        var getstatus = notif.status;
                        if (getstatus == "success") {
                            $("#modalForm").modal("hide");
                            afterSubmitForm();
                            loadView();
                        }else{
                            afterSubmitForm();
                            toastr.error(notif.message);
                        }
                    })
                    .catch((error) => {
                        afterSubmitForm();
                        $('.errormsg').show();
                        this.formErrors = error.response.data.errors;
                    });
                
            },
        },
    });
</script>

<style>
	.large-screen {}
		@media only screen and (max-width: 770px) {
		.large-screen {display: none;}
		}
		@media only screen and (min-width: 770px) {
		.large-screen {display: block;}
		}
	
	.small-screen {}
		@media only screen and (max-width: 770px) {
		.small-screen {display: block;}
		}
		@media only screen and (min-width: 770px) {
		.small-screen {display: none;}
		}

        tr.hide-table-padding td {
        padding: 0;
    }
    
    .expand-button {
        position: relative;
    }
    
    .accordion-toggle .expand-button:after {
        position: absolute;
        left:.75rem;
        top: 50%;
        transform: translate(0, -50%);
        content: '-';
    }
    .accordion-toggle.collapsed .expand-button:after {content: '+';}
</style>