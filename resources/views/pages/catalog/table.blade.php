<style>
.dropdown-menu{
    position: absolute !important;
}
</style>

<!--
<div class="table-responsive">
    <table class="align-middle mb-0 table table-borderless table-striped table-hover">
        <thead>
            <tr>
                <th class="pt-4 pb-4 text-center" nowrap>#</th>
                <th class="pt-4 pb-4" nowrap>Catalog Title</th>
                <th class="pt-4 pb-4" nowrap>URL</th>
                <th class="pt-4 pb-4 text-center" nowrap>Feature</th>
                <th class="pt-4 pb-4" nowrap>Layout Items</th>
                <th class="pt-4 pb-4 text-center d-none" nowrap>QRCode</th>
                <th class="pt-4 pb-4 text-center" nowrap>Status</th>
                <th class="pt-4 pb-4 text-center" nowrap>Actions</th>
            </tr>
        </thead>
        <tbody>
            @foreach($getData as $key=>$value)
            <tr>
                <form class="mb-0" id="delete_form_{{ $value['id'] }}" action="{{ route('catalog.destroy', $value['id']) }}" method="post">
                    @csrf
                    @method('delete')
                    <th class="text-center text-muted" nowrap>{{ $getData->firstItem() + $key }}</th>
                    <td nowrap>
                        <div class="widget-content p-0">
                            <div class="widget-content-wrapper">
                                <div class="widget-content-left mr-3">
                                    <div class="widget-content-left">
                                        <a href="javascript:void(0)" data-featherlight="{{ myFunction::getProtocol().$value['catalog_logo'].'?'.time() }}">
                                            <img width="40" class="rounded" src="{{ myFunction::getProtocol().$value['catalog_logo'].'?'.time() }}" alt="" />
                                        </a>
                                    </div>
                                </div>
                                <div class="widget-content-left flex2">
                                    <div class="widget-heading">{{ $value['catalog_title'] }}</div>
                                    <div class="widget-subheading opacity-7">Created : {{ Date::fullDate($value['created_at']) }}</div>
                                </div>
                            </div>
                        </div>
                    </td>
                    <td nowrap>
                        <a href="https://{{ $value['catalog_username'].'.'.$value['domain'] }}" target="_blank">{{ $value['catalog_username'].'.'.$value['domain'] }}</a>
                    </td>
                    <td class="text-center" nowrap>
                        {{ $value['feature'] }}
                    </td>
                    <td nowrap>
                        {{ $value['layout'] }}
                    </td>
                    <td class="text-center d-none" nowrap>
                        {!! \QrCode::format('svg')->size(50)->color(40,40,40)->generate($value['catalog_username'].'.'.$value['domain']) !!}
                    </td>
                    <td class="text-center" nowrap>
                        {{ $value['show_catalog'] }}
                    </td>
                    <td class="text-center" nowrap>
                        <button type="button" aria-haspopup="true" aria-expanded="false" data-toggle="dropdown" class="mr-2 dropdown-toggle btn btn-outline-link"><i class="fa fa-fw fa-wrench"></i></button>

                        <div tabindex="-1" role="menu" aria-hidden="true" class="dropdown-menu">
                            <ul class="nav flex-column">
                                <li class="nav-item"><a href="{{ url('/catalog/items/'.$value['id']) }}" class="nav-link">Manage Items</a></li>
                                <li class="nav-item"><a href="{{ url('/viewmenus/'.$value['id']) }}" class="nav-link">Items Layout</a></li>
                                <li class="nav-item"><a href="{{ url('/catalog/qrcode/'.$value['id']) }}" class="nav-link" target="_blank">Print QRCode</a></li>
                                <li class="nav-item"><a href="javascript:void(0);" data-id="{{ $value['id'] }}" class="editlink nav-link">Edit</a></li>
                                <!-- <li class="nav-item"><a href="javascript:void(0);" data-id="{{ $value['id'] }}" class="edittype nav-link">Change Type</a></li> --><!--
                                <li class="nav-item"><a href="javascript:void(0);" data-id="{{ $value['id'] }}" class="deletelink nav-link">Delete</a></li>
                            </ul>
                        </div>

                        <div role="group" class="btn-group-sm btn-group btn-group-toggle d-none">
                            <a href="{{ url('/catalog/items/'.$value['id']) }}" class="btn btn-shadow btn-primary">Manage Items</a>
                            <a href="{{ url('/viewmenus/'.$value['id']) }}" class="btn btn-shadow btn-primary">View Items</a>
                            <a href="{{ url('/catalog/qrcode/'.$value['id']) }}" class="btn btn-shadow btn-primary" target="_blank">Print QRCode</a>
                            <a href="javascript:void(0)" data-id="{{ $value['id'] }}" class="editlink btn btn-shadow btn-info">Edit</a>
                            <a href="javascript:void(0)" data-id="{{ $value['id'] }}" class="deletelink btn btn-shadow btn-danger">Delete</a>
                        </div>

                    </td>
                </form>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
-->

<div class="table-responsive">
    <table class="table" style="width: 100%">
      <thead>
        <tr>
          <th scope="col-1" style="text-align: center">#</th>
          <th scope="col-5">Catalog Title</th>
          <th scope="col-2" style="text-align: center">Actions</th>
        </tr>
      </thead>
      <tbody>
          @foreach ($getData as $value)                          
          <tr class="accordion-toggle collapsed" id="accordion{{ $value['id'] }}" data-toggle="collapse" data-parent="#accordion{{ $value['id'] }}" href="#coll{{ $value['id'] }}">
            <form class="mb-0" id="delete_form_{{ $value['id'] }}" action="{{ route('catalog.destroy', $value['id']) }}" method="post">
                @csrf
                @method('delete')
                <th class="text-center text-muted" nowrap>{{ $getData->firstItem() + $key }}</th>
                <td nowrap>
                    <div class="widget-content p-0">
                        <div class="widget-content-wrapper">
                            <div class="widget-content-left mr-3">
                                <div class="widget-content-left">
                                    <a href="javascript:void(0)" data-featherlight="{{ myFunction::getProtocol().$value['catalog_logo'].'?'.time() }}">
                                        <img width="40" class="rounded" src="{{ myFunction::getProtocol().$value['catalog_logo'].'?'.time() }}" alt="" />
                                    </a>
                                </div>
                            </div>
                            <div class="widget-content-left flex2">
                                <div class="widget-heading">{{ $value['catalog_title'] }}</div>
                                <div class="widget-subheading opacity-8">Created : <br> {{ Date::fullDate($value['created_at']) }}</div>
                            </div>
                        </div>
                    </div>
                </td>
                
                <td class="text-center" nowrap>
                    <button type="button" aria-haspopup="true" aria-expanded="false" data-toggle="dropdown" class="mr-2 dropdown-toggle btn btn-outline-link"><i class="fa fa-fw fa-wrench"></i></button>

                    <div tabindex="-1" role="menu" aria-hidden="true" class="dropdown-menu">
                        <ul class="nav flex-column">
                            <li class="nav-item"><a href="{{ url('/catalog/items/'.$value['id']) }}" class="nav-link">Manage Items</a></li>
                            <li class="nav-item"><a href="{{ url('/viewmenus/'.$value['id']) }}" class="nav-link">Items Layout</a></li>
                            <li class="nav-item"><a href="{{ url('/catalog/qrcode/'.$value['id']) }}" class="nav-link" target="_blank">Print QRCode</a></li>
                            <li class="nav-item"><a href="javascript:void(0);" data-id="{{ $value['id'] }}" class="editlink nav-link">Edit</a></li>
                            <!-- <li class="nav-item"><a href="javascript:void(0);" data-id="{{ $value['id'] }}" class="edittype nav-link">Change Type</a></li> -->
                            <li class="nav-item"><a href="javascript:void(0);" data-id="{{ $value['id'] }}" class="deletelink nav-link">Delete</a></li>
                        </ul>
                    </div>

                    <div role="group" class="btn-group-sm btn-group btn-group-toggle d-none">
                        <a href="{{ url('/catalog/items/'.$value['id']) }}" class="btn btn-shadow btn-primary">Manage Items</a>
                        <a href="{{ url('/viewmenus/'.$value['id']) }}" class="btn btn-shadow btn-primary">View Items</a>
                        <a href="{{ url('/catalog/qrcode/'.$value['id']) }}" class="btn btn-shadow btn-primary" target="_blank">Print QRCode</a>
                        <a href="javascript:void(0)" data-id="{{ $value['id'] }}" class="editlink btn btn-shadow btn-info">Edit</a>
                        <a href="javascript:void(0)" data-id="{{ $value['id'] }}" class="deletelink btn btn-shadow btn-danger">Delete</a>
                    </div>

                </td>
            </form>           
          </tr>
<!-- 

<td nowrap>
                    <a href="https://{{ $value['catalog_username'].'.'.$value['domain'] }}" target="_blank">{{ $value['catalog_username'].'.'.$value['domain'] }}</a>
                </td>
                <td class="text-center" nowrap>
                    {{ $value['feature'] }}
                </td>
                <td nowrap>
                    {{ $value['layout'] }}
                </td>
                <td class="text-center d-none" nowrap>
                    {!! \QrCode::format('svg')->size(50)->color(40,40,40)->generate($value['catalog_username'].'.'.$value['domain']) !!}
                </td>
                <td class="text-center" nowrap>
                    {{ $value['show_catalog'] }}
                </td>

-->
          <tr class="hide-table-padding">
              <td></td>
              <td colspan="3">
              <div id="coll{{ $value['id'] }}" class="collapse in p-3">
                  <div class="row">
                    <div class="col-4" nowrap>URL</div>
                    <div class="col-6"><a href="https://{{ $value['catalog_username'].'.'.$value['domain'] }}" target="_blank">{{ $value['catalog_username'].'.'.$value['domain'] }}</a></div>
                  </div>
                  <div class="row">
                    <div class="col-4" nowrap>Feature</div>
                    <div class="col-6">{{ $value['feature'] }}</div>
                  </div>
                  <div class="row">
                    <div class="col-4" nowrap>Items</div>
                    <div class="col-6">{{ $value['layout'] }}</div>
                  </div>
                  <div class="row">
                    <div class="col-4" nowrap>Status</div>
                    <div class="col-6" nowrap>{{ $value['show_catalog'] }}</div>
                  </div>
                  <div class="row mt-2">
                    <div class="col-4" nowrap>QR Code</div>
                    <div class="col-6"> {!! \QrCode::format('svg')->size(50)->color(40,40,40)->generate($value['catalog_username'].'.'.$value['domain']) !!}</div>
                  </div>
              </div></td>
          </tr>
          @endforeach
      </tbody>
    </table>
</div>

<div class="d-block justify-content-center card-footer">
    <nav class="mt-3">
        {!! $pagination !!}
    </nav>
</div>

<style>
    tr.hide-table-padding td {
        padding: 0;
    }
    
    .expand-button {
        position: relative;
    }
    
    .accordion-toggle .expand-button:after {
        position: absolute;
        left:.75rem;
        top: 50%;
        transform: translate(0, -50%);
        content: '-';
    }
    .accordion-toggle.collapsed .expand-button:after {content: '+';}	        
</style>