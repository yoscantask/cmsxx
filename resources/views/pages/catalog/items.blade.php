@extends('layouts.main') 
@section('content')
<div class="app-page-title">
    <div class="page-title-wrapper">
        <div class="page-title-heading">
            <div class="page-title-icon">
                <i class="pe-7s-display2 icon-gradient bg-ripe-malin"> </i>
            </div>
            <div>
                {{ $maintitle }} ( {{ $catalog['catalog_title'] }} )
                <div class="page-title-subheading">This dashboard was created as an example of the flexibility that Architect offers.</div>
            </div>
        </div>
        <div class="page-title-actions">
            <a href="{{ route('catalog.index') }}" class="btn-shadow btn btn-success btn-sm"><i class="icon lnr-arrow-left"></i> Back</a>
        </div>
    </div>
</div>

<div id="indexVue">
    <div class="tabs-animation">
        <div class="row">
            <div class="col-md-12">
                <div class="d-flex flex-wrap justify-content-between mb-3">
                    <div>
                        <a href="javascript:void(0)" class="btn-shadow btn btn-dark btn-sm" v-on:click="showForm('create')"><i class="fa fa-plus"></i> Add Items</a>
                    </div>
                    <div class="col-12 col-md-3 p-0 mb-3"></div>
                </div>
                <div class="main-card mb-3 card" style="min-height: 250px;">
                    @include('blocks.skeleton') 
                    <div id="loadpage"></div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection 

@section('modal')
<div class="modal fade" id="modalForm" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div id="modalSize" class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <!-- Form Loader -->
            <div class="formLoader">
                <div class="jumper">
                    <div></div>
                    <div></div>
                    <div></div>
                </div>
            </div>
            <!-- End -->
            <div class="modal-header">
                <h5 class="modal-title" id="titleModal"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div id="contentForm"></div>
        </div>
    </div>
</div>
@endsection 

@section('customjs')
<script type="text/javascript" src="https://www.jqueryscript.net/demo/color-picker-predefined-palette/jquery.simple-color.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
<script type="text/javascript">
    $(document).ready(function() {
        loadView();
    });
    new Vue({
        el: "#indexVue",
        data() {
            return {
                csrf: "",
                deletedata: "",
                formErrors: {},
                notif: [],
            };
        },
        mounted: function () {
            this.csrf = "{{ csrf_token() }}";
            this.deletedata = "DELETE";
            let self = this;
            $(document).on("click", ".editlink", function (evetn, id) {
                var id = $(this).attr("data-id");
                self.showForm("edit", id);
            });
            $(document).on("click", ".deletelink", function (evetn, id) {
                var catalog = $(this).attr("data-catalog");
                var me = $(this).attr("data-me");
                var type = $(this).attr("data-type");
                self.confirmDialog(catalog,me,type);
            });
            $(document).on("click", ".availablelink", function (evetn, id) {
                var catalog = $(this).attr("data-catalog");
                var me = $(this).attr("data-me");
                var available = $(this).attr("data-available");
                self.confirmDialogAvailable(catalog,me,available);
            });
            $(document).on("click", ".changeposition", function (evetn, id) {
                var catalog = $(this).attr("data-catalog");
                var me = $(this).attr("data-me");
                var current = $(this).attr("data-current");
                var status = $(this).attr("data-status");
                self.changePosition(catalog,me,current,status);
            });
        },
        methods: {
            showForm: function (action, id = null) {
                preloader();
                $("#modalSize").removeClass("modal-sm");
                $("#modalSize").addClass("modal-xl");
                if (action == "create") {
                    $("#titleModal").html("Create New");
                    $.ajax({
                        url: "{{ url('/catalog/additems/'.$catalog['id']) }}", 
                        type: "GET", 
                    })
                    .done(function(data) {
                        $("#modalForm").modal("show");
                        $("#contentForm").html(data);
                        afterpreloader();
                    })
                    .fail(function() {
                        Swal.fire("Ops!", "Load data failed.", "error");
                    });
                }
            },
            confirmDialog: function (catalog, me, type) {
                let self = this;
                Swal.fire({
                    title: "Are you sure ?",
                    text: "Data will be permanently deleted",
                    icon: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#3085d6",
                    cancelButtonColor: "#d33",
                    confirmButtonText: "Yes",
                    cancelButtonText: "Cancel",
                }).then((result) => {
                    if (result.value) {
                        preloadContent();
                        axios.get("{{ url('/catalog/delete/element') }}" + "/" + catalog + "/" + me + "/" + type)
                            .then((response) => {
                                let self = this;
                                var notif = response.data;
                                var getstatus = notif.status;
                                if (getstatus == "success") {
                                    loadView();
                                }else{
                                    afterPreloadContent();
                                    toastr.error(notif.message);
                                }
                            })
                            .catch(function (error) {
                                Swal.fire("Ops!", "Load data failed.", "error");
                                afterPreloadContent();
                            });
                    }
                });
            },
            confirmDialogAvailable: function (catalog, me, available) {
                let self = this;
                if(available == 'Y'){
                    mystatus = 'Show';
                }else{
                    mystatus = 'Hide';
                }
                Swal.fire({
                    title: "Are you sure ?",
                    text: "Item "+mystatus+" in the catalog",
                    icon: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#3085d6",
                    cancelButtonColor: "#d33",
                    confirmButtonText: "Yes",
                    cancelButtonText: "Cancel",
                }).then((result) => {
                    if (result.value) {
                        preloadContent();
                        axios.get("{{ url('/catalog/available/element') }}" + "/" + catalog + "/" + me + "/" + available)
                            .then((response) => {
                                let self = this;
                                var notif = response.data;
                                var getstatus = notif.status;
                                if (getstatus == "success") {
                                    loadView();
                                }else{
                                    afterPreloadContent();
                                    toastr.error(notif.message);
                                }
                            })
                            .catch(function (error) {
                                Swal.fire("Ops!", "Load data failed.", "error");
                                afterPreloadContent();
                            });
                    }
                });
            },
            changePosition: function (catalog, me, current, status) {
                preloader();
                $("#modalSize").removeClass("modal-xl");
                $("#modalSize").addClass("modal-sm");
                $("#titleModal").html("Change " + status + " Position");
                $.ajax({
                        url: "{{ url('/catalog/change/position') }}" + "/" + catalog + "/" + me + "/" + current + "/" + status
                        , type: "GET"
                        , dataType: "html"
                    , })
                    .done(function(data) {
                        $("#modalForm").modal("show");
                        $("#contentForm").html(data);
                        $("#modalForm").modal("show");
                        afterpreloader();
                    })
                    .fail(function() {
                        Swal.fire("Ops!", "Load data failed.", "error");
                    });
            }
        },
    });
</script>
<script type="text/javascript">
    function loadView() {
        preloadContent();
        var url = "{{ url('/catalog/items/') }}" + "/{{ $catalog['id'] }}";
        var obj = new Object();
        obj.searchfield = $("#searchfield").val();
        axios.post(url, obj, {
                headers: {
                    "X-CSRF-TOKEN": "{{ csrf_token() }}",
                    Accept: "application/json",
                },
            })
            .then((response) => {
                $("#loadpage").html(response.data);
                afterPreloadContent();
            })
            .catch((error) => {
                afterpreloader();
                $(".errormsg").css("visibility", "visible");
                this.formErrors = error.response.data.errors;
            });
    }
    function getPosition(){
        catalog = "{{ $catalog['id'] }}";
        category = $("#category_id").val();
        $.ajax({
            url: "{{ url('/catalog/position/category') }}"+'/'+catalog+'/'+category,
            type: 'GET',
        })
        .done(function(data) {
            $("#category_position").attr({
               "max" : data,
               "value" : data
            });
            getPositionSub();
        })
        .fail(function() {
            console.log("error");
        });
    }
    function getPositionSub(){
        catalog = "{{ $catalog['id'] }}";
        category = $("#category_id").val();
        subcategory = $("#subcategory_id").val();
        $.ajax({
            url: "{{ url('/catalog/position/subcategory') }}"+'/'+catalog+'/'+category+'/'+subcategory,
            type: 'GET',
        })
        .done(function(data) {
            $("#subcategory_position").attr({
               "max" : data,
               "value" : data
            });
        })
        .fail(function() {
            console.log("error");
        });
    }
</script>
@endsection
