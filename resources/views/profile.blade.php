@extends('layouts.main')
@section('content')
<div class="app-page-title">{{app()->setLocale(Session::get('locale'))}}
    <div class="page-title-wrapper">
        <div class="page-title-heading">
            <div class="page-title-icon">
                <i class="pe-7s-menu icon-gradient bg-ripe-malin"> </i>
            </div>
            <div>
                {{ $maintitle }}
                <div class="page-title-subheading">This dashboard was created as an example of the flexibility that Architect offers.</div>
            </div>
        </div>
    </div>
</div>

<div id="profileVue">
    <div class="tabs-animation">
        <div class="row">
            <div class="col-md-12">
                <div class="main-card mb-3 card" style="min-height: 250px;">
                  @include('blocks.skeleton') 
                  <form action="{{ url('/profile/'.$id) }}" method="post">
                    @csrf
                    <div class="g-3 col" style="max-width: 600px" id="idbio">
                      <div class="mt-3">
                        <label for="email" class="form-label">Email</label>
                        <input value="{{$biodata->email}}" disabled type="email" class="form-control" id="email">
                      </div>
                      <div class="mt-3">
                        <label for="username" class="form-label">Username</label>
                        <input value="{{$biodata->username}}" disabled type="username" class="form-control" id="username">
                      </div>
                      <div class="mt-3">
                        <label for="nama" class="form-label">{{ __('lang.name') }}</label>
                        <input value="{{$biodata->name}}" required type="name" class="form-control" name="name" id="name">
                      </div>
                      <div class="mt-3">
                        <label for="phone" class="form-label">{{ __('lang.phone') }}</label>
                        <input value="{{$biodata->phone}}" required type="phone" class="form-control" name="phone" id="phone">
                      </div>
                      <div class="mt-3">
                        <label for="alamat" class="form-label">{{ __('lang.address') }}</label>
                        <input value="{{$biodata->address}}" type="text" class="form-control" name="address" id="address">
                      </div>
                    </div>
                    @if ($check != "index")
                        <div id="indexx" class="m-3" style="color: blue">{{ $check }}</div>
                    @endif
                    <div class="g-3 col" style="max-width: 600px" id="idpass">
                      <h3 class="mt-3">Insert Old Password</h3>
                      <div class="mt-3">
                        <input type="password" class="form-control" name="oldpassword" id="oldpassword"/>
                      </div>
                    </div>

                    <div class="g-3 col" style="max-width: 600px" id="idpasss">
                        <h3 class="mt-3">{{ __('lang.change')}} Password</h3>
                      <div class="mt-3">
                        <label for="password" class="form-label">New Password</label>
                        <input type="password" class="form-control" name="password" id="password" pattern="^\S{6,}$"
                        onchange="this.setCustomValidity(this.validity.patternMismatch ?
                        'Must have at least 6 characters' : ''); 
                        if(this.checkValidity()) form.repassword.pattern = this.value;
                        ">
                      </div>
                      <div class="mt-3">
                        <label for="repassword" class="form-label">{{ __('lang.retype')}}</label>
                        <input type="password" class="form-control" name="repassword" id="repassword" pattern="^\S{6,}$"
                        onchange="this.setCustomValidity(this.validity.patternMismatch ?
                        'Please enter the same Password as above' : '');
                        ">                        
                        </div>                      
                    </div>
                    <div class="col mt-3 mb-3 g-3">
                      <button style="width: 150px" type="button" id="btnEdit" onclick="editBio()" class="btn btn-primary">Edit Profile</button>
                      <button style="width: 150px" type="button" id="btnChangePass" onclick="changePass()" class="mt-2 btn btn-primary">{{ __('lang.change') }} Password</button>
                      <button style="width: 100px" type="submit" id="btnSave" class="btn btn-primary">Save</button>
                      <button style="width: 100px" type="submit" id="btnConfirm" class="btn btn-primary">Confirm</button>
                      <a style="width: 100px" type="button" id="btnCancel" href="{{ url('/profile') }}" class="mt-2 btn btn-primary">Cancel</a>
                    </div>

                  </form>

                </div>
            </div>
        </div>
    </div>
</div>

<script>
  x = document.getElementById("idbio");
  y = document.getElementById("idpass");
  z = document.getElementById("idpasss");
  xx = document.getElementById("indexx");

  btnEdit = document.getElementById("btnEdit");
  btnPass = document.getElementById("btnChangePass");
  btnSave = document.getElementById("btnSave");
  btnConfirm = document.getElementById("btnConfirm");
  btnCancel = document.getElementById("btnCancel");

  inputNama = document.getElementById("name");
  inputPhone = document.getElementById("phone");
  inputAddress = document.getElementById("address");

  function normalView() {
    x.style.display = "block"
    btnEdit.style.display = "block"
    btnPass.style.display = "block"
    y.style.display = "none"
    z.style.display = "none"
    btnSave.style.display = "none"
    btnCancel.style.display = "none"
    btnConfirm.style.display = "none"

    inputNama.disabled = true
    inputPhone.disabled = true
    inputAddress.disabled = true

    resetBio()
  }

  function normalViewCancel() {
    if ("{{$check}}" != "index") {xx.style.display = "none"}
    x.style.display = "block"
    btnEdit.style.display = "block"
    btnPass.style.display = "block"
    y.style.display = "none"
    z.style.display = "none"
    btnSave.style.display = "none"
    btnCancel.style.display = "none"
    btnConfirm.style.display = "none"

    inputNama.disabled = true
    inputPhone.disabled = true
    inputAddress.disabled = true

    resetBio()
  }

  function changePass() {
    //if ("{{$check}}" != "index") {xx.style.display = "none"}
    x.style.display = "none"
    btnEdit.style.display = "none"
    btnPass.style.display = "none"
    z.style.display = "none"
    y.style.display = "block"
    btnSave.style.display = "none"
    btnConfirm.style.display = "block"
    btnCancel.style.display = "block"   
    resetPass()
  }

  function changePasss() {
    x.style.display = "none"
    btnEdit.style.display = "none"
    btnPass.style.display = "none"
    z.style.display = "block"
    y.style.display = "none"
    btnSave.style.display = "block"
    btnCancel.style.display = "block"   
    btnConfirm.style.display = "none"
    resetPass()
  }

  function resetPass() {
    document.getElementById("oldpassword").value = ''
    document.getElementById("password").value = ''
    document.getElementById("repassword").value = ''
  }

  function resetBio() {
    document.getElementById("name").value = '{!! $biodata->name !!}'
    document.getElementById("phone").value = '{!! $biodata->phone !!}'
    document.getElementById("address").value = '{!! $biodata->address !!}'
  }

  function editBio() {
    if ("{{$check}}" != "index") {xx.style.display = "none"}
    x.style.display = "block"
    btnEdit.style.display = "none"
    btnPass.style.display = "none"
    y.style.display = "none"
    btnSave.style.display = "block"
    btnCancel.style.display = "block"   
    btnConfirm.style.display = "none"
    
    inputNama.disabled = false
    inputPhone.disabled = false
    inputAddress.disabled = false
  }
  
  normalView()
  if (window.location.search == '?cp=changepass') {changePass()}
  if ("{{$check}}" == "Password Salah") {changePass()}
  if ("{{$check}}" == "Password Lama Sesuai, Silahkan Input Password Baru Anda") {changePasss()}

</script>

@endsection
