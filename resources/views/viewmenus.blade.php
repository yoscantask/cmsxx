@extends('layouts.main')
@section('content')
<div class="app-page-title">
    <div class="page-title-wrapper">
        <div class="page-title-heading">
            <div class="page-title-icon">
                <i class="pe-7s-menu icon-gradient bg-ripe-malin"> </i>
            </div>
            <div>
                {{ $maintitle }}
                <div class="page-title-subheading">This dashboard was created as an example of the flexibility that Architect offers.</div>
            </div>
        </div>
    </div>
</div>

<div id="indexVue">
    <div class="tabs-animation">
        <div class="row">
            <div class="col-md-12">
                <div class="main-card mb-3 card" style="min-height: 250px;">
                    @include('blocks.skeleton') 
                <form action="{{ url('/viewmenus').'/'.$id }}" method="post">
                    @csrf
                      <div class="m-2" id="loadpage">
                          <div id="SetingSubCat" class="col">
                              <h5 class="large-screen"><strong>{{ __('lang.setting')}} View Sub Category</strong></h5>   
                              <div class="small-screen"><strong>{{ __('lang.setting')}} View Sub Category</strong></div>   
  
                              <div class="row m-2">

                                  @if ($view_subcat == 'grid')
                                    <div class="custom-control custom-radio custom-control-inline mr-3">
                                      <input onclick="subcatgridsample()" value="grid" class="custom-control-input" type="radio" name="view_subcat" id="RadioSubCat1" checked>
                                      <label class="custom-control-label" for="RadioSubCat1">
                                        Grid View
                                      </label>
                                    </div>
                                    <div class="custom-control custom-radio custom-control-inline mr-3">
                                      <input onclick="subcatlistsample()" value="list" class="custom-control-input" type="radio" name="view_subcat" id="RadioSubCat2">
                                      <label class="custom-control-label" for="RadioSubCat2">
                                        List View
                                      </label>
                                    </div>
                                  @endif
                                  @if ($view_subcat == 'list')
                                      <div class="custom-control custom-radio custom-control-inline mr-3">
                                        <input onclick="subcatgridsample()" value="grid" class="custom-control-input" type="radio" name="view_subcat" id="RadioSubCat1">
                                        <label class="custom-control-label" for="RadioSubCat1">
                                          Grid View
                                        </label>
                                      </div>
                                      <div class="custom-control custom-radio custom-control-inline mr-3">
                                        <input onclick="subcatlistsample()" value="list" class="custom-control-input" type="radio" name="view_subcat" id="RadioSubCat2" checked>
                                        <label class="custom-control-label" for="RadioSubCat2">
                                          List View
                                        </label>
                                      </div>                                    
                                  @endif
                              </div>  
                              <img id="subgridview" style="maxwidth: 380px" src="{{asset('/images/viewmenusample/subgridview.png')}}" class="img-fluid mt-2">
                              <img id="sublistview" style="maxwidth: 380px" src="{{asset('/images/viewmenusample/sublistview.png')}}" class="img-fluid mt-2">
                            </div>

                          <div id="SetingMenus" class="mt-4 col">
                            <h5 class="large-screen"><strong>{{ __('lang.setting')}} View Menu</strong></h5>   
                            <div class="small-screen"><strong>{{ __('lang.setting')}} View Menu</strong></div>
                              
                              <div class="row m-2">
                                  @if ($view_item == 'grid')
                                    <div class="custom-control custom-radio custom-control-inline mr-3">
                                      <input onclick="menugridsample()" value="grid" class="custom-control-input" type="radio" name="view_item" id="RadioItem1" checked>
                                      <label class="custom-control-label" for="RadioItem1">
                                        Grid View
                                      </label>
                                    </div>
                                    <div class="custom-control custom-radio custom-control-inline mr-3">
                                      <input onclick="menulistsample()" value="list" class="custom-control-input" type="radio" name="view_item" id="RadioItem2">
                                      <label class="custom-control-label" for="RadioItem2">
                                        List View
                                      </label>
                                    </div>
                                  @endif
                                  @if ($view_item == 'list')
                                      <div class="custom-control custom-radio custom-control-inline mr-3">
                                        <input onclick="menugridsample()" value="grid" class="custom-control-input" type="radio" name="view_item" id="RadioItem1">
                                        <label class="custom-control-label" for="RadioItem1">
                                          Grid View
                                        </label>
                                      </div>
                                      <div class="custom-control custom-radio custom-control-inline mr-3">
                                        <input onclick="menulistsample()" value="list" class="custom-control-input" type="radio" name="view_item" id="RadioItem2" checked>
                                        <label class="custom-control-label" for="RadioItem2">
                                          List View
                                        </label>
                                      </div>                                    
                                  @endif
                            </div>
                            <img id="menugridview" style="maxwidth: 380px" src="{{asset('/images/viewmenusample/itemgridview.png')}}" class="img-fluid mt-2">
                            <img id="menulistview" style="maxwidth: 380px" src="{{asset('/images/viewmenusample/itemlistview.png')}}" class="img-fluid mt-2">
  
                          </div>
                          <button class="mt-2 ml-4 btn btn-primary btn-lg" type="submit">Save</button>
                        </div>
                      </div>
                    </form>
            </div>
        </div>
    </div>
</div>

<script>
  var x = document.getElementById("subgridview")
  var y = document.getElementById("sublistview")
  var z = document.getElementById("menugridview")
  var a = document.getElementById("menulistview")

  if({!! json_encode($view_subcat) !!} == 'grid') {
    subcatgridsample()
  }

  if({!! json_encode($view_subcat) !!} == 'list') {
    subcatlistsample()
  }

  if({!! json_encode($view_item) !!} == 'grid') {
    menugridsample()
  }

  if({!! json_encode($view_item) !!} == 'list') {
    menulistsample()
  }
  
  function subcatgridsample() {
    x.style.display = "block"
    y.style.display = "none"
  }

  function subcatlistsample() {
    y.style.display = "block"
    x.style.display = "none"
  }  

  function menugridsample() {
    z.style.display = "block"
    a.style.display = "none"
  }  

  function menulistsample() {
    z.style.display = "none"
    a.style.display = "block"
  }
</script>

<style>
	.large-screen {}
		@media only screen and (max-width: 770px) {
		.large-screen {display: none;}
		}
		@media only screen and (min-width: 770px) {
		.large-screen {display: block;}
		}
	
	.small-screen {}
		@media only screen and (max-width: 770px) {
		.small-screen {display: block;}
		}
		@media only screen and (min-width: 770px) {
		.small-screen {display: none;}
		}
</style>

@endsection