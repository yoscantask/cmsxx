<div class="app-sidebar sidebar-shadow">
    <div class="app-header__logo">
        <div class="logo-src"></div>
        <div class="header__pane ml-auto">
            <div>
                <button type="button" class="hamburger close-sidebar-btn hamburger--elastic" data-class="closed-sidebar">
                    <span class="hamburger-box">
                        <span class="hamburger-inner"></span>
                    </span>
                </button>
            </div>
        </div>
    </div>
    <div class="app-header__mobile-menu">
        <div>
            <button type="button" class="hamburger hamburger--elastic mobile-toggle-nav">
                <span class="hamburger-box">
                    <span class="hamburger-inner"></span>
                </span>
            </button>
        </div>
    </div>
    <div class="app-header__menu">
        <span>
            <button type="button" class="btn-icon btn-icon-only btn btn-primary btn-sm mobile-toggle-header-nav">
                <span class="btn-icon-wrapper">
                    <i class="fa fa-ellipsis-v fa-w-6"></i>
                </span>
            </button>
        </span>
    </div>
    <div class="scrollbar-sidebar">
        <div class="app-sidebar__inner">
            <ul class="vertical-nav-menu">
                @if(Auth::user()->level != 'Super Admin' || Auth::user()->level != 'User')
                <li class="d-lg-none d-xl-none">
                    <a href="#" aria-expanded="false">
                        <i class="metismenu-icon pe-7s-keypad"></i>{{__('lang.select')}} Catalog
                        <i class="metismenu-state-icon pe-7s-angle-down caret-left"></i>
                    </a>
                    <ul class="mm-collapse" style="height: 7.04px;">
                        @if(getData::haveCatalog() == 'True')
                            @if(getData::getCatalog()->count() > 1)
                                <li>
                                    <a href="javascript:void(0)" onclick="catalogSessionMenu('All')">
                                        {{__('lang.all')}} Catalog
                                    </a>
                                </li>
                            @endif
                            @foreach(getData::getCatalog() as $keycat => $catalogsession)
                                <li>
                                    <a href="javascript:void(0)" onclick="catalogSessionMenu('{{ $catalogsession['id'] }}')">
                                        {{ $catalogsession['catalog_title'] }}
                                    </a>
                                </li>
                            @endforeach
                        @endif
                    </ul>
                </li>
                @endif
                <li class="mm-active mt-3">
                    <a href="{{ url('/') }}">
                        <i class="metismenu-icon lnr-screen"></i>
                        Dashboards
                    </a>
                </li>
                
                <li class="">
                    <a href="https://wa.me/6285157130040?text=Hallo,%20Saya%20ingin%20menanyakan%20perihal%20" target="_blank">
                        <i class="metismenu-icon lnr-smile"></i>
                        Butuh Bantuan
                    </a>
                </li>

                @if(getData::haveCatalog() == 'True')
                    @if(Session::get('catalogsession') != 'null')
                        <li>
                            <a href="{{ url('/pos') }}">
                                <i class="metismenu-icon lnr-laptop-phone"></i>
                                Point of Sales
                            </a>
                        </li>
                        @endif
                        @endif
                        
                        @if(Auth::user()->owner == 1)
                            <li>
                                <a href="{{ route('bell.index') }}">
                                    <i class="metismenu-icon pe-7s-bell"></i>
                                    {{ __('lang.bellnotif')}}
                                </a>
                            </li>
                <li class="app-sidebar__heading">{{ __('lang.masterdata')}}</li>
                <li>
                    <a href="{{ route('catalog.index') }}">
                        <i class="metismenu-icon pe-7s-display2"></i>
                        {{ __('lang.manage')}} Catalog
                    </a>
                </li>
                <li class="">
                    <a href="#" aria-expanded="false">
                        <i class="metismenu-icon lnr-layers"></i>{{ __('lang.manage')}} Items
                        <i class="metismenu-state-icon pe-7s-angle-down caret-left"></i>
                    </a>
                    <ul class="mm-collapse" style="height: 7.04px;">
                        <li>
                            <a href="{{ route('category.index') }}">
                                {{ __('lang.category')}}
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('subcategory.index') }}">
                            {{ __('lang.subcategory')}}
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('items.index') }}">
                            {{ __('lang.items')}}
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('sliders.index') }}">
                                Sliders
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="">
                    <a href="#" aria-expanded="false">
                        <i class="metismenu-icon lnr-plus-circle"></i>{{ __('lang.manage')}} Add Ons
                        <i class="metismenu-state-icon pe-7s-angle-down caret-left"></i>
                    </a>
                    <ul class="mm-collapse" style="height: 7.04px;">
                        <li>
                            <a href="{{ route('categoryadd.index') }}">
                                {{ __('lang.category')}}
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('addons.index') }}">
                                {{ __('lang.addonlist')}}
                            </a>
                        </li>
                    </ul>
                </li>

                <li>
                    <a href="{{ route('material.index') }}">
                        <i class="metismenu-icon lnr-database"></i>
                        {{ (Auth::user()->owner == 1)?'Warehouse':'Material' }} Stock
                    </a>
                </li>
                
                <li>
                    <a href="{{ route('user.index') }}">
                        <i class="metismenu-icon lnr-users"></i>
                        {{ __('lang.users')}}
                    </a>
                </li>
                <li>
                    <a href="{{ route('memberstatus.index') }}">
                        <i class="metismenu-icon lnr-store"></i>
                        Status Member
                    </a>
                </li>
                @endif

                @if(Auth::user()->id == 1)
                    <li class="app-sidebar__heading">Yo Hotel</li>
                    <li>
                        <a href="{{ route('tutorial.index') }}">
                            <i class="metismenu-icon lnr-book"></i>
                            Tutorial
                        </a>
                    </li>

                    <li class="">
                        <a href="#" aria-expanded="false">
                            <i class="metismenu-icon lnr-hand"></i>Service
                            <i class="metismenu-state-icon pe-7s-angle-down caret-left"></i>
                        </a>
                        <ul class="mm-collapse" style="height: 7.04px;">
                            <li>
                                <a href="{{ route('service.index') }}">
                                    List Data
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('user_service.index') }}">
                                    User Service
                                </a>
                            </li>
                        </ul>
                    </li>

                    <li class="">
                        <a href="#" aria-expanded="false">
                            <i class="metismenu-icon lnr-sad"></i>Complaint
                            <i class="metismenu-state-icon pe-7s-angle-down caret-left"></i>
                        </a>
                        <ul class="mm-collapse" style="height: 7.04px;">
                            <li>
                                <a href="{{ route('complaint.index') }}">
                                    List Data
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('user_complaint.index') }}">
                                    User Complaint
                                </a>
                            </li>
                        </ul>
                    </li>
                @endif


                @if(Session::get('catalogsession') != 'null' && getData::getCatalogSession('feature') == 'Full')
                <li class="app-sidebar__heading">Resto Order Tracking</li>
                <li>
                    <a href="{{ url('/transaction/index/checkout') }}">
                        <i class="metismenu-icon pe-7s-next-2"></i>
                        {{ __('lang.checkout')}}
                    </a>
                </li>
                @if(getData::checkStepTransaction('Approve',Session::get('catalogsession')))
                <li>
                    <a href="{{ url('/transaction/index/approve') }}">
                        <i class="metismenu-icon lnr-select"></i>
                        {{ __('lang.approve')}}
                    </a>
                </li>
                @endif
                @if(getData::checkStepTransaction('Process',Session::get('catalogsession')))
                <li>
                    <a href="{{ url('/transaction/index/process') }}">
                        <i class="metismenu-icon lnr-hourglass"></i>
                        {{ __('lang.process')}}
                    </a>
                </li>
                @endif
                @if(getData::checkStepTransaction('Delivered',Session::get('catalogsession')))
                <li>
                    <a href="{{ url('/transaction/index/delivered') }}">
                        <i class="metismenu-icon lnr-location"></i>
                        {{ __('lang.devlivered')}}
                    </a>
                </li>
                @endif
                <li>
                    <a href="{{ url('/transaction/index/completed') }}">
                        <i class="metismenu-icon lnr-checkmark-circle"></i>
                        {{ __('lang.completed')}}
                    </a>
                </li>
                <li>
                    <a href="{{ url('/transaction/index/cancel') }}">
                        <i class="metismenu-icon lnr-cross-circle"></i>
                        {{ __('lang.cancel')}}
                    </a>
                </li>
                @endif

                @if(Auth::user()->owner == 1)
                    @if(Session::get('catalogsession') != 'null' || Session::get('catalogsession') == 'All')
                    <li class="app-sidebar__heading">Report</li>
                    <li>
                        <a href="{{ url('/transaction/index/all') }}">
                            <i class="metismenu-icon lnr-printer"></i>
                            {{ __('lang.transaction')}}
                        </a>
                    </li>
                    @endif
                    @if(Auth::user()->owner == 1)
                    <li>
                        <a href="{{ route('customer.index') }}">
                            <i class="metismenu-icon lnr-heart"></i>
                            Customer
                        </a>
                    </li>
                    @endif
                @endif
            </ul>
        </div>
    </div>
</div>
