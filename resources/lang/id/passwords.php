<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'reset' => 'Password anda sudah di reset!',
    'sent' => 'Link reset password sudah kami kirim ke email anda',
    'throttled' => 'Tunggu sebentar 5 menit untuk mencoba lagi.',
    'token' => 'Token reset password tidak sah.',
    'user' => "Kami tidak menemukan email tersebut.",
    'emailaddress' => "Alamat E-Mail",
    'sendpassresetlink' => "Kirim Reset",

];
